/**
 * UserOrgHierarchyServiceInterfaceV1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public interface UserOrgHierarchyServiceInterfaceV1 extends java.rmi.Remote {
    public com.flexnet.operations.webservices.v1.CreateOrgResponseType createOrganization(com.flexnet.operations.webservices.v1.CreateOrgRequestType createOrgRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType linkOrganizations(com.flexnet.operations.webservices.v1.LinkOrganizationsDataType[] linkOrganizationsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType updateOrganization(com.flexnet.operations.webservices.v1.UpdateOrgDataType[] updateOrganizationRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType deleteOrganization(com.flexnet.operations.webservices.v1.DeleteOrgDataType[] deleteOrganizationRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType getOrganizationsQuery(com.flexnet.operations.webservices.v1.GetOrganizationsQueryRequestType getOrganizationsQueryRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType getOrganizationCount(com.flexnet.operations.webservices.v1.GetOrganizationCountRequestType getOrganizationCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType getParentOrganizations(com.flexnet.operations.webservices.v1.GetParentOrganizationsRequestType getParentOrganizationsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType getSubOrganizations(com.flexnet.operations.webservices.v1.GetSubOrganizationsRequestType getSubOrganizationsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetUsersQueryResponseType getUsersQuery(com.flexnet.operations.webservices.v1.GetUsersQueryRequestType getUsersQueryRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetUserCountResponseType getUserCount(com.flexnet.operations.webservices.v1.GetUserCountRequestType getUserCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.CreateUserResponseType createUser(com.flexnet.operations.webservices.v1.CreateUserRequestType createUserRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UpdateUserResponseType updateUser(com.flexnet.operations.webservices.v1.UpdateUserDataType[] updateUserRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType updateUserRoles(com.flexnet.operations.webservices.v1.UpdateUserRolesDataType[] updateUserRolesRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.DeleteUserResponseType deleteUser(com.flexnet.operations.webservices.v1.UserIdentifierType[] deleteUserRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType relateOrganizations(com.flexnet.operations.webservices.v1.RelateOrganizationsDataType[] relateOrganizationsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType getRelatedOrganizations(com.flexnet.operations.webservices.v1.GetRelatedOrganizationsRequestType getRelatedOrganizationsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType getUserPermissions(com.flexnet.operations.webservices.v1.GetUserPermissionsRequestType getUserPermissionsRequest) throws java.rmi.RemoteException;
}
