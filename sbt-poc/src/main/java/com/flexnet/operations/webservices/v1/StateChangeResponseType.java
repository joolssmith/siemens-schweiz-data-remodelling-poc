/**
 * StateChangeResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class StateChangeResponseType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.FeatureStateChangeDataType[] featureList;

    private com.flexnet.operations.webservices.v1.FeatureBundleStateChangeDataType[] featureBundleList;

    private com.flexnet.operations.webservices.v1.ProductStateChangeDataType[] productList;

    private com.flexnet.operations.webservices.v1.LicenseModelStateChangeDataType[] licenseModelList;

    private com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] simpleEntitlementList;

    private com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] bulkEntitlementList;

    public StateChangeResponseType() {
    }

    public StateChangeResponseType(
           com.flexnet.operations.webservices.v1.FeatureStateChangeDataType[] featureList,
           com.flexnet.operations.webservices.v1.FeatureBundleStateChangeDataType[] featureBundleList,
           com.flexnet.operations.webservices.v1.ProductStateChangeDataType[] productList,
           com.flexnet.operations.webservices.v1.LicenseModelStateChangeDataType[] licenseModelList,
           com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] simpleEntitlementList,
           com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] bulkEntitlementList) {
           this.featureList = featureList;
           this.featureBundleList = featureBundleList;
           this.productList = productList;
           this.licenseModelList = licenseModelList;
           this.simpleEntitlementList = simpleEntitlementList;
           this.bulkEntitlementList = bulkEntitlementList;
    }


    /**
     * Gets the featureList value for this StateChangeResponseType.
     * 
     * @return featureList
     */
    public com.flexnet.operations.webservices.v1.FeatureStateChangeDataType[] getFeatureList() {
        return featureList;
    }


    /**
     * Sets the featureList value for this StateChangeResponseType.
     * 
     * @param featureList
     */
    public void setFeatureList(com.flexnet.operations.webservices.v1.FeatureStateChangeDataType[] featureList) {
        this.featureList = featureList;
    }


    /**
     * Gets the featureBundleList value for this StateChangeResponseType.
     * 
     * @return featureBundleList
     */
    public com.flexnet.operations.webservices.v1.FeatureBundleStateChangeDataType[] getFeatureBundleList() {
        return featureBundleList;
    }


    /**
     * Sets the featureBundleList value for this StateChangeResponseType.
     * 
     * @param featureBundleList
     */
    public void setFeatureBundleList(com.flexnet.operations.webservices.v1.FeatureBundleStateChangeDataType[] featureBundleList) {
        this.featureBundleList = featureBundleList;
    }


    /**
     * Gets the productList value for this StateChangeResponseType.
     * 
     * @return productList
     */
    public com.flexnet.operations.webservices.v1.ProductStateChangeDataType[] getProductList() {
        return productList;
    }


    /**
     * Sets the productList value for this StateChangeResponseType.
     * 
     * @param productList
     */
    public void setProductList(com.flexnet.operations.webservices.v1.ProductStateChangeDataType[] productList) {
        this.productList = productList;
    }


    /**
     * Gets the licenseModelList value for this StateChangeResponseType.
     * 
     * @return licenseModelList
     */
    public com.flexnet.operations.webservices.v1.LicenseModelStateChangeDataType[] getLicenseModelList() {
        return licenseModelList;
    }


    /**
     * Sets the licenseModelList value for this StateChangeResponseType.
     * 
     * @param licenseModelList
     */
    public void setLicenseModelList(com.flexnet.operations.webservices.v1.LicenseModelStateChangeDataType[] licenseModelList) {
        this.licenseModelList = licenseModelList;
    }


    /**
     * Gets the simpleEntitlementList value for this StateChangeResponseType.
     * 
     * @return simpleEntitlementList
     */
    public com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] getSimpleEntitlementList() {
        return simpleEntitlementList;
    }


    /**
     * Sets the simpleEntitlementList value for this StateChangeResponseType.
     * 
     * @param simpleEntitlementList
     */
    public void setSimpleEntitlementList(com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] simpleEntitlementList) {
        this.simpleEntitlementList = simpleEntitlementList;
    }


    /**
     * Gets the bulkEntitlementList value for this StateChangeResponseType.
     * 
     * @return bulkEntitlementList
     */
    public com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] getBulkEntitlementList() {
        return bulkEntitlementList;
    }


    /**
     * Sets the bulkEntitlementList value for this StateChangeResponseType.
     * 
     * @param bulkEntitlementList
     */
    public void setBulkEntitlementList(com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[] bulkEntitlementList) {
        this.bulkEntitlementList = bulkEntitlementList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StateChangeResponseType)) return false;
        StateChangeResponseType other = (StateChangeResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.featureList==null && other.getFeatureList()==null) || 
             (this.featureList!=null &&
              java.util.Arrays.equals(this.featureList, other.getFeatureList()))) &&
            ((this.featureBundleList==null && other.getFeatureBundleList()==null) || 
             (this.featureBundleList!=null &&
              java.util.Arrays.equals(this.featureBundleList, other.getFeatureBundleList()))) &&
            ((this.productList==null && other.getProductList()==null) || 
             (this.productList!=null &&
              java.util.Arrays.equals(this.productList, other.getProductList()))) &&
            ((this.licenseModelList==null && other.getLicenseModelList()==null) || 
             (this.licenseModelList!=null &&
              java.util.Arrays.equals(this.licenseModelList, other.getLicenseModelList()))) &&
            ((this.simpleEntitlementList==null && other.getSimpleEntitlementList()==null) || 
             (this.simpleEntitlementList!=null &&
              java.util.Arrays.equals(this.simpleEntitlementList, other.getSimpleEntitlementList()))) &&
            ((this.bulkEntitlementList==null && other.getBulkEntitlementList()==null) || 
             (this.bulkEntitlementList!=null &&
              java.util.Arrays.equals(this.bulkEntitlementList, other.getBulkEntitlementList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFeatureList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFeatureList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFeatureList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFeatureBundleList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFeatureBundleList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFeatureBundleList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getProductList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProductList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProductList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLicenseModelList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseModelList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseModelList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSimpleEntitlementList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSimpleEntitlementList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSimpleEntitlementList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getBulkEntitlementList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBulkEntitlementList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBulkEntitlementList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StateChangeResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "stateChangeResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("featureList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureStateChangeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "feature"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("featureBundleList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleStateChangeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundle"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productStateChangeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "product"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseModelList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelStateChangeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModel"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("simpleEntitlementList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "simpleEntitlementList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateChangeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bulkEntitlementList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlementList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateChangeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
