/**
 * V1UserOrgHierarchyServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class V1UserOrgHierarchyServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[17];
        _initOperationDesc1();
        _initOperationDesc2();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createOrganization");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createOrgRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createOrgRequestType"), com.flexnet.operations.webservices.v1.CreateOrgRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createOrgResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.CreateOrgResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createOrgResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("linkOrganizations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsRequestType"), com.flexnet.operations.webservices.v1.LinkOrganizationsDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateOrganization");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrganizationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrganizationRequestType"), com.flexnet.operations.webservices.v1.UpdateOrgDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrganizationResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrganizationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteOrganization");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrganizationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrganizationRequestType"), com.flexnet.operations.webservices.v1.DeleteOrgDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrganizationResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrganizationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOrganizationsQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryRequestType"), com.flexnet.operations.webservices.v1.GetOrganizationsQueryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOrganizationCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountRequestType"), com.flexnet.operations.webservices.v1.GetOrganizationCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getParentOrganizations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getParentOrganizationsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getParentOrganizationsRequestType"), com.flexnet.operations.webservices.v1.GetParentOrganizationsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getParentOrganizationsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getParentOrganizationsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSubOrganizations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getSubOrganizationsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getSubOrganizationsRequestType"), com.flexnet.operations.webservices.v1.GetSubOrganizationsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getSubOrganizationsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getSubOrganizationsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUsersQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryRequestType"), com.flexnet.operations.webservices.v1.GetUsersQueryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetUsersQueryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUserCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountRequestType"), com.flexnet.operations.webservices.v1.GetUserCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetUserCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserRequestType"), com.flexnet.operations.webservices.v1.CreateUserRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.CreateUserResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRequestType"), com.flexnet.operations.webservices.v1.UpdateUserDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UpdateUserResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateUserRoles");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesRequestType"), com.flexnet.operations.webservices.v1.UpdateUserRolesDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteUserRequestType"), com.flexnet.operations.webservices.v1.UserIdentifierType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "user"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteUserResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.DeleteUserResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("relateOrganizations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsRequestType"), com.flexnet.operations.webservices.v1.RelateOrganizationsDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getRelatedOrganizations");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getRelatedOrganizationsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getRelatedOrganizationsRequestType"), com.flexnet.operations.webservices.v1.GetRelatedOrganizationsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getRelatedOrganizationsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getRelatedOrganizationsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getUserPermissions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsRequestType"), com.flexnet.operations.webservices.v1.GetUserPermissionsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

    }

    public V1UserOrgHierarchyServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public V1UserOrgHierarchyServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public V1UserOrgHierarchyServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addressDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddressDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CollectionOperationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CollectionOperationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdOrganizationDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organization");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdUserDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "user");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createOrgRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateOrgRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createOrgResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateOrgResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CreateOrUpdateOperationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateUserDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserOrganizationRolesListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RoleIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "roleIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "role");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserOrganizationsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateUserOrganizationType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserOrganizationType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgRoles");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserOrganizationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateUserOrganizationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateUserRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateUserResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "datedSearchType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DatedSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "DateTimeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DateTimeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrganizationRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteOrgDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrgDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrganizationResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteOrgDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteOrgDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteUserRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "user");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteUserResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteUserResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedCreateOrgDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedCreateOrgDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedCreateOrgDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedOrg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedCreateOrgDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedCreateOrgDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedCreateUserDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedCreateUserDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedCreateUserDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUser");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedCreateUserDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedCreateUserDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteOrgDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteOrgDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteOrgDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedOrg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteOrgDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteOrgDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteUserDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteUserDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteUserDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUser");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteUserDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteUserDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkOrgDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedLinkOrgDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkOrgDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedOrgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkOrgDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedLinkOrgDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRelateOrganizationsDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedRelateOrganizationsDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRelateOrganizationsDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedOrgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRelateOrganizationsDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedRelateOrganizationsDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateOrgDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateOrgDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateOrgDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedOrg");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateOrgDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateOrgDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateUserDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateUserDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateUserDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUser");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateUserDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateUserDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateUserRolesDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateUserRolesDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateUserRolesDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUser");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateUserRolesDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateUserRolesDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetOrganizationCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountResponseDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetOrganizationCountResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetOrganizationsQueryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryResponseDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationDetailDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationDetailDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getOrganizationsQueryResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getParentOrganizationsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetParentOrganizationsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getParentOrganizationsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getRelatedOrganizationsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetRelatedOrganizationsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getRelatedOrganizationsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getSubOrganizationsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetSubOrganizationsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getSubOrganizationsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUserCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountResponseDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUserCountResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUserCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUserPermissionsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsResponseDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUserPermissionsResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUserPermissionsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUsersQueryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryResponseDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserDetailDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userDetailDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "user");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getUsersQueryResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetUsersQueryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkOrganizationsDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkOrganizationsDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkOrganizationsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationDetailDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationDetailDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationQueryParametersType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationQueryParametersType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgCustomAttributeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrgCustomAttributeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgCustomAttributesQueryListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrgCustomAttributeQueryType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgCustomAttributeQueryType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "OrgType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrgType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "OrgTypeList");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrgType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "OrgType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgType");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "permissionListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "permission");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RelateOrganizationsDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RelateOrganizationsDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relateOrganizationsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "roleIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RoleIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "rolePKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RolePKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "SimpleQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SimpleQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "simpleSearchType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SimpleSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StatusInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StatusInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StatusType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StatusType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrganizationRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateOrgDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrgDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrganizationResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrgDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateOrgDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateRelatedOrganizationsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateRelatedOrganizationsListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSubOrganizationsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateSubOrganizationsListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserOrganizationRolesListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RoleIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "roleIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "role");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserOrganizationsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserOrganizationsListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserOrganizationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserOrganizationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserRolesDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserRolesListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesOrganizationDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserRolesOrganizationDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserRolesDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userCustomAttributeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserCustomAttributeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userCustomAttributesQueryListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserCustomAttributeQueryType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userCustomAttributeQueryType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userDetailDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserDetailDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userOrganizationRolesListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "roleName");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userOrganizationsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserOrganizationType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userOrganizationType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "orgRoles");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userOrganizationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserOrganizationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userQueryParametersType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserQueryParametersType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "UserStatusType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserStatusType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.flexnet.operations.webservices.v1.CreateOrgResponseType createOrganization(com.flexnet.operations.webservices.v1.CreateOrgRequestType createOrgRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createOrganization"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createOrgRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.CreateOrgResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.CreateOrgResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.CreateOrgResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType linkOrganizations(com.flexnet.operations.webservices.v1.LinkOrganizationsDataType[] linkOrganizationsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "linkOrganizations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {linkOrganizationsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType updateOrganization(com.flexnet.operations.webservices.v1.UpdateOrgDataType[] updateOrganizationRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateOrganization"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateOrganizationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType deleteOrganization(com.flexnet.operations.webservices.v1.DeleteOrgDataType[] deleteOrganizationRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteOrganization"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteOrganizationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType getOrganizationsQuery(com.flexnet.operations.webservices.v1.GetOrganizationsQueryRequestType getOrganizationsQueryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getOrganizationsQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getOrganizationsQueryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType getOrganizationCount(com.flexnet.operations.webservices.v1.GetOrganizationCountRequestType getOrganizationCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getOrganizationCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getOrganizationCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType getParentOrganizations(com.flexnet.operations.webservices.v1.GetParentOrganizationsRequestType getParentOrganizationsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getParentOrganizations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getParentOrganizationsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType getSubOrganizations(com.flexnet.operations.webservices.v1.GetSubOrganizationsRequestType getSubOrganizationsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getSubOrganizations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getSubOrganizationsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetUsersQueryResponseType getUsersQuery(com.flexnet.operations.webservices.v1.GetUsersQueryRequestType getUsersQueryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getUsersQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getUsersQueryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetUsersQueryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetUsersQueryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetUsersQueryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetUserCountResponseType getUserCount(com.flexnet.operations.webservices.v1.GetUserCountRequestType getUserCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getUserCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getUserCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetUserCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetUserCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetUserCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.CreateUserResponseType createUser(com.flexnet.operations.webservices.v1.CreateUserRequestType createUserRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createUserRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.CreateUserResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.CreateUserResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.CreateUserResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UpdateUserResponseType updateUser(com.flexnet.operations.webservices.v1.UpdateUserDataType[] updateUserRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateUserRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UpdateUserResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UpdateUserResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UpdateUserResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType updateUserRoles(com.flexnet.operations.webservices.v1.UpdateUserRolesDataType[] updateUserRolesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateUserRoles"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateUserRolesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.DeleteUserResponseType deleteUser(com.flexnet.operations.webservices.v1.UserIdentifierType[] deleteUserRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteUserRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.DeleteUserResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.DeleteUserResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.DeleteUserResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType relateOrganizations(com.flexnet.operations.webservices.v1.RelateOrganizationsDataType[] relateOrganizationsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "relateOrganizations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {relateOrganizationsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType getRelatedOrganizations(com.flexnet.operations.webservices.v1.GetRelatedOrganizationsRequestType getRelatedOrganizationsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getRelatedOrganizations"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getRelatedOrganizationsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType getUserPermissions(com.flexnet.operations.webservices.v1.GetUserPermissionsRequestType getUserPermissionsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getUserPermissions"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getUserPermissionsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
