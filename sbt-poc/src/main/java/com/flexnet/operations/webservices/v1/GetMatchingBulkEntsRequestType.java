/**
 * GetMatchingBulkEntsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class GetMatchingBulkEntsRequestType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.GetMatchingBulkEntInfoType[] bulkEntList;

    public GetMatchingBulkEntsRequestType() {
    }

    public GetMatchingBulkEntsRequestType(
           com.flexnet.operations.webservices.v1.GetMatchingBulkEntInfoType[] bulkEntList) {
           this.bulkEntList = bulkEntList;
    }


    /**
     * Gets the bulkEntList value for this GetMatchingBulkEntsRequestType.
     * 
     * @return bulkEntList
     */
    public com.flexnet.operations.webservices.v1.GetMatchingBulkEntInfoType[] getBulkEntList() {
        return bulkEntList;
    }


    /**
     * Sets the bulkEntList value for this GetMatchingBulkEntsRequestType.
     * 
     * @param bulkEntList
     */
    public void setBulkEntList(com.flexnet.operations.webservices.v1.GetMatchingBulkEntInfoType[] bulkEntList) {
        this.bulkEntList = bulkEntList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetMatchingBulkEntsRequestType)) return false;
        GetMatchingBulkEntsRequestType other = (GetMatchingBulkEntsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bulkEntList==null && other.getBulkEntList()==null) || 
             (this.bulkEntList!=null &&
              java.util.Arrays.equals(this.bulkEntList, other.getBulkEntList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBulkEntList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBulkEntList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBulkEntList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetMatchingBulkEntsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bulkEntList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntInfoType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntInfo"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
