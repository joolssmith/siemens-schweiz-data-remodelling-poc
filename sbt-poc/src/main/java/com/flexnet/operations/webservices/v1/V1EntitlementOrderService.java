/**
 * V1EntitlementOrderService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public interface V1EntitlementOrderService extends javax.xml.rpc.Service {
    public java.lang.String getV1EntitlementOrderServiceAddress();

    public com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 getV1EntitlementOrderService() throws javax.xml.rpc.ServiceException;

    public com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 getV1EntitlementOrderService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
