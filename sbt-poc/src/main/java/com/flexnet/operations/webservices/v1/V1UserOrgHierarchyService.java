/**
 * V1UserOrgHierarchyService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public interface V1UserOrgHierarchyService extends javax.xml.rpc.Service {
    public java.lang.String getV1UserOrgHierarchyServiceAddress();

    public com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 getV1UserOrgHierarchyService() throws javax.xml.rpc.ServiceException;

    public com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 getV1UserOrgHierarchyService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
