/**
 * V1EntitlementOrderServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class V1EntitlementOrderServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[42];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
        _initOperationDesc5();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createBulkEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementRequestType"), com.flexnet.operations.webservices.v1.CreateBulkEntitlementDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlement"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createSimpleEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementRequestType"), com.flexnet.operations.webservices.v1.CreateSimpleEntitlementRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementRequestType"), com.flexnet.operations.webservices.v1.DeleteEntitlementDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createWebRegKey");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyRequestType"), com.flexnet.operations.webservices.v1.AddWebRegKeyRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateBulkEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementRequestType"), com.flexnet.operations.webservices.v1.UpdateBulkEntitlementDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlement"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateSimpleEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementRequestType"), com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "simpleEntitlement"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createEntitlementLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createEntitlementLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addOnlyEntitlementLineItemRequestType"), com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addOnlyEntitlementLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createEntitlementLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("replaceEntitlementLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "replaceEntitlementLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "replaceOnlyEntitlementLineItemRequestType"), com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItem"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "replaceOnlyEntitlementLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "replaceEntitlementLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteEntitlementLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemRequestType"), com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("updateEntitlementLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemRequestType"), com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEntitlementsQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementRequestType"), com.flexnet.operations.webservices.v1.SearchEntitlementRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SearchEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBulkEntitlementPropertiesQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesRequestType"), com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBulkEntitlementCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountRequestType"), com.flexnet.operations.webservices.v1.GetBulkEntitlementCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getActivatableItemsQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemRequestType"), com.flexnet.operations.webservices.v1.SearchActivatableItemRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEntitlementLineItemPropertiesQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesRequestType"), com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEntitlementCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountRequestType"), com.flexnet.operations.webservices.v1.GetEntitlementCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getActivatableItemCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountRequestType"), com.flexnet.operations.webservices.v1.GetActivatableItemCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getExactAvailableCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountRequestType"), com.flexnet.operations.webservices.v1.GetExactAvailableCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setEntitlementState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateRequestType"), com.flexnet.operations.webservices.v1.EntitlementStateDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getWebRegKeyCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountRequestType"), com.flexnet.operations.webservices.v1.GetWebRegKeyCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getWebRegKeysQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryRequestType"), com.flexnet.operations.webservices.v1.GetWebRegKeysQueryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getEntitlementAttributesFromModel");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesRequestType"), com.flexnet.operations.webservices.v1.GetEntitlementAttributesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("renewLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewEntitlementRequestType"), com.flexnet.operations.webservices.v1.RenewEntitlementDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.RenewEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("upgradeLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "upgradeLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "EntitlementLifeCycleRequestType"), com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "EntitlementLifeCycleResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "upgradeLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("upsellLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "upsellLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "EntitlementLifeCycleRequestType"), com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "EntitlementLifeCycleResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "upsellLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("mapEntitlementsToUser");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserRequestType"), com.flexnet.operations.webservices.v1.MapEntitlementsToUserRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("emailEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementRequestType"), com.flexnet.operations.webservices.v1.EmailEntitlementRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.EmailEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("emailActivatableItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemRequestType"), com.flexnet.operations.webservices.v1.EmailActivatableItemRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setLineItemState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateRequestType"), com.flexnet.operations.webservices.v1.LineItemStateDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItem"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SetLineItemStateResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setMaintenanceLineItemState");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateRequestType"), com.flexnet.operations.webservices.v1.MaintenanceLineItemStateDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItem"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteWebRegKey");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyRequestType"), com.flexnet.operations.webservices.v1.DeleteWebRegKeyRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("mergeEntitlements");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsRequestType"), com.flexnet.operations.webservices.v1.MergeEntitlementsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("transferEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsRequestType"), com.flexnet.operations.webservices.v1.TransferEntitlementsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("transferLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsRequestType"), com.flexnet.operations.webservices.v1.TransferLineItemsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.TransferLineItemsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getStateChangeHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryRequestType"), com.flexnet.operations.webservices.v1.GetStateChangeHistoryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("linkMaintenanceLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemRequestType"), com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("splitLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemRequestType"), com.flexnet.operations.webservices.v1.SplitLineItemRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SplitLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[36] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("splitBulkEntitlement");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementRequestType"), com.flexnet.operations.webservices.v1.SplitBulkEntitlementRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[37] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getMatchingLineItems");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsRequestType"), com.flexnet.operations.webservices.v1.GetMatchingLineItemsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[38] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getMatchingBulkEnts");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsRequestType"), com.flexnet.operations.webservices.v1.GetMatchingBulkEntsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[39] = oper;

    }

    private static void _initOperationDesc5(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteMaintenanceLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemRequestType"), com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItemData"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[40] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("unlinkMaintenanceLineItem");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemRequestType"), com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemResponseType"));
        oper.setReturnClass(com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[41] = oper;

    }

    public V1EntitlementOrderServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public V1EntitlementOrderServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public V1EntitlementOrderServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
        addBindings2();
        addBindings3();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "acpiGenerationIdLicensePolicyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AcpiGenerationIdLicensePolicyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "activatableItemDetailType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ActivatableItemDetailType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "ActivatableItemType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ActivatableItemType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addedEntitlementLineItemDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddedEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addedEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addedLineItems");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addedEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddedEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addOnlyEntitlementLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addOnlyEntitlementLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddWebRegKeyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddWebRegKeyRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "advancedReinstallPolicyType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AdvancedReinstallPolicyType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "AttributeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeMetaDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeMetaDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeMetaDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeMetaDescriptorType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AttributeMetaDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.BulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlementPropertiesType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.BulkEntitlementPropertiesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlementResponseConfigRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.BulkEntitlementResponseConfigRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "BulkEntitlementType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.BulkEntitlementType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "cancelLicensePolicyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CancelLicensePolicyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "channelPartnerDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ChannelPartnerDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "channelPartnerDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "channelPartner");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "channelPartnerDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ChannelPartnerDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CollectionOperationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CollectionOperationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdBulkEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdBulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdEntitlementLifeCycleDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedEntitlementLifeCycleDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdEntitlementLifeCycleDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdEntitlementLifeCycleDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedEntitlementLifeCycleDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdRenewEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedRenewEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdRenewEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdRenewEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedRenewEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdSimpleEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedSimpleEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdSimpleEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdSimpleEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createdSimpleEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreatedSimpleEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CreateOrUpdateOperationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateSimpleEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateSimpleEntitlementRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributeDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CustomAttributeDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributeDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributeDescriptorType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CustomAttributeDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CustomAttributeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributesQueryListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.CustomAttributeQueryType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributeQueryType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "datedSearchType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DatedSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "DateQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DateQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "DateTimeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DateTimeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItemData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteWebRegKeyRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "DurationType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DurationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "DurationUnit");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.DurationUnit.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EmailActivatableItemRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailContactListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailId");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EmailEntitlementRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EmailEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entCustomAttributeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntCustomAttributeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entCustomAttributesQueryListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntCustomAttributeQueryType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entCustomAttributeQueryType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitledProductDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitledProductDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitledProductDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitledProduct");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitledProductDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitledProductDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLifeCycleDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "EntitlementLifeCycleRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLifeCycleDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "EntitlementLifeCycleResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLineItemIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLineItemIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLineItemPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLineItemPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLineItemPropertiesType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLineItemPropertiesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementLineItemResponseConfigRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementLineItemResponseConfigRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateChangeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateChangeListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementStateChangeDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateChangeDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementStateDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "expirationTermsDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ExpirationTermsDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "extraActivationDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ExtraActivationDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddEntitlementLineItemDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedAddEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItemData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedAddEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddWebRegKeyDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedAddWebRegKeyDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddWebRegKeyDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedWebRegKeyData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddWebRegKeyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedAddWebRegKeyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteMaintenanceLineItemDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteMaintenanceLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteMaintenanceLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteWebRegKeyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteWebRegKeyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteWebRegKeyListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedDeleteWebRegKeyDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedDeleteWebRegKeyDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedWebRegKey");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlementLifeCycleDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedEntitlementLifeCycleDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlementLifeCycleDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlementLifeCycleDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedEntitlementLifeCycleDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlementStateDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedEntitlementStateDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlementStateDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlementStateDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedEntitlementStateDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedIdDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedIdDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItemStateDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedLineItemStateDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItemStateDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItemStateDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedLineItemStateDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedLinkMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkMaintenanceLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedLinkMaintenanceLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkMaintenanceLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLinkMaintenanceLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceLineItemStateDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedMaintenanceLineItemStateDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceLineItemStateDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceLineItemStateDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedMaintenanceLineItemStateDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMapEntitlementsToUserDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedIdDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedIdDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedId");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMatchingBulkEntDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedMatchingBulkEntDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMatchingBulkEntsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedMatchingBulkEntDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMatchingBulkEntDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEnt");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMatchingLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedMatchingLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMatchingLineItemsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedMatchingLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMatchingLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRemoveEntitlementLineItemDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedRemoveEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRemoveEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRemoveEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedRemoveEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRenewEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedRenewEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRenewEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedRenewEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedRenewEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSimpleEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedSimpleEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSimpleEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSimpleEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSimpleEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedSimpleEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSplitBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedSplitBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSplitBulkEntitlementListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedSplitBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSplitBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSplitLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedSplitLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSplitLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedSplitLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSplitLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedTransferEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedTransferEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedTransferEntitlementListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedTransferEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedTransferEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedTransferLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedTransferLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedTransferLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedTransferLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedTransferLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUnlinkMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUnlinkMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUnlinkMaintenanceLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUnlinkMaintenanceLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUnlinkMaintenanceLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUnlinkMaintenanceLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateBulkEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedBulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateEntitlementLineItemDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateSimpleEntitlementDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateSimpleEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateSimpleEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedSimpleEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedUpdateSimpleEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FailedUpdateSimpleEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureBundleIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureBundleIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundlePKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureBundlePKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleStateChangeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureBundleStateChangeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleStateChangeListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureBundleStateChangeDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundleStateChangeDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureBundle");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featurePKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeaturePKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureStateChangeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureStateChangeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureStateChangeListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.FeatureStateChangeDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "featureStateChangeDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "feature");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetActivatableItemCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetBulkEntitlementCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetEntitlementAttributesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetEntitlementCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetExactAvailableCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingBulkEntInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingBulkEntInfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntInfoType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingBulkEntsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsResponseListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MatchingBulkEntDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "matchingBulkEntDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "matchingBulkEnt");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingLineItemInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingLineItemInfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemInfoType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingLineItemsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsResponseListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MatchingLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "matchingLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "matchingLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetStateChangeHistoryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetWebRegKeyCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetWebRegKeysQueryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "idListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "id");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "idType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.IdType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseModelIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseModelIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseModelPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelStateChangeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseModelStateChangeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelStateChangeListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseModelStateChangeDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModelStateChangeDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseModel");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseTechnologyIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseTechnologyIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "licenseTechnologyPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LicenseTechnologyPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lifeCycleLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LifeCycleLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemCustomAttributeQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LineItemCustomAttributeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemCustomAttributesQueryListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LineItemCustomAttributeQueryType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemCustomAttributeQueryType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemLifeCycleDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LineItemLifeCycleDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemStateDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LineItemStateDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "LineItemType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LineItemType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItemPropertiesType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MaintenanceLineItemPropertiesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItemStateDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MaintenanceLineItemStateDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MapEntitlementsToUserRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "matchingBulkEntDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MatchingBulkEntDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "matchingLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MatchingLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MergeEntitlementsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings2() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "newEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.NewEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "NumberQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.NumberQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "numberSearchType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.NumberSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.OrganizationPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "PartnerTierQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PartnerTierQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "partNumberIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PartNumberIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "partNumberPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PartNumberPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "policyAttributesDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PolicyAttributesDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "policyAttributesListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PolicyAttributesListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "policyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PolicyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "policyTermType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.PolicyTermType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ProductIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ProductIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ProductPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productStateChangeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ProductStateChangeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productStateChangeListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ProductStateChangeDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "productStateChangeDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "product");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "ProductType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ProductType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "reinstallPolicyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ReinstallPolicyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewedEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RenewedEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RenewEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RenewEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RenewEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RenewLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "renewParametersDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.RenewParametersDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "replaceOnlyEntitlementLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "replaceOnlyEntitlementLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchActivatableItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchActivatableItemRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchEntitlementRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SearchEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.EntitlementStateDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementStateDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.LineItemStateDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemStateDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SetLineItemStateResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.MaintenanceLineItemStateDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItemStateDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "maintenanceLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "simpleEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SimpleEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "SimpleQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SimpleQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "simpleSearchType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SimpleSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitBulkEntitlementInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitBulkEntitlementInfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementInfoType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlementInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitBulkEntitlementRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementResponseListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitLineItemInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitLineItemInfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemInfoType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitLineItemRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemResponseListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.SplitLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StartDateOptionType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StartDateOptionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "stateChangeDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StateChangeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StateChangeEventType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StateChangeEventType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "stateChangeResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StateChangeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StateQueryType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StateQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StateType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StateType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StatusInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StatusInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StatusType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.StatusType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferEntitlementInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferEntitlementInfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementInfoType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "entitlementInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferEntitlementsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemInfoType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferLineItemInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferLineItemInfoType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemInfoType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemInfo");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferLineItemsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferLineItemsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferredEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredEntitlementsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferredEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferredLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredLineItemMapType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferredLineItemMapType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredLineItemsListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.TransferredLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "transferredLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItem");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateBulkEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateBulkEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "bulkEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitledProductDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateEntitledProductDataListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItemData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateMaintenanceLineItemDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateMaintenanceLineItemDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementRequestType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "simpleEntitlement");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementResponseType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userIdentifierType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "userPKType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.UserPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings3() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "valueType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "value");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "versionDateAttributesType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.VersionDateAttributesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "VersionDateOptionType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.VersionDateOptionType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "virtualLicensePolicyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.VirtualLicensePolicyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeyCountDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.WebRegKeyCountDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeyDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.AddWebRegKeyDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyDataType");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeyData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeyDataType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.WebRegKeyDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeysDataListType");
            cachedSerQNames.add(qName);
            cls = com.flexnet.operations.webservices.v1.WebRegKeysDataListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeysListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKey");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKeyType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "webRegKey");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType createBulkEntitlement(com.flexnet.operations.webservices.v1.CreateBulkEntitlementDataType[] createBulkEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createBulkEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createBulkEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType createSimpleEntitlement(com.flexnet.operations.webservices.v1.CreateSimpleEntitlementRequestType createSimpleEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createSimpleEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createSimpleEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType deleteEntitlement(com.flexnet.operations.webservices.v1.DeleteEntitlementDataType[] deleteEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType createWebRegKey(com.flexnet.operations.webservices.v1.AddWebRegKeyRequestType addWebRegKeyRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createWebRegKey"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {addWebRegKeyRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType updateBulkEntitlement(com.flexnet.operations.webservices.v1.UpdateBulkEntitlementDataType[] updateBulkEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateBulkEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateBulkEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType updateSimpleEntitlement(com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementDataType[] updateSimpleEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateSimpleEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateSimpleEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType createEntitlementLineItem(com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemRequestType createEntitlementLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createEntitlementLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createEntitlementLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType replaceEntitlementLineItem(com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] replaceEntitlementLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "replaceEntitlementLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {replaceEntitlementLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType deleteEntitlementLineItem(com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemDataType[] removeEntitlementLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteEntitlementLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {removeEntitlementLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType updateEntitlementLineItem(com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemDataType[] updateEntitlementLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "updateEntitlementLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {updateEntitlementLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SearchEntitlementResponseType getEntitlementsQuery(com.flexnet.operations.webservices.v1.SearchEntitlementRequestType searchEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getEntitlementsQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {searchEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SearchEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SearchEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SearchEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType getBulkEntitlementPropertiesQuery(com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesRequestType getBulkEntitlementPropertiesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getBulkEntitlementPropertiesQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getBulkEntitlementPropertiesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType getBulkEntitlementCount(com.flexnet.operations.webservices.v1.GetBulkEntitlementCountRequestType getBulkEntitlementCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getBulkEntitlementCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getBulkEntitlementCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType getActivatableItemsQuery(com.flexnet.operations.webservices.v1.SearchActivatableItemRequestType searchActivatableItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getActivatableItemsQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {searchActivatableItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType getEntitlementLineItemPropertiesQuery(com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesRequestType searchEntitlementLineItemPropertiesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getEntitlementLineItemPropertiesQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {searchEntitlementLineItemPropertiesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType getEntitlementCount(com.flexnet.operations.webservices.v1.GetEntitlementCountRequestType getEntitlementCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getEntitlementCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getEntitlementCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType getActivatableItemCount(com.flexnet.operations.webservices.v1.GetActivatableItemCountRequestType getActivatableItemCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getActivatableItemCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getActivatableItemCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType getExactAvailableCount(com.flexnet.operations.webservices.v1.GetExactAvailableCountRequestType getExactAvailableCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getExactAvailableCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getExactAvailableCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType setEntitlementState(com.flexnet.operations.webservices.v1.EntitlementStateDataType[] setEntitlementStateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "setEntitlementState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {setEntitlementStateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType getWebRegKeyCount(com.flexnet.operations.webservices.v1.GetWebRegKeyCountRequestType getWebRegKeyCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getWebRegKeyCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getWebRegKeyCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType getWebRegKeysQuery(com.flexnet.operations.webservices.v1.GetWebRegKeysQueryRequestType getWebRegKeysQueryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getWebRegKeysQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getWebRegKeysQueryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType getEntitlementAttributesFromModel(com.flexnet.operations.webservices.v1.GetEntitlementAttributesRequestType getEntitlementAttributesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getEntitlementAttributesFromModel"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getEntitlementAttributesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.RenewEntitlementResponseType renewLicense(com.flexnet.operations.webservices.v1.RenewEntitlementDataType[] renewLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "renewLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {renewLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.RenewEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.RenewEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.RenewEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType upgradeLicense(com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[] upgradeLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "upgradeLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {upgradeLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType upsellLicense(com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[] upsellLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "upsellLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {upsellLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType mapEntitlementsToUser(com.flexnet.operations.webservices.v1.MapEntitlementsToUserRequestType mapEntitlementsToUserRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "mapEntitlementsToUser"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {mapEntitlementsToUserRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.EmailEntitlementResponseType emailEntitlement(com.flexnet.operations.webservices.v1.EmailEntitlementRequestType emailEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "emailEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {emailEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.EmailEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.EmailEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.EmailEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType emailActivatableItem(com.flexnet.operations.webservices.v1.EmailActivatableItemRequestType emailActivatableItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "emailActivatableItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {emailActivatableItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SetLineItemStateResponseType setLineItemState(com.flexnet.operations.webservices.v1.LineItemStateDataType[] setLineItemStateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "setLineItemState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {setLineItemStateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SetLineItemStateResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SetLineItemStateResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SetLineItemStateResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType setMaintenanceLineItemState(com.flexnet.operations.webservices.v1.MaintenanceLineItemStateDataType[] setMaintenanceLineItemStateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "setMaintenanceLineItemState"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {setMaintenanceLineItemStateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType deleteWebRegKey(com.flexnet.operations.webservices.v1.DeleteWebRegKeyRequestType deleteWebRegKeyRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteWebRegKey"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteWebRegKeyRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType mergeEntitlements(com.flexnet.operations.webservices.v1.MergeEntitlementsRequestType mergeEntitlementsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "mergeEntitlements"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {mergeEntitlementsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType transferEntitlement(com.flexnet.operations.webservices.v1.TransferEntitlementsRequestType transferEntitlementsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "transferEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {transferEntitlementsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.TransferLineItemsResponseType transferLineItem(com.flexnet.operations.webservices.v1.TransferLineItemsRequestType transferLineItemsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "transferLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {transferLineItemsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.TransferLineItemsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.TransferLineItemsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.TransferLineItemsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType getStateChangeHistory(com.flexnet.operations.webservices.v1.GetStateChangeHistoryRequestType getStateChangeHistoryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getStateChangeHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getStateChangeHistoryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType linkMaintenanceLineItem(com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemRequestType linkMaintenanceLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "linkMaintenanceLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {linkMaintenanceLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SplitLineItemResponseType splitLineItem(com.flexnet.operations.webservices.v1.SplitLineItemRequestType splitLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[36]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "splitLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {splitLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SplitLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SplitLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SplitLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType splitBulkEntitlement(com.flexnet.operations.webservices.v1.SplitBulkEntitlementRequestType splitBulkEntitlementRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[37]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "splitBulkEntitlement"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {splitBulkEntitlementRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType getMatchingLineItems(com.flexnet.operations.webservices.v1.GetMatchingLineItemsRequestType getMatchingLineItemsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[38]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getMatchingLineItems"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getMatchingLineItemsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType getMatchingBulkEnts(com.flexnet.operations.webservices.v1.GetMatchingBulkEntsRequestType getMatchingBulkEntsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[39]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getMatchingBulkEnts"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getMatchingBulkEntsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType deleteMaintenanceLineItem(com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemDataType[] deleteMaintenanceLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[40]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteMaintenanceLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteMaintenanceLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType unlinkMaintenanceLineItem(com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemRequestType unlinkMaintenanceLineItemRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[41]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "unlinkMaintenanceLineItem"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {unlinkMaintenanceLineItemRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
