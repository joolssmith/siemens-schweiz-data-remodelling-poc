/**
 * AddOnlyEntitlementLineItemResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class AddOnlyEntitlementLineItemResponseType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.StatusInfoType statusInfo;

    private com.flexnet.operations.webservices.v1.FailedAddEntitlementLineItemDataType[] failedData;

    private com.flexnet.operations.webservices.v1.AddedEntitlementLineItemDataType[] responseData;

    public AddOnlyEntitlementLineItemResponseType() {
    }

    public AddOnlyEntitlementLineItemResponseType(
           com.flexnet.operations.webservices.v1.StatusInfoType statusInfo,
           com.flexnet.operations.webservices.v1.FailedAddEntitlementLineItemDataType[] failedData,
           com.flexnet.operations.webservices.v1.AddedEntitlementLineItemDataType[] responseData) {
           this.statusInfo = statusInfo;
           this.failedData = failedData;
           this.responseData = responseData;
    }


    /**
     * Gets the statusInfo value for this AddOnlyEntitlementLineItemResponseType.
     * 
     * @return statusInfo
     */
    public com.flexnet.operations.webservices.v1.StatusInfoType getStatusInfo() {
        return statusInfo;
    }


    /**
     * Sets the statusInfo value for this AddOnlyEntitlementLineItemResponseType.
     * 
     * @param statusInfo
     */
    public void setStatusInfo(com.flexnet.operations.webservices.v1.StatusInfoType statusInfo) {
        this.statusInfo = statusInfo;
    }


    /**
     * Gets the failedData value for this AddOnlyEntitlementLineItemResponseType.
     * 
     * @return failedData
     */
    public com.flexnet.operations.webservices.v1.FailedAddEntitlementLineItemDataType[] getFailedData() {
        return failedData;
    }


    /**
     * Sets the failedData value for this AddOnlyEntitlementLineItemResponseType.
     * 
     * @param failedData
     */
    public void setFailedData(com.flexnet.operations.webservices.v1.FailedAddEntitlementLineItemDataType[] failedData) {
        this.failedData = failedData;
    }


    /**
     * Gets the responseData value for this AddOnlyEntitlementLineItemResponseType.
     * 
     * @return responseData
     */
    public com.flexnet.operations.webservices.v1.AddedEntitlementLineItemDataType[] getResponseData() {
        return responseData;
    }


    /**
     * Sets the responseData value for this AddOnlyEntitlementLineItemResponseType.
     * 
     * @param responseData
     */
    public void setResponseData(com.flexnet.operations.webservices.v1.AddedEntitlementLineItemDataType[] responseData) {
        this.responseData = responseData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddOnlyEntitlementLineItemResponseType)) return false;
        AddOnlyEntitlementLineItemResponseType other = (AddOnlyEntitlementLineItemResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusInfo==null && other.getStatusInfo()==null) || 
             (this.statusInfo!=null &&
              this.statusInfo.equals(other.getStatusInfo()))) &&
            ((this.failedData==null && other.getFailedData()==null) || 
             (this.failedData!=null &&
              java.util.Arrays.equals(this.failedData, other.getFailedData()))) &&
            ((this.responseData==null && other.getResponseData()==null) || 
             (this.responseData!=null &&
              java.util.Arrays.equals(this.responseData, other.getResponseData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusInfo() != null) {
            _hashCode += getStatusInfo().hashCode();
        }
        if (getFailedData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFailedData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFailedData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getResponseData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResponseData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResponseData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddOnlyEntitlementLineItemResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addOnlyEntitlementLineItemResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "statusInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StatusInfoType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("failedData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedAddEntitlementLineItemDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedLineItemData"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "responseData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addedEntitlementLineItemDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addedLineItems"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
