/**
 * UpdateUserRolesListType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class UpdateUserRolesListType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.RoleIdentifierType[] role;

    private com.flexnet.operations.webservices.v1.CollectionOperationType opType;

    public UpdateUserRolesListType() {
    }

    public UpdateUserRolesListType(
           com.flexnet.operations.webservices.v1.RoleIdentifierType[] role,
           com.flexnet.operations.webservices.v1.CollectionOperationType opType) {
           this.role = role;
           this.opType = opType;
    }


    /**
     * Gets the role value for this UpdateUserRolesListType.
     * 
     * @return role
     */
    public com.flexnet.operations.webservices.v1.RoleIdentifierType[] getRole() {
        return role;
    }


    /**
     * Sets the role value for this UpdateUserRolesListType.
     * 
     * @param role
     */
    public void setRole(com.flexnet.operations.webservices.v1.RoleIdentifierType[] role) {
        this.role = role;
    }

    public com.flexnet.operations.webservices.v1.RoleIdentifierType getRole(int i) {
        return this.role[i];
    }

    public void setRole(int i, com.flexnet.operations.webservices.v1.RoleIdentifierType _value) {
        this.role[i] = _value;
    }


    /**
     * Gets the opType value for this UpdateUserRolesListType.
     * 
     * @return opType
     */
    public com.flexnet.operations.webservices.v1.CollectionOperationType getOpType() {
        return opType;
    }


    /**
     * Sets the opType value for this UpdateUserRolesListType.
     * 
     * @param opType
     */
    public void setOpType(com.flexnet.operations.webservices.v1.CollectionOperationType opType) {
        this.opType = opType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateUserRolesListType)) return false;
        UpdateUserRolesListType other = (UpdateUserRolesListType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.role==null && other.getRole()==null) || 
             (this.role!=null &&
              java.util.Arrays.equals(this.role, other.getRole()))) &&
            ((this.opType==null && other.getOpType()==null) || 
             (this.opType!=null &&
              this.opType.equals(other.getOpType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRole() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRole());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRole(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOpType() != null) {
            _hashCode += getOpType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateUserRolesListType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateUserRolesListType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("role");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "role"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "roleIdentifierType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "opType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CollectionOperationType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
