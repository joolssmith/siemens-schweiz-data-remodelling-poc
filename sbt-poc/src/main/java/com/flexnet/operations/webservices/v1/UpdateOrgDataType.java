/**
 * UpdateOrgDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class UpdateOrgDataType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.OrganizationIdentifierType organization;

    private java.lang.String name;

    private java.lang.String displayName;

    private java.lang.String description;

    private com.flexnet.operations.webservices.v1.AddressDataType address;

    private com.flexnet.operations.webservices.v1.UpdateSubOrganizationsListType subOrganizations;

    private com.flexnet.operations.webservices.v1.UpdateRelatedOrganizationsListType relatedOrganizations;

    private java.lang.Boolean visible;

    private com.flexnet.operations.webservices.v1.AttributeDescriptorType[] customAttributes;

    public UpdateOrgDataType() {
    }

    public UpdateOrgDataType(
           com.flexnet.operations.webservices.v1.OrganizationIdentifierType organization,
           java.lang.String name,
           java.lang.String displayName,
           java.lang.String description,
           com.flexnet.operations.webservices.v1.AddressDataType address,
           com.flexnet.operations.webservices.v1.UpdateSubOrganizationsListType subOrganizations,
           com.flexnet.operations.webservices.v1.UpdateRelatedOrganizationsListType relatedOrganizations,
           java.lang.Boolean visible,
           com.flexnet.operations.webservices.v1.AttributeDescriptorType[] customAttributes) {
           this.organization = organization;
           this.name = name;
           this.displayName = displayName;
           this.description = description;
           this.address = address;
           this.subOrganizations = subOrganizations;
           this.relatedOrganizations = relatedOrganizations;
           this.visible = visible;
           this.customAttributes = customAttributes;
    }


    /**
     * Gets the organization value for this UpdateOrgDataType.
     * 
     * @return organization
     */
    public com.flexnet.operations.webservices.v1.OrganizationIdentifierType getOrganization() {
        return organization;
    }


    /**
     * Sets the organization value for this UpdateOrgDataType.
     * 
     * @param organization
     */
    public void setOrganization(com.flexnet.operations.webservices.v1.OrganizationIdentifierType organization) {
        this.organization = organization;
    }


    /**
     * Gets the name value for this UpdateOrgDataType.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this UpdateOrgDataType.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the displayName value for this UpdateOrgDataType.
     * 
     * @return displayName
     */
    public java.lang.String getDisplayName() {
        return displayName;
    }


    /**
     * Sets the displayName value for this UpdateOrgDataType.
     * 
     * @param displayName
     */
    public void setDisplayName(java.lang.String displayName) {
        this.displayName = displayName;
    }


    /**
     * Gets the description value for this UpdateOrgDataType.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this UpdateOrgDataType.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the address value for this UpdateOrgDataType.
     * 
     * @return address
     */
    public com.flexnet.operations.webservices.v1.AddressDataType getAddress() {
        return address;
    }


    /**
     * Sets the address value for this UpdateOrgDataType.
     * 
     * @param address
     */
    public void setAddress(com.flexnet.operations.webservices.v1.AddressDataType address) {
        this.address = address;
    }


    /**
     * Gets the subOrganizations value for this UpdateOrgDataType.
     * 
     * @return subOrganizations
     */
    public com.flexnet.operations.webservices.v1.UpdateSubOrganizationsListType getSubOrganizations() {
        return subOrganizations;
    }


    /**
     * Sets the subOrganizations value for this UpdateOrgDataType.
     * 
     * @param subOrganizations
     */
    public void setSubOrganizations(com.flexnet.operations.webservices.v1.UpdateSubOrganizationsListType subOrganizations) {
        this.subOrganizations = subOrganizations;
    }


    /**
     * Gets the relatedOrganizations value for this UpdateOrgDataType.
     * 
     * @return relatedOrganizations
     */
    public com.flexnet.operations.webservices.v1.UpdateRelatedOrganizationsListType getRelatedOrganizations() {
        return relatedOrganizations;
    }


    /**
     * Sets the relatedOrganizations value for this UpdateOrgDataType.
     * 
     * @param relatedOrganizations
     */
    public void setRelatedOrganizations(com.flexnet.operations.webservices.v1.UpdateRelatedOrganizationsListType relatedOrganizations) {
        this.relatedOrganizations = relatedOrganizations;
    }


    /**
     * Gets the visible value for this UpdateOrgDataType.
     * 
     * @return visible
     */
    public java.lang.Boolean getVisible() {
        return visible;
    }


    /**
     * Sets the visible value for this UpdateOrgDataType.
     * 
     * @param visible
     */
    public void setVisible(java.lang.Boolean visible) {
        this.visible = visible;
    }


    /**
     * Gets the customAttributes value for this UpdateOrgDataType.
     * 
     * @return customAttributes
     */
    public com.flexnet.operations.webservices.v1.AttributeDescriptorType[] getCustomAttributes() {
        return customAttributes;
    }


    /**
     * Sets the customAttributes value for this UpdateOrgDataType.
     * 
     * @param customAttributes
     */
    public void setCustomAttributes(com.flexnet.operations.webservices.v1.AttributeDescriptorType[] customAttributes) {
        this.customAttributes = customAttributes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateOrgDataType)) return false;
        UpdateOrgDataType other = (UpdateOrgDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.organization==null && other.getOrganization()==null) || 
             (this.organization!=null &&
              this.organization.equals(other.getOrganization()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.displayName==null && other.getDisplayName()==null) || 
             (this.displayName!=null &&
              this.displayName.equals(other.getDisplayName()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.address==null && other.getAddress()==null) || 
             (this.address!=null &&
              this.address.equals(other.getAddress()))) &&
            ((this.subOrganizations==null && other.getSubOrganizations()==null) || 
             (this.subOrganizations!=null &&
              this.subOrganizations.equals(other.getSubOrganizations()))) &&
            ((this.relatedOrganizations==null && other.getRelatedOrganizations()==null) || 
             (this.relatedOrganizations!=null &&
              this.relatedOrganizations.equals(other.getRelatedOrganizations()))) &&
            ((this.visible==null && other.getVisible()==null) || 
             (this.visible!=null &&
              this.visible.equals(other.getVisible()))) &&
            ((this.customAttributes==null && other.getCustomAttributes()==null) || 
             (this.customAttributes!=null &&
              java.util.Arrays.equals(this.customAttributes, other.getCustomAttributes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOrganization() != null) {
            _hashCode += getOrganization().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getDisplayName() != null) {
            _hashCode += getDisplayName().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getAddress() != null) {
            _hashCode += getAddress().hashCode();
        }
        if (getSubOrganizations() != null) {
            _hashCode += getSubOrganizations().hashCode();
        }
        if (getRelatedOrganizations() != null) {
            _hashCode += getRelatedOrganizations().hashCode();
        }
        if (getVisible() != null) {
            _hashCode += getVisible().hashCode();
        }
        if (getCustomAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateOrgDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateOrgDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organization");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organization"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "organizationIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "displayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("address");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "address"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addressDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("subOrganizations");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "subOrganizations"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateSubOrganizationsListType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relatedOrganizations");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "relatedOrganizations"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "updateRelatedOrganizationsListType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("visible");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "visible"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "customAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "attribute"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
