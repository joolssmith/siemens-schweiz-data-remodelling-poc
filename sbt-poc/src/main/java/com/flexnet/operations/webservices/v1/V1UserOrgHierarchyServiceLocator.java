/**
 * V1UserOrgHierarchyServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class V1UserOrgHierarchyServiceLocator extends org.apache.axis.client.Service implements com.flexnet.operations.webservices.v1.V1UserOrgHierarchyService {

    public V1UserOrgHierarchyServiceLocator() {
    }


    public V1UserOrgHierarchyServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public V1UserOrgHierarchyServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for V1UserOrgHierarchyService
    private java.lang.String V1UserOrgHierarchyService_address = "http://win-qpp9cma26ji:8888/flexnet/services/v1/UserOrgHierarchyService";

    public java.lang.String getV1UserOrgHierarchyServiceAddress() {
        return V1UserOrgHierarchyService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String V1UserOrgHierarchyServiceWSDDServiceName = "v1/UserOrgHierarchyService";

    public java.lang.String getV1UserOrgHierarchyServiceWSDDServiceName() {
        return V1UserOrgHierarchyServiceWSDDServiceName;
    }

    public void setV1UserOrgHierarchyServiceWSDDServiceName(java.lang.String name) {
        V1UserOrgHierarchyServiceWSDDServiceName = name;
    }

    public com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 getV1UserOrgHierarchyService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(V1UserOrgHierarchyService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getV1UserOrgHierarchyService(endpoint);
    }

    public com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 getV1UserOrgHierarchyService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.flexnet.operations.webservices.v1.V1UserOrgHierarchyServiceSoapBindingStub _stub = new com.flexnet.operations.webservices.v1.V1UserOrgHierarchyServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getV1UserOrgHierarchyServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setV1UserOrgHierarchyServiceEndpointAddress(java.lang.String address) {
        V1UserOrgHierarchyService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1.class.isAssignableFrom(serviceEndpointInterface)) {
                com.flexnet.operations.webservices.v1.V1UserOrgHierarchyServiceSoapBindingStub _stub = new com.flexnet.operations.webservices.v1.V1UserOrgHierarchyServiceSoapBindingStub(new java.net.URL(V1UserOrgHierarchyService_address), this);
                _stub.setPortName(getV1UserOrgHierarchyServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("v1/UserOrgHierarchyService".equals(inputPortName)) {
            return getV1UserOrgHierarchyService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "v1/UserOrgHierarchyService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "v1/UserOrgHierarchyService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("V1UserOrgHierarchyService".equals(portName)) {
            setV1UserOrgHierarchyServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
