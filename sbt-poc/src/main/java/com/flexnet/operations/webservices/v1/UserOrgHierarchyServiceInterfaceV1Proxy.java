package com.flexnet.operations.webservices.v1;

public class UserOrgHierarchyServiceInterfaceV1Proxy implements com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 {
  private String _endpoint = null;
  private com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 userOrgHierarchyServiceInterfaceV1 = null;
  
  public UserOrgHierarchyServiceInterfaceV1Proxy() {
    _initUserOrgHierarchyServiceInterfaceV1Proxy();
  }
  
  public UserOrgHierarchyServiceInterfaceV1Proxy(String endpoint) {
    _endpoint = endpoint;
    _initUserOrgHierarchyServiceInterfaceV1Proxy();
  }
  
  private void _initUserOrgHierarchyServiceInterfaceV1Proxy() {
    try {
      userOrgHierarchyServiceInterfaceV1 = (new com.flexnet.operations.webservices.v1.V1UserOrgHierarchyServiceLocator()).getV1UserOrgHierarchyService();
      if (userOrgHierarchyServiceInterfaceV1 != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userOrgHierarchyServiceInterfaceV1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userOrgHierarchyServiceInterfaceV1)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userOrgHierarchyServiceInterfaceV1 != null)
      ((javax.xml.rpc.Stub)userOrgHierarchyServiceInterfaceV1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1 getUserOrgHierarchyServiceInterfaceV1() {
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1;
  }
  
  public com.flexnet.operations.webservices.v1.CreateOrgResponseType createOrganization(com.flexnet.operations.webservices.v1.CreateOrgRequestType createOrgRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.createOrganization(createOrgRequest);
  }
  
  public com.flexnet.operations.webservices.v1.LinkOrganizationsResponseType linkOrganizations(com.flexnet.operations.webservices.v1.LinkOrganizationsDataType[] linkOrganizationsRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.linkOrganizations(linkOrganizationsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType updateOrganization(com.flexnet.operations.webservices.v1.UpdateOrgDataType[] updateOrganizationRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.updateOrganization(updateOrganizationRequest);
  }
  
  public com.flexnet.operations.webservices.v1.DeleteOrganizationResponseType deleteOrganization(com.flexnet.operations.webservices.v1.DeleteOrgDataType[] deleteOrganizationRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.deleteOrganization(deleteOrganizationRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType getOrganizationsQuery(com.flexnet.operations.webservices.v1.GetOrganizationsQueryRequestType getOrganizationsQueryRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getOrganizationsQuery(getOrganizationsQueryRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType getOrganizationCount(com.flexnet.operations.webservices.v1.GetOrganizationCountRequestType getOrganizationCountRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getOrganizationCount(getOrganizationCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetParentOrganizationsResponseType getParentOrganizations(com.flexnet.operations.webservices.v1.GetParentOrganizationsRequestType getParentOrganizationsRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getParentOrganizations(getParentOrganizationsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetSubOrganizationsResponseType getSubOrganizations(com.flexnet.operations.webservices.v1.GetSubOrganizationsRequestType getSubOrganizationsRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getSubOrganizations(getSubOrganizationsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetUsersQueryResponseType getUsersQuery(com.flexnet.operations.webservices.v1.GetUsersQueryRequestType getUsersQueryRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getUsersQuery(getUsersQueryRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetUserCountResponseType getUserCount(com.flexnet.operations.webservices.v1.GetUserCountRequestType getUserCountRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getUserCount(getUserCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.CreateUserResponseType createUser(com.flexnet.operations.webservices.v1.CreateUserRequestType createUserRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.createUser(createUserRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UpdateUserResponseType updateUser(com.flexnet.operations.webservices.v1.UpdateUserDataType[] updateUserRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.updateUser(updateUserRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UpdateUserRolesResponseType updateUserRoles(com.flexnet.operations.webservices.v1.UpdateUserRolesDataType[] updateUserRolesRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.updateUserRoles(updateUserRolesRequest);
  }
  
  public com.flexnet.operations.webservices.v1.DeleteUserResponseType deleteUser(com.flexnet.operations.webservices.v1.UserIdentifierType[] deleteUserRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.deleteUser(deleteUserRequest);
  }
  
  public com.flexnet.operations.webservices.v1.RelateOrganizationsResponseType relateOrganizations(com.flexnet.operations.webservices.v1.RelateOrganizationsDataType[] relateOrganizationsRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.relateOrganizations(relateOrganizationsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetRelatedOrganizationsResponseType getRelatedOrganizations(com.flexnet.operations.webservices.v1.GetRelatedOrganizationsRequestType getRelatedOrganizationsRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getRelatedOrganizations(getRelatedOrganizationsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetUserPermissionsResponseType getUserPermissions(com.flexnet.operations.webservices.v1.GetUserPermissionsRequestType getUserPermissionsRequest) throws java.rmi.RemoteException{
    if (userOrgHierarchyServiceInterfaceV1 == null)
      _initUserOrgHierarchyServiceInterfaceV1Proxy();
    return userOrgHierarchyServiceInterfaceV1.getUserPermissions(getUserPermissionsRequest);
  }
  
  
}