/**
 * CreateUserRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class CreateUserRequestType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.CreateUserDataType[] user;

    private com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType opType;

    public CreateUserRequestType() {
    }

    public CreateUserRequestType(
           com.flexnet.operations.webservices.v1.CreateUserDataType[] user,
           com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType opType) {
           this.user = user;
           this.opType = opType;
    }


    /**
     * Gets the user value for this CreateUserRequestType.
     * 
     * @return user
     */
    public com.flexnet.operations.webservices.v1.CreateUserDataType[] getUser() {
        return user;
    }


    /**
     * Sets the user value for this CreateUserRequestType.
     * 
     * @param user
     */
    public void setUser(com.flexnet.operations.webservices.v1.CreateUserDataType[] user) {
        this.user = user;
    }

    public com.flexnet.operations.webservices.v1.CreateUserDataType getUser(int i) {
        return this.user[i];
    }

    public void setUser(int i, com.flexnet.operations.webservices.v1.CreateUserDataType _value) {
        this.user[i] = _value;
    }


    /**
     * Gets the opType value for this CreateUserRequestType.
     * 
     * @return opType
     */
    public com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType getOpType() {
        return opType;
    }


    /**
     * Sets the opType value for this CreateUserRequestType.
     * 
     * @param opType
     */
    public void setOpType(com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType opType) {
        this.opType = opType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateUserRequestType)) return false;
        CreateUserRequestType other = (CreateUserRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.user==null && other.getUser()==null) || 
             (this.user!=null &&
              java.util.Arrays.equals(this.user, other.getUser()))) &&
            ((this.opType==null && other.getOpType()==null) || 
             (this.opType!=null &&
              this.opType.equals(other.getOpType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getUser() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUser());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUser(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOpType() != null) {
            _hashCode += getOpType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateUserRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("user");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "user"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "createUserDataType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "opType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CreateOrUpdateOperationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
