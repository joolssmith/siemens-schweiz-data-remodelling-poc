/**
 * SetMaintenanceLineItemStateResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class SetMaintenanceLineItemStateResponseType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.StatusInfoType statusInfo;

    private com.flexnet.operations.webservices.v1.FailedMaintenanceLineItemStateDataType[] failedMaintenanceData;

    public SetMaintenanceLineItemStateResponseType() {
    }

    public SetMaintenanceLineItemStateResponseType(
           com.flexnet.operations.webservices.v1.StatusInfoType statusInfo,
           com.flexnet.operations.webservices.v1.FailedMaintenanceLineItemStateDataType[] failedMaintenanceData) {
           this.statusInfo = statusInfo;
           this.failedMaintenanceData = failedMaintenanceData;
    }


    /**
     * Gets the statusInfo value for this SetMaintenanceLineItemStateResponseType.
     * 
     * @return statusInfo
     */
    public com.flexnet.operations.webservices.v1.StatusInfoType getStatusInfo() {
        return statusInfo;
    }


    /**
     * Sets the statusInfo value for this SetMaintenanceLineItemStateResponseType.
     * 
     * @param statusInfo
     */
    public void setStatusInfo(com.flexnet.operations.webservices.v1.StatusInfoType statusInfo) {
        this.statusInfo = statusInfo;
    }


    /**
     * Gets the failedMaintenanceData value for this SetMaintenanceLineItemStateResponseType.
     * 
     * @return failedMaintenanceData
     */
    public com.flexnet.operations.webservices.v1.FailedMaintenanceLineItemStateDataType[] getFailedMaintenanceData() {
        return failedMaintenanceData;
    }


    /**
     * Sets the failedMaintenanceData value for this SetMaintenanceLineItemStateResponseType.
     * 
     * @param failedMaintenanceData
     */
    public void setFailedMaintenanceData(com.flexnet.operations.webservices.v1.FailedMaintenanceLineItemStateDataType[] failedMaintenanceData) {
        this.failedMaintenanceData = failedMaintenanceData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetMaintenanceLineItemStateResponseType)) return false;
        SetMaintenanceLineItemStateResponseType other = (SetMaintenanceLineItemStateResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusInfo==null && other.getStatusInfo()==null) || 
             (this.statusInfo!=null &&
              this.statusInfo.equals(other.getStatusInfo()))) &&
            ((this.failedMaintenanceData==null && other.getFailedMaintenanceData()==null) || 
             (this.failedMaintenanceData!=null &&
              java.util.Arrays.equals(this.failedMaintenanceData, other.getFailedMaintenanceData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusInfo() != null) {
            _hashCode += getStatusInfo().hashCode();
        }
        if (getFailedMaintenanceData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFailedMaintenanceData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFailedMaintenanceData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetMaintenanceLineItemStateResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "statusInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "StatusInfoType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("failedMaintenanceData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceLineItemStateDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "failedMaintenanceLineItem"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
