/**
 * V1EntitlementOrderServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class V1EntitlementOrderServiceLocator extends org.apache.axis.client.Service implements com.flexnet.operations.webservices.v1.V1EntitlementOrderService {

    public V1EntitlementOrderServiceLocator() {
    }


    public V1EntitlementOrderServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public V1EntitlementOrderServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for V1EntitlementOrderService
    private java.lang.String V1EntitlementOrderService_address = "https://atvies991c7srv.ad001.siemens.net:11443/flexnet/services/v1/EntitlementOrderService";

    public java.lang.String getV1EntitlementOrderServiceAddress() {
        return V1EntitlementOrderService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String V1EntitlementOrderServiceWSDDServiceName = "v1/EntitlementOrderService";

    public java.lang.String getV1EntitlementOrderServiceWSDDServiceName() {
        return V1EntitlementOrderServiceWSDDServiceName;
    }

    public void setV1EntitlementOrderServiceWSDDServiceName(java.lang.String name) {
        V1EntitlementOrderServiceWSDDServiceName = name;
    }

    public com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 getV1EntitlementOrderService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(V1EntitlementOrderService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getV1EntitlementOrderService(endpoint);
    }

    public com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 getV1EntitlementOrderService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.flexnet.operations.webservices.v1.V1EntitlementOrderServiceSoapBindingStub _stub = new com.flexnet.operations.webservices.v1.V1EntitlementOrderServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getV1EntitlementOrderServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setV1EntitlementOrderServiceEndpointAddress(java.lang.String address) {
        V1EntitlementOrderService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1.class.isAssignableFrom(serviceEndpointInterface)) {
                com.flexnet.operations.webservices.v1.V1EntitlementOrderServiceSoapBindingStub _stub = new com.flexnet.operations.webservices.v1.V1EntitlementOrderServiceSoapBindingStub(new java.net.URL(V1EntitlementOrderService_address), this);
                _stub.setPortName(getV1EntitlementOrderServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("v1/EntitlementOrderService".equals(inputPortName)) {
            return getV1EntitlementOrderService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "v1/EntitlementOrderService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "v1/EntitlementOrderService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("V1EntitlementOrderService".equals(portName)) {
            setV1EntitlementOrderServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
