/**
 * AddOnlyEntitlementLineItemRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public class AddOnlyEntitlementLineItemRequestType  implements java.io.Serializable {
    private com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] lineItem;

    private com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType opType;

    public AddOnlyEntitlementLineItemRequestType() {
    }

    public AddOnlyEntitlementLineItemRequestType(
           com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] lineItem,
           com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType opType) {
           this.lineItem = lineItem;
           this.opType = opType;
    }


    /**
     * Gets the lineItem value for this AddOnlyEntitlementLineItemRequestType.
     * 
     * @return lineItem
     */
    public com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] getLineItem() {
        return lineItem;
    }


    /**
     * Sets the lineItem value for this AddOnlyEntitlementLineItemRequestType.
     * 
     * @param lineItem
     */
    public void setLineItem(com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] lineItem) {
        this.lineItem = lineItem;
    }

    public com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType getLineItem(int i) {
        return this.lineItem[i];
    }

    public void setLineItem(int i, com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType _value) {
        this.lineItem[i] = _value;
    }


    /**
     * Gets the opType value for this AddOnlyEntitlementLineItemRequestType.
     * 
     * @return opType
     */
    public com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType getOpType() {
        return opType;
    }


    /**
     * Sets the opType value for this AddOnlyEntitlementLineItemRequestType.
     * 
     * @param opType
     */
    public void setOpType(com.flexnet.operations.webservices.v1.CreateOrUpdateOperationType opType) {
        this.opType = opType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AddOnlyEntitlementLineItemRequestType)) return false;
        AddOnlyEntitlementLineItemRequestType other = (AddOnlyEntitlementLineItemRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.lineItem==null && other.getLineItem()==null) || 
             (this.lineItem!=null &&
              java.util.Arrays.equals(this.lineItem, other.getLineItem()))) &&
            ((this.opType==null && other.getOpType()==null) || 
             (this.opType!=null &&
              this.opType.equals(other.getOpType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLineItem() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLineItem());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLineItem(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOpType() != null) {
            _hashCode += getOpType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AddOnlyEntitlementLineItemRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addOnlyEntitlementLineItemRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "lineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "addEntitlementLineItemDataType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("opType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "opType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:v1.webservices.operations.flexnet.com", "CreateOrUpdateOperationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
