/**
 * EntitlementOrderServiceInterfaceV1.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.flexnet.operations.webservices.v1;

public interface EntitlementOrderServiceInterfaceV1 extends java.rmi.Remote {
    public com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType createBulkEntitlement(com.flexnet.operations.webservices.v1.CreateBulkEntitlementDataType[] createBulkEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType createSimpleEntitlement(com.flexnet.operations.webservices.v1.CreateSimpleEntitlementRequestType createSimpleEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType deleteEntitlement(com.flexnet.operations.webservices.v1.DeleteEntitlementDataType[] deleteEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType createWebRegKey(com.flexnet.operations.webservices.v1.AddWebRegKeyRequestType addWebRegKeyRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType updateBulkEntitlement(com.flexnet.operations.webservices.v1.UpdateBulkEntitlementDataType[] updateBulkEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType updateSimpleEntitlement(com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementDataType[] updateSimpleEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType createEntitlementLineItem(com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemRequestType createEntitlementLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType replaceEntitlementLineItem(com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] replaceEntitlementLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType deleteEntitlementLineItem(com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemDataType[] removeEntitlementLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType updateEntitlementLineItem(com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemDataType[] updateEntitlementLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SearchEntitlementResponseType getEntitlementsQuery(com.flexnet.operations.webservices.v1.SearchEntitlementRequestType searchEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType getBulkEntitlementPropertiesQuery(com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesRequestType getBulkEntitlementPropertiesRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType getBulkEntitlementCount(com.flexnet.operations.webservices.v1.GetBulkEntitlementCountRequestType getBulkEntitlementCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType getActivatableItemsQuery(com.flexnet.operations.webservices.v1.SearchActivatableItemRequestType searchActivatableItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType getEntitlementLineItemPropertiesQuery(com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesRequestType searchEntitlementLineItemPropertiesRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType getEntitlementCount(com.flexnet.operations.webservices.v1.GetEntitlementCountRequestType getEntitlementCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType getActivatableItemCount(com.flexnet.operations.webservices.v1.GetActivatableItemCountRequestType getActivatableItemCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType getExactAvailableCount(com.flexnet.operations.webservices.v1.GetExactAvailableCountRequestType getExactAvailableCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType setEntitlementState(com.flexnet.operations.webservices.v1.EntitlementStateDataType[] setEntitlementStateRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType getWebRegKeyCount(com.flexnet.operations.webservices.v1.GetWebRegKeyCountRequestType getWebRegKeyCountRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType getWebRegKeysQuery(com.flexnet.operations.webservices.v1.GetWebRegKeysQueryRequestType getWebRegKeysQueryRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType getEntitlementAttributesFromModel(com.flexnet.operations.webservices.v1.GetEntitlementAttributesRequestType getEntitlementAttributesRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.RenewEntitlementResponseType renewLicense(com.flexnet.operations.webservices.v1.RenewEntitlementDataType[] renewLicenseRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType upgradeLicense(com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[] upgradeLicenseRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType upsellLicense(com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[] upsellLicenseRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType mapEntitlementsToUser(com.flexnet.operations.webservices.v1.MapEntitlementsToUserRequestType mapEntitlementsToUserRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.EmailEntitlementResponseType emailEntitlement(com.flexnet.operations.webservices.v1.EmailEntitlementRequestType emailEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType emailActivatableItem(com.flexnet.operations.webservices.v1.EmailActivatableItemRequestType emailActivatableItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SetLineItemStateResponseType setLineItemState(com.flexnet.operations.webservices.v1.LineItemStateDataType[] setLineItemStateRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType setMaintenanceLineItemState(com.flexnet.operations.webservices.v1.MaintenanceLineItemStateDataType[] setMaintenanceLineItemStateRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType deleteWebRegKey(com.flexnet.operations.webservices.v1.DeleteWebRegKeyRequestType deleteWebRegKeyRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType mergeEntitlements(com.flexnet.operations.webservices.v1.MergeEntitlementsRequestType mergeEntitlementsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType transferEntitlement(com.flexnet.operations.webservices.v1.TransferEntitlementsRequestType transferEntitlementsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.TransferLineItemsResponseType transferLineItem(com.flexnet.operations.webservices.v1.TransferLineItemsRequestType transferLineItemsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType getStateChangeHistory(com.flexnet.operations.webservices.v1.GetStateChangeHistoryRequestType getStateChangeHistoryRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType linkMaintenanceLineItem(com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemRequestType linkMaintenanceLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SplitLineItemResponseType splitLineItem(com.flexnet.operations.webservices.v1.SplitLineItemRequestType splitLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType splitBulkEntitlement(com.flexnet.operations.webservices.v1.SplitBulkEntitlementRequestType splitBulkEntitlementRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType getMatchingLineItems(com.flexnet.operations.webservices.v1.GetMatchingLineItemsRequestType getMatchingLineItemsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType getMatchingBulkEnts(com.flexnet.operations.webservices.v1.GetMatchingBulkEntsRequestType getMatchingBulkEntsRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType deleteMaintenanceLineItem(com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemDataType[] deleteMaintenanceLineItemRequest) throws java.rmi.RemoteException;
    public com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType unlinkMaintenanceLineItem(com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemRequestType unlinkMaintenanceLineItemRequest) throws java.rmi.RemoteException;
}
