package com.flexnet.operations.webservices.v1;

public class EntitlementOrderServiceInterfaceV1Proxy implements com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 {
  private String _endpoint = null;
  private com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 entitlementOrderServiceInterfaceV1 = null;
  
  public EntitlementOrderServiceInterfaceV1Proxy() {
    _initEntitlementOrderServiceInterfaceV1Proxy();
  }
  
  public EntitlementOrderServiceInterfaceV1Proxy(String endpoint) {
    _endpoint = endpoint;
    _initEntitlementOrderServiceInterfaceV1Proxy();
  }
  
  private void _initEntitlementOrderServiceInterfaceV1Proxy() {
    try {
      entitlementOrderServiceInterfaceV1 = (new com.flexnet.operations.webservices.v1.V1EntitlementOrderServiceLocator()).getV1EntitlementOrderService();
      if (entitlementOrderServiceInterfaceV1 != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)entitlementOrderServiceInterfaceV1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)entitlementOrderServiceInterfaceV1)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (entitlementOrderServiceInterfaceV1 != null)
      ((javax.xml.rpc.Stub)entitlementOrderServiceInterfaceV1)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1 getEntitlementOrderServiceInterfaceV1() {
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1;
  }
  
  public com.flexnet.operations.webservices.v1.CreateBulkEntitlementResponseType createBulkEntitlement(com.flexnet.operations.webservices.v1.CreateBulkEntitlementDataType[] createBulkEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.createBulkEntitlement(createBulkEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.CreateSimpleEntitlementResponseType createSimpleEntitlement(com.flexnet.operations.webservices.v1.CreateSimpleEntitlementRequestType createSimpleEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.createSimpleEntitlement(createSimpleEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.DeleteEntitlementResponseType deleteEntitlement(com.flexnet.operations.webservices.v1.DeleteEntitlementDataType[] deleteEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.deleteEntitlement(deleteEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.AddWebRegKeyResponseType createWebRegKey(com.flexnet.operations.webservices.v1.AddWebRegKeyRequestType addWebRegKeyRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.createWebRegKey(addWebRegKeyRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UpdateBulkEntitlementResponseType updateBulkEntitlement(com.flexnet.operations.webservices.v1.UpdateBulkEntitlementDataType[] updateBulkEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.updateBulkEntitlement(updateBulkEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementResponseType updateSimpleEntitlement(com.flexnet.operations.webservices.v1.UpdateSimpleEntitlementDataType[] updateSimpleEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.updateSimpleEntitlement(updateSimpleEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemResponseType createEntitlementLineItem(com.flexnet.operations.webservices.v1.AddOnlyEntitlementLineItemRequestType createEntitlementLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.createEntitlementLineItem(createEntitlementLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.ReplaceOnlyEntitlementLineItemResponseType replaceEntitlementLineItem(com.flexnet.operations.webservices.v1.AddEntitlementLineItemDataType[] replaceEntitlementLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.replaceEntitlementLineItem(replaceEntitlementLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemResponseType deleteEntitlementLineItem(com.flexnet.operations.webservices.v1.RemoveEntitlementLineItemDataType[] removeEntitlementLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.deleteEntitlementLineItem(removeEntitlementLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemResponseType updateEntitlementLineItem(com.flexnet.operations.webservices.v1.UpdateEntitlementLineItemDataType[] updateEntitlementLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.updateEntitlementLineItem(updateEntitlementLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SearchEntitlementResponseType getEntitlementsQuery(com.flexnet.operations.webservices.v1.SearchEntitlementRequestType searchEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getEntitlementsQuery(searchEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesResponseType getBulkEntitlementPropertiesQuery(com.flexnet.operations.webservices.v1.GetBulkEntitlementPropertiesRequestType getBulkEntitlementPropertiesRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getBulkEntitlementPropertiesQuery(getBulkEntitlementPropertiesRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetBulkEntitlementCountResponseType getBulkEntitlementCount(com.flexnet.operations.webservices.v1.GetBulkEntitlementCountRequestType getBulkEntitlementCountRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getBulkEntitlementCount(getBulkEntitlementCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SearchActivatableItemResponseType getActivatableItemsQuery(com.flexnet.operations.webservices.v1.SearchActivatableItemRequestType searchActivatableItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getActivatableItemsQuery(searchActivatableItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType getEntitlementLineItemPropertiesQuery(com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesRequestType searchEntitlementLineItemPropertiesRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getEntitlementLineItemPropertiesQuery(searchEntitlementLineItemPropertiesRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType getEntitlementCount(com.flexnet.operations.webservices.v1.GetEntitlementCountRequestType getEntitlementCountRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getEntitlementCount(getEntitlementCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetActivatableItemCountResponseType getActivatableItemCount(com.flexnet.operations.webservices.v1.GetActivatableItemCountRequestType getActivatableItemCountRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getActivatableItemCount(getActivatableItemCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetExactAvailableCountResponseType getExactAvailableCount(com.flexnet.operations.webservices.v1.GetExactAvailableCountRequestType getExactAvailableCountRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getExactAvailableCount(getExactAvailableCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SetEntitlementStateResponseType setEntitlementState(com.flexnet.operations.webservices.v1.EntitlementStateDataType[] setEntitlementStateRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.setEntitlementState(setEntitlementStateRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetWebRegKeyCountResponseType getWebRegKeyCount(com.flexnet.operations.webservices.v1.GetWebRegKeyCountRequestType getWebRegKeyCountRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getWebRegKeyCount(getWebRegKeyCountRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetWebRegKeysQueryResponseType getWebRegKeysQuery(com.flexnet.operations.webservices.v1.GetWebRegKeysQueryRequestType getWebRegKeysQueryRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getWebRegKeysQuery(getWebRegKeysQueryRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetEntitlementAttributesResponseType getEntitlementAttributesFromModel(com.flexnet.operations.webservices.v1.GetEntitlementAttributesRequestType getEntitlementAttributesRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getEntitlementAttributesFromModel(getEntitlementAttributesRequest);
  }
  
  public com.flexnet.operations.webservices.v1.RenewEntitlementResponseType renewLicense(com.flexnet.operations.webservices.v1.RenewEntitlementDataType[] renewLicenseRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.renewLicense(renewLicenseRequest);
  }
  
  public com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType upgradeLicense(com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[] upgradeLicenseRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.upgradeLicense(upgradeLicenseRequest);
  }
  
  public com.flexnet.operations.webservices.v1.EntitlementLifeCycleResponseType upsellLicense(com.flexnet.operations.webservices.v1.EntitlementLifeCycleDataType[] upsellLicenseRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.upsellLicense(upsellLicenseRequest);
  }
  
  public com.flexnet.operations.webservices.v1.MapEntitlementsToUserResponseType mapEntitlementsToUser(com.flexnet.operations.webservices.v1.MapEntitlementsToUserRequestType mapEntitlementsToUserRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.mapEntitlementsToUser(mapEntitlementsToUserRequest);
  }
  
  public com.flexnet.operations.webservices.v1.EmailEntitlementResponseType emailEntitlement(com.flexnet.operations.webservices.v1.EmailEntitlementRequestType emailEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.emailEntitlement(emailEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.EmailActivatableItemResponseType emailActivatableItem(com.flexnet.operations.webservices.v1.EmailActivatableItemRequestType emailActivatableItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.emailActivatableItem(emailActivatableItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SetLineItemStateResponseType setLineItemState(com.flexnet.operations.webservices.v1.LineItemStateDataType[] setLineItemStateRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.setLineItemState(setLineItemStateRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SetMaintenanceLineItemStateResponseType setMaintenanceLineItemState(com.flexnet.operations.webservices.v1.MaintenanceLineItemStateDataType[] setMaintenanceLineItemStateRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.setMaintenanceLineItemState(setMaintenanceLineItemStateRequest);
  }
  
  public com.flexnet.operations.webservices.v1.DeleteWebRegKeyResponseType deleteWebRegKey(com.flexnet.operations.webservices.v1.DeleteWebRegKeyRequestType deleteWebRegKeyRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.deleteWebRegKey(deleteWebRegKeyRequest);
  }
  
  public com.flexnet.operations.webservices.v1.MergeEntitlementsResponseType mergeEntitlements(com.flexnet.operations.webservices.v1.MergeEntitlementsRequestType mergeEntitlementsRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.mergeEntitlements(mergeEntitlementsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.TransferEntitlementsResponseType transferEntitlement(com.flexnet.operations.webservices.v1.TransferEntitlementsRequestType transferEntitlementsRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.transferEntitlement(transferEntitlementsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.TransferLineItemsResponseType transferLineItem(com.flexnet.operations.webservices.v1.TransferLineItemsRequestType transferLineItemsRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.transferLineItem(transferLineItemsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetStateChangeHistoryResponseType getStateChangeHistory(com.flexnet.operations.webservices.v1.GetStateChangeHistoryRequestType getStateChangeHistoryRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getStateChangeHistory(getStateChangeHistoryRequest);
  }
  
  public com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemResponseType linkMaintenanceLineItem(com.flexnet.operations.webservices.v1.LinkMaintenanceLineItemRequestType linkMaintenanceLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.linkMaintenanceLineItem(linkMaintenanceLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SplitLineItemResponseType splitLineItem(com.flexnet.operations.webservices.v1.SplitLineItemRequestType splitLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.splitLineItem(splitLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.SplitBulkEntitlementResponseType splitBulkEntitlement(com.flexnet.operations.webservices.v1.SplitBulkEntitlementRequestType splitBulkEntitlementRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.splitBulkEntitlement(splitBulkEntitlementRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetMatchingLineItemsResponseType getMatchingLineItems(com.flexnet.operations.webservices.v1.GetMatchingLineItemsRequestType getMatchingLineItemsRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getMatchingLineItems(getMatchingLineItemsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.GetMatchingBulkEntsResponseType getMatchingBulkEnts(com.flexnet.operations.webservices.v1.GetMatchingBulkEntsRequestType getMatchingBulkEntsRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.getMatchingBulkEnts(getMatchingBulkEntsRequest);
  }
  
  public com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemResponseType deleteMaintenanceLineItem(com.flexnet.operations.webservices.v1.DeleteMaintenanceLineItemDataType[] deleteMaintenanceLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.deleteMaintenanceLineItem(deleteMaintenanceLineItemRequest);
  }
  
  public com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemResponseType unlinkMaintenanceLineItem(com.flexnet.operations.webservices.v1.UnlinkMaintenanceLineItemRequestType unlinkMaintenanceLineItemRequest) throws java.rmi.RemoteException{
    if (entitlementOrderServiceInterfaceV1 == null)
      _initEntitlementOrderServiceInterfaceV1Proxy();
    return entitlementOrderServiceInterfaceV1.unlinkMaintenanceLineItem(unlinkMaintenanceLineItemRequest);
  }
  
  
}