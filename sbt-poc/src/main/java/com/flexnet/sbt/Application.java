package com.flexnet.sbt;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.flexnet.sbt.cockpit.entity.EntityManager;
import com.flexnet.sbt.cockpit.services.FnoService;

/**
 * 
 * @author juliansmith
 *
 */
@SpringBootApplication
@EnableScheduling
@Component
public class Application {
  /**
   * STATIC
   */
	private final static Logger logger = LoggerFactory.getLogger(Application.class);

  private static Application instance;
  
  static {
    logger.debug("static");
  }
  
  public static Application getInstance() {
    return Application.instance;
  }
  
  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }
  
  /**
   * PRIVATE 
   */
  private final ObjectMapper json = new ObjectMapper()
    .configure(SerializationFeature.INDENT_OUTPUT, true)
    .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
    .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
    .configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false)
    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  
  @Autowired
  private ApplicationConfig config;
  
  private final EntityManager entityManager = new EntityManager();

  private volatile AppState state = AppState.INACTIVE;
  
  /**
   * PRIVATE METHODS
   */

  private void logMe() {
    logger.debug("Aplication {} {}", this.getClass().getName(), this.hashCode());
  }

  /**
   * PUBLIC METHODS
   */
  public Application() {
    Trace.in(logger);
    try {
      logMe();
      Application.instance = this;
    }
    finally {
      Trace.out(logger);
    }
  }
  
  @PostConstruct
  public void postConstruct(){
    Trace.in(logger);
    try {

    }
    finally {
      Trace.out(logger);
    }
  }
  
  @PreDestroy
  public void preDestroy() {
    Trace.in(logger);
    try {
      this.entityManager.terminate();
    }
    finally {
      Trace.out(logger);
    }
  }
  
	@Scheduled(fixedRate = 60000)
  public void persistPoolManager() {
    Trace.in(logger);
    try {
      if (this.state == AppState.RUNNING) {
        logMe();
      }
    }
    finally {
      Trace.out(logger);
    }
  }
	
  public ApplicationConfig getApplicationConfiguration() {
    return this.config;
  }

	@Bean
	public CommandLineRunner initialize(ApplicationContext ctx) {
		return args -> {
			try {
				Trace.in(logger);
		    logMe();
		    	    
	      FnoService.setHostname(this.config.getFnoHostName());
	      FnoService.setUsername(this.config.getFnoUserName());
	      FnoService.setPassword(this.config.getFnoPassword());
	      FnoService.setBatchSize(this.config.getFnoBatchSize());

	      this.entityManager.initialize(this.config.getCacheFilepath());
	      
	      logger.info("loading entities...");
	      this.entityManager.load();

        this.state = AppState.RUNNING;
      }
      catch (Exception ex) {
        logger.error("Error", ex);
      }
      finally {
        Trace.out(logger);
      }
		};
	}

	/**
	 * GETTERS
	 */
  public ObjectMapper getJson() {
    return json;
  }

  public ApplicationConfig getConfig() {
    return config;
  }

  public EntityManager getEntityManager() {
    return entityManager;
  }

  public AppState getState() {
    return state;
  }

}
