package com.flexnet.sbt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfig {
  
  @Value("${fno.username}")
  private String fnoUserName;
 
  @Value("${fno.password}")
  private String fnoPassword;
  
  @Value("${fno.hostname}")
  private String fnoHostname;
  
  @Value("${fno.domain}")
  private String fnoDomain;
  
  @Value("${fno.batchsize}")
  private String fnoBatchSize;
  
  @Value("${cache.filepath}")
  private String cacheFilepath;
  
  public ApplicationConfig() {
  }

  public String getFnoUserName() {
    return fnoUserName;
  }

  public String getFnoPassword() {
    return fnoPassword;
  }

  public String getFnoHostName() {
    return fnoHostname;
  }

  public String getFnoDomain() {
    return fnoDomain;
  }

  public long getFnoBatchSize() {
    return  Long.valueOf(fnoBatchSize);
  }

  public String getCacheFilepath() {
    return cacheFilepath;
  }

  
}
