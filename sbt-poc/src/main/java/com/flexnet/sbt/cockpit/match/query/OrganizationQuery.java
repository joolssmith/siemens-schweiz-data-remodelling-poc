package com.flexnet.sbt.cockpit.match.query;

import com.flexnet.sbt.cockpit.entity.Organization;
import com.flexnet.sbt.cockpit.match.Matchable;

public class OrganizationQuery extends Query<Organization> {

  protected Matchable<Integer> csids;
  protected Matchable<String> branch;
  protected Matchable<String> country;
  protected Matchable<String> name1;
  protected Matchable<String> name2;
  protected Matchable<String> streetName;
  protected Matchable<String> streetNumber;
  protected Matchable<String> postalCode;
  protected Matchable<String> stateProvince;
  protected Matchable<String> salesChannel;
  protected Matchable<String> buildingType;
  protected Matchable<String> vapNumber;
  protected Matchable<String> ifaNumber;
  protected Matchable<String> localSiteId;
  protected Matchable<String> createdBy;
  protected Matchable<String> createdAt;
  protected Matchable<String> modifiedBy;
  protected Matchable<String> modifiedAt;
  protected Matchable<String> hardwareId;
//  protected Matchable<String> rcPurchaseOrderNumber;
//  protected Matchable<String> hqSalesOrderNumber;
//  protected Matchable<String> dongleNumber;
   
  public boolean match(final Organization org) {
    return 
      doMatch(this.csids, org.csid) &&
      doMatch(this.country, org.customAttributeMap, Organization.Site_Country) &&
      doMatch(this.branch, org.customAttributeMap, Organization.Site_Branch) &&
      doMatch(this.name1, org.customAttributeMap, Organization.Site_Name) &&
      doMatch(this.name2, org.customAttributeMap, Organization.Site_Name_2) &&
      doMatch(this.streetName,org.address.address) &&
      doMatch(this.streetNumber, org.address.street) &&
      doMatch(this.postalCode,org.address.zip) &&
      doMatch(this.stateProvince, org.address.state) &&
      doMatch(this.salesChannel, org.customAttributeMap, Organization.Site_Sales_Channel) &&
      doMatch(this.buildingType, org.customAttributeMap, Organization.Site_Market_Segment) &&
      doMatch(this.vapNumber, org.customAttributeMap, Organization.Site_VAP_Number) &&
      doMatch(this.ifaNumber,org.customAttributeMap, Organization.Site_Ifa_Number) &&
      doMatch(this.localSiteId, org.customAttributeMap, Organization.Site_Local_Site_Id) &&
      doMatch(this.createdAt, org.customAttributeMap, Organization.Site_Created_At) &&
      doMatch(this.modifiedAt, org.customAttributeMap, Organization.Site_Created_At) &&
      doMatch(this.createdBy, org.customAttributeMap, Organization.Site_Modified_At) &&
      doMatch(this.modifiedBy, org.customAttributeMap, Organization.Site_Modified_By) &&
      doMatch(this.hardwareId, org.customAttributeMap, Organization.Site_Hardware_Id);      
  }
 }
