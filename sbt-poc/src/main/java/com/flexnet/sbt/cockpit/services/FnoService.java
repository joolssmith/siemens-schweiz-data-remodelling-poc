package com.flexnet.sbt.cockpit.services;

import java.net.MalformedURLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class FnoService {

  private static Boolean quickQuit;
  private static String hostname;
  private static String username;
  private static String password;
  private static Long batchSize;

  protected static ExecutorService executorService;

  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  static {
    FnoService.batchSize = 1024L;
    FnoService.quickQuit = false;
  }

  protected FnoService() {
  }

  /**
   * 
   * @throws ServiceException
   * @throws MalformedURLException
   */
  public abstract void initializeService() throws ServiceException, MalformedURLException;

  /**
   * 
   * @return
   */
  public static Boolean getQuickQuit() {
    return FnoService.quickQuit;
  }

  /**
   * 
   * @param quickQuit
   */
  public static void setQuickQuit(Boolean quickQuit) {
    FnoService.quickQuit = quickQuit;
  }

  public static long getBatchSize() {
    return batchSize;
  }

  public static void setBatchSize(long batchSize) {
    FnoService.batchSize = batchSize;
  }

  /**
   * 
   * @return
   */
  public static String getHostname() {
    return FnoService.hostname;
  }

  /**
   * 
   * @param hostname
   */
  public static void setHostname(String hostname) {
    FnoService.hostname = hostname;
  }

  /**
   * 
   * @return
   */
  public static String getUsername() {
    return FnoService.username;
  }

  /**
   * 
   * @param username
   */
  public static void setUsername(String username) {
    FnoService.username = username;
  }

  public static String getPassword() {
    return FnoService.password;
  }

  /**
   * 
   * @param password
   */
  public static void setPassword(String password) {
    FnoService.password = password;
  }

  /**
   * 
   */
  public static void terminate() {
    FnoService.executorService.shutdown();
  }

  /**
   * 
   */
  public static void initialize() {
    FnoService.executorService = Executors.newFixedThreadPool(64);
  }
}
