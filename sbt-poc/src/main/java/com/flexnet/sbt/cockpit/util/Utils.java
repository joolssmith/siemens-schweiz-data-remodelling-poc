package com.flexnet.sbt.cockpit.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;

import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;

public class Utils {

  public static String toStringOrNull(final Object obj) {
    return obj == null ? null : obj.toString();
  }

  public static Date toDateOrNull(final Calendar cal) {
    return null == cal ? null : cal.getTime();
  }

  public static Date getLastMidnight() {
    final Calendar midnight = new GregorianCalendar();
    midnight.set(Calendar.HOUR_OF_DAY, 0);
    midnight.set(Calendar.MINUTE, 0);
    midnight.set(Calendar.SECOND, 0);
    midnight.set(Calendar.MILLISECOND, 0);

    return midnight.getTime();
  }

  public static Integer getCsid(final String name) {

    if (name != null) {
      try {
        return Integer.valueOf(name.replaceAll("[^0-9]", "").trim());
      }
      catch (final NumberFormatException e) {
      }
    }
    return 0;
  }

  public static Integer getCsid(final UniqueIdValue name) {

    if (UniqueIdValue.isValid(name)) {
      try {
        return Integer.valueOf(name.value.replaceAll("[^0-9]", "").trim());
      }
      catch (final NumberFormatException e) {
      }
    }
    return 0;
  }

  static final DateFormat odf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
  static final DateFormat ymdhms = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  static final DateFormat ymd = new SimpleDateFormat("yyyy-MM-dd");

  public static String toCanonicalDateTimeMillis(final Date date) {
    return date != null ? odf.format(date) : null;
  }

  public static String toCanonicalDateTime(final Date date) {
    return date != null ? ymdhms.format(date) : null;
  }

  public static String toCanonicalDate(final Date date) {
    return date != null ? ymd.format(date) : null;
  }

  public static String padR(final String str, final int length) {
    return StringUtils.rightPad(str == null ? "" : str, length);
  }
}
