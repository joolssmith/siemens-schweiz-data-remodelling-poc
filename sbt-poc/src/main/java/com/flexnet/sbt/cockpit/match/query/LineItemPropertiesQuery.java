package com.flexnet.sbt.cockpit.match.query;

import com.flexnet.sbt.cockpit.entity.LineItemProperties;
import com.flexnet.sbt.cockpit.match.Matchable;

public class LineItemPropertiesQuery extends Query<LineItemProperties>{

  protected Matchable<String> organizations;
  
  @Override
  public boolean match(LineItemProperties line) {
    return doMatch(this.organizations, line.soldTo);
  }

}
