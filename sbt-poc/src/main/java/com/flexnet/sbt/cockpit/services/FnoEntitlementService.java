package com.flexnet.sbt.cockpit.services;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.xml.rpc.Call;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;

import com.flexnet.operations.webservices.v1.DateQueryType;
import com.flexnet.operations.webservices.v1.DateTimeQueryType;
import com.flexnet.operations.webservices.v1.DatedSearchType;
import com.flexnet.operations.webservices.v1.EntitlementLineItemResponseConfigRequestType;
import com.flexnet.operations.webservices.v1.EntitlementOrderServiceInterfaceV1;
import com.flexnet.operations.webservices.v1.GetEntitlementCountRequestType;
import com.flexnet.operations.webservices.v1.GetEntitlementCountResponseType;
import com.flexnet.operations.webservices.v1.PartnerTierQueryType;
import com.flexnet.operations.webservices.v1.SearchActivatableItemDataType;
import com.flexnet.operations.webservices.v1.SearchEntitlementDataType;
import com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesRequestType;
import com.flexnet.operations.webservices.v1.SearchEntitlementLineItemPropertiesResponseType;
import com.flexnet.operations.webservices.v1.SearchEntitlementRequestType;
import com.flexnet.operations.webservices.v1.SearchEntitlementResponseType;
import com.flexnet.operations.webservices.v1.SimpleSearchType;
import com.flexnet.operations.webservices.v1.StateQueryType;
import com.flexnet.operations.webservices.v1.StateType;
import com.flexnet.operations.webservices.v1.StatusType;
import com.flexnet.operations.webservices.v1.V1EntitlementOrderServiceLocator;
import com.flexnet.sbt.cockpit.entity.Entitlement;
import com.flexnet.sbt.cockpit.entity.LineItemProperties;

public class FnoEntitlementService extends FnoService {

  private EntitlementOrderServiceInterfaceV1 service;
   
  /**
   * 
   * @param username
   * @param password
   * @throws ServiceException
   * @throws MalformedURLException 
   */
  @Override
  public void initializeService() throws ServiceException, MalformedURLException {

    final V1EntitlementOrderServiceLocator locator = new V1EntitlementOrderServiceLocator();

    this.service = locator.getV1EntitlementOrderService(
        new URL(getHostname() + locator.getV1EntitlementOrderServiceWSDDServiceName()));

    ((Stub)this.service)._setProperty(Call.USERNAME_PROPERTY, getUsername());
    ((Stub)this.service)._setProperty(Call.PASSWORD_PROPERTY, getPassword());
  }

  /**
   * 
   * @return
   * @throws RemoteException
   */
  public Integer getEntitlementCount() throws RemoteException {

    @SuppressWarnings("serial")
    GetEntitlementCountResponseType response = service.getEntitlementCount(new GetEntitlementCountRequestType() {
      {
        this.setQueryParams(new SearchEntitlementDataType() {
          {
          }
        });
      }
    });

    if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
      log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
      throw new RuntimeException(response.getStatusInfo().getReason());
    }

    return response.getCount().intValue();
  }

  private List<com.flexnet.sbt.cockpit.entity.LineItemProperties> doGetEntitlementLineItems(final SearchEntitlementLineItemPropertiesRequestType request) throws RemoteException  {

    List<com.flexnet.sbt.cockpit.entity.LineItemProperties> results = new ArrayList<>();

    for (int i = 1;; i++) {
      request.setPageNumber(BigInteger.valueOf(i));

      log.trace("getEntitlementLineItems: page - " + request.getPageNumber().toString() + " - size " + results.size());

      SearchEntitlementLineItemPropertiesResponseType response = null;
      
      for (int j = 1; ; j++) {
        try
        {
          response = this.service.getEntitlementLineItemPropertiesQuery(request);
          break;
        }
        catch (final Exception e) {
          log.error(e.getMessage());
          if (j >= 3) {
            throw new RemoteException(e.getMessage());
          }
        }
      }

      if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
        log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
        throw new RuntimeException(response.getStatusInfo().getReason());
      }

      if (response.getEntitlementLineItem() == null) {
        log.trace("getEntitlementLineItems: no data returned");
        break;
      }

      // add into array
      Arrays.asList(response.getEntitlementLineItem()).stream().filter(x -> x != null)
          .forEach(x -> results.add(EntityFactory.getLineItemProperties(x)));

      if (response.getEntitlementLineItem().length < request.getBatchSize().intValue()) {
        log.trace("getEntitlementLineItems: complete");
        break;
      }

      if (FnoService.getQuickQuit()) {
        break;
      }
    }
    return results;
  }

  /**
   * 
   * @return
   * @throws RemoteException
   */
  public List<com.flexnet.sbt.cockpit.entity.LineItemProperties> getEntitlementLineItems(final Calendar lastUpdateTime) throws RemoteException  {

    log.trace("getEntitlementLineItems: date is " + (lastUpdateTime == null ? "null" :  lastUpdateTime.toString()));
    @SuppressWarnings("serial")
    SearchEntitlementLineItemPropertiesRequestType request = new SearchEntitlementLineItemPropertiesRequestType() {
      {
        this.setQueryParams(new SearchActivatableItemDataType() {
          {
            if (null != lastUpdateTime) {
              this.setLastModifiedDateTime(new DateTimeQueryType() {
                {
                  this.setSearchType(DatedSearchType.AFTER);
                  this.setValue(lastUpdateTime);
                }
              });
            }
          }
        });
        this.setEntitlementLineItemResponseConfig(new EntitlementLineItemResponseConfigRequestType() {
          {
            this.setActivatableItemType(true);
            this.setActivationId(true);
            this.setCreatedOnDateTime(true);
            this.setDescription(true);
            this.setEntitlementDescription(true);
            this.setEntitlementId(true);
            this.setEntitlementState(true);
            this.setExpirationDate(true);
            this.setFulfilledAmount(true);
            this.setIsPermanent(true);
            this.setIsTrusted(true);
            this.setLastModifiedDateTime(true);
            this.setLicenseModel(true);
            this.setLicenseTechnology(true);
            this.setLineItemAttributes(true);
            this.setLineItemSupportAction(true);
            this.setMaintenance(true);
            this.setMaintenancePartNumber(true);
            this.setNumberOfCopies(true);
            this.setNumberOfRemainingCopies(true);
            this.setOrderId(true);
            this.setOrderLineNumber(true);
            this.setParentLineItem(true);
            this.setPartNumber(true);
            this.setPartNumberDescription(true);
            this.setProduct(true);
            this.setSoldTo(true);
            this.setSoldToDisplayName(true);
            this.setStartDate(true);
            this.setStartDateOption(true);
            this.setState(true);
            this.setTerm(true);
//            this.setCustomAttributes(new CustomAttributeDescriptorType[] {
//                new CustomAttributeDescriptorType() {
//                  {
//                    this.setAttributeName("csid");
//                  }
//                },
//                new CustomAttributeDescriptorType() {
//                  {
//                    this.setAttributeName("sbt_csid_compatible");
//                  }
//                },
//                new CustomAttributeDescriptorType() {
//                  {
//                    this.setAttributeName("sbt_local_site_id");
//                  }
//                }
//            });

          }
        });
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };
    
    return doGetEntitlementLineItems(request);
  }

  /**
   * 
   * @return
   * @throws RemoteException
   */
  public List<com.flexnet.sbt.cockpit.entity.LineItemProperties> getEntitlementLineItemsForCsid(final String orgName) throws RemoteException  {

    log.trace("getEntitlementLineItems for CSDI " + orgName);
    @SuppressWarnings("serial")
      
    SearchEntitlementLineItemPropertiesRequestType request = new SearchEntitlementLineItemPropertiesRequestType() {
      {
        this.setQueryParams(new SearchActivatableItemDataType() {
          {
            this.setOrganizationUnitName(new PartnerTierQueryType() {
              {
                this.setPartnerTier("bo.constants.partnertiernames.endcustomer");
                this.setSearchType(SimpleSearchType.EQUALS);
                this.setValue(orgName);
              }
            });
          }
        });
        this.setEntitlementLineItemResponseConfig(new EntitlementLineItemResponseConfigRequestType() {
          {
            this.setActivatableItemType(true);
            this.setActivationId(true);
            this.setCreatedOnDateTime(true);
            this.setDescription(true);
            this.setEntitlementDescription(true);
            this.setEntitlementId(true);
            this.setEntitlementState(true);
            this.setExpirationDate(true);
            this.setFulfilledAmount(true);
            this.setIsPermanent(true);
            this.setIsTrusted(true);
            this.setLastModifiedDateTime(true);
            this.setLicenseModel(true);
            this.setLicenseTechnology(true);
            this.setLineItemAttributes(true);
            this.setLineItemSupportAction(true);
            this.setMaintenance(true);
            this.setMaintenancePartNumber(true);
            this.setNumberOfCopies(true);
            this.setNumberOfRemainingCopies(true);
            this.setOrderId(true);
            this.setOrderLineNumber(true);
            this.setParentLineItem(true);
            this.setPartNumber(true);
            this.setPartNumberDescription(true);
            this.setProduct(true);
            this.setSoldTo(true);
            this.setSoldToDisplayName(true);
            this.setStartDate(true);
            this.setStartDateOption(true);
            this.setState(true);
            this.setTerm(true);
          }
        });
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };
    return doGetEntitlementLineItems(request);
  }

  public List<com.flexnet.sbt.cockpit.entity.LineItemProperties> getEntitlementLineItemsForCsids(final Collection<String> orgs)
      throws InterruptedException, ExecutionException {

    log.trace("query line items for " + orgs.size() + " orgs");
    final List<com.flexnet.sbt.cockpit.entity.LineItemProperties> results = new ArrayList<>();

    final List<Future<List<com.flexnet.sbt.cockpit.entity.LineItemProperties>>> futures = new ArrayList<>();

    for (final String org : orgs) {

      futures.add(executorService.submit(new Callable<List<com.flexnet.sbt.cockpit.entity.LineItemProperties>>() {
        @Override
        public List<LineItemProperties> call() throws Exception {
          return getEntitlementLineItemsForCsid(org);
        }
      }));
    }

    log.trace("query line items for " + orgs.size() + " orgs - waiting for results");
    for (final Future<List<com.flexnet.sbt.cockpit.entity.LineItemProperties>> future : futures) {
      final List<com.flexnet.sbt.cockpit.entity.LineItemProperties> result = future.get();
      if (result != null) {
        results.addAll(result);
      }
    }

    return results;
  }

  private List<com.flexnet.sbt.cockpit.entity.Entitlement> doGetEntitlements(final SearchEntitlementRequestType request)  throws RemoteException {
    
    List<com.flexnet.sbt.cockpit.entity.Entitlement> results = new ArrayList<>();

    for (int i = 1;; i++) {
      request.setPageNumber(BigInteger.valueOf(i));

      log.trace("getEntitlements: page - " + request.getPageNumber().toString() + " - size " + results.size());

      SearchEntitlementResponseType response = null;

      for (int j = 1; ; j++) {
        try
        {
          response = this.service.getEntitlementsQuery(request);
          break;
        }
        catch (final Exception e) {
          log.error(e.getMessage());
          if (j >= 3) {
            throw new RemoteException(e.getMessage());
          }
        }
      }
      
      if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
        log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
        throw new RuntimeException(response.getStatusInfo().getReason());
      }

      if (response.getEntitlement() == null) {
        log.trace("getEntitlements: no data returned");
        break;
      }

      // add into array
      Arrays.asList(response.getEntitlement()).stream().filter(x -> {
        return x != null && x.getSimpleEntitlement() != null;
      }).forEach(x -> {
        results.add(EntityFactory.getEntitlement(x.getSimpleEntitlement()));
      });

      if (response.getEntitlement().length < request.getBatchSize().intValue()) {
        log.trace("getEntitlements: complete");
        break;
      }
      
      if (FnoService.getQuickQuit()) {
        break;
      }
    }

    return results;
  }
  /**
   * 
   * @param lastUpdateTime
   * @return
   * @throws RemoteException
   */
  public List<com.flexnet.sbt.cockpit.entity.Entitlement> getEntitlements(final Calendar lastUpdateTime)  throws RemoteException {

    log.trace("getEntitlements: date is " + (lastUpdateTime == null ? "null" :  lastUpdateTime.toString()));
    
    @SuppressWarnings("serial")
    SearchEntitlementRequestType request = new SearchEntitlementRequestType() {
      {
        this.setEntitlementSearchCriteria(new SearchEntitlementDataType() {
          {
            if (null != lastUpdateTime) {
              this.setLastModifiedDate(new DateQueryType() {
                {
                  this.setSearchType(DatedSearchType.AFTER);
                  this.setValue(lastUpdateTime.getTime());
                }
              });
            }
          }
        });
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };

    return doGetEntitlements(request);
  }

  /**
   * 
   * @param lastUpdateTime
   * @return
   * @throws RemoteException
   */
  public List<com.flexnet.sbt.cockpit.entity.Entitlement> getEntitlementsForCsid(final String orgName) throws RemoteException {

    log.trace("getEntitlements for CSID " + orgName);

    @SuppressWarnings("serial")
    SearchEntitlementRequestType request = new SearchEntitlementRequestType() {
      {
        this.setEntitlementSearchCriteria(new SearchEntitlementDataType() {
          {
            this.setOrganizationUnitName(new PartnerTierQueryType() {
              {
                this.setPartnerTier("bo.constants.partnertiernames.endcustomer");
                this.setSearchType(SimpleSearchType.EQUALS);
                this.setValue(orgName);
              }
            });
            this.setState(new StateQueryType() {
              {
                this.setSearchType(SimpleSearchType.EQUALS);
                this.setValue(StateType.DEPLOYED);
              }
            });
          }
        });
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };

    return doGetEntitlements(request);
  }

  /**
   * 
   * @param orgs
   * @return
   * @throws InterruptedException
   * @throws ExecutionException
   * @throws RemoteException
   */
  public List<com.flexnet.sbt.cockpit.entity.Entitlement> getEntitlementsForCsids(final Collection<String> orgs)
      throws InterruptedException, ExecutionException, RemoteException {

    log.trace("query entitlements for " + orgs.size() + " orgs");

    final List<com.flexnet.sbt.cockpit.entity.Entitlement> results = new ArrayList<>();

    final List<Future<List<com.flexnet.sbt.cockpit.entity.Entitlement>>> futures = new ArrayList<>();

    for (final String org : orgs) {

      futures.add(executorService.submit(new Callable<List<com.flexnet.sbt.cockpit.entity.Entitlement>>() {
        @Override
        public List<Entitlement> call() throws Exception {
          return getEntitlementsForCsid(org);
        }
      }));
    }

    log.trace("query entitlements for " + orgs.size() + " orgs - waiting for results");
    
    for (final Future<List<com.flexnet.sbt.cockpit.entity.Entitlement>> future : futures) {

      final List<Entitlement> result = future.get();
      if (result != null) {
        results.addAll(result);
      }
    }

    return results;
  }
}
