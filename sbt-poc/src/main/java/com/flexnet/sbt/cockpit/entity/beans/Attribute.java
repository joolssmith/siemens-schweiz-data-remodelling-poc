package com.flexnet.sbt.cockpit.entity.beans;

import java.util.Date;
import java.util.List;

public class Attribute {
  public AttributeType type = AttributeType.VOID;
  public String name;
  public String stringValue;
  public Integer integerValue;
  public Date dateValue;
  public Boolean booleanValue;
  public List<String> arrayValue;
  
  public Attribute() {
    
  }
}
