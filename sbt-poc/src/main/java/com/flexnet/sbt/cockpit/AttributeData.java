package com.flexnet.sbt.cockpit;

import com.flexnet.sbt.cockpit.entity.Entitlement;
import com.flexnet.sbt.cockpit.entity.Fulfilment;
import com.flexnet.sbt.cockpit.entity.LineItemProperties;
import com.flexnet.sbt.cockpit.entity.Organization;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class AttributeData {

  public Integer csid;

  public String salesOrder;
  public String country;
  public String branch;
  public String name1;
  public String name2;
  public String streetName;
  public String streetNo;
  public String zipPostCode;
  public String locationCity;
  public String stateProvince;
  public String channel;
  public String buildingType;
  public String vapNo;
  public String createdBy;
  public String createdOn;
  public String changedBy;
  public String changedOn;
  public String hardwareId;
  public String vmSystemType;
  public String operatingSystem;
  public String regionalSettings;
  
  public String ifaNumber;

  public Boolean hasMaintenance = false;
  public Boolean hasHistory = false;
  public Boolean hasUpgrade = false;
  public Boolean hasLicense = false;

  public void update(final Organization org) {
    if (org != null) {
      this.csid = org.csid;
      this.country = org.country;
      this.branch = org.getAttribute(Organization.Site_Branch);
      this.name1 = org.getAttribute(Organization.Site_Name);
      this.name2 = org.getAttribute(Organization.Site_Name_2);
      this.channel = org.getAttribute(Organization.Site_Sales_Channel);
      this.vapNo = org.getAttribute(Organization.Site_VAP_Number);
      this.createdOn = org.getAttribute(Organization.Site_Created_At);
      this.createdBy = org.getAttribute(Organization.Site_Created_By);
      this.changedBy = org.getAttribute(Organization.Site_Modified_By);
      this.changedOn = org.getAttribute(Organization.Site_Modified_At);
      this.hardwareId = org.getAttribute(Organization.Site_Hardware_Id);
      this.regionalSettings = org.getAttribute(Organization.Site_Regional_Settings);
      this.vmSystemType = org.getAttribute(Organization.Site_VM_System_Type);
      this.operatingSystem = org.getAttribute(Organization.Site_Operating_System);
      this.ifaNumber = org.getAttribute(Organization.Site_Ifa_Number);
      this.buildingType = org.getAttribute(Organization.Site_Market_Segment);
      
      this.streetNo = org.address.address;
      this.streetName = org.address.street;
      this.zipPostCode = org.address.zip;
      this.locationCity = org.address.city;
      this.stateProvince = org.address.state;
    }
  }

  public void update(final Entitlement ent) {
    if (ent != null) {
      if (!this.hasLicense) {
        this.hasLicense |= ent.hasLicese();
      }
      if (!this.hasMaintenance) {
        this.hasMaintenance |= ent.hasVaidMaintenance();
      }
      if (!this.hasUpgrade) {
        this.hasUpgrade |= ent.hasUpgrade();
      }

    }
  }
  
  public void update(final LineItemProperties ent) {
    if (ent != null) {
      if (!this.hasLicense) {
        this.hasLicense |= ent.hasLicese();
      }
      if (!this.hasMaintenance) {
        this.hasMaintenance |= ent.hasVaidMaintenance();
      }
      if (!this.hasUpgrade) {
        this.hasUpgrade |= ent.hasUpgrade();
      }

    }
  }
  
  public void update(final Fulfilment frec) {
    if (frec != null) {
      if (!this.hasHistory) {
        this.hasHistory |= frec.hasHistory();
      }
    }
  }
  
  public static AttributeData get(final Organization org) {
    return new AttributeData() {
      {
        update(org);
      }
    };
  }

  private static Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
  
  @Override
  public String toString() {
    return gson.toJson(this, new TypeToken<AttributeData>() {}.getType());
  }
}
