package com.flexnet.sbt.cockpit.entity.beans;

public class Address {

  public String address;
  public String street;
  public String city;
  public String country;
  public String region;
  public String zip;
  public String state;
  
  public Address() {
    
  }
}
