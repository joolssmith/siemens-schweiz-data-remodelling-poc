package com.flexnet.sbt.cockpit.util;

public class Counter
{
  private long count = 0;
  
  public void reset() {
    this.count = 0;
  }
  
  public long increment() {
    return ++this.count;
  }
  
  public long getCount() {
    return this.count;
  }
}