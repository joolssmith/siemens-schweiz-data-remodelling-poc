package com.flexnet.sbt.cockpit.entity.beans;

public class UniqueIdValue {
  public String uniqueId;
  public String value;
  
  public UniqueIdValue() {
    
  }

  public static Boolean isValid(final UniqueIdValue value) {
    return value != null && value.value != null;
  }
  
  public static String getValue(final UniqueIdValue value) {
    return value != null ? value.value : null;
  }
}
