package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.flexnet.sbt.cockpit.entity.beans.Attribute;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.flexnet.sbt.cockpit.util.Utils;
import com.google.gson.reflect.TypeToken;

public class Entitlement implements Entity {
//  private static final Logger log = LogManager.getLogger(Entitlement.class);
  
  public String entitlementId;

  public String description;

  public String soldTo;

  public String shipToEmail;

  public String shipToAddress;

  public String state;

  public String createdUserId;

  public List<LineItem> lineItems;

  public List<MaintenanceLineItem> maintenanceLineItems;

  // public com.flexnet.operations.webservices.v1.ChannelPartnerDataType[]
  // channelPartners;

  public Boolean allowPortalLogin;

  public List<Attribute> entitlementAttributes;

  public transient Integer csid = null;

  public Entitlement() {

  }

  @Override
  public String getKey() {
    return this.entitlementId;
  }

  @Override
  public Type getStorageType() {
    return new TypeToken<Map<String, Entitlement>>() {
    }.getType();
  }

  @Override
  public String toString() {
    return "Entitlement " + this.entitlementId;
  }

  @Override
  public void onLoad() {
    this.csid = Utils.getCsid(this.soldTo);
  }

  public Boolean hasLicese() {
    return this.lineItems != null && this.lineItems.size() > 0;
  }

  public Boolean hasValidLicese() {
    final Date today = Utils.getLastMidnight();

    if (this.lineItems != null) {
      for (final LineItem line : this.lineItems) {
        if (line.isPermanent || (line.expirationDate != null && line.expirationDate.after(today))) {
          return true;
        }
      }
    }
    return false;
  }

  public Boolean hasMaintenance() {
    return this.maintenanceLineItems != null && this.maintenanceLineItems.size() > 0;
  }

  public Boolean hasVaidMaintenance() {
    final Date today = Utils.getLastMidnight();

    if (this.maintenanceLineItems != null && this.lineItems != null) {

      final Set<String> actIds = this.lineItems
          .stream()
          .filter(x -> x.isPermanent || x.expirationDate.after(today))
          .map(x -> x.activationId)
          .collect(Collectors.toCollection(HashSet::new));
      
      for (final MaintenanceLineItem line : this.maintenanceLineItems) {
        if (line.associatedLineItems != null && (line.isPermanent || line.expirationDate.after(today))) {
          for (final UniqueIdValue maint : line.associatedLineItems) {
            if (actIds.contains(maint.value)) {
              return true;
            }
          }
        }
      }
    }
    return false;
  }
  
  public Boolean hasUpgrade() {
    final Date today = Utils.getLastMidnight();

    if (this.lineItems != null) {
      for (final LineItem line : this.lineItems) {
        if (line.isPermanent || (line.expirationDate != null && line.expirationDate.after(today))) {
          if ("UPGRADE".equals(line.lineItemType)) {
            return true;
          }
        }
      }
    }
    return false;
  }
}
