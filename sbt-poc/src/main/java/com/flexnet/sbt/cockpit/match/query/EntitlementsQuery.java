package com.flexnet.sbt.cockpit.match.query;

import com.flexnet.sbt.cockpit.entity.Entitlement;
import com.flexnet.sbt.cockpit.match.Matchable;

public class EntitlementsQuery extends Query<Entitlement>{

  protected Matchable<String> organizations;
  
  @Override
  public boolean match(Entitlement line) {
    return doMatch(this.organizations, line.soldTo);
  }
}
