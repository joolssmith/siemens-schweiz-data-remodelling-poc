package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import com.flexnet.sbt.cockpit.entity.beans.Attribute;
import com.flexnet.sbt.cockpit.entity.beans.Duration;
import com.flexnet.sbt.cockpit.entity.beans.Product;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.flexnet.sbt.cockpit.util.Utils;
import com.google.gson.reflect.TypeToken;

public class LineItemProperties implements Entity {
  public String activationId;

  public String description;

  public String state;

  public String activatableItemType;

  public String orderId;

  public String orderLineNumber;

  public String entitlementId;

  public UniqueIdValue soldTo;

  public String soldToDisplayName;

  public String entitlementState;

  public String entitlementDescription;

  public Boolean allowPortalLogin;

  public String shipToEmail;

  public String shipToAddress;

//  public UniqueIdValue parentBulkEntitlementId;

//  public UniqueIdValue bulkEntSoldTo;

//  public String bulkEntSoldToDisplayName;

  public Product product;

  public String productDescription;

  public UniqueIdValue partNumber;

  public String partNumberDescription;

  public UniqueIdValue licenseTechnology;

  public UniqueIdValue licenseModel;

//  public UniqueIdValue alternateLicenseModel1;

//  public UniqueIdValue alternateLicenseModel2;

  public String lineItemSupportAction;

  public UniqueIdValue parentLineItem;

  public Date startDate;

//  public com.flexnet.operations.webservices.v1.StartDateOptionType startDateOption;

  public Boolean isPermanent;

  public Duration term;

  public Date expirationDate;

//  public Date versionDate;
//
//  public com.flexnet.operations.webservices.v1.VersionDateAttributesType versionDateAttributes;

  public Integer numberOfCopies;

  public Integer fulfilledAmount;

  public Integer numberOfRemainingCopies;

  public Boolean isTrusted;

  public List<Attribute> customAttributes;

  public List<Product> entitledProducts;

  //public com.flexnet.operations.webservices.v1.ChannelPartnerDataType[] channelPartners;

  public List<MaintenanceLineItemProperties> maintenanceLineItems;

//  public String FNPTimeZoneValue;

//  public Date createdOnDateTime;

  public Date lastModifiedDateTime;

  public List<Attribute> lineItemAttributes;
  public List<Attribute> customeItemAttributes;
  
  
  
  public transient Integer csid = null;
  
  public LineItemProperties() {
  	
  }
  
  public Date getLastModifiedDateTime() {
    return lastModifiedDateTime;
  }

  @Override
  public String getKey() {
    return this.activationId;
  }
  
	@Override
	public Type getStorageType() {
		return new TypeToken<Map<String, LineItemProperties>>(){}.getType();
	}

	@Override
	public String toString() {
		return "LineItemProperties " + this.activationId;
	}

  @Override
  public void onLoad() {
    this.csid = Utils.getCsid(this.soldTo);
  }
	
	public Boolean hasMaintenance() {

	  return this.maintenanceLineItems != null && this.maintenanceLineItems.size() > 0;
	}
	
  public Boolean hasVaidMaintenance() {
    final Calendar midnight = new GregorianCalendar();
    midnight.set(Calendar.HOUR_OF_DAY, 0);
    midnight.set(Calendar.MINUTE, 0);
    midnight.set(Calendar.SECOND, 0);
    midnight.set(Calendar.MILLISECOND, 0);

    final Date today = midnight.getTime();

    if (this.maintenanceLineItems != null) {
      for (final MaintenanceLineItemProperties line : this.maintenanceLineItems) {
        if (line.isPermanent || line.expirationDate.after(today)) {
          return true;
        }
      }
    }
    return false;
  }
  
  public Boolean hasUpgrade() {
    final Date today = Utils.getLastMidnight();

    if (this.isPermanent || (this.expirationDate != null && this.expirationDate.after(today))) {
      if ("UPGRADE".equals(this.lineItemSupportAction)) {
        return true;
      }
    }

    return false;
  }
  
  public Boolean hasLicese() {
    return true;
  }

  public Boolean hasValidLicese() {
    final Date today = Utils.getLastMidnight();

    if (this.isPermanent || (this.expirationDate != null && this.expirationDate.after(today))) {
      return true;
    }

    return false;
  }
}
