package com.flexnet.sbt.cockpit;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.flexnet.sbt.cockpit.entity.EntityManager;
import com.flexnet.sbt.cockpit.match.query.OrganizationQuery;
import com.flexnet.sbt.cockpit.services.FnoEntitlementService;
import com.flexnet.sbt.cockpit.services.FnoFulfilmentService;

public class LikeForLikeQuery extends QueryBase {

  private final Map<String, Integer> activationIds = new HashMap<>();
  
  public LikeForLikeQuery(final EntityManager manager) {
    super(manager);
  }

  private void processEntitlements(final FnoEntitlementService entitlementService) throws RemoteException, InterruptedException, ExecutionException {
    super.count.reset();

    entitlementService.getEntitlementsForCsids(super.organizationsMap.keySet()).forEach(ent -> {
      super.count.increment();
      if (super.attributes.containsKey(ent.csid)) {
        // update the AD with this Entitlement record
        super.attributes.get(ent.csid).update(ent);
        ent.lineItems.forEach(line -> {
          if (line.state.equals("DEPLOYED")) {
            // create the LD from this LineItem record
            super.licenses.put(line.activationId, LicenseData.get(ent, line));
            
            this.activationIds.put(line.activationId, ent.csid);
          }
        });
      }
    });
    log.info("retrieved " + super.count.getCount() + " entitlements in " + timer.getLapSeconds(true) + " seconds");
  }
  
  @SuppressWarnings("unused")
  private void processLineItemProperties(final FnoEntitlementService service) throws InterruptedException, ExecutionException {
    super.count.reset();
    
    service.getEntitlementLineItemsForCsids(super.organizationsMap.keySet()).forEach(line -> {
      super.count.increment();
      if (super.attributes.containsKey(line.csid)) {
        // update the AD with this Entitlement record
        super.attributes.get(line.csid).update(line);
        if (line.state.equals("DEPLOYED")) {
          super.licenses.put(line.activationId, LicenseData.get(line));
        }
        else  {
          log.warn("ignoring line item " + line.activationId + " state " + line.state);
        }
      }
      else {
        log.warn("CSID " + line.csid + " not in attributes " + line.activationId);
      }
      // map the activationId to the csid
      activationIds.put(line.activationId, line.csid);
    });
    log.info("retrieved " + super.count.getCount() + " lineitems in " + timer.getLapSeconds(true) + " seconds");
  }
  
  private void processFulfilments(final FnoFulfilmentService service) throws RemoteException, InterruptedException, ExecutionException {
    super.count.reset();
    
    service.getFulfilmentsForCsids(super.organizationsMap.keySet()).forEach(fid -> {
      super.count.increment();
      if (super.licenses.containsKey(fid.activationId)) {
        super.licenses.get(fid.activationId).update(fid);
        
        if (activationIds.containsKey(fid.activationId)) {
          final Integer key = activationIds.get(fid.activationId);
          if (attributes.containsKey(key)) {
            attributes.get(key).update(fid);
          }
        }
        else {
          log.warn("no csid mapped for fulfilment line item " + fid.activationId + " not in mapped...");
        }
      }
      else {
        log.warn("no license for fulfilment line item " + fid.activationId + " " + fid.state);
      }
    });
    log.info("retrieved " + super.count.getCount() + " fulfilments in " + timer.getLapSeconds(true) + " seconds");
  }
  
  @Override
  public void run(final OrganizationQuery orgsQuery) throws Exception { 

    super.timer.reset();
    super.clear();
    this.activationIds.clear();
    try {
    
       /* ORGANIZATIONS */
      super.processOrganizations(super.entityManager.getOrganizations()
          .filterValues(org -> orgsQuery.match(org)));

      final FnoEntitlementService entitlementService = new FnoEntitlementService();
      entitlementService.initializeService();
      
      /* ENTITLEMENTS */
      processEntitlements(entitlementService);
      
      /* LINEITEM PROPERTIES */
      //processLineItemProperties(entitlementService);
      
      final FnoFulfilmentService fulfilmentService = new FnoFulfilmentService();
      fulfilmentService.initializeService();

      /* FULFILMENTS */
      processFulfilments(fulfilmentService);
      
    }
    finally {
      log.info("query complete in " + timer.getElapsedSeconds() + " seconds");
    }
  }
}
