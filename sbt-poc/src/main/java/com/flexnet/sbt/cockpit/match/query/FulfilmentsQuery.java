package com.flexnet.sbt.cockpit.match.query;

import com.flexnet.sbt.cockpit.entity.Fulfilment;
import com.flexnet.sbt.cockpit.match.Matchable;

public class FulfilmentsQuery  extends Query<Fulfilment>{

  protected Matchable<String> activationIds;
  protected Matchable<String> state;
  @Override
  public boolean match(final Fulfilment fid) {
    return doMatch(this.activationIds, fid.activationId) &&
           doMatch(this.state, fid.state);
  }


}
