package com.flexnet.sbt.cockpit.match;

public class Match<T> implements Matchable<T> {

  public final T value;
  
  public Match(final T val) {
    this.value = val;
  }
  
  @Override
  public boolean matches(T val) {
   return this.value.equals(val);
  }

}
