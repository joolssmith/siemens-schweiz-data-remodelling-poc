package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;

public interface Entity {
  public String getKey();
  
  public Type getStorageType();
  
  public void onLoad();
   
}
