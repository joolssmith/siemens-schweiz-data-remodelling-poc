package com.flexnet.sbt.cockpit.entity;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flexnet.sbt.cockpit.util.Stopwatch;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class EntityMap<T extends Entity> {
  public static final Logger log = LoggerFactory.getLogger(EntityMap.class);

  private final ReentrantLock lock = new ReentrantLock();

  private final Path filePath;

  private Class<? extends Entity> type;

  private Map<String, T> entityMap = new HashMap<>();

  public EntityMap(final Class<T> type, final String root) {

    this.filePath = Paths.get(root, type.getCanonicalName() + ".json");

    this.type = type;
  }

  public void load(final Gson gson) throws IOException, JsonSyntaxException, InstantiationException, IllegalAccessException {
    final Stopwatch timer = new Stopwatch();
    try {
      this.lock.lock();

      if (Files.exists(this.filePath, LinkOption.NOFOLLOW_LINKS)) {
        final String filename = this.filePath.toString();

        try (final Reader reader = new FileReader(filename)) {        
          
          this.entityMap = gson.fromJson(reader, type.newInstance().getStorageType());

          log.debug("loaded " + this.entityMap.size() + 
              " " + this.type.getSimpleName() + 
              " entities from " + filename + 
              " in " + timer.getElapsedSeconds() + " seconds");
        }
        
        this.entityMap.forEach((k,v) -> {
          v.onLoad();
        });
      }
    }
    finally {
      this.lock.unlock();
    }
  }

  public void put(final String key, final T value) {
    try {
      this.lock.lock();

      entityMap.put(key, value);
    }
    finally {
      this.lock.unlock();
    }
  }

  public void save(final Gson gson) throws IOException {
    final Stopwatch timer = new Stopwatch();
    try {
      this.lock.lock();

      final String filename = this.filePath.toString();
      // overwrite do NOT append
      try (final Writer writer = new FileWriter(filename, false)) {

        gson.toJson(entityMap, writer);
        
        log.debug("saved " + this.entityMap.size() + 
            " " + this.type.getSimpleName() + 
            " entities to " + filename + 
            " in " + timer.getElapsedSeconds() + " seconds");
      }
    }
    finally {
      this.lock.unlock();
    }
  }

  public int getSize() {
    try {
      this.lock.lock();

      return this.entityMap.size();
    }
    finally {
      this.lock.unlock();
    }
    
  }

  public List<T> getValues() {

    try {
      this.lock.lock();

      return new ArrayList<T>(entityMap.values());
    }
    finally {
      this.lock.unlock();
    }
  }

  public Stream<T> filterValues(final Predicate<? super T> mesh) {

    try {
      this.lock.lock();

      return entityMap.values().stream().filter(mesh);
    }
    finally {
      this.lock.unlock();
    }
  }
  /**
   * 
   * @param items
   */
  public void updateValues(final List<T> items) {

    try {
      this.lock.lock();

      items.forEach(v -> {
        this.entityMap.put(v.getKey(), v);
      });

    }
    finally {
      this.lock.unlock();
    }
  }

  /**
   * 
   * @param items
   */
  public void replace(final List<T> items) {

    try {
      this.lock.lock();

      this.entityMap.clear();

      items.forEach(v -> {
        this.entityMap.put(v.getKey(), v);
      });

    }
    finally {
      this.lock.unlock();
    }
  }

  public Class<? extends Entity> getType() {
    return type;
  }
  
  
}
