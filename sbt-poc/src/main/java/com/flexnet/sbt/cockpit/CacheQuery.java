package com.flexnet.sbt.cockpit;

import java.util.HashMap;
import java.util.Map;

import com.flexnet.sbt.cockpit.entity.EntityManager;
import com.flexnet.sbt.cockpit.match.StringMatch;
import com.flexnet.sbt.cockpit.match.StringSetMatch;
import com.flexnet.sbt.cockpit.match.query.EntitlementsQuery;
import com.flexnet.sbt.cockpit.match.query.FulfilmentsQuery;
import com.flexnet.sbt.cockpit.match.query.LineItemPropertiesQuery;
import com.flexnet.sbt.cockpit.match.query.OrganizationQuery;

public class CacheQuery extends QueryBase {

  private final Map<String, Integer> activationIds = new HashMap<>();
  
  public CacheQuery(final EntityManager manager) {
    super(manager);
  }

  private void processEntitlements() {
    this.count.reset();
    final EntitlementsQuery entitlementsQuery = new EntitlementsQuery() {
      {
        this.organizations = new StringSetMatch(organizationsMap.keySet());
      }
    };
    entityManager.getEntitlements().filterValues(ent -> entitlementsQuery.match(ent)).forEach(ent -> {
      // is the CSID valid?
      if (this.attributes.containsKey(ent.csid)) {

        // update the Attribute with this Entitlement record
        attributes.get(ent.csid).update(ent);

        // then for each line item
        ent.lineItems.forEach(line -> {
          // create the LD from this LineItem record
          licenses.put(line.activationId, LicenseData.get(ent, line));

          // map the activationId to the csid
          activationIds.put(line.activationId, ent.csid);
        });
        this.count.increment();
      }
    });
    log.info("retrieved " + this.count.getCount() + " entitlements in " + super.timer.getLapSeconds(true) + " seconds");
  }
  
  private void processLineItemProperties() {
    this.count.reset();
    final LineItemPropertiesQuery lineItemPropertiesQuery = new LineItemPropertiesQuery() {
      {
        this.organizations = new StringSetMatch(organizationsMap.keySet());
      }
    };

    entityManager.getLineItemProperties().filterValues(line -> lineItemPropertiesQuery.match(line)).forEach(line -> {
      // update the LD with this LineItemProperties record
      this.licenses.get(line.activationId).update(line);

      // map the activationId to the csid
      activationIds.put(line.activationId, line.csid);
      
      this.count.increment();
    });
    log.info("retrieved " + this.count.getCount() + " line items in " + super.timer.getLapSeconds(true) + " seconds"); 
  }
  
  private void processFulfilments() {
    this.count.reset();
    final FulfilmentsQuery fidsQuery = new FulfilmentsQuery() {
      {
        this.activationIds = new StringSetMatch(licenses.keySet());
        this.state = new StringMatch("ACTIVE");
      }
    };

    entityManager.getFulfilments().filterValues(fid -> fidsQuery.match(fid)).forEach(fid -> {
      this.licenses.get(fid.activationId).update(fid);
      this.attributes.get(activationIds.get(fid.activationId)).update(fid);
      this.count.increment();
    });
    log.info("retrieved " + this.count.getCount() + " line items in " + super.timer.getLapSeconds(true) + " seconds");
  }
  
  @Override
  public void run(final OrganizationQuery orgsQuery) throws Exception {
    try {
      super.timer.reset();
      super.clear();

      super.processOrganizations(super.entityManager.getOrganizations()
          .filterValues(org -> orgsQuery.match(org)));

      processEntitlements();
      
      processLineItemProperties();
      
      processFulfilments();
      
    }
    finally {
      log.info("query complete in " + super.timer.getElapsedSeconds() + " seconds");
    }
  }
}
