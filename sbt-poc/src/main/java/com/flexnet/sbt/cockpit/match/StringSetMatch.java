package com.flexnet.sbt.cockpit.match;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class StringSetMatch implements Matchable<String> {

  protected final Set<String> values = new HashSet<>();

  public StringSetMatch(final String... args) {

    for (final String arg : args) {
      this.values.add(arg.toLowerCase());
    }
  }

  public StringSetMatch(final Collection<String> col) {
    col.forEach(arg -> {
      this.values.add(arg.toLowerCase());
    });

  }

  @Override
  public boolean matches(final String value) {
    if (value != null) {
      return values.contains(value.toLowerCase());
    }
    return false;
  }
}
