package com.flexnet.sbt.cockpit.services;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.xml.rpc.Call;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;

import com.flexnet.operations.webservices.v1.AttributeDescriptorType;
import com.flexnet.operations.webservices.v1.DateTimeQueryType;
import com.flexnet.operations.webservices.v1.DatedSearchType;
import com.flexnet.operations.webservices.v1.GetOrganizationCountRequestType;
import com.flexnet.operations.webservices.v1.GetOrganizationCountResponseType;
import com.flexnet.operations.webservices.v1.GetOrganizationsQueryRequestType;
import com.flexnet.operations.webservices.v1.GetOrganizationsQueryResponseType;
import com.flexnet.operations.webservices.v1.OrganizationIdentifierType;
import com.flexnet.operations.webservices.v1.OrganizationPKType;
import com.flexnet.operations.webservices.v1.OrganizationQueryParametersType;
import com.flexnet.operations.webservices.v1.StatusType;
import com.flexnet.operations.webservices.v1.UpdateOrgDataType;
import com.flexnet.operations.webservices.v1.UpdateOrganizationResponseType;
import com.flexnet.operations.webservices.v1.UserOrgHierarchyServiceInterfaceV1;
import com.flexnet.operations.webservices.v1.V1UserOrgHierarchyServiceLocator;
import com.flexnet.sbt.cockpit.entity.Organization;

public class FnoOrganizationService extends FnoService {

  private UserOrgHierarchyServiceInterfaceV1 service;

  /**
   * 
   * @param username
   * @param password
   * @throws ServiceException
   * @throws MalformedURLException
   */
  @Override
  public void initializeService() throws ServiceException, MalformedURLException {

    final V1UserOrgHierarchyServiceLocator locator = new V1UserOrgHierarchyServiceLocator();

    this.service = locator
        .getV1UserOrgHierarchyService(
            new URL(getHostname() + locator.getV1UserOrgHierarchyServiceWSDDServiceName()));

    ((Stub) this.service)._setProperty(Call.USERNAME_PROPERTY, getUsername());
    ((Stub) this.service)._setProperty(Call.PASSWORD_PROPERTY, getPassword());
  }

  /**
   * 
   * @return
   * @throws RemoteException
   */
  public Integer getOrganizationCount() throws RemoteException {

    @SuppressWarnings("serial")
    GetOrganizationCountResponseType response = service.getOrganizationCount(new GetOrganizationCountRequestType() {
      {
        this.setQueryParams(new OrganizationQueryParametersType() {
          {
          }
        });
      }
    });

    if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
      log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
      throw new RuntimeException(response.getStatusInfo().getReason());
    }

    return response.getResponseData().getCount().intValue();
  }

  /**
   * 
   * @return
   * @throws RemoteException
   */
  public List<com.flexnet.sbt.cockpit.entity.Organization> getOrganizations(final Calendar lastUpdateTime)
      throws RemoteException {

    log.trace("getOrganizationsQuery: date is " + (lastUpdateTime == null ? "null" : lastUpdateTime.toString()));
    @SuppressWarnings("serial")
    final GetOrganizationsQueryRequestType request = new GetOrganizationsQueryRequestType() {
      {
        this.setQueryParams(new OrganizationQueryParametersType() {
          {
            if (lastUpdateTime != null) {
              this.setLastModifiedDateTime(new DateTimeQueryType() {
                {
                  this.setSearchType(DatedSearchType.AFTER);
                  this.setValue(lastUpdateTime);
                }
              });
            }
          }
        });
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };

    List<com.flexnet.sbt.cockpit.entity.Organization> results = new ArrayList<>();

    for (int i = 1;; i++) {

      request.setPageNumber(BigInteger.valueOf(i));

      log.trace("getOrganizationsQuery: page - " + request.getPageNumber().toString() + " - size " + results.size());

      GetOrganizationsQueryResponseType response = null;

      for (int j = 1;; j++) {
        try {
          response = this.service.getOrganizationsQuery(request);
          break;
        }
        catch (final Exception e) {
          log.error(e.getMessage());
          if (j >= 3) {
            throw new RemoteException(e.getMessage());
          }
        }
      }

      if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
        log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
        throw new RuntimeException(response.getStatusInfo().getReason());
      }

      if (response.getResponseData() == null) {
        log.trace("getOrganizationsQuery: no data returned");
        break;
      }

      Arrays.asList(response.getResponseData())
        .stream()
        .filter(x -> x != null)
        .forEach(x -> results.add(EntityFactory.getOrganization(x)));

      
      if (response.getResponseData().length < request.getBatchSize().intValue()) {
        log.trace("getOrganizationsQuery: complete");
        break;
      }

      if (FnoService.getQuickQuit()) {
        break;
      }
    }

    return results;
  }
  
  /**
   * 
   * @param csid
   * @param name_2
   * @param createdBy
   * @param createdOn
   * @param modifiedBy
   * @param modifiedOn
   * @throws RemoteException
   */
  @SuppressWarnings("serial")
  public void upateOrganization(
      String csid, String name_2, String createdBy, String createdOn, String modifiedBy, String modifiedOn) throws RemoteException {
    
    final UpdateOrganizationResponseType response = this.service.updateOrganization(new UpdateOrgDataType[] {
        new UpdateOrgDataType() {
          {
            this.setOrganization(new OrganizationIdentifierType() {
              {
                this.setPrimaryKeys(new OrganizationPKType() {
                  {
                    this.setName("org_" + csid);
                  }
                });
              }
            });
            this.setCustomAttributes(new AttributeDescriptorType[] {
                new AttributeDescriptorType() {
                  {
                    this.setAttributeName(Organization.Site_Name_2);
                    this.setStringValue(name_2);
                  }
                },
                new AttributeDescriptorType() {
                  {
                    this.setAttributeName(Organization.Site_Created_At);
                    this.setStringValue(createdOn);
                  }
                },
                new AttributeDescriptorType() {
                  {
                    this.setAttributeName(Organization.Site_Created_By);
                    this.setStringValue(createdBy);
                  }
                },
                new AttributeDescriptorType() {
                  {
                    this.setAttributeName(Organization.Site_Modified_At);
                    this.setStringValue(modifiedOn);
                  }
                },
                new AttributeDescriptorType() {
                  {
                    this.setAttributeName(Organization.Site_Modified_By);
                    this.setStringValue(modifiedBy);
                  }
                }
            });
          }
        }
    });
    
    if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
      log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
      throw new RuntimeException(response.getStatusInfo().getReason());
    }
    
  }
}
