package com.flexnet.sbt.cockpit.match;

public class WildcardStringMatch implements Matchable<String> {

  public enum SearchType {
    EQUALS, STARTS_WITH, ENDS_WITH, CONTAINS
  }

  public final SearchType type;
  public final String value;

  private WildcardStringMatch(final String val, final SearchType typ) {
    this.value = val;
    this.type = typ;
  }

  @Override
  public boolean matches(final String value) {
    switch (this.type) {
    case CONTAINS:
      return value.toLowerCase().contains(this.value);
    case STARTS_WITH:
      return value.toLowerCase().startsWith(this.value);
    case ENDS_WITH:
      return value.toLowerCase().endsWith(this.value);
    default:
      return value.equalsIgnoreCase(this.value);
    }
  }

  public static WildcardStringMatch get(final String value) {

    final String tmp = value.trim();

    final int i0 = 0;
    int i = i0;
    while (i < tmp.length() && tmp.charAt(i) == '*') {
      i++;
    }

    final int j0 = tmp.length() - 1;
    int j = j0;
    while (j >= 0 && tmp.charAt(j) == '*') {
      j--;
    }

    if (i <= j) {
      // substring end index is exclusive!
      final String str = tmp.substring(i, j + 1).toLowerCase();

      if (i == i0 && j == j0) {
        // no wildcards
        return new WildcardStringMatch(str, SearchType.EQUALS);
      }

      if (i == i0) {
        return new WildcardStringMatch(str, SearchType.STARTS_WITH);
      }

      if (j == j0) {
        return new WildcardStringMatch(str, SearchType.ENDS_WITH);
      }

      return new WildcardStringMatch(str, SearchType.CONTAINS);

    }
    else {
      // effectively no search string
      return null;
    }
  }
}
