package com.flexnet.sbt.cockpit.match;

public class RangeMatch<T extends Comparable<T>> implements Matchable<T> {

  public final T upperBound;
  public final T lowerBound;
  
  public RangeMatch(final T lb, final T ub) {
    this.lowerBound = lb.compareTo(ub) < 0 ? lb : ub;
    this.upperBound = lb.compareTo(ub) < 0 ? ub : lb;
  }
  
  @Override
  public boolean matches(final T value) {
    // match is inclusive of endpoints
    return value != null && this.lowerBound.compareTo(value) <= 0 && this.upperBound.compareTo(value) >= 0;
  }

}
