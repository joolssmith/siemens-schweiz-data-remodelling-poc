package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.flexnet.sbt.cockpit.entity.beans.Address;
import com.flexnet.sbt.cockpit.entity.beans.Attribute;
import com.flexnet.sbt.cockpit.util.Utils;
import com.google.gson.reflect.TypeToken;

public class Organization implements Entity {

  /**
   * Attribute names
   */
  public final static String Site_Regional_Settings = "Site_Regional_Settings";
  public final static String Site_Operating_System = "Site_Operating_System";
  public final static String Site_DesigoCC_Version = "Site_DesigoCC_Version";
  public final static String Site_Hardware_Id = "Site_Hardware_Id";
  public final static String Site_Location = "Site_Location";
  public final static String Site_VM_System_Type = "Site_VM_System_Type";
  public final static String Site_Country = "Site_Country";
  public final static String Site_Market_Segment = "Site_Market_Segment";
  public final static String Site_Name = "Site_Name";
  public final static String Site_Local_Site_Id = "Site_Local_Site_Id";
  public final static String Site_Branch = "Site_Branch";
  public final static String Site_Sales_Channel = "Site_Sales_Channel";
  public final static String Site_Country_Origin = "Site_Country_Origin";
  public final static String Site_VAP_Number = "Site_VAP_Number";
  public final static String Site_Created_By = "Site_Created_By";
  public final static String Site_Modified_By = "Site_Modified_By";
  public final static String Site_Created_At = "Site_Created_At";
  public final static String Site_Modified_At = "Site_Modified_At";
  public final static String Site_Name_2 = "Site_Name_2";
  public final static String Site_Ifa_Number = "Site_Ifa_Number";  
  public final static String Site_Additional_Info = "Site_Additional_Info";

  public String name;
  public String displayName;
  public String type;
  public Date lastModifiedDate;
  public String country;
  public List<Attribute> customAttributes;
  public Address address;

  /**
   * do not serialize this will be populated on load
   */
  public transient Map<String,String> customAttributeMap = null;
  public transient Integer csid = null;
  
  public String getAttribute(final String attributeName) {
    if (null != this.customAttributeMap && this.customAttributeMap.containsKey(attributeName)) {
      return customAttributeMap.get(attributeName);
    }
    return null;
  }
  
  public Organization() {

  }

  public void onLoad() {
    
    if (null != this.customAttributes) {
      this.customAttributeMap = new HashMap<>();
      this.customAttributes.forEach(att -> {
        this.customAttributeMap.put(att.name, att.stringValue);
      });
    }
    
    this.csid = Utils.getCsid(this.name);
  }
  
  public Integer getCsid() {
    return this.csid;
  }
  
  @Override
  public String getKey() {
    return this.name;
  }

  @Override
  public Type getStorageType() {
    return new TypeToken<Map<String, Organization>>() {
    }.getType();
  }

  @Override
  public String toString() {
    return "Organization " + this.name;
  }
}
