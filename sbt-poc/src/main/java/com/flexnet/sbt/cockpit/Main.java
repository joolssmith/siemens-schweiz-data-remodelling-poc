package com.flexnet.sbt.cockpit;

import java.io.FileWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.axis.AxisProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flexnet.sbt.cockpit.entity.EntityManager;
import com.flexnet.sbt.cockpit.match.StringMatch;
import com.flexnet.sbt.cockpit.match.query.OrganizationQuery;
import com.flexnet.sbt.cockpit.services.FnoService;

public class Main {
  private static final Logger log = LoggerFactory.getLogger(Main.class);

  private static Boolean testProperty(final String key) {
    return Boolean.valueOf(System.getProperty(key, "false"));
  }
  
  public static void main(String[] args) {

    System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

    // massive kludge - took 3hrs to find!!
    AxisProperties.setProperty("axis.socketSecureFactory", "org.apache.axis.components.net.SunFakeTrustSocketFactory");

    try {
      log.info("starting...");

      FnoService.setHostname("https://localhost:11443/flexnet/services/");
      
      FnoService.initialize();
      FnoService.setUsername("Julian_Smith");
      FnoService.setPassword("Smellydog@123");
      FnoService.setBatchSize(256);
      FnoService.setQuickQuit(false);

      //final EntityManager entityManager = new EntityManager("c:\\data");
      //final EntityManager entityManager = new EntityManager("\\\\DV00NQ9C.ad001.siemens.net\\c$\\data");
      final EntityManager entityManager = new EntityManager();
      
      entityManager.initialize("d:\\jools\\data");
      
      if (testProperty("testLikeForLike")) {
        
        entityManager.setLikeForLike(true);
        
        entityManager.load();
        
        final QueryBase query = new LikeForLikeQuery(entityManager);
        
        query.run(new OrganizationQuery() {
          {
            // this.csids = new RangeMatch<Integer>(1, 2999);
            // this.csids = new Match<Integer>(2590);
            // this.csids = new RangeMatch<Integer>(10000, 20000);
            // this.branch = new StringSetMatch("Ost", "Bayern");
             this.branch = new StringMatch("Bayern");
//             this.country = new StringMatch("DE");
            // this.localSiteId = WildcardStringMatch.get("416*");
            // this.createdAt = new RangeMatch<String>("2016-01-01 00:00:00.000",
            // "2016-12-31 23:59:59.999");
          }
        });
        
        log.debug("exportig results to licenses.json");
        try (final FileWriter writer = new FileWriter("licenses.json", false)) {
          
          for(final LicenseData license : query.getLicenses().values()) {
            writer.write(license.toString());
          }
          writer.flush();
        }
      }
      
      if (testProperty("testCaching")) {
              
        entityManager.load();
        
        final Calendar threshold = GregorianCalendar.getInstance();
        threshold.add(Calendar.HOUR, -24);
        
        entityManager.refresh(threshold);
        
        final QueryBase query = new CacheQuery(entityManager);
        
        query.run(new OrganizationQuery() {
          {
            // this.csids = new RangeMatch<Integer>(1, 2999);
            // this.csids = new Match<Integer>(2590);
            // this.csids = new RangeMatch<Integer>(10000, 20000);
            // this.branch = new StringSetMatch("Ost", "Bayern");
            // this.branch = new StringMatch("Bayern");
             this.country = new StringMatch("DE");
            // this.localSiteId = WildcardStringMatch.get("416*");
            // this.createdAt = new RangeMatch<String>("2016-01-01 00:00:00.000",
            // "2016-12-31 23:59:59.999");
          }
        });
        
        log.debug("exporting results to licenses.json");
        try (final FileWriter writer = new FileWriter("licenses.json", false)) {
          
          for(final LicenseData license : query.getLicenses().values()) {
            writer.write(license.toString());
          }

          writer.flush();
        }
      }
      
      FnoService.terminate();
      
      entityManager.terminate();
    }
    catch (final Exception e) {
      e.printStackTrace();
    }
    finally {
      log.debug("quitting...");
    }
  }
}
