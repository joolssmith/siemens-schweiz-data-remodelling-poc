package com.flexnet.sbt.cockpit.match.query;

import java.util.Date;
import java.util.Map;

import com.flexnet.sbt.cockpit.entity.Entity;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.flexnet.sbt.cockpit.match.Matchable;

public abstract class Query<T extends Entity> {

  protected boolean doMatch(final Matchable<String> matchable, final String value) {
    return matchable == null || matchable.matches(value);
  }
  
  protected boolean doMatch(final Matchable<Integer> matchable, final Integer value) {
    return matchable == null || matchable.matches(value);
  }
  
  protected boolean doMatch(final Matchable<Date> matchable, final Date value) {
    return matchable == null || matchable.matches(value);
  }
  
  protected boolean doMatch(final Matchable<String> matchable, final UniqueIdValue value) {
    if (matchable == null)
        return true;
    
    if (value != null)
      return matchable.matches(value.value);
    
    return false;
  }
  
  // TODO: review this logic
  protected boolean doMatch(final Matchable<String> matchable, final Map<String,String> attributes, final String attributeName) {

    if (matchable == null)
      return true;
    
    else if (attributes != null && attributes.containsKey(attributeName)) {
      return matchable.matches(attributes.get(attributeName));
    }
    
    return false;
//    return matchable == null || (attributes != null && attributes.containsKey(attributeName) && matchable.matches(attributes.get(attributeName)));
  }
  
  public abstract boolean match(final T org);
  
 }
