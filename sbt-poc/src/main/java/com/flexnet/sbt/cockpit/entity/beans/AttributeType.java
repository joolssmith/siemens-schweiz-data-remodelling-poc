package com.flexnet.sbt.cockpit.entity.beans;

public enum AttributeType {
  VOID, STRING, INTEGER, BOOLEAN, DATE, ARRAY

}
