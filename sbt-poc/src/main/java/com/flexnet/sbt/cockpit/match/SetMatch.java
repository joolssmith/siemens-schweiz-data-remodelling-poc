package com.flexnet.sbt.cockpit.match;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SetMatch<T> implements Matchable<T> {

  protected final Set<T> values = new HashSet<>();
   
  public SetMatch(final Collection<T> col) {
    this.values.addAll(col);
  }
  
  @Override
  public boolean matches(final T value) {
    if (value != null) {
      return values.contains(value);
    }
    return false;
  }
}