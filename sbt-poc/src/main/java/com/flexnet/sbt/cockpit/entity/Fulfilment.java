package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.flexnet.sbt.cockpit.entity.beans.Attribute;
import com.flexnet.sbt.cockpit.entity.beans.Product;
import com.google.gson.reflect.TypeToken;

public class Fulfilment implements Entity {

  public String entitlementId;
  public String activationId;
  public String fulfilmentId;
  public String parentFulfilmentId;
  public Product product;
  public String source;
  public String fulfilmentType;
  public String supportAction;
  public String activationType;
  public Date startDate;
  public Date expirationDate;
  public Date fulfilmentDate;

  public Integer fulfiledCount;
  
  public String state;
  
  public List<Product> entitledProducts;
  
  public Date fulfilmetDateTime;
  public Date lastModifiedDateTime;
  
  public List<Attribute> licenseModelAttributes;
  
  public Fulfilment() {
    
  }
  
  public boolean hasHistory() {
    return !StringUtils.isEmpty(this.supportAction);
  }
  
  @Override
  public String getKey() {
    return this.fulfilmentId;
  }
  
  @Override
  public Type getStorageType() {
    return new TypeToken<Map<String, Fulfilment>>(){}.getType();
  }

  @Override
  public String toString() {
    return "Fulfilment " + this.entitlementId;
  }

  @Override
  public void onLoad() {
    
  }
}
