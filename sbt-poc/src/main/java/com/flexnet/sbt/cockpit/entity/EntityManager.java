package com.flexnet.sbt.cockpit.entity;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;

import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flexnet.sbt.cockpit.services.FnoEntitlementService;
import com.flexnet.sbt.cockpit.services.FnoFulfilmentService;
import com.flexnet.sbt.cockpit.services.FnoOrganizationService;
import com.flexnet.sbt.cockpit.util.Stopwatch;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class EntityManager {
  private static final Logger log = LoggerFactory.getLogger(EntityManager.class);
  
  private final ReentrantLock lock = new ReentrantLock();

  private final Map<Class<? extends Entity>, EntityMap<? extends Entity>> entities = new HashMap<>();

  private ExecutorService executorService;

  private final Gson gson;

  private boolean likeForLike = false;
  /**
   * 
   */
  public EntityManager() {

    this.gson = new GsonBuilder()
      .setPrettyPrinting()
      .enableComplexMapKeySerialization()
      .create();
  }

  public void initialize(final String root) {

    this.executorService = Executors.newFixedThreadPool(4);

    final EntityMap<Entitlement> map_a = new EntityMap<Entitlement>(Entitlement.class, root);
    this.entities.put(Entitlement.class, map_a);

    final EntityMap<LineItemProperties> map_b = new EntityMap<LineItemProperties>(LineItemProperties.class, root);
    this.entities.put(LineItemProperties.class, map_b);

    final EntityMap<Fulfilment> map_c = new EntityMap<Fulfilment>(Fulfilment.class, root);
    this.entities.put(Fulfilment.class, map_c);

    final EntityMap<Organization> map_d = new EntityMap<Organization>(Organization.class, root);
    this.entities.put(Organization.class, map_d);
  }

  public void terminate() {
    this.executorService.shutdown();
  }
  /**
   * 
   * @throws IOException
   * @throws JsonSyntaxException
   * @throws InstantiationException
   * @throws IllegalAccessException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  public void load() throws IOException, JsonSyntaxException, InstantiationException, IllegalAccessException,
      InterruptedException, ExecutionException {
    final Stopwatch timer = new Stopwatch();
    try {
      lock.lock();
      final List<Future<Boolean>> futures = new ArrayList<>();

        for (final EntityMap<?> entityMap : this.entities.values()) {
          
          // just load orgs for testing...
          if (this.likeForLike) {
            if (!entityMap.getType().equals(Organization.class))
              continue;
          }
          
          futures.add(executorService.submit(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
              entityMap.load(EntityManager.this.gson);
              return true;
            }
          }));
        }

      for (final Future<Boolean> future : futures) {
        future.get();
      }
    }
    finally {
      lock.unlock();
      log.info("load complete in " + timer.getElapsedSeconds() + " seconds");
    }
  }

  /**
   * 
   * @throws IOException
   * @throws InterruptedException
   * @throws ExecutionException
   */
  public void save() throws IOException, InterruptedException, ExecutionException {
    final Stopwatch timer = new Stopwatch();
    try {
      lock.lock();
      final List<Future<Boolean>> futures = new ArrayList<>();

      for (final EntityMap<?> entityMap : this.entities.values()) {
        futures.add(executorService.submit(new Callable<Boolean>() {
          @Override
          public Boolean call() throws Exception {
            entityMap.save(EntityManager.this.gson);
            return true;
          }
        }));
      }

      for (final Future<Boolean> future : futures) {
        future.get();
      }
    }
    finally {
      lock.unlock();
      log.info("save complete in " + timer.getElapsedSeconds() + " seconds");
    }
  }

  /**
   * 
   * @return
   */
  @SuppressWarnings("unchecked")
  public EntityMap<LineItemProperties> getLineItemProperties() {
    try {
      lock.lock();
      return (EntityMap<LineItemProperties>) this.entities.get(LineItemProperties.class);
    }
    finally {
      lock.unlock();
    }
  }

  /**
   * 
   * @return
   */
  @SuppressWarnings("unchecked")
  public EntityMap<Entitlement> getEntitlements() {
    try {
      lock.lock();
      return (EntityMap<Entitlement>) this.entities.get(Entitlement.class);
    }
    finally {
      lock.unlock();
    }
  }

  /**
   * 
   * @return
   */
  @SuppressWarnings("unchecked")
  public EntityMap<Fulfilment> getFulfilments() {
    try {
      lock.lock();
      return (EntityMap<Fulfilment>) this.entities.get(Fulfilment.class);
    }
    finally {
      lock.unlock();
    }
  }

  /**
   * 
   * @return
   */
  @SuppressWarnings("unchecked")
  public EntityMap<Organization> getOrganizations() {
    try {
      lock.lock();
      return (EntityMap<Organization>) this.entities.get(Organization.class);
    }
    finally {
      lock.unlock();
    }
  }

  /**
   * 
   * @param update
   * @return
   * @throws RemoteException
   * @throws ServiceException
   * @throws MalformedURLException
   */
  private List<LineItemProperties> queryLineItemProperties(final Calendar threshold)
      throws RemoteException, ServiceException, MalformedURLException {

    final FnoEntitlementService service = new FnoEntitlementService();

    service.initializeService();

    if (threshold != null) {
      return service.getEntitlementLineItems(threshold);
    }
    else {
      return service.getEntitlementLineItems(null);
    }
  }

  /**
   * 
   * @param update
   * @return
   * @throws RemoteException
   * @throws ServiceException
   * @throws MalformedURLException
   */
  private List<Entitlement> queryEntitlements(final Calendar threshold)
      throws RemoteException, ServiceException, MalformedURLException {

    final FnoEntitlementService service = new FnoEntitlementService();

    service.initializeService();

    if (threshold != null) {
      return service.getEntitlements(threshold);
    }
    else {
      return service.getEntitlements(null);
    }
  }

  /**
   * 
   * @param update
   * @return
   * @throws MalformedURLException
   * @throws ServiceException
   * @throws RemoteException
   */
  private List<Fulfilment> queryFulfilments(final Calendar threshold)
      throws MalformedURLException, ServiceException, RemoteException {

    final FnoFulfilmentService service = new FnoFulfilmentService();

    service.initializeService();
    if (threshold != null) {
      return service.getFulfilments(threshold);
    }
    else {
      return service.getFulfilments(null);
    }

  }

  /**
   * 
   * @param update
   * @return
   * @throws MalformedURLException
   * @throws ServiceException
   * @throws RemoteException
   */
  private List<Organization> queryOrganizations(final Calendar threshold)
      throws MalformedURLException, ServiceException, RemoteException {

    final FnoOrganizationService service = new FnoOrganizationService();

    service.initializeService();
    if (threshold != null) {
      return service.getOrganizations(threshold);
    }
    else {
      return service.getOrganizations(null);
    }
  }

  /**
   * @throws ExecutionException 
   * @throws InterruptedException 
   * 
   */
  public void renew() throws InterruptedException, ExecutionException {
    final Stopwatch timer = new Stopwatch();
    try {
      lock.lock();

      final Future<List<LineItemProperties>> lineItemPropertiesFuture = executorService
          .submit(new Callable<List<LineItemProperties>>() {
            @Override
            public List<LineItemProperties> call() throws Exception {
              return queryLineItemProperties(null);
            }
          });

      final Future<List<Entitlement>> entitlementsFuture = executorService.submit(new Callable<List<Entitlement>>() {
        @Override
        public List<Entitlement> call() throws Exception {
          return queryEntitlements(null);
        }
      });

      final Future<List<Fulfilment>> fulfilmentsFuture = executorService.submit(new Callable<List<Fulfilment>>() {
        @Override
        public List<Fulfilment> call() throws Exception {
          return queryFulfilments(null);
        }
      });

      final Future<List<Organization>> organizationsFuture = executorService.submit(new Callable<List<Organization>>() {
        @Override
        public List<Organization> call() throws Exception {
          return queryOrganizations(null);
        }
      });

      final List<Organization> organizations = organizationsFuture.get();
      getOrganizations().replace(organizations);
      
      final List<Entitlement> entitlements = entitlementsFuture.get();
      getEntitlements().replace(entitlements);
      
      final List<LineItemProperties> lines = lineItemPropertiesFuture.get();
      getLineItemProperties().replace(lines);
      
      final List<Fulfilment> fulfilments = fulfilmentsFuture.get();
      getFulfilments().replace(fulfilments);
    }
    finally {
      lock.unlock();
      log.info("renew complete in " + timer.getElapsedSeconds() + " seconds");
    }
  }

  /**
   * @throws ExecutionException 
   * @throws InterruptedException 
   * 
   */
  public void refresh(final Calendar threshold) throws InterruptedException, ExecutionException {
    final Stopwatch timer = new Stopwatch();
    try {
      lock.lock();

      final Future<List<LineItemProperties>> lineItemPropertiesFuture = executorService
          .submit(new Callable<List<LineItemProperties>>() {
            @Override
            public List<LineItemProperties> call() throws Exception {
              return queryLineItemProperties(threshold);
            }
          });

      final Future<List<Entitlement>> entitlementsFuture = executorService.submit(new Callable<List<Entitlement>>() {
        @Override
        public List<Entitlement> call() throws Exception {
          return queryEntitlements(threshold);
        }
      });

      final Future<List<Fulfilment>> fulfilmentsFuture = executorService.submit(new Callable<List<Fulfilment>>() {
        @Override
        public List<Fulfilment> call() throws Exception {
          return queryFulfilments(threshold);
        }
      });

      final Future<List<Organization>> organizationsFuture = executorService.submit(new Callable<List<Organization>>() {
        @Override
        public List<Organization> call() throws Exception {
          return queryOrganizations(threshold);
        }
      });

      final List<Organization> organizations = organizationsFuture.get();
      getOrganizations().updateValues(organizations);
      
      final List<Entitlement> entitlements = entitlementsFuture.get();
      getEntitlements().updateValues(entitlements);
      
      final List<LineItemProperties> lines = lineItemPropertiesFuture.get();
      getLineItemProperties().updateValues(lines);

      final List<Fulfilment> fulfilments = fulfilmentsFuture.get();
      getFulfilments().updateValues(fulfilments);

    }
    finally {
      lock.unlock();
      log.info("refresh complete in " + timer.getElapsedSeconds() + " seconds");
    }
  }

  public boolean isLikeForLike() {
    return likeForLike;
  }

  public void setLikeForLike(final boolean likeForLike) {
    this.likeForLike = likeForLike;
  }
  
}
