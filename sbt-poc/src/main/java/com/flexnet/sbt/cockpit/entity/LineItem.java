package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.flexnet.sbt.cockpit.entity.beans.Attribute;
import com.flexnet.sbt.cockpit.entity.beans.Duration;
import com.flexnet.sbt.cockpit.entity.beans.Product;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.google.gson.reflect.TypeToken;


public class LineItem implements Entity {

  public String activationId;

  public String description;

  public Product product;

  public UniqueIdValue partNumber;

  public UniqueIdValue licenseModelUniqueId;

  public UniqueIdValue alternativeLicenseModelUniqueId1;

  public UniqueIdValue alternativeLicenseModelUniqueId2;

  public List<Attribute> licenseModelAttributes;

//  public String FNPTimeZoneValue;

//   public com.flexnet.operations.webservices.v1.PolicyAttributesListType policyAttributes;

  public String orderId;

  public String orderLineNumber;

  public Integer numberOfCopies;

  public Date startDate;

//  public com.flexnet.operations.webservices.v1.StartDateOptionType startDateOption;

  public Boolean isPermanent;

  public Duration term;

  public Date expirationDate;

//  public Date versionDate;
//
//  public com.flexnet.operations.webservices.v1.VersionDateAttributesType versionDateAttributes;

  public String lineItemType;

  public List<Product> entitledProducts;

  public List<Attribute> lineItemAttributes;

  public Integer numberOfRemainingCopies;

  public Integer availableExtraActivations;

  public Boolean isTrustedType;

  public String state;

  public UniqueIdValue licenseTechnology;

  public UniqueIdValue parentLineItem;

  public Date createdOnDateTime;

  public Date lastModifiedDateTime;

  public Integer overdraftMax;

  public Integer remainingOverdraftCount;

  public LineItem() {
    
  }
  
  @Override
  public String getKey() {
    return this.activationId;
  }
  
	@Override
	public Type getStorageType() {
		return new TypeToken<Map<String, LineItem>>(){}.getType();
	}

  @Override
  public void onLoad() {
    
  }
 
}
