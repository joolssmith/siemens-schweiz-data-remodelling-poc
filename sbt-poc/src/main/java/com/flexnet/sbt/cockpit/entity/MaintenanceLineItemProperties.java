package com.flexnet.sbt.cockpit.entity;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;

import com.flexnet.sbt.cockpit.entity.beans.Product;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.google.gson.reflect.TypeToken;

public class MaintenanceLineItemProperties implements Entity{
  public String activationId;

  public Product maintenanceProduct;

  public UniqueIdValue partNumber;

  public String orderId;

  public String orderLineNumber;

  public Date startDate;

  public Date expirationDate;

  public Boolean isPermanent;

  public String state;
  

  @Override
  public String getKey() {
    return this.activationId;
  }
  
	@Override
	public Type getStorageType() {
		return new TypeToken<Map<String, MaintenanceLineItemProperties>>(){}.getType();
	}

  @Override
  public void onLoad() {
  }
}
