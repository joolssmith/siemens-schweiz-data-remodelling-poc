package com.flexnet.sbt.cockpit.match;

public class StringMatch implements Matchable<String> {

  protected String match;
  
  public StringMatch(final String value) {
    this.match = value;
  }
  
  @Override
  public boolean matches(final String value) {
    return value != null && value.compareToIgnoreCase(this.match) == 0;
  }


}
