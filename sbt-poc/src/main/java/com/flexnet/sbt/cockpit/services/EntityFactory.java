package com.flexnet.sbt.cockpit.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.flexnet.sbt.cockpit.entity.Entitlement;
import com.flexnet.sbt.cockpit.entity.Fulfilment;
import com.flexnet.sbt.cockpit.entity.LineItem;
import com.flexnet.sbt.cockpit.entity.LineItemProperties;
import com.flexnet.sbt.cockpit.entity.MaintenanceLineItem;
import com.flexnet.sbt.cockpit.entity.MaintenanceLineItemProperties;
import com.flexnet.sbt.cockpit.entity.Organization;
import com.flexnet.sbt.cockpit.entity.beans.Address;
import com.flexnet.sbt.cockpit.entity.beans.Attribute;
import com.flexnet.sbt.cockpit.entity.beans.AttributeType;
import com.flexnet.sbt.cockpit.entity.beans.Duration;
import com.flexnet.sbt.cockpit.entity.beans.Product;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.flexnet.sbt.cockpit.util.Utils;

public class EntityFactory {

  /**
   * 
   * @param data
   * @return
   */
  public static Entitlement getEntitlement(final com.flexnet.operations.webservices.v1.SimpleEntitlementDataType data) {
    // do not use anonymous class constructor - this messes up Gson
    final Entitlement value = new Entitlement();
    value.entitlementId = data.getEntitlementId().getId();
    value.description = data.getDescription();
    value.soldTo = data.getSoldTo();
    value.shipToAddress = data.getShipToAddress();
    value.shipToEmail = data.getShipToEmail();
    value.state = Utils.toStringOrNull(data.getState());
    value.createdUserId = data.getCreatedUserId();
    value.allowPortalLogin = data.getAllowPortalLogin();

    value.lineItems = EntityFactory.getLineItemList(data.getLineItems());

    value.maintenanceLineItems = EntityFactory.getMaintenanceLineItemList(data.getMaintenanceLineItems());

    value.entitlementAttributes = EntityFactory.getAttributeList(data.getEntitlementAttributes());

    value.onLoad();
    
    return value;
  }

  /**
   * 
   * @param lines
   * @return
   */
  public static List<LineItem> getLineItemList(
      final com.flexnet.operations.webservices.v1.EntitlementLineItemDataType[] lines) {
    List<LineItem> list = null;
    if (null != lines) {
      list = new ArrayList<>();
      for (final com.flexnet.operations.webservices.v1.EntitlementLineItemDataType line : lines) {
        list.add(EntityFactory.getLineItem(line));
      }
    }
    return list;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static LineItem getLineItem(final com.flexnet.operations.webservices.v1.EntitlementLineItemDataType data) {
    // do not use anonymous class constructore - this messes up Gson
    final LineItem value = new LineItem();

    value.activationId = data.getActivationId().getId();
    value.description = data.getDescription();
    value.product = EntityFactory.getProduct(data.getProduct());
    value.partNumber = EntityFactory.getUniqueIdValue(data.getPartNumber());
    value.licenseModelUniqueId = EntityFactory.getUniqueIdValue(data.getLicenseModel());
    value.alternativeLicenseModelUniqueId1 = EntityFactory.getUniqueIdValue(data.getAlternateLicenseModel1());
    value.alternativeLicenseModelUniqueId2 = EntityFactory.getUniqueIdValue(data.getAlternateLicenseModel1());
    value.entitledProducts = EntityFactory.getProductsList(data.getEntitledProducts());
    value.licenseModelAttributes = EntityFactory.getAttributeList(data.getLicenseModelAttributes());
    value.orderId = data.getOrderId();
    value.orderLineNumber = data.getOrderLineNumber();
    value.numberOfCopies = data.getNumberOfCopies().intValue();
    value.startDate = data.getStartDate();
    value.isPermanent = data.getIsPermanent();

    if (data.getTerm() != null) {
      value.term = EntityFactory.getDuration(data.getTerm());
    }

    value.expirationDate = data.getExpirationDate();
    value.numberOfRemainingCopies = data.getNumberOfRemainingCopies().intValue();
    value.availableExtraActivations = data.getAvailableExtraActivations().intValue();
    value.isTrustedType = data.getIsTrustedType();
    value.createdOnDateTime = Utils.toDateOrNull(data.getCreatedOnDateTime());
    value.lastModifiedDateTime = Utils.toDateOrNull(data.getLastModifiedDateTime());
    value.overdraftMax = data.getOverdraftMax().intValue();
    value.remainingOverdraftCount = data.getRemainingOverdraftCount().intValue();
    value.state = Utils.toStringOrNull(data.getState());
    value.licenseTechnology = EntityFactory.getUniqueIdValue(data.getLicenseTechnology());
    value.parentLineItem = EntityFactory.getUniqueIdValue(data.getParentLineItem());
    value.lineItemType = Utils.toStringOrNull(data.getLineItemType());
    value.licenseModelAttributes = EntityFactory.getAttributeList(data.getLineItemAttributes());

    value.onLoad();
    
    return value;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static MaintenanceLineItem getMaintenanceLineItem(
      final com.flexnet.operations.webservices.v1.MaintenanceLineItemDataType data) {
    // do not use anonymous class constructore - this messes up Gson
    final MaintenanceLineItem value = new MaintenanceLineItem();

    value.activationId = data.getActivationId().getId();
    value.maintenanceProduct = EntityFactory.getProduct(data.getMaintenanceProduct());
    value.partNumber = EntityFactory.getUniqueIdValue(data.getPartNumber());
    value.orderId = data.getOrderId();
    value.orderLineNumber = data.getOrderLineNumber();
    value.startDate = data.getStartDate();
    value.expirationDate = data.getExpirationDate();
    value.isPermanent = data.getIsPermanent();
    value.parentLineItem = EntityFactory.getUniqueIdValue(data.getParentLineItem());

    if (data.getAssociatedLineItems() != null) {
      value.associatedLineItems = new ArrayList<>();

      for (final com.flexnet.operations.webservices.v1.EntitlementLineItemIdentifierType ent : data
          .getAssociatedLineItems()) {
        value.associatedLineItems.add(EntityFactory.getUniqueIdValue(ent));
      }
    }

    value.onLoad();
    
    return value;
  }

  /**
   * 
   * @param lines
   * @return
   */
  public static List<MaintenanceLineItem> getMaintenanceLineItemList(
      final com.flexnet.operations.webservices.v1.MaintenanceLineItemDataType[] lines) {
    List<MaintenanceLineItem> list = null;
    if (null != lines) {
      list = new ArrayList<>();
      for (final com.flexnet.operations.webservices.v1.MaintenanceLineItemDataType line : lines) {
        list.add(getMaintenanceLineItem(line));
      }
    }
    return list;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static MaintenanceLineItemProperties getMaintenanceLineItemProperties(
      final com.flexnet.operations.webservices.v1.MaintenanceLineItemPropertiesType data) {
    // do not use anonymous class constructore - this messes up Gson
    final MaintenanceLineItemProperties value = new MaintenanceLineItemProperties();

    value.activationId = data.getActivationId().getPrimaryKeys().getActivationId();
    value.maintenanceProduct = EntityFactory.getProduct(data.getMaintenanceProduct());
    value.partNumber = EntityFactory.getUniqueIdValue(data.getPartNumber());
    value.orderId = data.getOrderId();
    value.orderLineNumber = data.getOrderLineNumber();
    value.startDate = data.getStartDate();
    value.expirationDate = data.getExpirationDate();
    value.isPermanent = data.getIsPermanent();
    value.state = Utils.toStringOrNull(data.getState());

    value.onLoad();
    
    return value;
  }

  /**
   * 
   * @param lines
   * @return
   */
  public static List<MaintenanceLineItemProperties> getMaintenanceLineItemPropertiesList(
      final com.flexnet.operations.webservices.v1.MaintenanceLineItemPropertiesType[] lines) {
    List<MaintenanceLineItemProperties> list = null;
    if (null != lines) {
      list = new ArrayList<>();
      for (final com.flexnet.operations.webservices.v1.MaintenanceLineItemPropertiesType line : lines) {
        list.add(EntityFactory.getMaintenanceLineItemProperties(line));
      }
    }
    return list;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Organization getOrganization(
      final com.flexnet.operations.webservices.v1.OrganizationDetailDataType data) {
    // do not use anonymous class constructore - this messes up Gson
    final Organization value = new Organization();

    value.name = data.getOrganization().getPrimaryKeys().getName();
    value.displayName = data.getDisplayName();
    value.lastModifiedDate = Utils.toDateOrNull(data.getDateLastModified());
    value.type = Utils.toStringOrNull(data.getOrgType());
    value.customAttributes = EntityFactory.getAttributeList(data.getCustomAttributes());
    value.address = EntityFactory.getAddress(data.getAddress());
    value.onLoad();
    return value;
  }

  public static Fulfilment getFulfilment(final flexnet.macrovision.com.FulfillmentDataType data) {
    // do not use anonymous class constructore - this messes up Gson
    final Fulfilment value = new Fulfilment();

    value.entitlementId = data.getEntitlementIdentifier().getPrimaryKeys().getEntitlementId();
    value.activationId = data.getLineItem().getPrimaryKeys().getActivationId();
    value.fulfilmentId = data.getFulfillmentIdentifier().getPrimaryKeys().getFulfillmentId();

    if (data.getParentFulfillmentId() != null) {
      value.parentFulfilmentId = data.getParentFulfillmentId().getPrimaryKeys().getFulfillmentId();
    }

    value.expirationDate = data.getExpirationDate();
    value.startDate = data.getStartDate();
    value.fulfiledCount = Integer.valueOf(data.getFulfilledCount());
    value.source = Utils.toStringOrNull(data.getFulfillmentSource());
    value.fulfilmentType = data.getFulfillmentType();
    value.activationType = Utils.toStringOrNull(data.getActivationType());
    value.fulfilmetDateTime = Utils.toDateOrNull(data.getFulfillDateTime());
    value.lastModifiedDateTime = Utils.toDateOrNull(data.getLastModifiedDateTime());
    value.state = Utils.toStringOrNull(data.getState());
    value.product = EntityFactory.getProduct(data.getProduct());
    value.entitledProducts = EntityFactory.getProductsList(data.getEntitledProducts());
    value.supportAction = Utils.toStringOrNull(data.getSupportAction());
    value.licenseModelAttributes = EntityFactory.getAttributeList(data.getLicenseModelAttributes());

    value.onLoad();
    
    return value;
  }

  public static LineItemProperties getLineItemProperties(
      final com.flexnet.operations.webservices.v1.EntitlementLineItemPropertiesType data) {

    final LineItemProperties value = new LineItemProperties();

    value.activationId = data.getActivationId().getPrimaryKeys().getActivationId();
    value.description = data.getDescription();
    value.state = Utils.toStringOrNull(data.getState());
    value.activatableItemType = Utils.toStringOrNull(data.getActivatableItemType());
    value.orderId = data.getOrderId();
    value.orderLineNumber = data.getOrderLineNumber();
    value.entitlementId = data.getEntitlementId().getPrimaryKeys().getEntitlementId();
    value.soldTo = EntityFactory.getUniqueIdValue(data.getSoldTo());
    value.soldToDisplayName = data.getSoldToDisplayName();
    value.entitlementState = Utils.toStringOrNull(data.getEntitlementState());
    value.entitlementDescription = data.getEntitlementDescription();
    value.shipToAddress = data.getShipToAddress();
    value.shipToEmail = data.getShipToEmail();
    value.allowPortalLogin = data.getAllowPortalLogin();
    // value.parentBulkEntitlementId =
    // EntityFactory.getUniqueIdValue(line.getParentBulkEntitlementId());
    // value.bulkEntSoldToDisplayName = line.getBulkEntSoldToDisplayName();
    value.product = EntityFactory.getProduct(data.getProduct());
    value.productDescription = data.getProductDescription();
    value.partNumber = EntityFactory.getUniqueIdValue(data.getPartNumber());
    value.partNumberDescription = data.getPartNumberDescription();
    value.licenseTechnology = EntityFactory.getUniqueIdValue(data.getLicenseTechnology());
    value.licenseModel = EntityFactory.getUniqueIdValue(data.getLicenseModel());
    // value.alternateLicenseModel1 =
    // EntityFactory.getUniqueIdValue(line.getAlternateLicenseModel1());
    // value.alternateLicenseModel2 =
    // EntityFactory.getUniqueIdValue(line.getAlternateLicenseModel2());
    value.lineItemSupportAction = null;
    value.parentLineItem = EntityFactory.getUniqueIdValue(data.getParentLineItem());
    value.startDate = data.getStartDate();
    value.isPermanent = data.getIsPermanent();
    value.term = EntityFactory.getDuration(data.getTerm());
    value.expirationDate = data.getExpirationDate();
    value.numberOfCopies = data.getNumberOfCopies().intValue();
    value.fulfilledAmount = data.getFulfilledAmount().intValue();
    value.numberOfRemainingCopies = data.getNumberOfRemainingCopies().intValue();
    value.isTrusted = data.getIsTrusted();
    value.customAttributes = EntityFactory.getAttributeList(data.getCustomAttributes());
    value.entitledProducts = EntityFactory.getProductsList(data.getEntitledProducts());
    value.maintenanceLineItems = EntityFactory.getMaintenanceLineItemPropertiesList(data.getMaintenanceLineItems());

    // if (line.getCreatedOnDateTime() != null) {
    // value.createdOnDateTime = line.getCreatedOnDateTime().getTime();
    // }

    value.lastModifiedDateTime = Utils.toDateOrNull(data.getLastModifiedDateTime());

    value.lineItemAttributes = EntityFactory.getAttributeList(data.getLineItemAttributes());
    value.customAttributes = EntityFactory.getAttributeList(data.getCustomAttributes());
    if (data.getLineItemSupportAction() != null) {
      value.lineItemSupportAction = data.getLineItemSupportAction().toString();
    }
    value.onLoad();
    return value;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static UniqueIdValue getUniqueIdValue(
      final com.flexnet.operations.webservices.v1.PartNumberIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final UniqueIdValue value = new UniqueIdValue();
      value.uniqueId = data.getUniqueId();
      value.value = data.getPrimaryKeys().getPartId();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  public static UniqueIdValue getUniqueIdValue(
      final com.flexnet.operations.webservices.v1.LicenseModelIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final UniqueIdValue value = new UniqueIdValue();
      value.uniqueId = data.getUniqueId();
      value.value = data.getPrimaryKeys().getName();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  public static UniqueIdValue getUniqueIdValue(
      final com.flexnet.operations.webservices.v1.LicenseTechnologyIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final UniqueIdValue value = new UniqueIdValue();
      value.uniqueId = data.getUniqueId();
      value.value = data.getPrimaryKeys().getName();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  // com.flexnet.operations.webservices.v1.EntitlementLineItemIdentifierType
  public static UniqueIdValue getUniqueIdValue(
      final com.flexnet.operations.webservices.v1.EntitlementLineItemIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final UniqueIdValue value = new UniqueIdValue();
      value.uniqueId = data.getUniqueId();
      value.value = data.getPrimaryKeys().getActivationId();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  // com.flexnet.operations.webservices.v1.OrganizationIdentifierType
  public static UniqueIdValue getUniqueIdValue(
      final com.flexnet.operations.webservices.v1.OrganizationIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final UniqueIdValue value = new UniqueIdValue();
      value.uniqueId = data.getUniqueId();
      value.value = data.getPrimaryKeys().getName();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  // com.flexnet.operations.webservices.v1.EntitlementIdentifierType
  public static UniqueIdValue getUniqueIdValue(
      final com.flexnet.operations.webservices.v1.EntitlementIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final UniqueIdValue value = new UniqueIdValue();
      value.uniqueId = data.getUniqueId();
      value.value = data.getPrimaryKeys().getEntitlementId();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Product getProduct(final com.flexnet.operations.webservices.v1.ProductIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final Product value = new Product();
      value.uniqueId = data.getUniqueId();
      value.name = data.getPrimaryKeys().getName();
      value.version = data.getPrimaryKeys().getVersion();
      value.quantity = 1;
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Product getProduct(final flexnet.macrovision.com.ProductIdentifierType data) {
    if (data == null) {
      return null;
    }
    else {
      final Product value = new Product();
      value.uniqueId = data.getUniqueId();
      value.name = data.getPrimaryKeys().getName();
      value.version = data.getPrimaryKeys().getVersion();
      value.quantity = 1;
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Product getProduct(final com.flexnet.operations.webservices.v1.EntitledProductDataType data) {
    if (data == null) {
      return null;
    }
    else {
      final Product value = EntityFactory.getProduct(data.getProduct());

      value.quantity = data.getQuantity().intValue();
      return value;
    }
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Product getProduct(final flexnet.macrovision.com.EntitledProductDataType data) {
    if (data == null) {
      return null;
    }
    else {
      final Product value = EntityFactory.getProduct(data.getProduct());

      value.quantity = data.getQuantity().intValue();
      return value;
    }
  }

  /**
   * 
   * @param products
   * @return
   */
  public static List<Product> getProductsList(final com.flexnet.operations.webservices.v1.EntitledProductDataType[] products) {
    List<Product> list = null;
    if (products != null) {
      list = new ArrayList<>();
      for (final com.flexnet.operations.webservices.v1.EntitledProductDataType prod : products) {
        list.add(EntityFactory.getProduct(prod));
      }
    }
    return list;
  }

  /**
   * 
   * @param products
   * @return
   */
  public static List<Product> getProductsList(final flexnet.macrovision.com.EntitledProductDataType[] products) {
    List<Product> list = null;
    if (products != null) {
      list = new ArrayList<>();
      for (final flexnet.macrovision.com.EntitledProductDataType prod : products) {
        list.add(EntityFactory.getProduct(prod));
      }
    }
    return list;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Duration getDuration(final com.flexnet.operations.webservices.v1.DurationType data) {
    if (data == null) {
      return null;
    }
    else {
      final Duration value = new Duration();
      value.length = data.getNumDuration().intValue();
      value.type = Utils.toStringOrNull(data.getDurationUnit());
      return value;
    }
  }

  
  /**
   * 
   * @param data
   * @return
   */
  public static Attribute getAttribute(final com.flexnet.operations.webservices.v1.AttributeDescriptorType data) {

    final Attribute value = new Attribute();

    value.name = data.getAttributeName();

    if (data.getBooleanValue() != null) {
      value.booleanValue = data.getBooleanValue();
      value.type = AttributeType.BOOLEAN;
    }

    if (data.getArrayValue() != null) {
      value.arrayValue = Arrays.asList(data.getArrayValue());
      value.type = AttributeType.ARRAY;
    }

    if (data.getDateValue() != null) {
      value.dateValue = data.getDateValue();
      value.type = AttributeType.DATE;
    }

    if (data.getIntegerValue() != null) {
      value.integerValue = data.getIntegerValue().intValue();
      value.type = AttributeType.INTEGER;
    }

    if (data.getStringValue() != null) {
      value.stringValue = data.getStringValue();
      value.type = AttributeType.STRING;
    }

    return value;
  }

  /**
   * 
   * @param data
   * @return
   */
  public static Attribute getAttribute(final flexnet.macrovision.com.AttributeDescriptorType data) {

    final Attribute value = new Attribute();

    value.name = data.getAttributeName();

    if (data.getBooleanValue() != null) {
      value.booleanValue = data.getBooleanValue();
      value.type = AttributeType.BOOLEAN;
    }

    if (data.getArrayValue() != null) {
      value.arrayValue = Arrays.asList(data.getArrayValue());
      value.type = AttributeType.ARRAY;
    }

    if (data.getDateValue() != null) {
      value.dateValue = data.getDateValue();
      value.type = AttributeType.DATE;
    }

    if (data.getIntegerValue() != null) {
      value.integerValue = data.getIntegerValue().intValue();
      value.type = AttributeType.INTEGER;
    }

    if (data.getStringValue() != null) {
      value.stringValue = data.getStringValue();
      value.type = AttributeType.STRING;
    }

    return value;
  }

  /**
   * 
   * @param attributes
   * @return
   */
  public static List<Attribute> getAttributeList(
      final com.flexnet.operations.webservices.v1.AttributeDescriptorType[] attributes) {

    List<Attribute> list = null;

    if (attributes != null) {
      list = new ArrayList<>();
      for (final com.flexnet.operations.webservices.v1.AttributeDescriptorType att : attributes) {
        final Attribute attr = EntityFactory.getAttribute(att);
        if (attr.type != AttributeType.VOID) {
          list.add(attr);
        }
      }
    }
    return list;
  }

  /**
   * 
   * @param attributes
   * @return
   */
  public static List<Attribute> getAttributeList(final flexnet.macrovision.com.AttributeDescriptorType[] attributes) {

    List<Attribute> list = null;

    if (attributes != null) {
      list = new ArrayList<>();
      for (final flexnet.macrovision.com.AttributeDescriptorType att : attributes) {
        final Attribute attr = EntityFactory.getAttribute(att);
        if (attr.type != AttributeType.VOID) {
          list.add(attr);
        }
      }
    }
    return list;
  }
  
  /**
   * 
   * @param data
   * @return
   */
  public static Address getAddress(final com.flexnet.operations.webservices.v1.AddressDataType data) {
    if (data == null) {
      return null;
    }
    else {
      final Address value = new Address();
      value.address = data.getAddress1();
      value.street = data.getAddress2();
      value.city = data.getCity();
      value.country = data.getCountry();
      value.region = data.getRegion();
      value.zip = data.getZipcode();
      value.state = data.getState();
      return value;
    }
  }
}
