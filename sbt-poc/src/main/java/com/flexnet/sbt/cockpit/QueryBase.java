package com.flexnet.sbt.cockpit;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.flexnet.sbt.cockpit.entity.EntityManager;
import com.flexnet.sbt.cockpit.entity.Organization;
import com.flexnet.sbt.cockpit.match.query.OrganizationQuery;
import com.flexnet.sbt.cockpit.util.Counter;
import com.flexnet.sbt.cockpit.util.Stopwatch;

public abstract class QueryBase {
  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  protected EntityManager entityManager;

  // map of CSID -> Attribute
  protected final Map<Integer, AttributeData> attributes = new HashMap<>();

  protected final Map<String, LicenseData> licenses = new HashMap<>();

  protected final Map<String, Organization> organizationsMap = new HashMap<>();

  protected final Stopwatch timer = new Stopwatch();
  protected final Counter count = new Counter();
  
  protected QueryBase(final EntityManager manager) {
    this.entityManager = manager;
  }
  
  protected void clear() {
    this.attributes.clear();
    this.licenses.clear();
    this.organizationsMap.clear();
  }
  /**
   * filter organizations and place in map against CSID
   */
  protected void processOrganizations(final Stream<Organization> stream) {

    log.info("querying organizations");
    stream.forEach(org -> {
      // map organizations
      organizationsMap.put(org.name, org);
      // map attributes
      this.attributes.put(org.csid, AttributeData.get(org));
    });
    log.info("retrieved " + this.attributes.size() + " organizations in " + timer.getLapSeconds(true) + " seconds");
  }
  
  /**
   * 
   * @param orgsQuery
   * @throws Exception
   */
  public abstract void run(final OrganizationQuery orgsQuery) throws Exception;

  // getters
  
  public EntityManager getEntityManager() {
    return entityManager;
  }

  public Map<Integer, AttributeData> getAttributes() {
    return attributes;
  }

  public Map<String, LicenseData> getLicenses() {
    return licenses;
  }

}