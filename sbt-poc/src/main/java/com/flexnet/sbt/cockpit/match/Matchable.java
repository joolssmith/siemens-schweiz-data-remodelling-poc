package com.flexnet.sbt.cockpit.match;

public interface Matchable<T> {

  public boolean matches(T value);
}

