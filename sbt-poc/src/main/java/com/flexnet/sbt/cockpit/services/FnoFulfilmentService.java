package com.flexnet.sbt.cockpit.services;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.xml.rpc.Call;
import javax.xml.rpc.ServiceException;

import org.apache.axis.client.Stub;

import com.flexnet.sbt.cockpit.entity.Fulfilment;

import flexnet.macrovision.com.DateTimeQueryType;
import flexnet.macrovision.com.DatedSearchType;
import flexnet.macrovision.com.FulfillmentsQueryParametersType;
import flexnet.macrovision.com.GetFulfillmentsQueryRequestType;
import flexnet.macrovision.com.GetFulfillmentsQueryResponseType;
import flexnet.macrovision.com.LicenseFulfillmentServiceInterface;
import flexnet.macrovision.com.LicenseServiceLocator;
import flexnet.macrovision.com.SimpleQueryType;
import flexnet.macrovision.com.SimpleSearchType;
import flexnet.macrovision.com.StateQueryType;
import flexnet.macrovision.com.StateType;
import flexnet.macrovision.com.StatusType;

public class FnoFulfilmentService extends FnoService {

  private LicenseFulfillmentServiceInterface service;

  /**
   * 
   * @param username
   * @param password
   * @throws ServiceException
   * @throws MalformedURLException
   */
  @Override
  public void initializeService() throws ServiceException, MalformedURLException {

    final LicenseServiceLocator locator = new LicenseServiceLocator();

    this.service = locator.getLicenseService(new URL(getHostname() + locator.getLicenseServiceWSDDServiceName()));
    ((Stub) service)._setProperty(Call.USERNAME_PROPERTY, getUsername());
    ((Stub) service)._setProperty(Call.PASSWORD_PROPERTY, getPassword());
  }

  public List<Fulfilment> doGetFulfilments(final GetFulfillmentsQueryRequestType request) throws RemoteException {

    final List<Fulfilment> results = new ArrayList<>();

    for (int i = 1;; i++) {
      request.setPageNumber(BigInteger.valueOf(i));

      log.trace("getFulfilments: page - " + request.getPageNumber().toString() + " - size " + results.size());

      GetFulfillmentsQueryResponseType response = null;

      for (int j = 1;; j++) {
        try {
          response = this.service.getFulfillmentsQuery(request);
          break;
        }
        catch (final Exception e) {
          log.error(e.getMessage());
          if (j >= 3) {
            throw new RemoteException(e.getMessage());
          }
        }
      }

      if (response.getStatusInfo().getStatus() != StatusType.SUCCESS) {
        log.error(response.getStatusInfo().getStatus().toString() + ": " + response.getStatusInfo().getReason());
        throw new RuntimeException(response.getStatusInfo().getReason());
      }

      if (response.getResponseData() == null) {
        log.trace("getFulfillmentsQuery: no data returned");
        break;
      }

      // add into array
      Arrays.asList(response.getResponseData()).stream().filter(x -> x != null)
          .forEach(x -> results.add(EntityFactory.getFulfilment(x)));

      if (response.getResponseData().length < request.getBatchSize().intValue()) {
        log.trace("getFulfillmentsQuery: complete");
        break;
      }

      if (FnoService.getQuickQuit()) {
        break;
      }
    }
    return results;
  }
  
  public List<Fulfilment> getFulfilments(final Calendar lastUpdateTime) throws RemoteException {

    log.trace("getFulfilments: date is " + (lastUpdateTime == null ? "null" : lastUpdateTime.toString()));

    @SuppressWarnings("serial")
    final GetFulfillmentsQueryRequestType request = new GetFulfillmentsQueryRequestType() {
      {
        this.setQueryParams(new FulfillmentsQueryParametersType() {
          {
            if (null != lastUpdateTime) {
              this.setLastModifiedDateTime(new DateTimeQueryType() {
                {
                  this.setSearchType(DatedSearchType.AFTER);
                  this.setValue(lastUpdateTime);
                }
              });
            }
          }
        });
        this.setIncludeLicenseText(false);
        this.setIncludeConsolidatedHostLicense(false);
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };

    return doGetFulfilments(request);
  }
  
  public List<Fulfilment> getFulfilmentsForActivationId(final String activationId) throws RemoteException {

    log.trace("getFulfilmentsForActivationId for " + activationId);

    @SuppressWarnings("serial")
    final GetFulfillmentsQueryRequestType request = new GetFulfillmentsQueryRequestType() {
      {
        this.setQueryParams(new FulfillmentsQueryParametersType() {
          {
           this.setActivationId(new SimpleQueryType() {
             {
               this.setSearchType(SimpleSearchType.EQUALS);
               this.setValue(activationId);
             }
           });
           this.setState(new StateQueryType() {
             {
               this.setSearchType(SimpleSearchType.EQUALS);
               this.setValue(StateType.ACTIVE);
             }
           });
          }
        });
        this.setIncludeLicenseText(false);
        this.setIncludeConsolidatedHostLicense(false);
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };

    return doGetFulfilments(request);
  }
  
  public List<Fulfilment> getFulfilmentsForCsid(final String orgName) throws RemoteException {

    log.trace("getFulfilmentsForCsid for CSID " + orgName);

    @SuppressWarnings("serial")
    final GetFulfillmentsQueryRequestType request = new GetFulfillmentsQueryRequestType() {
      {
        this.setQueryParams(new FulfillmentsQueryParametersType() {
          {
            this.setSoldTo(new SimpleQueryType() {
              {
                this.setSearchType(SimpleSearchType.EQUALS);
                this.setValue(orgName);
              }
            });
           this.setState(new StateQueryType() {
             {
               this.setSearchType(SimpleSearchType.EQUALS);
               this.setValue(StateType.ACTIVE);
             }
           });
          }
        });
        this.setIncludeLicenseText(false);
        this.setIncludeConsolidatedHostLicense(false);
        this.setBatchSize(BigInteger.valueOf(FnoService.getBatchSize()));
      }
    };

    return doGetFulfilments(request);
  }
  
  public List<Fulfilment> getFulfilmentsForActivationIds(final Collection<String> activationIds)
      throws InterruptedException, ExecutionException, RemoteException {

    log.trace("query fulfilments for " + activationIds.size() + " orgs");

    final List<Fulfilment> results = new ArrayList<>();

    final List<Future<List<Fulfilment>>> futures = new ArrayList<>();

    for (final String aid : activationIds) {

      futures.add(executorService.submit(new Callable<List<Fulfilment>>() {
        @Override
        public List<Fulfilment> call() throws Exception {
          return getFulfilmentsForActivationId(aid);
        }
      }));
    }

    log.trace("query fulfilments for " + activationIds.size() + " fulfilments - waiting for results");
    
    for (final Future<List<Fulfilment>> future : futures) {

      final List<Fulfilment> result = future.get();
      if (result != null) {
        results.addAll(result);
      }
    }

    return results;
  }
  
  public List<Fulfilment> getFulfilmentsForCsids(final Collection<String> orgs)
      throws InterruptedException, ExecutionException, RemoteException {

    log.trace("query fulfilments for " + orgs.size() + " orgs");

    final List<com.flexnet.sbt.cockpit.entity.Fulfilment> results = new ArrayList<>();

    final List<Future<List<com.flexnet.sbt.cockpit.entity.Fulfilment>>> futures = new ArrayList<>();

    for (final String org : orgs) {

      futures.add(executorService.submit(new Callable<List<com.flexnet.sbt.cockpit.entity.Fulfilment>>() {
        @Override
        public List<Fulfilment> call() throws Exception {
          return getFulfilmentsForCsid(org);
        }
      }));
    }

    log.trace("query entitlements for " + orgs.size() + " orgs - waiting for results");
    
    for (final Future<List<com.flexnet.sbt.cockpit.entity.Fulfilment>> future : futures) {

      final List<Fulfilment> result = future.get();
      if (result != null) {
        results.addAll(result);
      }
    }

    return results;
  }
}

