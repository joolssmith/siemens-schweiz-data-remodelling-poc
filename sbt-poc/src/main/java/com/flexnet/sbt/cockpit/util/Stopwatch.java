package com.flexnet.sbt.cockpit.util;

public class Stopwatch {
  private long start;
  private long lap;
  
  public Stopwatch() {
    this.lap = this.start = System.nanoTime();
  }

  public long reset() {
    final long elapse = System.nanoTime() - this.start;
    this.lap = this.start = System.nanoTime();
    return elapse;
  }
  
  public double getLapSeconds(final boolean reset) { 
    final long now = System.nanoTime();
    try {
      return (now - this.lap) / 1000000000.0;
    }
    finally {
      if (reset) {
        this.lap = now;
      }
    }
  }
  
  public double getElapsedSeconds() { 
    return (System.nanoTime() - this.start) / 1000000000.0;
  }
  
  public long getElapsedNanoSeconds() {
    return System.nanoTime() - this.start;
  }
  
  public static double nanosecsToSecs(final long nanos) {
    return nanos / 1000000000.0;
  }
}
