package com.flexnet.sbt.cockpit;

import java.util.Date;

import com.flexnet.sbt.cockpit.entity.Entitlement;
import com.flexnet.sbt.cockpit.entity.Fulfilment;
import com.flexnet.sbt.cockpit.entity.LineItem;
import com.flexnet.sbt.cockpit.entity.LineItemProperties;
import com.flexnet.sbt.cockpit.entity.beans.UniqueIdValue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.reflect.TypeToken;

public class LicenseData {

  @Expose
  public String partNumber;
  public int quantity;
  public String partNumberDescription;
  public String version;
  public String hqSalesOrder;
  public String salesOrderItem;
  public Date createdOn;
  public String entitlementId;
  public String activationId;
  public String fnoStatus;
  public Date activatedOn;
  public Date startDate;
  public Date expiryDate;
  public String productName;
  public String emailReviewer;
  public String dongleId; 
  
  public String parentLineItem;
  public boolean isUpgrade;
  public String type;
  
  public LicenseData() {
    
  }
  
  public void update(final Entitlement entitlement, final LineItem line) {
    if (null != line) {
      
      this.partNumber = UniqueIdValue.getValue(line.partNumber);
      this.quantity = line.numberOfCopies;
      this.hqSalesOrder = line.orderId;
      this.salesOrderItem = line.orderLineNumber;

      this.entitlementId = entitlement.entitlementId;
      
      this.activationId = line.activationId;
//      this.fnoStatus = line.state;

      this.startDate = line.startDate;
      this.expiryDate = line.expirationDate;
      if (line.product != null) {
        this.productName = line.product.name;
        this.version = line.product.version;
      }
      
      this.createdOn = line.createdOnDateTime;
      
      if (line.lineItemType != null) {
        this.type = line.lineItemType;
        this.isUpgrade |= line.lineItemType.equals("UPGRADE");
      }

      this.parentLineItem = UniqueIdValue.getValue(line.parentLineItem);
    }
  }
  
  public void update(final LineItemProperties line) {
    if (null != line) {
      
      this.partNumber = UniqueIdValue.getValue(line.partNumber);
      this.quantity = line.numberOfCopies;
      this.partNumberDescription = line.partNumberDescription;
      
      this.hqSalesOrder = line.orderId;
      this.salesOrderItem = line.orderLineNumber;
      this.entitlementId = line.entitlementId;
      this.activationId = line.activationId;
      this.fnoStatus = line.state;

      this.startDate = line.startDate;
      this.expiryDate = line.expirationDate;
      if (line.product != null) {
        this.productName = line.product.name;
        this.version = line.product.version;
      }
      
      this.type = line.activatableItemType;
      this.parentLineItem = UniqueIdValue.getValue(line.parentLineItem);
    }
  }
  
  public void update(final Fulfilment frec) {
    if (null != frec) {
      
      if (this.activatedOn == null || this.activatedOn.after(frec.startDate)) {
        this.activatedOn = frec.startDate;
        
        if (frec.startDate != null) {
          this.startDate = frec.startDate;
        }
 
        if (frec.expirationDate != null) {
          this.expiryDate = frec.expirationDate;
        }
      }
      
      if (frec.product != null) {
        this.productName = frec.product.name;
        this.version = frec.product.version;
      }
      this.fnoStatus = frec.supportAction;
    }
  }
  
  public static LicenseData get(final LineItemProperties line) {
    return new LicenseData() {
      {
        update(line);
      }
    };
  }

  public static LicenseData get(final Entitlement entitlement, final LineItem line) {
    return new LicenseData() {
      {
        update(entitlement, line);
      }
    };
  }
  
  private static Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
  
  @Override
  public String toString() {
    return gson.toJson(this, new TypeToken<LicenseData>() {}.getType());
  }
}
