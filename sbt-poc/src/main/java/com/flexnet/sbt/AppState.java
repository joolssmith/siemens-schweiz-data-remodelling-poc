package com.flexnet.sbt;

public enum AppState {
  INACTIVE, STARTING, RUNNING, SHUTTING_DOWN, ERROR
}
