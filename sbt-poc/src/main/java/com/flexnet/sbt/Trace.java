package com.flexnet.sbt;

import org.slf4j.Logger;

public class Trace {

  /**
   * 
   * @param logger
   */
  public static void in(final Logger logger) {
    logger.debug("==> {}", Thread.currentThread().getStackTrace()[2].getMethodName());
  }

  /**
   * 
   * @param logger
   */
  public static void out(final Logger logger) {
    logger.debug("<== {}", Thread.currentThread().getStackTrace()[2].getMethodName());
  }

}
