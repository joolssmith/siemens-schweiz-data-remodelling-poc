package com.flexnet.sbt.rest.beans;



import java.util.Arrays;

import com.flexnet.sbt.cockpit.match.Matchable;
import com.flexnet.sbt.cockpit.match.RangeMatch;
import com.flexnet.sbt.cockpit.match.StringSetMatch;
import com.flexnet.sbt.cockpit.match.WildcardStringMatch;

public class StringQueryElement {
//  private static final Logger log = LoggerFactory.getLogger(QueryElement.class);
  
  public String value;
  
  public String[] values;
  
  public Range<String> valueRange;

  public Matchable<String> generateQuery() {
    
    if (this.value != null) {
      return WildcardStringMatch.get(this.value);
    }
    
    if (this.values != null) {    
      return new StringSetMatch(Arrays.asList(this.values));  
    }
    
    if (this.valueRange != null) {
      return new RangeMatch<String>(this.valueRange.lower, this.valueRange.upper);
    }
    
    throw new IllegalArgumentException("no values found in query element");
  }
  
  public static Matchable<String> get(final StringQueryElement element) {
    return element == null ? null : element.generateQuery();
  }
}
