package com.flexnet.sbt.rest.beans;

import java.util.Arrays;

import com.flexnet.sbt.cockpit.match.Match;
import com.flexnet.sbt.cockpit.match.Matchable;
import com.flexnet.sbt.cockpit.match.RangeMatch;
import com.flexnet.sbt.cockpit.match.SetMatch;

public class IntegerQueryElement {
  // private static final Logger log =
  // LoggerFactory.getLogger(QueryElement.class);

  public Integer value;

  public Integer[] values;

  public Range<Integer> valueRange;

  public IntegerQueryElement() {
    
  }
  
  public Matchable<Integer> generateQuery() {

    if (this.value != null) {
      return new Match<Integer>(this.value);
    }

    if (this.values != null) {
      return new SetMatch<Integer>(Arrays.asList(this.values));
    }

    if (this.valueRange != null) {
      return new RangeMatch<Integer>(this.valueRange.lower, this.valueRange.upper);
    }

    throw new IllegalArgumentException("no values found in query element");
  }

  public static Matchable<Integer> get(final IntegerQueryElement element) {
    return element == null ? null : element.generateQuery();
  }
}
