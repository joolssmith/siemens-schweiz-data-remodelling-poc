package com.flexnet.sbt.rest.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author juliansmith
 *
 */
public abstract class BaseController {

  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  /**
   * Trace
   */
  protected void traceIn() {
    logger.info(Thread.currentThread().getStackTrace()[2].getMethodName() + " enter");
  }
  
  /**
   * Trace
   */
  protected void traceOut() {
    logger.info(Thread.currentThread().getStackTrace()[2].getMethodName() + " leave");
  }
  
}
