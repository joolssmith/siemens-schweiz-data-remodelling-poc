package com.flexnet.sbt.rest.beans;

import com.flexnet.sbt.cockpit.match.query.OrganizationQuery;

public class AttributeQuery {

  public IntegerQueryElement csids;
  public StringQueryElement branch;
  public StringQueryElement country;
  public StringQueryElement name1;
  public StringQueryElement name2;
  public StringQueryElement streetName;
  public StringQueryElement streetNumber;
  public StringQueryElement postalCode;
  public StringQueryElement stateProvince;
  public StringQueryElement salesChannel;
  public StringQueryElement buildingType;
  public StringQueryElement vapNumber;
  public StringQueryElement ifaNumber;
  public StringQueryElement localSiteId;
  public StringQueryElement createdBy;
  public StringQueryElement createdAt;
  public StringQueryElement modifiedBy;
  public StringQueryElement modifiedAt;
  public StringQueryElement hardwareId;
  
  public OrganizationQuery generateQuery() {
    
    final OrganizationQuery query = new OrganizationQuery() {
      {
        this.csids = IntegerQueryElement.get(AttributeQuery.this.csids);
        this.branch = StringQueryElement.get(AttributeQuery.this.branch);
        this.country = StringQueryElement.get(AttributeQuery.this.country);
        this.name1 = StringQueryElement.get(AttributeQuery.this.name1);
        this.name2 = StringQueryElement.get(AttributeQuery.this.name2);
        this.streetName = StringQueryElement.get(AttributeQuery.this.streetName);
        this.streetNumber = StringQueryElement.get(AttributeQuery.this.streetNumber);
        this.postalCode = StringQueryElement.get(AttributeQuery.this.postalCode);
        this.stateProvince = StringQueryElement.get(AttributeQuery.this.stateProvince);
        this.salesChannel = StringQueryElement.get(AttributeQuery.this.salesChannel);
        this.buildingType = StringQueryElement.get(AttributeQuery.this.buildingType);
        this.vapNumber = StringQueryElement.get(AttributeQuery.this.vapNumber);
        this.ifaNumber = StringQueryElement.get(AttributeQuery.this.ifaNumber);
        this.localSiteId = StringQueryElement.get(AttributeQuery.this.localSiteId);
        this.createdBy = StringQueryElement.get(AttributeQuery.this.createdBy);
        this.createdAt = StringQueryElement.get(AttributeQuery.this.createdAt);
        this.modifiedBy = StringQueryElement.get(AttributeQuery.this.modifiedBy);
        this.modifiedAt = StringQueryElement.get(AttributeQuery.this.modifiedAt);
        this.hardwareId = StringQueryElement.get(AttributeQuery.this.hardwareId);
      }
    };
    
    return query;
  }
}