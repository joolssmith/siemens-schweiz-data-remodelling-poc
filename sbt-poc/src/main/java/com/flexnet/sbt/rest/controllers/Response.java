package com.flexnet.sbt.rest.controllers;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.flexnet.sbt.cockpit.util.Stopwatch;

public class Response {

  @JsonIgnore
  private Stopwatch timer = new Stopwatch();
  
  public Boolean status;
  public String message;
  public Double duration;
  public Object request;
  public Object response;
  
  public Response() {
  }
  
  public Response(final Object request) {
    this.request = request;
  }
  
  public Response ok() {
    this.duration = this.timer.getElapsedSeconds();
    this.message = "OK";
    this.status = true;
    
    return this;
  }
  
  public Response ok(final Object response) {
    
    ok().response = response;
    
    return this;
  }
  
  public Response error(final Throwable t) {
    this.duration = this.timer.getElapsedSeconds();
    this.message = t.getMessage();
    this.status = false;
    
    return this;
  }
}
