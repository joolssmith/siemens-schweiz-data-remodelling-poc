package com.flexnet.sbt.rest.controllers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.flexnet.sbt.Application;
import com.flexnet.sbt.cockpit.AttributeData;
import com.flexnet.sbt.cockpit.CacheQuery;
import com.flexnet.sbt.cockpit.LicenseData;
import com.flexnet.sbt.cockpit.LikeForLikeQuery;
import com.flexnet.sbt.cockpit.entity.Entitlement;
import com.flexnet.sbt.cockpit.entity.Fulfilment;
import com.flexnet.sbt.cockpit.entity.LineItemProperties;
import com.flexnet.sbt.cockpit.entity.Organization;
import com.flexnet.sbt.cockpit.match.StringMatch;
import com.flexnet.sbt.cockpit.match.StringSetMatch;
import com.flexnet.sbt.cockpit.match.query.EntitlementsQuery;
import com.flexnet.sbt.cockpit.match.query.FulfilmentsQuery;
import com.flexnet.sbt.cockpit.match.query.LineItemPropertiesQuery;
import com.flexnet.sbt.cockpit.match.query.OrganizationQuery;
import com.flexnet.sbt.cockpit.util.Stopwatch;
import com.flexnet.sbt.rest.beans.AttributeQuery;
import com.flexnet.sbt.rest.beans.IntegerQueryElement;
import com.flexnet.sbt.rest.beans.Range;
import com.flexnet.sbt.rest.beans.StringQueryElement;

@RestController
@RequestMapping("/cockpit")
public class CsidController extends BaseController {
 
  /**
   * Return the structure of the query builder
   * @return
   */
  @RequestMapping(value = "/test", method = RequestMethod.GET)
  public AttributeQuery test() {

    // show query format in client...
    return new AttributeQuery() {
      {
        this.csids = new IntegerQueryElement() {
          {
            this.value = 1;
            this.values = new Integer[] { 1, 2, 3, 4, 5 };
            // range is inclusive or endpoints
            this.valueRange = new Range<Integer>() {
              {
                this.lower = 100;
                this.upper = 200;
              }
            };
          }
        };
        this.branch = new StringQueryElement() {
          {
            // may have leading or trailing wildcards case is ignored
            this.value = "hello*";
            // match is exact but case is ignored
            this.values = new String[] { "hello", "world" };
            // range is inclusive or endpoints
            this.valueRange = new Range<String>() {
              {
                this.lower = "hello";
                this.upper = "world";
              }
            };
          }
        };
      }
    };
  }
  
 
  /**
   * Test method - return distinct activatableItemType
   * @return
   */
  @RequestMapping(value = "/list/activatableItemType", method = RequestMethod.GET)
  public Collection<String> listActivatableItemTypes() {

    return Application
        .getInstance()
        .getEntityManager()
        .getLineItemProperties()
        .filterValues(line -> true)
        .map(line -> line.activatableItemType)
        .collect(Collectors.toCollection(HashSet::new));
  }
  
  /**
   * Test method - return distinct lineItemType
   * @return
   */
  @RequestMapping(value = "/list/lineType", method = RequestMethod.GET)
  public Collection<String> listLineTypes() {

    return Application
        .getInstance()
        .getEntityManager()
        .getEntitlements()
        .filterValues(line -> true)
        .map(line -> line.lineItems)
        .flatMap(List::stream).map(x -> x.lineItemType)
        .collect(Collectors.toCollection(HashSet::new));
  }
  
  /**
   * Test method - return distinct activationType
   * @return
   */
  @RequestMapping(value = "/list/activationType", method = RequestMethod.GET)
  public Collection<String> listActivationTypes() {

    return Application
        .getInstance()
        .getEntityManager()
        .getFulfilments().filterValues(line -> true)
        .map(line -> line.activationType)
        .collect(Collectors.toCollection(HashSet::new));
  }
  
  /**
   * Test method - return distinct supportAction
   * @return
   */
  @RequestMapping(value = "/list/supportAction", method = RequestMethod.GET)
  public Collection<String> listSupportActions() {

    return Application
        .getInstance()
        .getEntityManager()
        .getFulfilments()
        .filterValues(line -> true)
        .map(line -> line.supportAction)
        .collect(Collectors.toCollection(HashSet::new));
  }
  
  /**
   * Get organizations matching the query
   * @param query
   * @return
   */
  @RequestMapping(value = "/query/organization", method = RequestMethod.GET)
  public Collection<Organization> getOrganizations(final OrganizationQuery query) {
    return Application.getInstance()
    	.getEntityManager()
        .getOrganizations()
        .filterValues(org -> query.match(org))
        .sorted(Comparator.comparing(Organization::getCsid))
        .collect(Collectors.toCollection(ArrayList::new));
  }
  
  /**
   * Get entitlements
   * @param query
   * @return
   */
  private static Collection<Entitlement> getEntitlements(final EntitlementsQuery query) {
	    return Application.getInstance()
    	.getEntityManager()
        .getEntitlements()
        .filterValues(ent -> query.match(ent))
        .sorted(Comparator.comparing(Entitlement::getKey))
        .collect(Collectors.toCollection(ArrayList::new));
  }
  
  /**
   * Get line items
   * @param query
   * @return
   */
	private static Collection<LineItemProperties> getLineItemProperties(final LineItemPropertiesQuery query) {
		return Application
				.getInstance()
				.getEntityManager()
				.getLineItemProperties()
				.filterValues(line -> query.match(line))
				.sorted(Comparator.comparing(LineItemProperties::getKey))
				.collect(Collectors.toCollection(ArrayList::new));
	}
  
  /**
   * Get fulfilments
   * @param query
   * @return
   */
  private static Collection<Fulfilment> getFulfilments(final FulfilmentsQuery query) {
	  return Application
	      .getInstance()
      	.getEntityManager().getFulfilments()
        .filterValues(fid -> query.match(fid))
        .sorted(Comparator.comparing(Fulfilment::getKey))
        .collect(Collectors.toCollection(ArrayList::new));
  }
  
  /**
   * Test method with hard-coded query parameters
   * @return
   */
  @RequestMapping(value = "/test/attributes", method = RequestMethod.GET)
  public Collection<AttributeData> getAttibutesTest() {
    final Map<Integer, AttributeData> attributes = new HashMap<>();
    final Map<String, LicenseData> licenses = new HashMap<>();
    
    final OrganizationQuery query = new OrganizationQuery() {
      {
        // this.csids = new RangeMatch<Integer>(1, 2999);
         this.branch = new StringSetMatch(Arrays.asList(new String[] {"Ost", "Bayern"}));
        //this.country = StringMatch.get("DE");
        // this.localSiteId = WildcardStringMatch.get("416*");
        // this.createdAt = new RangeMatch<String>("2016-01-01 00:00:00.000", "2016-12-31 23:59:59.999");
      }
    };
    getAttibutes(query, attributes, licenses);
    
    return attributes.values();
  }
  
  /**
   * Test method with hard-coded query parameters
   * @return
   */
  @RequestMapping(value = "/test/licenses", method = RequestMethod.GET)
  public Collection<LicenseData> getLicensesTest() {
    final Map<Integer, AttributeData> attributes = new HashMap<>();
    final Map<String, LicenseData> licenses = new HashMap<>();
    
    final OrganizationQuery query = new OrganizationQuery() {
      {
        // this.csids = new RangeMatch<Integer>(1, 2999);
        // this.branch = SetMatch.get("Ost", "Bayern");
        this.country = new StringMatch("DE");
        // this.localSiteId = WildcardStringMatch.get("416*");
        // this.createdAt = new RangeMatch<String>("2016-01-01 00:00:00.000", "2016-12-31 23:59:59.999");
      }
    };
    
    getAttibutes(query, attributes, licenses);
    
    return licenses.values();
  }
  
  /**
   * 
   * @return
   */
  @RequestMapping(value = "/cache/queryAttributes", method = RequestMethod.POST)
  public Collection<AttributeData> getAttributes(@RequestBody final AttributeQuery query,
      UriComponentsBuilder ucBuilder) {

    if (query != null) {

      final Map<Integer, AttributeData> attributes = new HashMap<>();

      getAttibutes(query.generateQuery(), attributes, null);

      return attributes.values();
    }
    else {
      return null;
    }
  }
  
  /**
   * 
   * @return
   */
  @RequestMapping(value = "/likeForLike/queryLicenses", method = RequestMethod.POST)
  public Collection<LicenseData> getLicensesLikeForLike(@RequestBody final AttributeQuery attributeQuery,
      UriComponentsBuilder ucBuilder) {

    super.traceIn();
    final LikeForLikeQuery query = new LikeForLikeQuery(Application.getInstance().getEntityManager());
    
    try {
      query.run(attributeQuery.generateQuery());
      
      return query.getLicenses().values();
    }
    catch (final Throwable t) {
      super.logger.error("unexpected error", t);
      return null;
    }
    finally {
      super.traceOut();
    }
  }
  
  /**
   * 
   * @return
   */
  @RequestMapping(value = "/cache/queryLicenses", method = RequestMethod.POST)
  public Response getLicensesCache(@RequestBody final AttributeQuery attributeQuery,
      UriComponentsBuilder ucBuilder) {
    final Response response = new Response(attributeQuery);
    super.traceIn();
    final CacheQuery query = new CacheQuery(Application.getInstance().getEntityManager());
    
    try {
      query.run(attributeQuery.generateQuery());
      
      return response.ok(query.getLicenses().values());
    }
    catch (final Throwable t) {
      super.logger.error("unexpected error", t);
      return response.error(t);
    }
    finally {
      super.traceOut();
    }
  }
  
  /**
   * 
   * @return
   */
  @RequestMapping(value = "/cache/refresh", method = RequestMethod.POST)
  public Response refreshCache(UriComponentsBuilder ucBuilder) {

    super.traceIn();
    
    final Response response = new Response();
    
    try {
      final Calendar threshold = GregorianCalendar.getInstance();
      threshold.add(Calendar.HOUR, -24);
      
      Application.getInstance().getEntityManager().refresh(threshold);
      
      return response.ok();
    }
    catch (final Throwable t) {
      super.logger.error("unexpected error", t);
      return response.error(t);
    }
    finally {
      super.traceOut();
    }
  }
  
//  /**
//   * 
//   * @return
//   */
//  @RequestMapping(value = "/cache/queryLicenses", method = RequestMethod.POST)
//  public Collection<LicenseData> getLicenses(@RequestBody final AttributeQuery query,
//      UriComponentsBuilder ucBuilder) {
//
//    if (query != null) {
//      final Map<Integer, AttributeData> attributes = new HashMap<>();
//      final Map<String, LicenseData> licenses = new HashMap<>();
//  
//      getAttibutes(query.generateQuery(), attributes, licenses);
//
//      return licenses.values();
//    }
//    else {
//      return null;
//    }
//  }
  
  /**
   * Run the query filter against ORGS, ENTITLEMENTS, LINE ITEMS and FULFILMENTS
   * Build results iteratively
   * @return
   */
  private void getAttibutes(final OrganizationQuery orgQuery, final Map<Integer, AttributeData> attributes, final Map<String, LicenseData> licenses) {
    
    final Stopwatch timer = new Stopwatch();
    try {
      /**
       * ORGANIZATIONS
       */
      super.logger.info("Organization (" + Organization.class.getCanonicalName() + ")");
      /**
       * get the organizations (= attributes) selected
       */
      final Collection<Organization> orgs = getOrganizations(orgQuery);
      orgs.forEach(org -> {
        attributes.put(org.csid, AttributeData.get(org));
      });
      super.logger.info("Elapse " + timer.getElapsedSeconds() + " stream size " + orgs.size());
  
      /**
       * ENTITLEMENTS - match on SoldTo
       */
      super.logger.info("Entitlement (" + Entitlement.class.getCanonicalName() + ")");
      final Collection<Entitlement> ents = CsidController.getEntitlements(new EntitlementsQuery() {
        {
          this.organizations = new StringSetMatch(orgs.stream()
                  .map(org -> org.name)
                  .collect(Collectors.toCollection(ArrayList::new)));
        }
      });
      
      ents.forEach(ent -> {
        if (attributes.containsKey(ent.csid)) {
          // update the attributes data with this Entitlement record
          attributes.get(ent.csid).update(ent);
          if (null != licenses) {
            ent.lineItems.forEach(line -> {
              // create the license data from this LineItem record
              licenses.put(line.activationId, LicenseData.get(ent, line));
            });
          }
        }
      });
      super.logger.info("Elapse " + timer.getElapsedSeconds() + " stream size " + ents.size());
      
      /**
       * ENTITLEMENT LINE ITEMS - match on SoldTo
       */
      super.logger.info("LineItemProperties (" + LineItemProperties.class.getCanonicalName() + ")");
      
      /**
       * map for activationId -> csid
       */
      final Map<String, Integer> activationCsid = new HashMap<>();
  
      /**
       * line item properties for the csids chosen
       */
      final Collection<LineItemProperties> lines = CsidController.getLineItemProperties(new LineItemPropertiesQuery() {
        {
          this.organizations = new StringSetMatch(orgs
              .stream()
              .map(org -> org.name)
              .collect(Collectors.toCollection(ArrayList::new)));
        }
      });
      
      lines.forEach(line -> {
        if (null != licenses) {
          // update the LD with this LineItemProperties record
          licenses.get(line.activationId).update(line);
        }
        // map the activationId to the csid
        activationCsid.put(line.activationId, line.csid);
      });
      super.logger.info("Elapse " + timer.getElapsedSeconds() + " stream size " + lines.size());
  
      /**
       * FULFILMENTS - match on activationId
       */
      super.logger.info("Fulfilment (" + Fulfilment.class.getCanonicalName() + ")");
      final Collection<Fulfilment> fids = getFulfilments(new FulfilmentsQuery() {
        {
          this.activationIds = new StringSetMatch(activationCsid.keySet());
          this.state = new StringMatch("ACTIVE");
        }
      });
      
      fids.forEach(fid -> {
        if (null != licenses) {
          licenses.get(fid.activationId).update(fid);
        }
        attributes.get(activationCsid.get(fid.activationId)).update(fid);
      });
      super.logger.info("Elapse " + timer.getElapsedSeconds() + " stream size " + fids.size());
    }
    finally {

      super.logger.info("query commplete in " + timer.getElapsedSeconds() + " secs");
    }
  }
}
