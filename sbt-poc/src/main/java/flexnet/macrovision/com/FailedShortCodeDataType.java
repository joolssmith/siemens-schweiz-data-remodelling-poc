/**
 * FailedShortCodeDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FailedShortCodeDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.CreateShortCodeDataType shortCodeData;

    private java.lang.String reason;

    private flexnet.macrovision.com.FulfillmentIdentifierType[] duplicateFulfillmentRecords;

    public FailedShortCodeDataType() {
    }

    public FailedShortCodeDataType(
           flexnet.macrovision.com.CreateShortCodeDataType shortCodeData,
           java.lang.String reason,
           flexnet.macrovision.com.FulfillmentIdentifierType[] duplicateFulfillmentRecords) {
           this.shortCodeData = shortCodeData;
           this.reason = reason;
           this.duplicateFulfillmentRecords = duplicateFulfillmentRecords;
    }


    /**
     * Gets the shortCodeData value for this FailedShortCodeDataType.
     * 
     * @return shortCodeData
     */
    public flexnet.macrovision.com.CreateShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }


    /**
     * Sets the shortCodeData value for this FailedShortCodeDataType.
     * 
     * @param shortCodeData
     */
    public void setShortCodeData(flexnet.macrovision.com.CreateShortCodeDataType shortCodeData) {
        this.shortCodeData = shortCodeData;
    }


    /**
     * Gets the reason value for this FailedShortCodeDataType.
     * 
     * @return reason
     */
    public java.lang.String getReason() {
        return reason;
    }


    /**
     * Sets the reason value for this FailedShortCodeDataType.
     * 
     * @param reason
     */
    public void setReason(java.lang.String reason) {
        this.reason = reason;
    }


    /**
     * Gets the duplicateFulfillmentRecords value for this FailedShortCodeDataType.
     * 
     * @return duplicateFulfillmentRecords
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType[] getDuplicateFulfillmentRecords() {
        return duplicateFulfillmentRecords;
    }


    /**
     * Sets the duplicateFulfillmentRecords value for this FailedShortCodeDataType.
     * 
     * @param duplicateFulfillmentRecords
     */
    public void setDuplicateFulfillmentRecords(flexnet.macrovision.com.FulfillmentIdentifierType[] duplicateFulfillmentRecords) {
        this.duplicateFulfillmentRecords = duplicateFulfillmentRecords;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FailedShortCodeDataType)) return false;
        FailedShortCodeDataType other = (FailedShortCodeDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.shortCodeData==null && other.getShortCodeData()==null) || 
             (this.shortCodeData!=null &&
              this.shortCodeData.equals(other.getShortCodeData()))) &&
            ((this.reason==null && other.getReason()==null) || 
             (this.reason!=null &&
              this.reason.equals(other.getReason()))) &&
            ((this.duplicateFulfillmentRecords==null && other.getDuplicateFulfillmentRecords()==null) || 
             (this.duplicateFulfillmentRecords!=null &&
              java.util.Arrays.equals(this.duplicateFulfillmentRecords, other.getDuplicateFulfillmentRecords())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getShortCodeData() != null) {
            _hashCode += getShortCodeData().hashCode();
        }
        if (getReason() != null) {
            _hashCode += getReason().hashCode();
        }
        if (getDuplicateFulfillmentRecords() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getDuplicateFulfillmentRecords());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getDuplicateFulfillmentRecords(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FailedShortCodeDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedShortCodeDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortCodeData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shortCodeData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createShortCodeDataType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reason");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "reason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duplicateFulfillmentRecords");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "duplicateFulfillmentRecords"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
