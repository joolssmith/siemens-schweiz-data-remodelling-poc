/**
 * AdvancedFulfillmentLCDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class AdvancedFulfillmentLCDataType  implements java.io.Serializable {
    private java.lang.String fulfillmentId;

    private java.lang.String childActivationId;

    private java.math.BigInteger fulfillCount;

    private java.util.Date startDate;

    private java.util.Date versionDate;

    private java.util.Date versionStartDate;

    private java.lang.String soldTo;

    private java.lang.String shipToEmail;

    private java.lang.String shipToAddress;

    private flexnet.macrovision.com.ServerIDsType serverHost;

    private java.lang.String nodeLockHost;

    private java.lang.String[] countedNodeLockHostIds;

    private flexnet.macrovision.com.CustomHostIDType customHost;

    private flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes;

    private java.lang.Boolean overridePolicy;

    private java.lang.String FNPTimeZoneValue;

    public AdvancedFulfillmentLCDataType() {
    }

    public AdvancedFulfillmentLCDataType(
           java.lang.String fulfillmentId,
           java.lang.String childActivationId,
           java.math.BigInteger fulfillCount,
           java.util.Date startDate,
           java.util.Date versionDate,
           java.util.Date versionStartDate,
           java.lang.String soldTo,
           java.lang.String shipToEmail,
           java.lang.String shipToAddress,
           flexnet.macrovision.com.ServerIDsType serverHost,
           java.lang.String nodeLockHost,
           java.lang.String[] countedNodeLockHostIds,
           flexnet.macrovision.com.CustomHostIDType customHost,
           flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes,
           java.lang.Boolean overridePolicy,
           java.lang.String FNPTimeZoneValue) {
           this.fulfillmentId = fulfillmentId;
           this.childActivationId = childActivationId;
           this.fulfillCount = fulfillCount;
           this.startDate = startDate;
           this.versionDate = versionDate;
           this.versionStartDate = versionStartDate;
           this.soldTo = soldTo;
           this.shipToEmail = shipToEmail;
           this.shipToAddress = shipToAddress;
           this.serverHost = serverHost;
           this.nodeLockHost = nodeLockHost;
           this.countedNodeLockHostIds = countedNodeLockHostIds;
           this.customHost = customHost;
           this.licenseModelAttributes = licenseModelAttributes;
           this.overridePolicy = overridePolicy;
           this.FNPTimeZoneValue = FNPTimeZoneValue;
    }


    /**
     * Gets the fulfillmentId value for this AdvancedFulfillmentLCDataType.
     * 
     * @return fulfillmentId
     */
    public java.lang.String getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this AdvancedFulfillmentLCDataType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(java.lang.String fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the childActivationId value for this AdvancedFulfillmentLCDataType.
     * 
     * @return childActivationId
     */
    public java.lang.String getChildActivationId() {
        return childActivationId;
    }


    /**
     * Sets the childActivationId value for this AdvancedFulfillmentLCDataType.
     * 
     * @param childActivationId
     */
    public void setChildActivationId(java.lang.String childActivationId) {
        this.childActivationId = childActivationId;
    }


    /**
     * Gets the fulfillCount value for this AdvancedFulfillmentLCDataType.
     * 
     * @return fulfillCount
     */
    public java.math.BigInteger getFulfillCount() {
        return fulfillCount;
    }


    /**
     * Sets the fulfillCount value for this AdvancedFulfillmentLCDataType.
     * 
     * @param fulfillCount
     */
    public void setFulfillCount(java.math.BigInteger fulfillCount) {
        this.fulfillCount = fulfillCount;
    }


    /**
     * Gets the startDate value for this AdvancedFulfillmentLCDataType.
     * 
     * @return startDate
     */
    public java.util.Date getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this AdvancedFulfillmentLCDataType.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the versionDate value for this AdvancedFulfillmentLCDataType.
     * 
     * @return versionDate
     */
    public java.util.Date getVersionDate() {
        return versionDate;
    }


    /**
     * Sets the versionDate value for this AdvancedFulfillmentLCDataType.
     * 
     * @param versionDate
     */
    public void setVersionDate(java.util.Date versionDate) {
        this.versionDate = versionDate;
    }


    /**
     * Gets the versionStartDate value for this AdvancedFulfillmentLCDataType.
     * 
     * @return versionStartDate
     */
    public java.util.Date getVersionStartDate() {
        return versionStartDate;
    }


    /**
     * Sets the versionStartDate value for this AdvancedFulfillmentLCDataType.
     * 
     * @param versionStartDate
     */
    public void setVersionStartDate(java.util.Date versionStartDate) {
        this.versionStartDate = versionStartDate;
    }


    /**
     * Gets the soldTo value for this AdvancedFulfillmentLCDataType.
     * 
     * @return soldTo
     */
    public java.lang.String getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this AdvancedFulfillmentLCDataType.
     * 
     * @param soldTo
     */
    public void setSoldTo(java.lang.String soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the shipToEmail value for this AdvancedFulfillmentLCDataType.
     * 
     * @return shipToEmail
     */
    public java.lang.String getShipToEmail() {
        return shipToEmail;
    }


    /**
     * Sets the shipToEmail value for this AdvancedFulfillmentLCDataType.
     * 
     * @param shipToEmail
     */
    public void setShipToEmail(java.lang.String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }


    /**
     * Gets the shipToAddress value for this AdvancedFulfillmentLCDataType.
     * 
     * @return shipToAddress
     */
    public java.lang.String getShipToAddress() {
        return shipToAddress;
    }


    /**
     * Sets the shipToAddress value for this AdvancedFulfillmentLCDataType.
     * 
     * @param shipToAddress
     */
    public void setShipToAddress(java.lang.String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }


    /**
     * Gets the serverHost value for this AdvancedFulfillmentLCDataType.
     * 
     * @return serverHost
     */
    public flexnet.macrovision.com.ServerIDsType getServerHost() {
        return serverHost;
    }


    /**
     * Sets the serverHost value for this AdvancedFulfillmentLCDataType.
     * 
     * @param serverHost
     */
    public void setServerHost(flexnet.macrovision.com.ServerIDsType serverHost) {
        this.serverHost = serverHost;
    }


    /**
     * Gets the nodeLockHost value for this AdvancedFulfillmentLCDataType.
     * 
     * @return nodeLockHost
     */
    public java.lang.String getNodeLockHost() {
        return nodeLockHost;
    }


    /**
     * Sets the nodeLockHost value for this AdvancedFulfillmentLCDataType.
     * 
     * @param nodeLockHost
     */
    public void setNodeLockHost(java.lang.String nodeLockHost) {
        this.nodeLockHost = nodeLockHost;
    }


    /**
     * Gets the countedNodeLockHostIds value for this AdvancedFulfillmentLCDataType.
     * 
     * @return countedNodeLockHostIds
     */
    public java.lang.String[] getCountedNodeLockHostIds() {
        return countedNodeLockHostIds;
    }


    /**
     * Sets the countedNodeLockHostIds value for this AdvancedFulfillmentLCDataType.
     * 
     * @param countedNodeLockHostIds
     */
    public void setCountedNodeLockHostIds(java.lang.String[] countedNodeLockHostIds) {
        this.countedNodeLockHostIds = countedNodeLockHostIds;
    }


    /**
     * Gets the customHost value for this AdvancedFulfillmentLCDataType.
     * 
     * @return customHost
     */
    public flexnet.macrovision.com.CustomHostIDType getCustomHost() {
        return customHost;
    }


    /**
     * Sets the customHost value for this AdvancedFulfillmentLCDataType.
     * 
     * @param customHost
     */
    public void setCustomHost(flexnet.macrovision.com.CustomHostIDType customHost) {
        this.customHost = customHost;
    }


    /**
     * Gets the licenseModelAttributes value for this AdvancedFulfillmentLCDataType.
     * 
     * @return licenseModelAttributes
     */
    public flexnet.macrovision.com.AttributeDescriptorType[] getLicenseModelAttributes() {
        return licenseModelAttributes;
    }


    /**
     * Sets the licenseModelAttributes value for this AdvancedFulfillmentLCDataType.
     * 
     * @param licenseModelAttributes
     */
    public void setLicenseModelAttributes(flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes) {
        this.licenseModelAttributes = licenseModelAttributes;
    }


    /**
     * Gets the overridePolicy value for this AdvancedFulfillmentLCDataType.
     * 
     * @return overridePolicy
     */
    public java.lang.Boolean getOverridePolicy() {
        return overridePolicy;
    }


    /**
     * Sets the overridePolicy value for this AdvancedFulfillmentLCDataType.
     * 
     * @param overridePolicy
     */
    public void setOverridePolicy(java.lang.Boolean overridePolicy) {
        this.overridePolicy = overridePolicy;
    }


    /**
     * Gets the FNPTimeZoneValue value for this AdvancedFulfillmentLCDataType.
     * 
     * @return FNPTimeZoneValue
     */
    public java.lang.String getFNPTimeZoneValue() {
        return FNPTimeZoneValue;
    }


    /**
     * Sets the FNPTimeZoneValue value for this AdvancedFulfillmentLCDataType.
     * 
     * @param FNPTimeZoneValue
     */
    public void setFNPTimeZoneValue(java.lang.String FNPTimeZoneValue) {
        this.FNPTimeZoneValue = FNPTimeZoneValue;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdvancedFulfillmentLCDataType)) return false;
        AdvancedFulfillmentLCDataType other = (AdvancedFulfillmentLCDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.childActivationId==null && other.getChildActivationId()==null) || 
             (this.childActivationId!=null &&
              this.childActivationId.equals(other.getChildActivationId()))) &&
            ((this.fulfillCount==null && other.getFulfillCount()==null) || 
             (this.fulfillCount!=null &&
              this.fulfillCount.equals(other.getFulfillCount()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.versionDate==null && other.getVersionDate()==null) || 
             (this.versionDate!=null &&
              this.versionDate.equals(other.getVersionDate()))) &&
            ((this.versionStartDate==null && other.getVersionStartDate()==null) || 
             (this.versionStartDate!=null &&
              this.versionStartDate.equals(other.getVersionStartDate()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.shipToEmail==null && other.getShipToEmail()==null) || 
             (this.shipToEmail!=null &&
              this.shipToEmail.equals(other.getShipToEmail()))) &&
            ((this.shipToAddress==null && other.getShipToAddress()==null) || 
             (this.shipToAddress!=null &&
              this.shipToAddress.equals(other.getShipToAddress()))) &&
            ((this.serverHost==null && other.getServerHost()==null) || 
             (this.serverHost!=null &&
              this.serverHost.equals(other.getServerHost()))) &&
            ((this.nodeLockHost==null && other.getNodeLockHost()==null) || 
             (this.nodeLockHost!=null &&
              this.nodeLockHost.equals(other.getNodeLockHost()))) &&
            ((this.countedNodeLockHostIds==null && other.getCountedNodeLockHostIds()==null) || 
             (this.countedNodeLockHostIds!=null &&
              java.util.Arrays.equals(this.countedNodeLockHostIds, other.getCountedNodeLockHostIds()))) &&
            ((this.customHost==null && other.getCustomHost()==null) || 
             (this.customHost!=null &&
              this.customHost.equals(other.getCustomHost()))) &&
            ((this.licenseModelAttributes==null && other.getLicenseModelAttributes()==null) || 
             (this.licenseModelAttributes!=null &&
              java.util.Arrays.equals(this.licenseModelAttributes, other.getLicenseModelAttributes()))) &&
            ((this.overridePolicy==null && other.getOverridePolicy()==null) || 
             (this.overridePolicy!=null &&
              this.overridePolicy.equals(other.getOverridePolicy()))) &&
            ((this.FNPTimeZoneValue==null && other.getFNPTimeZoneValue()==null) || 
             (this.FNPTimeZoneValue!=null &&
              this.FNPTimeZoneValue.equals(other.getFNPTimeZoneValue())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getChildActivationId() != null) {
            _hashCode += getChildActivationId().hashCode();
        }
        if (getFulfillCount() != null) {
            _hashCode += getFulfillCount().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getVersionDate() != null) {
            _hashCode += getVersionDate().hashCode();
        }
        if (getVersionStartDate() != null) {
            _hashCode += getVersionStartDate().hashCode();
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getShipToEmail() != null) {
            _hashCode += getShipToEmail().hashCode();
        }
        if (getShipToAddress() != null) {
            _hashCode += getShipToAddress().hashCode();
        }
        if (getServerHost() != null) {
            _hashCode += getServerHost().hashCode();
        }
        if (getNodeLockHost() != null) {
            _hashCode += getNodeLockHost().hashCode();
        }
        if (getCountedNodeLockHostIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCountedNodeLockHostIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCountedNodeLockHostIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHost() != null) {
            _hashCode += getCustomHost().hashCode();
        }
        if (getLicenseModelAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseModelAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseModelAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOverridePolicy() != null) {
            _hashCode += getOverridePolicy().hashCode();
        }
        if (getFNPTimeZoneValue() != null) {
            _hashCode += getFNPTimeZoneValue().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdvancedFulfillmentLCDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childActivationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "childActivationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "versionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "versionStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serverHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "serverHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeLockHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeLockHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countedNodeLockHostIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countedNodeLockHostIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseModelAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModelAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overridePolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overridePolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FNPTimeZoneValue");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FNPTimeZoneValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
