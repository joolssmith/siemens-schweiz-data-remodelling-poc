/**
 * AdvancedFulfillmentLCRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class AdvancedFulfillmentLCRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.AdvancedFulfillmentLCDataType[] fulfillmentList;

    public AdvancedFulfillmentLCRequestType() {
    }

    public AdvancedFulfillmentLCRequestType(
           flexnet.macrovision.com.AdvancedFulfillmentLCDataType[] fulfillmentList) {
           this.fulfillmentList = fulfillmentList;
    }


    /**
     * Gets the fulfillmentList value for this AdvancedFulfillmentLCRequestType.
     * 
     * @return fulfillmentList
     */
    public flexnet.macrovision.com.AdvancedFulfillmentLCDataType[] getFulfillmentList() {
        return fulfillmentList;
    }


    /**
     * Sets the fulfillmentList value for this AdvancedFulfillmentLCRequestType.
     * 
     * @param fulfillmentList
     */
    public void setFulfillmentList(flexnet.macrovision.com.AdvancedFulfillmentLCDataType[] fulfillmentList) {
        this.fulfillmentList = fulfillmentList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdvancedFulfillmentLCRequestType)) return false;
        AdvancedFulfillmentLCRequestType other = (AdvancedFulfillmentLCRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentList==null && other.getFulfillmentList()==null) || 
             (this.fulfillmentList!=null &&
              java.util.Arrays.equals(this.fulfillmentList, other.getFulfillmentList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFulfillmentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFulfillmentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdvancedFulfillmentLCRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCDataType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
