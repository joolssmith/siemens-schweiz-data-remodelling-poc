/**
 * FulfillmentsQueryParametersType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FulfillmentsQueryParametersType  implements java.io.Serializable {
    private flexnet.macrovision.com.SimpleQueryType entitlementId;

    private flexnet.macrovision.com.SimpleQueryType activationId;

    private flexnet.macrovision.com.SimpleQueryType fulfillmentId;

    private flexnet.macrovision.com.SimpleQueryType product;

    private flexnet.macrovision.com.SimpleQueryType hostId;

    private flexnet.macrovision.com.SimpleQueryType nodeLockHostId;

    private flexnet.macrovision.com.SimpleQueryType soldTo;

    private flexnet.macrovision.com.DateQueryType fulfillDate;

    private flexnet.macrovision.com.DateTimeQueryType fulfillDateTime;

    private flexnet.macrovision.com.DateQueryType startDate;

    private flexnet.macrovision.com.DateQueryType expirationDate;

    private flexnet.macrovision.com.DateTimeQueryType lastModifiedDateTime;

    private flexnet.macrovision.com.StateQueryType state;

    private flexnet.macrovision.com.SimpleQueryType licenseTechnology;

    private java.lang.String userId;

    private flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource;

    private flexnet.macrovision.com.CustomAttributeQueryType[] customAttributes;

    private flexnet.macrovision.com.CustomAttributeQueryType[] customHostAttributes;

    private flexnet.macrovision.com.ActivationType activationType;

    public FulfillmentsQueryParametersType() {
    }

    public FulfillmentsQueryParametersType(
           flexnet.macrovision.com.SimpleQueryType entitlementId,
           flexnet.macrovision.com.SimpleQueryType activationId,
           flexnet.macrovision.com.SimpleQueryType fulfillmentId,
           flexnet.macrovision.com.SimpleQueryType product,
           flexnet.macrovision.com.SimpleQueryType hostId,
           flexnet.macrovision.com.SimpleQueryType nodeLockHostId,
           flexnet.macrovision.com.SimpleQueryType soldTo,
           flexnet.macrovision.com.DateQueryType fulfillDate,
           flexnet.macrovision.com.DateTimeQueryType fulfillDateTime,
           flexnet.macrovision.com.DateQueryType startDate,
           flexnet.macrovision.com.DateQueryType expirationDate,
           flexnet.macrovision.com.DateTimeQueryType lastModifiedDateTime,
           flexnet.macrovision.com.StateQueryType state,
           flexnet.macrovision.com.SimpleQueryType licenseTechnology,
           java.lang.String userId,
           flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource,
           flexnet.macrovision.com.CustomAttributeQueryType[] customAttributes,
           flexnet.macrovision.com.CustomAttributeQueryType[] customHostAttributes,
           flexnet.macrovision.com.ActivationType activationType) {
           this.entitlementId = entitlementId;
           this.activationId = activationId;
           this.fulfillmentId = fulfillmentId;
           this.product = product;
           this.hostId = hostId;
           this.nodeLockHostId = nodeLockHostId;
           this.soldTo = soldTo;
           this.fulfillDate = fulfillDate;
           this.fulfillDateTime = fulfillDateTime;
           this.startDate = startDate;
           this.expirationDate = expirationDate;
           this.lastModifiedDateTime = lastModifiedDateTime;
           this.state = state;
           this.licenseTechnology = licenseTechnology;
           this.userId = userId;
           this.fulfillmentSource = fulfillmentSource;
           this.customAttributes = customAttributes;
           this.customHostAttributes = customHostAttributes;
           this.activationType = activationType;
    }


    /**
     * Gets the entitlementId value for this FulfillmentsQueryParametersType.
     * 
     * @return entitlementId
     */
    public flexnet.macrovision.com.SimpleQueryType getEntitlementId() {
        return entitlementId;
    }


    /**
     * Sets the entitlementId value for this FulfillmentsQueryParametersType.
     * 
     * @param entitlementId
     */
    public void setEntitlementId(flexnet.macrovision.com.SimpleQueryType entitlementId) {
        this.entitlementId = entitlementId;
    }


    /**
     * Gets the activationId value for this FulfillmentsQueryParametersType.
     * 
     * @return activationId
     */
    public flexnet.macrovision.com.SimpleQueryType getActivationId() {
        return activationId;
    }


    /**
     * Sets the activationId value for this FulfillmentsQueryParametersType.
     * 
     * @param activationId
     */
    public void setActivationId(flexnet.macrovision.com.SimpleQueryType activationId) {
        this.activationId = activationId;
    }


    /**
     * Gets the fulfillmentId value for this FulfillmentsQueryParametersType.
     * 
     * @return fulfillmentId
     */
    public flexnet.macrovision.com.SimpleQueryType getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this FulfillmentsQueryParametersType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(flexnet.macrovision.com.SimpleQueryType fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the product value for this FulfillmentsQueryParametersType.
     * 
     * @return product
     */
    public flexnet.macrovision.com.SimpleQueryType getProduct() {
        return product;
    }


    /**
     * Sets the product value for this FulfillmentsQueryParametersType.
     * 
     * @param product
     */
    public void setProduct(flexnet.macrovision.com.SimpleQueryType product) {
        this.product = product;
    }


    /**
     * Gets the hostId value for this FulfillmentsQueryParametersType.
     * 
     * @return hostId
     */
    public flexnet.macrovision.com.SimpleQueryType getHostId() {
        return hostId;
    }


    /**
     * Sets the hostId value for this FulfillmentsQueryParametersType.
     * 
     * @param hostId
     */
    public void setHostId(flexnet.macrovision.com.SimpleQueryType hostId) {
        this.hostId = hostId;
    }


    /**
     * Gets the nodeLockHostId value for this FulfillmentsQueryParametersType.
     * 
     * @return nodeLockHostId
     */
    public flexnet.macrovision.com.SimpleQueryType getNodeLockHostId() {
        return nodeLockHostId;
    }


    /**
     * Sets the nodeLockHostId value for this FulfillmentsQueryParametersType.
     * 
     * @param nodeLockHostId
     */
    public void setNodeLockHostId(flexnet.macrovision.com.SimpleQueryType nodeLockHostId) {
        this.nodeLockHostId = nodeLockHostId;
    }


    /**
     * Gets the soldTo value for this FulfillmentsQueryParametersType.
     * 
     * @return soldTo
     */
    public flexnet.macrovision.com.SimpleQueryType getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this FulfillmentsQueryParametersType.
     * 
     * @param soldTo
     */
    public void setSoldTo(flexnet.macrovision.com.SimpleQueryType soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the fulfillDate value for this FulfillmentsQueryParametersType.
     * 
     * @return fulfillDate
     */
    public flexnet.macrovision.com.DateQueryType getFulfillDate() {
        return fulfillDate;
    }


    /**
     * Sets the fulfillDate value for this FulfillmentsQueryParametersType.
     * 
     * @param fulfillDate
     */
    public void setFulfillDate(flexnet.macrovision.com.DateQueryType fulfillDate) {
        this.fulfillDate = fulfillDate;
    }


    /**
     * Gets the fulfillDateTime value for this FulfillmentsQueryParametersType.
     * 
     * @return fulfillDateTime
     */
    public flexnet.macrovision.com.DateTimeQueryType getFulfillDateTime() {
        return fulfillDateTime;
    }


    /**
     * Sets the fulfillDateTime value for this FulfillmentsQueryParametersType.
     * 
     * @param fulfillDateTime
     */
    public void setFulfillDateTime(flexnet.macrovision.com.DateTimeQueryType fulfillDateTime) {
        this.fulfillDateTime = fulfillDateTime;
    }


    /**
     * Gets the startDate value for this FulfillmentsQueryParametersType.
     * 
     * @return startDate
     */
    public flexnet.macrovision.com.DateQueryType getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this FulfillmentsQueryParametersType.
     * 
     * @param startDate
     */
    public void setStartDate(flexnet.macrovision.com.DateQueryType startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the expirationDate value for this FulfillmentsQueryParametersType.
     * 
     * @return expirationDate
     */
    public flexnet.macrovision.com.DateQueryType getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this FulfillmentsQueryParametersType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(flexnet.macrovision.com.DateQueryType expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the lastModifiedDateTime value for this FulfillmentsQueryParametersType.
     * 
     * @return lastModifiedDateTime
     */
    public flexnet.macrovision.com.DateTimeQueryType getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }


    /**
     * Sets the lastModifiedDateTime value for this FulfillmentsQueryParametersType.
     * 
     * @param lastModifiedDateTime
     */
    public void setLastModifiedDateTime(flexnet.macrovision.com.DateTimeQueryType lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }


    /**
     * Gets the state value for this FulfillmentsQueryParametersType.
     * 
     * @return state
     */
    public flexnet.macrovision.com.StateQueryType getState() {
        return state;
    }


    /**
     * Sets the state value for this FulfillmentsQueryParametersType.
     * 
     * @param state
     */
    public void setState(flexnet.macrovision.com.StateQueryType state) {
        this.state = state;
    }


    /**
     * Gets the licenseTechnology value for this FulfillmentsQueryParametersType.
     * 
     * @return licenseTechnology
     */
    public flexnet.macrovision.com.SimpleQueryType getLicenseTechnology() {
        return licenseTechnology;
    }


    /**
     * Sets the licenseTechnology value for this FulfillmentsQueryParametersType.
     * 
     * @param licenseTechnology
     */
    public void setLicenseTechnology(flexnet.macrovision.com.SimpleQueryType licenseTechnology) {
        this.licenseTechnology = licenseTechnology;
    }


    /**
     * Gets the userId value for this FulfillmentsQueryParametersType.
     * 
     * @return userId
     */
    public java.lang.String getUserId() {
        return userId;
    }


    /**
     * Sets the userId value for this FulfillmentsQueryParametersType.
     * 
     * @param userId
     */
    public void setUserId(java.lang.String userId) {
        this.userId = userId;
    }


    /**
     * Gets the fulfillmentSource value for this FulfillmentsQueryParametersType.
     * 
     * @return fulfillmentSource
     */
    public flexnet.macrovision.com.FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }


    /**
     * Sets the fulfillmentSource value for this FulfillmentsQueryParametersType.
     * 
     * @param fulfillmentSource
     */
    public void setFulfillmentSource(flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource) {
        this.fulfillmentSource = fulfillmentSource;
    }


    /**
     * Gets the customAttributes value for this FulfillmentsQueryParametersType.
     * 
     * @return customAttributes
     */
    public flexnet.macrovision.com.CustomAttributeQueryType[] getCustomAttributes() {
        return customAttributes;
    }


    /**
     * Sets the customAttributes value for this FulfillmentsQueryParametersType.
     * 
     * @param customAttributes
     */
    public void setCustomAttributes(flexnet.macrovision.com.CustomAttributeQueryType[] customAttributes) {
        this.customAttributes = customAttributes;
    }


    /**
     * Gets the customHostAttributes value for this FulfillmentsQueryParametersType.
     * 
     * @return customHostAttributes
     */
    public flexnet.macrovision.com.CustomAttributeQueryType[] getCustomHostAttributes() {
        return customHostAttributes;
    }


    /**
     * Sets the customHostAttributes value for this FulfillmentsQueryParametersType.
     * 
     * @param customHostAttributes
     */
    public void setCustomHostAttributes(flexnet.macrovision.com.CustomAttributeQueryType[] customHostAttributes) {
        this.customHostAttributes = customHostAttributes;
    }


    /**
     * Gets the activationType value for this FulfillmentsQueryParametersType.
     * 
     * @return activationType
     */
    public flexnet.macrovision.com.ActivationType getActivationType() {
        return activationType;
    }


    /**
     * Sets the activationType value for this FulfillmentsQueryParametersType.
     * 
     * @param activationType
     */
    public void setActivationType(flexnet.macrovision.com.ActivationType activationType) {
        this.activationType = activationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FulfillmentsQueryParametersType)) return false;
        FulfillmentsQueryParametersType other = (FulfillmentsQueryParametersType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.entitlementId==null && other.getEntitlementId()==null) || 
             (this.entitlementId!=null &&
              this.entitlementId.equals(other.getEntitlementId()))) &&
            ((this.activationId==null && other.getActivationId()==null) || 
             (this.activationId!=null &&
              this.activationId.equals(other.getActivationId()))) &&
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              this.product.equals(other.getProduct()))) &&
            ((this.hostId==null && other.getHostId()==null) || 
             (this.hostId!=null &&
              this.hostId.equals(other.getHostId()))) &&
            ((this.nodeLockHostId==null && other.getNodeLockHostId()==null) || 
             (this.nodeLockHostId!=null &&
              this.nodeLockHostId.equals(other.getNodeLockHostId()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.fulfillDate==null && other.getFulfillDate()==null) || 
             (this.fulfillDate!=null &&
              this.fulfillDate.equals(other.getFulfillDate()))) &&
            ((this.fulfillDateTime==null && other.getFulfillDateTime()==null) || 
             (this.fulfillDateTime!=null &&
              this.fulfillDateTime.equals(other.getFulfillDateTime()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.lastModifiedDateTime==null && other.getLastModifiedDateTime()==null) || 
             (this.lastModifiedDateTime!=null &&
              this.lastModifiedDateTime.equals(other.getLastModifiedDateTime()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.licenseTechnology==null && other.getLicenseTechnology()==null) || 
             (this.licenseTechnology!=null &&
              this.licenseTechnology.equals(other.getLicenseTechnology()))) &&
            ((this.userId==null && other.getUserId()==null) || 
             (this.userId!=null &&
              this.userId.equals(other.getUserId()))) &&
            ((this.fulfillmentSource==null && other.getFulfillmentSource()==null) || 
             (this.fulfillmentSource!=null &&
              this.fulfillmentSource.equals(other.getFulfillmentSource()))) &&
            ((this.customAttributes==null && other.getCustomAttributes()==null) || 
             (this.customAttributes!=null &&
              java.util.Arrays.equals(this.customAttributes, other.getCustomAttributes()))) &&
            ((this.customHostAttributes==null && other.getCustomHostAttributes()==null) || 
             (this.customHostAttributes!=null &&
              java.util.Arrays.equals(this.customHostAttributes, other.getCustomHostAttributes()))) &&
            ((this.activationType==null && other.getActivationType()==null) || 
             (this.activationType!=null &&
              this.activationType.equals(other.getActivationType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEntitlementId() != null) {
            _hashCode += getEntitlementId().hashCode();
        }
        if (getActivationId() != null) {
            _hashCode += getActivationId().hashCode();
        }
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getProduct() != null) {
            _hashCode += getProduct().hashCode();
        }
        if (getHostId() != null) {
            _hashCode += getHostId().hashCode();
        }
        if (getNodeLockHostId() != null) {
            _hashCode += getNodeLockHostId().hashCode();
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getFulfillDate() != null) {
            _hashCode += getFulfillDate().hashCode();
        }
        if (getFulfillDateTime() != null) {
            _hashCode += getFulfillDateTime().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getLastModifiedDateTime() != null) {
            _hashCode += getLastModifiedDateTime().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getLicenseTechnology() != null) {
            _hashCode += getLicenseTechnology().hashCode();
        }
        if (getUserId() != null) {
            _hashCode += getUserId().hashCode();
        }
        if (getFulfillmentSource() != null) {
            _hashCode += getFulfillmentSource().hashCode();
        }
        if (getCustomAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHostAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomHostAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomHostAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getActivationType() != null) {
            _hashCode += getActivationType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FulfillmentsQueryParametersType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentsQueryParametersType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitlementId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "product"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeLockHostId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeLockHostId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateTimeQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastModifiedDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lastModifiedDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateTimeQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseTechnology");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnology"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "userId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentSource");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FulfillmentSourceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHostAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHostAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ActivationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
