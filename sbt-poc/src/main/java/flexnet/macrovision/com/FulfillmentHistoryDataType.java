/**
 * FulfillmentHistoryDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FulfillmentHistoryDataType  implements java.io.Serializable {
    private java.lang.String activationId;

    private flexnet.macrovision.com.FulfillmentHistoryRecordType[] historyDetails;

    public FulfillmentHistoryDataType() {
    }

    public FulfillmentHistoryDataType(
           java.lang.String activationId,
           flexnet.macrovision.com.FulfillmentHistoryRecordType[] historyDetails) {
           this.activationId = activationId;
           this.historyDetails = historyDetails;
    }


    /**
     * Gets the activationId value for this FulfillmentHistoryDataType.
     * 
     * @return activationId
     */
    public java.lang.String getActivationId() {
        return activationId;
    }


    /**
     * Sets the activationId value for this FulfillmentHistoryDataType.
     * 
     * @param activationId
     */
    public void setActivationId(java.lang.String activationId) {
        this.activationId = activationId;
    }


    /**
     * Gets the historyDetails value for this FulfillmentHistoryDataType.
     * 
     * @return historyDetails
     */
    public flexnet.macrovision.com.FulfillmentHistoryRecordType[] getHistoryDetails() {
        return historyDetails;
    }


    /**
     * Sets the historyDetails value for this FulfillmentHistoryDataType.
     * 
     * @param historyDetails
     */
    public void setHistoryDetails(flexnet.macrovision.com.FulfillmentHistoryRecordType[] historyDetails) {
        this.historyDetails = historyDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FulfillmentHistoryDataType)) return false;
        FulfillmentHistoryDataType other = (FulfillmentHistoryDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.activationId==null && other.getActivationId()==null) || 
             (this.activationId!=null &&
              this.activationId.equals(other.getActivationId()))) &&
            ((this.historyDetails==null && other.getHistoryDetails()==null) || 
             (this.historyDetails!=null &&
              java.util.Arrays.equals(this.historyDetails, other.getHistoryDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActivationId() != null) {
            _hashCode += getActivationId().hashCode();
        }
        if (getHistoryDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHistoryDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHistoryDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FulfillmentHistoryDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("historyDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "historyDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryRecordType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "record"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
