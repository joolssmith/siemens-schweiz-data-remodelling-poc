/**
 * LicenseServiceSoapBindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class LicenseServiceSoapBindingStub extends org.apache.axis.client.Stub implements flexnet.macrovision.com.LicenseFulfillmentServiceInterface {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[36];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
        _initOperationDesc4();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFulfillmentCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountRequestType"), flexnet.macrovision.com.GetFulfillmentCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetFulfillmentCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFulfillmentsQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryRequestType"), flexnet.macrovision.com.GetFulfillmentsQueryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetFulfillmentsQueryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFulfillmentPropertiesQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesRequestType"), flexnet.macrovision.com.GetFulfillmentPropertiesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetFulfillmentPropertiesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("rehostLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentRequestType"), flexnet.macrovision.com.RehostFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.RehostFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("returnLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentRequestType"), flexnet.macrovision.com.ReturnFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.ReturnFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("repairLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentRequestType"), flexnet.macrovision.com.RepairFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.RepairFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("emergencyLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentRequestType"), flexnet.macrovision.com.EmergencyFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.EmergencyFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("publisherErrorLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentRequestType"), flexnet.macrovision.com.PublisherErrorFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.PublisherErrorFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("stopGapLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentRequestType"), flexnet.macrovision.com.StopGapFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.StopGapFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFulfillmentAttributesFromModel");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesRequestType"), flexnet.macrovision.com.GetFulfillmentAttributesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetFulfillmentAttributesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getHostAttributesFromLicenseTechnology");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesRequestType"), flexnet.macrovision.com.GetHostAttributesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetHostAttributesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("verifyCreateLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "verifyCreateLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentRequestType"), flexnet.macrovision.com.CreateFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.CreateFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "verifyCreateLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentRequestType"), flexnet.macrovision.com.CreateFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.CreateFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("activateShortCode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeRequestType"), flexnet.macrovision.com.ActivateShortCodeRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.ActivateShortCodeResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("repairShortCode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeRequestType"), flexnet.macrovision.com.RepairShortCodeRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.RepairShortCodeResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("returnShortCode");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeRequestType"), flexnet.macrovision.com.ReturnShortCodeRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.ReturnShortCodeResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("emailLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseRequestType"), flexnet.macrovision.com.EmailLicenseRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.EmailLicenseResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("consolidateFulfillments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsRequestType"), flexnet.macrovision.com.ConsolidateFulfillmentsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.ConsolidateFulfillmentsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getConsolidatedFulfillmentCount");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountRequestType"), flexnet.macrovision.com.GetConsolidatedFulfillmentCountRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getConsolidatedFulfillmentsQuery");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryRequestType"), flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFulfillmentAttributesForBatchActivation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationRequestType"), flexnet.macrovision.com.GetFmtAttributesForBatchActivationRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createLicensesAsBatch");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchRequestType"), flexnet.macrovision.com.CreateLicensesAsBatchRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.CreateLicensesAsBatchResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createLicensesAsBatchAndConsolidate");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchAndConsolidateRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchRequestType"), flexnet.macrovision.com.CreateLicensesAsBatchRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.ConsolidateFulfillmentsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchAndConsolidateResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("emailConsolidatedLicenses");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesRequestType"), flexnet.macrovision.com.EmailConsolidatedLicensesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.EmailConsolidatedLicensesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("manualActivation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "manualActivationRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedRequestType"), flexnet.macrovision.com.TrustedRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.TrustedResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "manualActivationResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[24] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("manualRepair");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "manualRepairRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedRequestType"), flexnet.macrovision.com.TrustedRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.TrustedResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "manualRepairResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[25] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("manualReturn");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "manualReturnRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedRequestType"), flexnet.macrovision.com.TrustedRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.TrustedResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "manualReturnResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[26] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getFulfillmentHistory");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryRequestType"), flexnet.macrovision.com.GetFulfillmentHistoryRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.GetFulfillmentHistoryResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[27] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("createChildLineItemFulfillment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentRequestType"), flexnet.macrovision.com.CreateChildLineItemFulfillmentDataType[].class, false, false);
        param.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[28] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("upgradeFulfillment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "upgradeFulfillmentRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCRequestType"), flexnet.macrovision.com.AdvancedFulfillmentLCRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "upgradeFulfillmentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[29] = oper;

    }

    private static void _initOperationDesc4(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("upsellFulfillment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "upsellFulfillmentRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCRequestType"), flexnet.macrovision.com.AdvancedFulfillmentLCRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "upsellFulfillmentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[30] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("renewFulfillment");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "renewFulfillmentRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCRequestType"), flexnet.macrovision.com.AdvancedFulfillmentLCRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "renewFulfillmentResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[31] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setLicense");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseRequestType"), flexnet.macrovision.com.SetLicenseRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.SetLicenseResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[32] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteOnholdFulfillments");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsRequestType"), flexnet.macrovision.com.DeleteOnholdFulfillmentsRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[33] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("offlineFNPTrustedStorageActivation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateLicensesRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateLicensesRequestType"), flexnet.macrovision.com.ActivateLicensesRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateLicensesResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.ActivateLicensesResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateLicensesResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[34] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("transferHost");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostRequest"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostRequestType"), flexnet.macrovision.com.TransferHostRequestType.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostResponseType"));
        oper.setReturnClass(flexnet.macrovision.com.TransferHostResponseType.class);
        oper.setReturnQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostResponse"));
        oper.setStyle(org.apache.axis.constants.Style.DOCUMENT);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[35] = oper;

    }

    public LicenseServiceSoapBindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public LicenseServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public LicenseServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        addBindings0();
        addBindings1();
        addBindings2();
    }

    private void addBindings0() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateLicensesRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivateLicensesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateLicensesResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivateLicensesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivateShortCodeRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivateShortCodeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivationDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIdOverDraftMapType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivationIdOverDraftMapType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIdsListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ActivationType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFmtLCResponseDataListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFmtLCResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFmtLCResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFmtLCResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFmtLCResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFulfillmentLCDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCInfoType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFulfillmentLCInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFulfillmentLCDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFulfillmentLCRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "AttributeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AttributeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AttributeDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AttributeDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeMetaDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AttributeMetaDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeMetaDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeMetaDescriptorType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.AttributeMetaDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "commonBatchDataSetType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CommonBatchDataSetType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedFulfillmentsQPType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ConsolidatedFulfillmentsQPType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedHostLicenseDataType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "license");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ConsolidatedLicenseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseIdListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseId");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ConsolidatedLicenseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicense");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ConsolidateFulfillmentsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ConsolidateFulfillmentsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countDataSetType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CountDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CountDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countForHostsType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CountForHostsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateChildLineItemFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateChildLineItemFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createChildLineItemFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdChildLIFmtResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreatedChildLIFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdChildLIFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdChildLIFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreatedChildLIFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdChildLIFulfillmentInfoType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreatedChildLIFulfillmentInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdFulfillmentDataListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreatedFulfillmentDataListType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreatedFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createdShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreatedShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateLicensesAsBatchRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateLicensesAsBatchResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CreateShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeDescriptorDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CustomAttributeDescriptorType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeDescriptorType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeDescriptorType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CustomAttributeDescriptorType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeQueryType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CustomAttributeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributesQueryListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CustomAttributeQueryType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeQueryType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.CustomHostIDType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "datedSearchType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DatedSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateQueryType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DateQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateTimeQueryType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DateTimeQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DeleteOnholdFulfillmentsRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "Dictionary");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.Dictionary.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DictionaryEntriesCollection");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DictionaryEntry[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DictionaryEntry");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "Entry");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DictionaryEntry");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.DictionaryEntry.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "duplicateFulfillmentRecordListDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmailConsolidatedLicensesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmailConsolidatedLicensesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailContactListType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailId");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmailLicenseRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmailLicenseResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmergencyFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmergencyFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmergencyFulfillmentResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmergencyFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EmergencyFulfillmentResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emergencyFulfillmentResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProductDataListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EntitledProductDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProductDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProduct");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProductDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EntitledProductDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EntitlementIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementLineItemIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EntitlementLineItemIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementLineItemPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EntitlementLineItemPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.EntitlementPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedAdvancedFmtLCDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedAdvancedFmtLCDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedAdvancedFmtLCResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedAdvancedFmtLCDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedAdvancedFmtLCDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedChildLIFmtResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedChildLIFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedChildLIFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedChildLIFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedChildLIFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedEmergencyResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedEmergencyResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedEmergencyResponselistDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedEmergencyResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedEmergencyResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillmentDataListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedLineItem");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedOnholdFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedOnholdFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedOnholdFulfillmentListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedOnholdFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedOnholdFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedPublisherErrorResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedPublisherErrorResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedPublisherErrorResponselistDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedPublisherErrorResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedPublisherErrorResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRehostResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedRehostResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRehostResponselistDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedRehostResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRehostResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRepairResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedRepairResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRepairResponselistDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedRepairResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRepairResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedRepairShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedRepairShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedReturnResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedReturnResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedReturnResponselistDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedReturnResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedReturnResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedReturnShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedReturnShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedSetLicenseOnholdFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedSetLicenseOnholdFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedSetLicenseOnholdFulfillmentListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedSetLicenseOnholdFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedSetLicenseOnholdFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFmtData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

    }
    private void addBindings1() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedStopGapResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedStopGapResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedStopGapResponselistDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedStopGapResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedStopGapResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedTransferHostDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedTransferHostDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedTransferHostListDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FailedTransferHostDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedTransferHostDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedHost");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentHistoryDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryDetailsType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentHistoryRecordType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryRecordType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "record");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryRecordType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentHistoryRecordType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentIdentifierType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentPropertiesType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentPropertiesType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentResponseConfigRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentResponseConfigRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FulfillmentSourceType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentSourceType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentsQueryParametersType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentsQueryParametersType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetConsolidatedFulfillmentCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ConsolidatedLicenseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicense");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFmtAttributesForBatchActivationRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFmtAttributesForBatchDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentAttributesDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentAttributesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentAttributesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentCountRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentCountResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentCountResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentCountResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentHistoryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentHistoryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentPropertiesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentPropertiesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentsQueryRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.FulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentsQueryResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetFulfillmentsQueryResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetHostAttributesDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetHostAttributesRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.GetHostAttributesResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataDetailsType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.HostIdDataDetailsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataSetType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.HostIdDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.HostIdDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDetailsType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.HostIdDataDetailsType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataDetailsType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostTypePKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.HostTypePKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.LicenseFileDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFile");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.LicenseFileDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModelIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.LicenseModelIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModelPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.LicenseModelPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnologyIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.LicenseTechnologyIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnologyPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.LicenseTechnologyPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "NodeIDsType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "NumberQueryType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.NumberQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "numberSearchType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.NumberSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onHoldFmtLicenseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.OnHoldFmtLicenseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onholdFulfillmentListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.OnHoldFmtLicenseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onHoldFmtLicenseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onholdFmtLicenseData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "organizationIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.OrganizationIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "organizationPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.OrganizationPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftDataListType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ActivationIdOverDraftMapType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIdOverDraftMapType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIdMap");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumberIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PartNumberIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumberPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PartNumberPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "productIdentifierType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ProductIdentifierType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "productPKType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ProductPKType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherAttributesListDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SimpleAttributeDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "simpleAttributeDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PublisherErrorFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PublisherErrorFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PublisherErrorFulfillmentResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PublisherErrorFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.PublisherErrorFulfillmentResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RehostFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RehostFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RehostFulfillmentResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RehostFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RehostFulfillmentResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairedShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairedShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairFulfillmentResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairFulfillmentResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairFulfillmentResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairShortCodeRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.RepairShortCodeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnedShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnedShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ReturnedShortCodeReturnReason");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnedShortCodeReturnReason.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnFulfillmentResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnFulfillmentResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnShortCodeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnShortCodeRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ReturnShortCodeResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ServerIDsType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SetLicenseRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SetLicenseResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ShortCodeActivationType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.ShortCodeActivationType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "simpleAttributeDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SimpleAttributeDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }
    private void addBindings2() {
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SimpleQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "simpleSearchType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SimpleSearchType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateQueryType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StateQueryType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StateType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StatusInfoType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StatusInfoType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StatusType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StatusType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StopGapFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StopGapFulfillmentDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StopGapFulfillmentResponseDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StopGapFulfillmentResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapResponseDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.StopGapFulfillmentResponseDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "stopGapFulfillmentResponseDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SupportLicenseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.SupportLicenseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(enumsf);
            cachedDeserFactories.add(enumdf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostIdDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TransferHostIdDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostList");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TransferHostIdDataType[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostIdDataType");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdentifier");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TransferHostRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TransferHostResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedRequestType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TrustedRequestType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "trustedResponseType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TrustedResponseType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "typeLineItem");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.TypeLineItem.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "valueType");
            cachedSerQNames.add(qName);
            cls = java.lang.String[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string");
            qName2 = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "value");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "verifiedFulfillmentDataType");
            cachedSerQNames.add(qName);
            cls = flexnet.macrovision.com.VerifiedFulfillmentDataType.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public flexnet.macrovision.com.GetFulfillmentCountResponseType getFulfillmentCount(flexnet.macrovision.com.GetFulfillmentCountRequestType getFulfillmentCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getFulfillmentCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFulfillmentCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetFulfillmentCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetFulfillmentCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetFulfillmentCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetFulfillmentsQueryResponseType getFulfillmentsQuery(flexnet.macrovision.com.GetFulfillmentsQueryRequestType getFulfillmentsQueryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getFulfillmentsQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFulfillmentsQueryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetFulfillmentsQueryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetFulfillmentsQueryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetFulfillmentsQueryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetFulfillmentPropertiesResponseType getFulfillmentPropertiesQuery(flexnet.macrovision.com.GetFulfillmentPropertiesRequestType getFulfillmentPropertiesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getFulfillmentPropertiesQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFulfillmentPropertiesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetFulfillmentPropertiesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetFulfillmentPropertiesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetFulfillmentPropertiesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.RehostFulfillmentResponseType rehostLicense(flexnet.macrovision.com.RehostFulfillmentDataType[] rehostLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "rehostLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {rehostLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.RehostFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.RehostFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.RehostFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.ReturnFulfillmentResponseType returnLicense(flexnet.macrovision.com.ReturnFulfillmentDataType[] returnLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "returnLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {returnLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.ReturnFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.ReturnFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.ReturnFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.RepairFulfillmentResponseType repairLicense(flexnet.macrovision.com.RepairFulfillmentDataType[] repairLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "repairLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {repairLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.RepairFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.RepairFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.RepairFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.EmergencyFulfillmentResponseType emergencyLicense(flexnet.macrovision.com.EmergencyFulfillmentDataType[] emergencyLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "emergencyLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {emergencyLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.EmergencyFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.EmergencyFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.EmergencyFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.PublisherErrorFulfillmentResponseType publisherErrorLicense(flexnet.macrovision.com.PublisherErrorFulfillmentDataType[] publisherErrorLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "publisherErrorLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {publisherErrorLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.PublisherErrorFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.PublisherErrorFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.PublisherErrorFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.StopGapFulfillmentResponseType stopGapLicense(flexnet.macrovision.com.StopGapFulfillmentDataType[] stopGapLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "stopGapLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {stopGapLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.StopGapFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.StopGapFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.StopGapFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetFulfillmentAttributesResponseType getFulfillmentAttributesFromModel(flexnet.macrovision.com.GetFulfillmentAttributesRequestType getFulfillmentAttributesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getFulfillmentAttributesFromModel"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFulfillmentAttributesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetFulfillmentAttributesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetFulfillmentAttributesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetFulfillmentAttributesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetHostAttributesResponseType getHostAttributesFromLicenseTechnology(flexnet.macrovision.com.GetHostAttributesRequestType getHostAttributesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getHostAttributesFromLicenseTechnology"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getHostAttributesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetHostAttributesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetHostAttributesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetHostAttributesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.CreateFulfillmentResponseType verifyCreateLicense(flexnet.macrovision.com.CreateFulfillmentDataType[] verifyCreateLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "verifyCreateLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {verifyCreateLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.CreateFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.CreateFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.CreateFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.CreateFulfillmentResponseType createLicense(flexnet.macrovision.com.CreateFulfillmentDataType[] createLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.CreateFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.CreateFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.CreateFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.ActivateShortCodeResponseType activateShortCode(flexnet.macrovision.com.ActivateShortCodeRequestType activateShortCodeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "activateShortCode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {activateShortCodeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.ActivateShortCodeResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.ActivateShortCodeResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.ActivateShortCodeResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.RepairShortCodeResponseType repairShortCode(flexnet.macrovision.com.RepairShortCodeRequestType repairShortCodeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "repairShortCode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {repairShortCodeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.RepairShortCodeResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.RepairShortCodeResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.RepairShortCodeResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.ReturnShortCodeResponseType returnShortCode(flexnet.macrovision.com.ReturnShortCodeRequestType returnShortCodeRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "returnShortCode"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {returnShortCodeRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.ReturnShortCodeResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.ReturnShortCodeResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.ReturnShortCodeResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.EmailLicenseResponseType emailLicense(flexnet.macrovision.com.EmailLicenseRequestType emailLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "emailLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {emailLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.EmailLicenseResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.EmailLicenseResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.EmailLicenseResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.ConsolidateFulfillmentsResponseType consolidateFulfillments(flexnet.macrovision.com.ConsolidateFulfillmentsRequestType consolidateFulfillmentsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "consolidateFulfillments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {consolidateFulfillmentsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.ConsolidateFulfillmentsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.ConsolidateFulfillmentsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.ConsolidateFulfillmentsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType getConsolidatedFulfillmentCount(flexnet.macrovision.com.GetConsolidatedFulfillmentCountRequestType getConsolidatedFulfillmentCountRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getConsolidatedFulfillmentCount"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getConsolidatedFulfillmentCountRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType getConsolidatedFulfillmentsQuery(flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryRequestType getConsolidatedFulfillmentsQueryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getConsolidatedFulfillmentsQuery"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getConsolidatedFulfillmentsQueryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType getFulfillmentAttributesForBatchActivation(flexnet.macrovision.com.GetFmtAttributesForBatchActivationRequestType getFmtAttributesForBatchActivationRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getFulfillmentAttributesForBatchActivation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFmtAttributesForBatchActivationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.CreateLicensesAsBatchResponseType createLicensesAsBatch(flexnet.macrovision.com.CreateLicensesAsBatchRequestType createLicensesAsBatchRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createLicensesAsBatch"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createLicensesAsBatchRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.CreateLicensesAsBatchResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.CreateLicensesAsBatchResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.CreateLicensesAsBatchResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.ConsolidateFulfillmentsResponseType createLicensesAsBatchAndConsolidate(flexnet.macrovision.com.CreateLicensesAsBatchRequestType createLicensesAsBatchAndConsolidateRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createLicensesAsBatchAndConsolidate"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createLicensesAsBatchAndConsolidateRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.ConsolidateFulfillmentsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.ConsolidateFulfillmentsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.ConsolidateFulfillmentsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.EmailConsolidatedLicensesResponseType emailConsolidatedLicenses(flexnet.macrovision.com.EmailConsolidatedLicensesRequestType emailConsolidatedLicensesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "emailConsolidatedLicenses"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {emailConsolidatedLicensesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.EmailConsolidatedLicensesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.EmailConsolidatedLicensesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.EmailConsolidatedLicensesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.TrustedResponseType manualActivation(flexnet.macrovision.com.TrustedRequestType manualActivationRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "manualActivation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {manualActivationRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.TrustedResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.TrustedResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.TrustedResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.TrustedResponseType manualRepair(flexnet.macrovision.com.TrustedRequestType manualRepairRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[25]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "manualRepair"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {manualRepairRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.TrustedResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.TrustedResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.TrustedResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.TrustedResponseType manualReturn(flexnet.macrovision.com.TrustedRequestType manualReturnRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[26]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "manualReturn"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {manualReturnRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.TrustedResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.TrustedResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.TrustedResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.GetFulfillmentHistoryResponseType getFulfillmentHistory(flexnet.macrovision.com.GetFulfillmentHistoryRequestType getFulfillmentHistoryRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[27]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "getFulfillmentHistory"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {getFulfillmentHistoryRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.GetFulfillmentHistoryResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.GetFulfillmentHistoryResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.GetFulfillmentHistoryResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType createChildLineItemFulfillment(flexnet.macrovision.com.CreateChildLineItemFulfillmentDataType[] createChildLineItemFulfillmentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[28]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "createChildLineItemFulfillment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {createChildLineItemFulfillmentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType upgradeFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType upgradeFulfillmentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[29]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "upgradeFulfillment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {upgradeFulfillmentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.AdvancedFulfillmentLCResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.AdvancedFulfillmentLCResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType upsellFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType upsellFulfillmentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[30]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "upsellFulfillment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {upsellFulfillmentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.AdvancedFulfillmentLCResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.AdvancedFulfillmentLCResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType renewFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType renewFulfillmentRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[31]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "renewFulfillment"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {renewFulfillmentRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.AdvancedFulfillmentLCResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.AdvancedFulfillmentLCResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.AdvancedFulfillmentLCResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.SetLicenseResponseType setLicense(flexnet.macrovision.com.SetLicenseRequestType setLicenseRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[32]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "setLicense"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {setLicenseRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.SetLicenseResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.SetLicenseResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.SetLicenseResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType deleteOnholdFulfillments(flexnet.macrovision.com.DeleteOnholdFulfillmentsRequestType deleteOnholdFulfillmentsRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[33]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "deleteOnholdFulfillments"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {deleteOnholdFulfillmentsRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.ActivateLicensesResponseType offlineFNPTrustedStorageActivation(flexnet.macrovision.com.ActivateLicensesRequestType activateLicensesRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[34]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "offlineFNPTrustedStorageActivation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {activateLicensesRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.ActivateLicensesResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.ActivateLicensesResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.ActivateLicensesResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public flexnet.macrovision.com.TransferHostResponseType transferHost(flexnet.macrovision.com.TransferHostRequestType transferHostRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[35]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("", "transferHost"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {transferHostRequest});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (flexnet.macrovision.com.TransferHostResponseType) _resp;
            } catch (java.lang.Exception _exception) {
                return (flexnet.macrovision.com.TransferHostResponseType) org.apache.axis.utils.JavaUtils.convert(_resp, flexnet.macrovision.com.TransferHostResponseType.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
