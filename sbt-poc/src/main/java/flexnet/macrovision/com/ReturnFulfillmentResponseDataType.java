/**
 * ReturnFulfillmentResponseDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ReturnFulfillmentResponseDataType  implements java.io.Serializable {
    private java.math.BigInteger recordRefNo;

    private flexnet.macrovision.com.FulfillmentDataType fulfillment;

    public ReturnFulfillmentResponseDataType() {
    }

    public ReturnFulfillmentResponseDataType(
           java.math.BigInteger recordRefNo,
           flexnet.macrovision.com.FulfillmentDataType fulfillment) {
           this.recordRefNo = recordRefNo;
           this.fulfillment = fulfillment;
    }


    /**
     * Gets the recordRefNo value for this ReturnFulfillmentResponseDataType.
     * 
     * @return recordRefNo
     */
    public java.math.BigInteger getRecordRefNo() {
        return recordRefNo;
    }


    /**
     * Sets the recordRefNo value for this ReturnFulfillmentResponseDataType.
     * 
     * @param recordRefNo
     */
    public void setRecordRefNo(java.math.BigInteger recordRefNo) {
        this.recordRefNo = recordRefNo;
    }


    /**
     * Gets the fulfillment value for this ReturnFulfillmentResponseDataType.
     * 
     * @return fulfillment
     */
    public flexnet.macrovision.com.FulfillmentDataType getFulfillment() {
        return fulfillment;
    }


    /**
     * Sets the fulfillment value for this ReturnFulfillmentResponseDataType.
     * 
     * @param fulfillment
     */
    public void setFulfillment(flexnet.macrovision.com.FulfillmentDataType fulfillment) {
        this.fulfillment = fulfillment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReturnFulfillmentResponseDataType)) return false;
        ReturnFulfillmentResponseDataType other = (ReturnFulfillmentResponseDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.recordRefNo==null && other.getRecordRefNo()==null) || 
             (this.recordRefNo!=null &&
              this.recordRefNo.equals(other.getRecordRefNo()))) &&
            ((this.fulfillment==null && other.getFulfillment()==null) || 
             (this.fulfillment!=null &&
              this.fulfillment.equals(other.getFulfillment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRecordRefNo() != null) {
            _hashCode += getRecordRefNo().hashCode();
        }
        if (getFulfillment() != null) {
            _hashCode += getFulfillment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReturnFulfillmentResponseDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordRefNo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "recordRefNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
