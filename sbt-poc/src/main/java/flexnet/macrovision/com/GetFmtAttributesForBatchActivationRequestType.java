/**
 * GetFmtAttributesForBatchActivationRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetFmtAttributesForBatchActivationRequestType  implements java.io.Serializable {
    private java.lang.String[] activationIds;

    public GetFmtAttributesForBatchActivationRequestType() {
    }

    public GetFmtAttributesForBatchActivationRequestType(
           java.lang.String[] activationIds) {
           this.activationIds = activationIds;
    }


    /**
     * Gets the activationIds value for this GetFmtAttributesForBatchActivationRequestType.
     * 
     * @return activationIds
     */
    public java.lang.String[] getActivationIds() {
        return activationIds;
    }


    /**
     * Sets the activationIds value for this GetFmtAttributesForBatchActivationRequestType.
     * 
     * @param activationIds
     */
    public void setActivationIds(java.lang.String[] activationIds) {
        this.activationIds = activationIds;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFmtAttributesForBatchActivationRequestType)) return false;
        GetFmtAttributesForBatchActivationRequestType other = (GetFmtAttributesForBatchActivationRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.activationIds==null && other.getActivationIds()==null) || 
             (this.activationIds!=null &&
              java.util.Arrays.equals(this.activationIds, other.getActivationIds())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActivationIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getActivationIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getActivationIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFmtAttributesForBatchActivationRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchActivationRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
