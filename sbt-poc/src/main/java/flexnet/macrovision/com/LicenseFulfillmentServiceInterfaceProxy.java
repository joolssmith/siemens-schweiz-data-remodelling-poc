package flexnet.macrovision.com;

public class LicenseFulfillmentServiceInterfaceProxy implements flexnet.macrovision.com.LicenseFulfillmentServiceInterface {
  private String _endpoint = null;
  private flexnet.macrovision.com.LicenseFulfillmentServiceInterface licenseFulfillmentServiceInterface = null;
  
  public LicenseFulfillmentServiceInterfaceProxy() {
    _initLicenseFulfillmentServiceInterfaceProxy();
  }
  
  public LicenseFulfillmentServiceInterfaceProxy(String endpoint) {
    _endpoint = endpoint;
    _initLicenseFulfillmentServiceInterfaceProxy();
  }
  
  private void _initLicenseFulfillmentServiceInterfaceProxy() {
    try {
      licenseFulfillmentServiceInterface = (new flexnet.macrovision.com.LicenseServiceLocator()).getLicenseService();
      if (licenseFulfillmentServiceInterface != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)licenseFulfillmentServiceInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)licenseFulfillmentServiceInterface)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (licenseFulfillmentServiceInterface != null)
      ((javax.xml.rpc.Stub)licenseFulfillmentServiceInterface)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public flexnet.macrovision.com.LicenseFulfillmentServiceInterface getLicenseFulfillmentServiceInterface() {
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface;
  }
  
  public flexnet.macrovision.com.GetFulfillmentCountResponseType getFulfillmentCount(flexnet.macrovision.com.GetFulfillmentCountRequestType getFulfillmentCountRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getFulfillmentCount(getFulfillmentCountRequest);
  }
  
  public flexnet.macrovision.com.GetFulfillmentsQueryResponseType getFulfillmentsQuery(flexnet.macrovision.com.GetFulfillmentsQueryRequestType getFulfillmentsQueryRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getFulfillmentsQuery(getFulfillmentsQueryRequest);
  }
  
  public flexnet.macrovision.com.GetFulfillmentPropertiesResponseType getFulfillmentPropertiesQuery(flexnet.macrovision.com.GetFulfillmentPropertiesRequestType getFulfillmentPropertiesRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getFulfillmentPropertiesQuery(getFulfillmentPropertiesRequest);
  }
  
  public flexnet.macrovision.com.RehostFulfillmentResponseType rehostLicense(flexnet.macrovision.com.RehostFulfillmentDataType[] rehostLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.rehostLicense(rehostLicenseRequest);
  }
  
  public flexnet.macrovision.com.ReturnFulfillmentResponseType returnLicense(flexnet.macrovision.com.ReturnFulfillmentDataType[] returnLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.returnLicense(returnLicenseRequest);
  }
  
  public flexnet.macrovision.com.RepairFulfillmentResponseType repairLicense(flexnet.macrovision.com.RepairFulfillmentDataType[] repairLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.repairLicense(repairLicenseRequest);
  }
  
  public flexnet.macrovision.com.EmergencyFulfillmentResponseType emergencyLicense(flexnet.macrovision.com.EmergencyFulfillmentDataType[] emergencyLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.emergencyLicense(emergencyLicenseRequest);
  }
  
  public flexnet.macrovision.com.PublisherErrorFulfillmentResponseType publisherErrorLicense(flexnet.macrovision.com.PublisherErrorFulfillmentDataType[] publisherErrorLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.publisherErrorLicense(publisherErrorLicenseRequest);
  }
  
  public flexnet.macrovision.com.StopGapFulfillmentResponseType stopGapLicense(flexnet.macrovision.com.StopGapFulfillmentDataType[] stopGapLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.stopGapLicense(stopGapLicenseRequest);
  }
  
  public flexnet.macrovision.com.GetFulfillmentAttributesResponseType getFulfillmentAttributesFromModel(flexnet.macrovision.com.GetFulfillmentAttributesRequestType getFulfillmentAttributesRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getFulfillmentAttributesFromModel(getFulfillmentAttributesRequest);
  }
  
  public flexnet.macrovision.com.GetHostAttributesResponseType getHostAttributesFromLicenseTechnology(flexnet.macrovision.com.GetHostAttributesRequestType getHostAttributesRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getHostAttributesFromLicenseTechnology(getHostAttributesRequest);
  }
  
  public flexnet.macrovision.com.CreateFulfillmentResponseType verifyCreateLicense(flexnet.macrovision.com.CreateFulfillmentDataType[] verifyCreateLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.verifyCreateLicense(verifyCreateLicenseRequest);
  }
  
  public flexnet.macrovision.com.CreateFulfillmentResponseType createLicense(flexnet.macrovision.com.CreateFulfillmentDataType[] createLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.createLicense(createLicenseRequest);
  }
  
  public flexnet.macrovision.com.ActivateShortCodeResponseType activateShortCode(flexnet.macrovision.com.ActivateShortCodeRequestType activateShortCodeRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.activateShortCode(activateShortCodeRequest);
  }
  
  public flexnet.macrovision.com.RepairShortCodeResponseType repairShortCode(flexnet.macrovision.com.RepairShortCodeRequestType repairShortCodeRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.repairShortCode(repairShortCodeRequest);
  }
  
  public flexnet.macrovision.com.ReturnShortCodeResponseType returnShortCode(flexnet.macrovision.com.ReturnShortCodeRequestType returnShortCodeRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.returnShortCode(returnShortCodeRequest);
  }
  
  public flexnet.macrovision.com.EmailLicenseResponseType emailLicense(flexnet.macrovision.com.EmailLicenseRequestType emailLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.emailLicense(emailLicenseRequest);
  }
  
  public flexnet.macrovision.com.ConsolidateFulfillmentsResponseType consolidateFulfillments(flexnet.macrovision.com.ConsolidateFulfillmentsRequestType consolidateFulfillmentsRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.consolidateFulfillments(consolidateFulfillmentsRequest);
  }
  
  public flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType getConsolidatedFulfillmentCount(flexnet.macrovision.com.GetConsolidatedFulfillmentCountRequestType getConsolidatedFulfillmentCountRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getConsolidatedFulfillmentCount(getConsolidatedFulfillmentCountRequest);
  }
  
  public flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType getConsolidatedFulfillmentsQuery(flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryRequestType getConsolidatedFulfillmentsQueryRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getConsolidatedFulfillmentsQuery(getConsolidatedFulfillmentsQueryRequest);
  }
  
  public flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType getFulfillmentAttributesForBatchActivation(flexnet.macrovision.com.GetFmtAttributesForBatchActivationRequestType getFmtAttributesForBatchActivationRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getFulfillmentAttributesForBatchActivation(getFmtAttributesForBatchActivationRequest);
  }
  
  public flexnet.macrovision.com.CreateLicensesAsBatchResponseType createLicensesAsBatch(flexnet.macrovision.com.CreateLicensesAsBatchRequestType createLicensesAsBatchRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.createLicensesAsBatch(createLicensesAsBatchRequest);
  }
  
  public flexnet.macrovision.com.ConsolidateFulfillmentsResponseType createLicensesAsBatchAndConsolidate(flexnet.macrovision.com.CreateLicensesAsBatchRequestType createLicensesAsBatchAndConsolidateRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.createLicensesAsBatchAndConsolidate(createLicensesAsBatchAndConsolidateRequest);
  }
  
  public flexnet.macrovision.com.EmailConsolidatedLicensesResponseType emailConsolidatedLicenses(flexnet.macrovision.com.EmailConsolidatedLicensesRequestType emailConsolidatedLicensesRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.emailConsolidatedLicenses(emailConsolidatedLicensesRequest);
  }
  
  public flexnet.macrovision.com.TrustedResponseType manualActivation(flexnet.macrovision.com.TrustedRequestType manualActivationRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.manualActivation(manualActivationRequest);
  }
  
  public flexnet.macrovision.com.TrustedResponseType manualRepair(flexnet.macrovision.com.TrustedRequestType manualRepairRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.manualRepair(manualRepairRequest);
  }
  
  public flexnet.macrovision.com.TrustedResponseType manualReturn(flexnet.macrovision.com.TrustedRequestType manualReturnRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.manualReturn(manualReturnRequest);
  }
  
  public flexnet.macrovision.com.GetFulfillmentHistoryResponseType getFulfillmentHistory(flexnet.macrovision.com.GetFulfillmentHistoryRequestType getFulfillmentHistoryRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.getFulfillmentHistory(getFulfillmentHistoryRequest);
  }
  
  public flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType createChildLineItemFulfillment(flexnet.macrovision.com.CreateChildLineItemFulfillmentDataType[] createChildLineItemFulfillmentRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.createChildLineItemFulfillment(createChildLineItemFulfillmentRequest);
  }
  
  public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType upgradeFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType upgradeFulfillmentRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.upgradeFulfillment(upgradeFulfillmentRequest);
  }
  
  public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType upsellFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType upsellFulfillmentRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.upsellFulfillment(upsellFulfillmentRequest);
  }
  
  public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType renewFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType renewFulfillmentRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.renewFulfillment(renewFulfillmentRequest);
  }
  
  public flexnet.macrovision.com.SetLicenseResponseType setLicense(flexnet.macrovision.com.SetLicenseRequestType setLicenseRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.setLicense(setLicenseRequest);
  }
  
  public flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType deleteOnholdFulfillments(flexnet.macrovision.com.DeleteOnholdFulfillmentsRequestType deleteOnholdFulfillmentsRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.deleteOnholdFulfillments(deleteOnholdFulfillmentsRequest);
  }
  
  public flexnet.macrovision.com.ActivateLicensesResponseType offlineFNPTrustedStorageActivation(flexnet.macrovision.com.ActivateLicensesRequestType activateLicensesRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.offlineFNPTrustedStorageActivation(activateLicensesRequest);
  }
  
  public flexnet.macrovision.com.TransferHostResponseType transferHost(flexnet.macrovision.com.TransferHostRequestType transferHostRequest) throws java.rmi.RemoteException{
    if (licenseFulfillmentServiceInterface == null)
      _initLicenseFulfillmentServiceInterfaceProxy();
    return licenseFulfillmentServiceInterface.transferHost(transferHostRequest);
  }
  
  
}