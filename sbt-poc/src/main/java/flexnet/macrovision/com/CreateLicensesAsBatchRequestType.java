/**
 * CreateLicensesAsBatchRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class CreateLicensesAsBatchRequestType  implements java.io.Serializable {
    private java.lang.String[] activationIds;

    private flexnet.macrovision.com.HostIdDataType[] hostIdDataSet;

    private flexnet.macrovision.com.CountDataType[] countDataSet;

    private flexnet.macrovision.com.CommonBatchDataSetType commonBatchDataSet;

    public CreateLicensesAsBatchRequestType() {
    }

    public CreateLicensesAsBatchRequestType(
           java.lang.String[] activationIds,
           flexnet.macrovision.com.HostIdDataType[] hostIdDataSet,
           flexnet.macrovision.com.CountDataType[] countDataSet,
           flexnet.macrovision.com.CommonBatchDataSetType commonBatchDataSet) {
           this.activationIds = activationIds;
           this.hostIdDataSet = hostIdDataSet;
           this.countDataSet = countDataSet;
           this.commonBatchDataSet = commonBatchDataSet;
    }


    /**
     * Gets the activationIds value for this CreateLicensesAsBatchRequestType.
     * 
     * @return activationIds
     */
    public java.lang.String[] getActivationIds() {
        return activationIds;
    }


    /**
     * Sets the activationIds value for this CreateLicensesAsBatchRequestType.
     * 
     * @param activationIds
     */
    public void setActivationIds(java.lang.String[] activationIds) {
        this.activationIds = activationIds;
    }


    /**
     * Gets the hostIdDataSet value for this CreateLicensesAsBatchRequestType.
     * 
     * @return hostIdDataSet
     */
    public flexnet.macrovision.com.HostIdDataType[] getHostIdDataSet() {
        return hostIdDataSet;
    }


    /**
     * Sets the hostIdDataSet value for this CreateLicensesAsBatchRequestType.
     * 
     * @param hostIdDataSet
     */
    public void setHostIdDataSet(flexnet.macrovision.com.HostIdDataType[] hostIdDataSet) {
        this.hostIdDataSet = hostIdDataSet;
    }


    /**
     * Gets the countDataSet value for this CreateLicensesAsBatchRequestType.
     * 
     * @return countDataSet
     */
    public flexnet.macrovision.com.CountDataType[] getCountDataSet() {
        return countDataSet;
    }


    /**
     * Sets the countDataSet value for this CreateLicensesAsBatchRequestType.
     * 
     * @param countDataSet
     */
    public void setCountDataSet(flexnet.macrovision.com.CountDataType[] countDataSet) {
        this.countDataSet = countDataSet;
    }


    /**
     * Gets the commonBatchDataSet value for this CreateLicensesAsBatchRequestType.
     * 
     * @return commonBatchDataSet
     */
    public flexnet.macrovision.com.CommonBatchDataSetType getCommonBatchDataSet() {
        return commonBatchDataSet;
    }


    /**
     * Sets the commonBatchDataSet value for this CreateLicensesAsBatchRequestType.
     * 
     * @param commonBatchDataSet
     */
    public void setCommonBatchDataSet(flexnet.macrovision.com.CommonBatchDataSetType commonBatchDataSet) {
        this.commonBatchDataSet = commonBatchDataSet;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateLicensesAsBatchRequestType)) return false;
        CreateLicensesAsBatchRequestType other = (CreateLicensesAsBatchRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.activationIds==null && other.getActivationIds()==null) || 
             (this.activationIds!=null &&
              java.util.Arrays.equals(this.activationIds, other.getActivationIds()))) &&
            ((this.hostIdDataSet==null && other.getHostIdDataSet()==null) || 
             (this.hostIdDataSet!=null &&
              java.util.Arrays.equals(this.hostIdDataSet, other.getHostIdDataSet()))) &&
            ((this.countDataSet==null && other.getCountDataSet()==null) || 
             (this.countDataSet!=null &&
              java.util.Arrays.equals(this.countDataSet, other.getCountDataSet()))) &&
            ((this.commonBatchDataSet==null && other.getCommonBatchDataSet()==null) || 
             (this.commonBatchDataSet!=null &&
              this.commonBatchDataSet.equals(other.getCommonBatchDataSet())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActivationIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getActivationIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getActivationIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHostIdDataSet() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHostIdDataSet());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHostIdDataSet(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCountDataSet() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCountDataSet());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCountDataSet(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCommonBatchDataSet() != null) {
            _hashCode += getCommonBatchDataSet().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateLicensesAsBatchRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createLicensesAsBatchRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostIdDataSet");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataSet"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdData"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countDataSet");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countDataSet"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countData"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("commonBatchDataSet");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "commonBatchDataSet"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "commonBatchDataSetType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
