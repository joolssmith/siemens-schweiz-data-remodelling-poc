/**
 * RehostFulfillmentDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class RehostFulfillmentDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier;

    private flexnet.macrovision.com.ServerIDsType serverIds;

    private java.lang.String[] nodeIds;

    private flexnet.macrovision.com.CustomHostIDType customHost;

    private java.math.BigInteger partialCount;

    private java.math.BigInteger overDraftCount;

    private java.lang.Boolean overridePolicy;

    public RehostFulfillmentDataType() {
    }

    public RehostFulfillmentDataType(
           flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier,
           flexnet.macrovision.com.ServerIDsType serverIds,
           java.lang.String[] nodeIds,
           flexnet.macrovision.com.CustomHostIDType customHost,
           java.math.BigInteger partialCount,
           java.math.BigInteger overDraftCount,
           java.lang.Boolean overridePolicy) {
           this.fulfillmentIdentifier = fulfillmentIdentifier;
           this.serverIds = serverIds;
           this.nodeIds = nodeIds;
           this.customHost = customHost;
           this.partialCount = partialCount;
           this.overDraftCount = overDraftCount;
           this.overridePolicy = overridePolicy;
    }


    /**
     * Gets the fulfillmentIdentifier value for this RehostFulfillmentDataType.
     * 
     * @return fulfillmentIdentifier
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }


    /**
     * Sets the fulfillmentIdentifier value for this RehostFulfillmentDataType.
     * 
     * @param fulfillmentIdentifier
     */
    public void setFulfillmentIdentifier(flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier) {
        this.fulfillmentIdentifier = fulfillmentIdentifier;
    }


    /**
     * Gets the serverIds value for this RehostFulfillmentDataType.
     * 
     * @return serverIds
     */
    public flexnet.macrovision.com.ServerIDsType getServerIds() {
        return serverIds;
    }


    /**
     * Sets the serverIds value for this RehostFulfillmentDataType.
     * 
     * @param serverIds
     */
    public void setServerIds(flexnet.macrovision.com.ServerIDsType serverIds) {
        this.serverIds = serverIds;
    }


    /**
     * Gets the nodeIds value for this RehostFulfillmentDataType.
     * 
     * @return nodeIds
     */
    public java.lang.String[] getNodeIds() {
        return nodeIds;
    }


    /**
     * Sets the nodeIds value for this RehostFulfillmentDataType.
     * 
     * @param nodeIds
     */
    public void setNodeIds(java.lang.String[] nodeIds) {
        this.nodeIds = nodeIds;
    }


    /**
     * Gets the customHost value for this RehostFulfillmentDataType.
     * 
     * @return customHost
     */
    public flexnet.macrovision.com.CustomHostIDType getCustomHost() {
        return customHost;
    }


    /**
     * Sets the customHost value for this RehostFulfillmentDataType.
     * 
     * @param customHost
     */
    public void setCustomHost(flexnet.macrovision.com.CustomHostIDType customHost) {
        this.customHost = customHost;
    }


    /**
     * Gets the partialCount value for this RehostFulfillmentDataType.
     * 
     * @return partialCount
     */
    public java.math.BigInteger getPartialCount() {
        return partialCount;
    }


    /**
     * Sets the partialCount value for this RehostFulfillmentDataType.
     * 
     * @param partialCount
     */
    public void setPartialCount(java.math.BigInteger partialCount) {
        this.partialCount = partialCount;
    }


    /**
     * Gets the overDraftCount value for this RehostFulfillmentDataType.
     * 
     * @return overDraftCount
     */
    public java.math.BigInteger getOverDraftCount() {
        return overDraftCount;
    }


    /**
     * Sets the overDraftCount value for this RehostFulfillmentDataType.
     * 
     * @param overDraftCount
     */
    public void setOverDraftCount(java.math.BigInteger overDraftCount) {
        this.overDraftCount = overDraftCount;
    }


    /**
     * Gets the overridePolicy value for this RehostFulfillmentDataType.
     * 
     * @return overridePolicy
     */
    public java.lang.Boolean getOverridePolicy() {
        return overridePolicy;
    }


    /**
     * Sets the overridePolicy value for this RehostFulfillmentDataType.
     * 
     * @param overridePolicy
     */
    public void setOverridePolicy(java.lang.Boolean overridePolicy) {
        this.overridePolicy = overridePolicy;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RehostFulfillmentDataType)) return false;
        RehostFulfillmentDataType other = (RehostFulfillmentDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentIdentifier==null && other.getFulfillmentIdentifier()==null) || 
             (this.fulfillmentIdentifier!=null &&
              this.fulfillmentIdentifier.equals(other.getFulfillmentIdentifier()))) &&
            ((this.serverIds==null && other.getServerIds()==null) || 
             (this.serverIds!=null &&
              this.serverIds.equals(other.getServerIds()))) &&
            ((this.nodeIds==null && other.getNodeIds()==null) || 
             (this.nodeIds!=null &&
              java.util.Arrays.equals(this.nodeIds, other.getNodeIds()))) &&
            ((this.customHost==null && other.getCustomHost()==null) || 
             (this.customHost!=null &&
              this.customHost.equals(other.getCustomHost()))) &&
            ((this.partialCount==null && other.getPartialCount()==null) || 
             (this.partialCount!=null &&
              this.partialCount.equals(other.getPartialCount()))) &&
            ((this.overDraftCount==null && other.getOverDraftCount()==null) || 
             (this.overDraftCount!=null &&
              this.overDraftCount.equals(other.getOverDraftCount()))) &&
            ((this.overridePolicy==null && other.getOverridePolicy()==null) || 
             (this.overridePolicy!=null &&
              this.overridePolicy.equals(other.getOverridePolicy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentIdentifier() != null) {
            _hashCode += getFulfillmentIdentifier().hashCode();
        }
        if (getServerIds() != null) {
            _hashCode += getServerIds().hashCode();
        }
        if (getNodeIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNodeIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNodeIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHost() != null) {
            _hashCode += getCustomHost().hashCode();
        }
        if (getPartialCount() != null) {
            _hashCode += getPartialCount().hashCode();
        }
        if (getOverDraftCount() != null) {
            _hashCode += getOverDraftCount().hashCode();
        }
        if (getOverridePolicy() != null) {
            _hashCode += getOverridePolicy().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RehostFulfillmentDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "rehostFulfillmentDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serverIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "serverIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partialCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partialCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overridePolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overridePolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
