/**
 * GetFmtAttributesForBatchDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetFmtAttributesForBatchDataType  implements java.io.Serializable {
    private boolean needStartDate;

    private boolean needVersionDate;

    private boolean needVersionStartDate;

    private boolean needServerId;

    private boolean needNodeLockId;

    private boolean needCustomHost;

    private boolean needCount;

    private boolean needSoldTo;

    private flexnet.macrovision.com.ActivationIdOverDraftMapType[] overDraftData;

    private java.lang.String modelType;

    private flexnet.macrovision.com.AttributeMetaDescriptorType[] modelAttributes;

    private flexnet.macrovision.com.AttributeMetaDescriptorType[] hostAttributes;

    private java.lang.Boolean needTimeZone;

    public GetFmtAttributesForBatchDataType() {
    }

    public GetFmtAttributesForBatchDataType(
           boolean needStartDate,
           boolean needVersionDate,
           boolean needVersionStartDate,
           boolean needServerId,
           boolean needNodeLockId,
           boolean needCustomHost,
           boolean needCount,
           boolean needSoldTo,
           flexnet.macrovision.com.ActivationIdOverDraftMapType[] overDraftData,
           java.lang.String modelType,
           flexnet.macrovision.com.AttributeMetaDescriptorType[] modelAttributes,
           flexnet.macrovision.com.AttributeMetaDescriptorType[] hostAttributes,
           java.lang.Boolean needTimeZone) {
           this.needStartDate = needStartDate;
           this.needVersionDate = needVersionDate;
           this.needVersionStartDate = needVersionStartDate;
           this.needServerId = needServerId;
           this.needNodeLockId = needNodeLockId;
           this.needCustomHost = needCustomHost;
           this.needCount = needCount;
           this.needSoldTo = needSoldTo;
           this.overDraftData = overDraftData;
           this.modelType = modelType;
           this.modelAttributes = modelAttributes;
           this.hostAttributes = hostAttributes;
           this.needTimeZone = needTimeZone;
    }


    /**
     * Gets the needStartDate value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needStartDate
     */
    public boolean isNeedStartDate() {
        return needStartDate;
    }


    /**
     * Sets the needStartDate value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needStartDate
     */
    public void setNeedStartDate(boolean needStartDate) {
        this.needStartDate = needStartDate;
    }


    /**
     * Gets the needVersionDate value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needVersionDate
     */
    public boolean isNeedVersionDate() {
        return needVersionDate;
    }


    /**
     * Sets the needVersionDate value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needVersionDate
     */
    public void setNeedVersionDate(boolean needVersionDate) {
        this.needVersionDate = needVersionDate;
    }


    /**
     * Gets the needVersionStartDate value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needVersionStartDate
     */
    public boolean isNeedVersionStartDate() {
        return needVersionStartDate;
    }


    /**
     * Sets the needVersionStartDate value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needVersionStartDate
     */
    public void setNeedVersionStartDate(boolean needVersionStartDate) {
        this.needVersionStartDate = needVersionStartDate;
    }


    /**
     * Gets the needServerId value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needServerId
     */
    public boolean isNeedServerId() {
        return needServerId;
    }


    /**
     * Sets the needServerId value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needServerId
     */
    public void setNeedServerId(boolean needServerId) {
        this.needServerId = needServerId;
    }


    /**
     * Gets the needNodeLockId value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needNodeLockId
     */
    public boolean isNeedNodeLockId() {
        return needNodeLockId;
    }


    /**
     * Sets the needNodeLockId value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needNodeLockId
     */
    public void setNeedNodeLockId(boolean needNodeLockId) {
        this.needNodeLockId = needNodeLockId;
    }


    /**
     * Gets the needCustomHost value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needCustomHost
     */
    public boolean isNeedCustomHost() {
        return needCustomHost;
    }


    /**
     * Sets the needCustomHost value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needCustomHost
     */
    public void setNeedCustomHost(boolean needCustomHost) {
        this.needCustomHost = needCustomHost;
    }


    /**
     * Gets the needCount value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needCount
     */
    public boolean isNeedCount() {
        return needCount;
    }


    /**
     * Sets the needCount value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needCount
     */
    public void setNeedCount(boolean needCount) {
        this.needCount = needCount;
    }


    /**
     * Gets the needSoldTo value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needSoldTo
     */
    public boolean isNeedSoldTo() {
        return needSoldTo;
    }


    /**
     * Sets the needSoldTo value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needSoldTo
     */
    public void setNeedSoldTo(boolean needSoldTo) {
        this.needSoldTo = needSoldTo;
    }


    /**
     * Gets the overDraftData value for this GetFmtAttributesForBatchDataType.
     * 
     * @return overDraftData
     */
    public flexnet.macrovision.com.ActivationIdOverDraftMapType[] getOverDraftData() {
        return overDraftData;
    }


    /**
     * Sets the overDraftData value for this GetFmtAttributesForBatchDataType.
     * 
     * @param overDraftData
     */
    public void setOverDraftData(flexnet.macrovision.com.ActivationIdOverDraftMapType[] overDraftData) {
        this.overDraftData = overDraftData;
    }


    /**
     * Gets the modelType value for this GetFmtAttributesForBatchDataType.
     * 
     * @return modelType
     */
    public java.lang.String getModelType() {
        return modelType;
    }


    /**
     * Sets the modelType value for this GetFmtAttributesForBatchDataType.
     * 
     * @param modelType
     */
    public void setModelType(java.lang.String modelType) {
        this.modelType = modelType;
    }


    /**
     * Gets the modelAttributes value for this GetFmtAttributesForBatchDataType.
     * 
     * @return modelAttributes
     */
    public flexnet.macrovision.com.AttributeMetaDescriptorType[] getModelAttributes() {
        return modelAttributes;
    }


    /**
     * Sets the modelAttributes value for this GetFmtAttributesForBatchDataType.
     * 
     * @param modelAttributes
     */
    public void setModelAttributes(flexnet.macrovision.com.AttributeMetaDescriptorType[] modelAttributes) {
        this.modelAttributes = modelAttributes;
    }


    /**
     * Gets the hostAttributes value for this GetFmtAttributesForBatchDataType.
     * 
     * @return hostAttributes
     */
    public flexnet.macrovision.com.AttributeMetaDescriptorType[] getHostAttributes() {
        return hostAttributes;
    }


    /**
     * Sets the hostAttributes value for this GetFmtAttributesForBatchDataType.
     * 
     * @param hostAttributes
     */
    public void setHostAttributes(flexnet.macrovision.com.AttributeMetaDescriptorType[] hostAttributes) {
        this.hostAttributes = hostAttributes;
    }


    /**
     * Gets the needTimeZone value for this GetFmtAttributesForBatchDataType.
     * 
     * @return needTimeZone
     */
    public java.lang.Boolean getNeedTimeZone() {
        return needTimeZone;
    }


    /**
     * Sets the needTimeZone value for this GetFmtAttributesForBatchDataType.
     * 
     * @param needTimeZone
     */
    public void setNeedTimeZone(java.lang.Boolean needTimeZone) {
        this.needTimeZone = needTimeZone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFmtAttributesForBatchDataType)) return false;
        GetFmtAttributesForBatchDataType other = (GetFmtAttributesForBatchDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.needStartDate == other.isNeedStartDate() &&
            this.needVersionDate == other.isNeedVersionDate() &&
            this.needVersionStartDate == other.isNeedVersionStartDate() &&
            this.needServerId == other.isNeedServerId() &&
            this.needNodeLockId == other.isNeedNodeLockId() &&
            this.needCustomHost == other.isNeedCustomHost() &&
            this.needCount == other.isNeedCount() &&
            this.needSoldTo == other.isNeedSoldTo() &&
            ((this.overDraftData==null && other.getOverDraftData()==null) || 
             (this.overDraftData!=null &&
              java.util.Arrays.equals(this.overDraftData, other.getOverDraftData()))) &&
            ((this.modelType==null && other.getModelType()==null) || 
             (this.modelType!=null &&
              this.modelType.equals(other.getModelType()))) &&
            ((this.modelAttributes==null && other.getModelAttributes()==null) || 
             (this.modelAttributes!=null &&
              java.util.Arrays.equals(this.modelAttributes, other.getModelAttributes()))) &&
            ((this.hostAttributes==null && other.getHostAttributes()==null) || 
             (this.hostAttributes!=null &&
              java.util.Arrays.equals(this.hostAttributes, other.getHostAttributes()))) &&
            ((this.needTimeZone==null && other.getNeedTimeZone()==null) || 
             (this.needTimeZone!=null &&
              this.needTimeZone.equals(other.getNeedTimeZone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += (isNeedStartDate() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedVersionDate() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedVersionStartDate() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedServerId() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedNodeLockId() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedCustomHost() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedCount() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNeedSoldTo() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getOverDraftData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOverDraftData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOverDraftData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getModelType() != null) {
            _hashCode += getModelType().hashCode();
        }
        if (getModelAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getModelAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getModelAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHostAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHostAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHostAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNeedTimeZone() != null) {
            _hashCode += getNeedTimeZone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFmtAttributesForBatchDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFmtAttributesForBatchDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needVersionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needVersionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needVersionStartDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needVersionStartDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needServerId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needServerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needNodeLockId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needNodeLockId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needCustomHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needCustomHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needSoldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needSoldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIdOverDraftMapType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationIdMap"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modelType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "modelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modelAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "modelAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeMetaDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeMetaDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needTimeZone");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needTimeZone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
