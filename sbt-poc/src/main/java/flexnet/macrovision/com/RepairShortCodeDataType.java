/**
 * RepairShortCodeDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class RepairShortCodeDataType  implements java.io.Serializable {
    private java.lang.String shortCode;

    private java.lang.String webRegKey;

    private flexnet.macrovision.com.SimpleAttributeDataType[] publisherAttributes;

    private java.lang.Boolean overridePolicy;

    public RepairShortCodeDataType() {
    }

    public RepairShortCodeDataType(
           java.lang.String shortCode,
           java.lang.String webRegKey,
           flexnet.macrovision.com.SimpleAttributeDataType[] publisherAttributes,
           java.lang.Boolean overridePolicy) {
           this.shortCode = shortCode;
           this.webRegKey = webRegKey;
           this.publisherAttributes = publisherAttributes;
           this.overridePolicy = overridePolicy;
    }


    /**
     * Gets the shortCode value for this RepairShortCodeDataType.
     * 
     * @return shortCode
     */
    public java.lang.String getShortCode() {
        return shortCode;
    }


    /**
     * Sets the shortCode value for this RepairShortCodeDataType.
     * 
     * @param shortCode
     */
    public void setShortCode(java.lang.String shortCode) {
        this.shortCode = shortCode;
    }


    /**
     * Gets the webRegKey value for this RepairShortCodeDataType.
     * 
     * @return webRegKey
     */
    public java.lang.String getWebRegKey() {
        return webRegKey;
    }


    /**
     * Sets the webRegKey value for this RepairShortCodeDataType.
     * 
     * @param webRegKey
     */
    public void setWebRegKey(java.lang.String webRegKey) {
        this.webRegKey = webRegKey;
    }


    /**
     * Gets the publisherAttributes value for this RepairShortCodeDataType.
     * 
     * @return publisherAttributes
     */
    public flexnet.macrovision.com.SimpleAttributeDataType[] getPublisherAttributes() {
        return publisherAttributes;
    }


    /**
     * Sets the publisherAttributes value for this RepairShortCodeDataType.
     * 
     * @param publisherAttributes
     */
    public void setPublisherAttributes(flexnet.macrovision.com.SimpleAttributeDataType[] publisherAttributes) {
        this.publisherAttributes = publisherAttributes;
    }


    /**
     * Gets the overridePolicy value for this RepairShortCodeDataType.
     * 
     * @return overridePolicy
     */
    public java.lang.Boolean getOverridePolicy() {
        return overridePolicy;
    }


    /**
     * Sets the overridePolicy value for this RepairShortCodeDataType.
     * 
     * @param overridePolicy
     */
    public void setOverridePolicy(java.lang.Boolean overridePolicy) {
        this.overridePolicy = overridePolicy;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RepairShortCodeDataType)) return false;
        RepairShortCodeDataType other = (RepairShortCodeDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.shortCode==null && other.getShortCode()==null) || 
             (this.shortCode!=null &&
              this.shortCode.equals(other.getShortCode()))) &&
            ((this.webRegKey==null && other.getWebRegKey()==null) || 
             (this.webRegKey!=null &&
              this.webRegKey.equals(other.getWebRegKey()))) &&
            ((this.publisherAttributes==null && other.getPublisherAttributes()==null) || 
             (this.publisherAttributes!=null &&
              java.util.Arrays.equals(this.publisherAttributes, other.getPublisherAttributes()))) &&
            ((this.overridePolicy==null && other.getOverridePolicy()==null) || 
             (this.overridePolicy!=null &&
              this.overridePolicy.equals(other.getOverridePolicy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getShortCode() != null) {
            _hashCode += getShortCode().hashCode();
        }
        if (getWebRegKey() != null) {
            _hashCode += getWebRegKey().hashCode();
        }
        if (getPublisherAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPublisherAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPublisherAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOverridePolicy() != null) {
            _hashCode += getOverridePolicy().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RepairShortCodeDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "repairShortCodeDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shortCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webRegKey");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "webRegKey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publisherAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "simpleAttributeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overridePolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overridePolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
