/**
 * FulfillmentPropertiesType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FulfillmentPropertiesType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentId;

    private java.lang.String fulfillmentType;

    private flexnet.macrovision.com.StateType state;

    private flexnet.macrovision.com.EntitlementIdentifierType entitlementId;

    private flexnet.macrovision.com.EntitlementLineItemIdentifierType lineitemId;

    private flexnet.macrovision.com.ProductIdentifierType product;

    private java.lang.String productDescription;

    private flexnet.macrovision.com.PartNumberIdentifierType partNumber;

    private java.lang.String partNumberDescription;

    private flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnology;

    private flexnet.macrovision.com.LicenseModelIdentifierType licenseModel;

    private flexnet.macrovision.com.OrganizationIdentifierType soldTo;

    private java.lang.String soldToDisplayName;

    private java.lang.String shipToEmail;

    private java.lang.String shipToAddress;

    private java.lang.String licenseHost;

    private java.math.BigInteger fulfilledCount;

    private java.math.BigInteger overDraftCount;

    private java.util.Date fulfillDate;

    private java.util.Calendar fulfillDateTime;

    private java.lang.Boolean isPermanent;

    private java.util.Date startDate;

    private java.util.Date expirationDate;

    private java.util.Date versionDate;

    private java.lang.String licenseFileType;

    private java.lang.String licenseText;

    private byte[] binaryLicense;

    private flexnet.macrovision.com.SupportLicenseType supportAction;

    private java.util.Calendar lastModifiedDateTime;

    private flexnet.macrovision.com.FulfillmentIdentifierType parentFulfillmentId;

    private flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource;

    private java.lang.String orderId;

    private java.lang.String orderLineNumber;

    private java.lang.String lineitemDescription;

    private java.math.BigInteger totalCopies;

    private java.math.BigInteger numberOfRemainingCopies;

    private java.lang.Boolean isTrusted;

    private flexnet.macrovision.com.AttributeDescriptorType[] customAttributes;

    private flexnet.macrovision.com.AttributeDescriptorType[] customHostAttributes;

    private java.lang.String migrationId;

    private java.lang.String vendorDaemonName;

    private flexnet.macrovision.com.LicenseFileDataType[] licenseFiles;

    private flexnet.macrovision.com.EntitledProductDataType[] entitledProducts;

    private java.lang.String FNPTimeZoneValue;

    private flexnet.macrovision.com.ActivationType activationType;

    public FulfillmentPropertiesType() {
    }

    public FulfillmentPropertiesType(
           flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentId,
           java.lang.String fulfillmentType,
           flexnet.macrovision.com.StateType state,
           flexnet.macrovision.com.EntitlementIdentifierType entitlementId,
           flexnet.macrovision.com.EntitlementLineItemIdentifierType lineitemId,
           flexnet.macrovision.com.ProductIdentifierType product,
           java.lang.String productDescription,
           flexnet.macrovision.com.PartNumberIdentifierType partNumber,
           java.lang.String partNumberDescription,
           flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnology,
           flexnet.macrovision.com.LicenseModelIdentifierType licenseModel,
           flexnet.macrovision.com.OrganizationIdentifierType soldTo,
           java.lang.String soldToDisplayName,
           java.lang.String shipToEmail,
           java.lang.String shipToAddress,
           java.lang.String licenseHost,
           java.math.BigInteger fulfilledCount,
           java.math.BigInteger overDraftCount,
           java.util.Date fulfillDate,
           java.util.Calendar fulfillDateTime,
           java.lang.Boolean isPermanent,
           java.util.Date startDate,
           java.util.Date expirationDate,
           java.util.Date versionDate,
           java.lang.String licenseFileType,
           java.lang.String licenseText,
           byte[] binaryLicense,
           flexnet.macrovision.com.SupportLicenseType supportAction,
           java.util.Calendar lastModifiedDateTime,
           flexnet.macrovision.com.FulfillmentIdentifierType parentFulfillmentId,
           flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource,
           java.lang.String orderId,
           java.lang.String orderLineNumber,
           java.lang.String lineitemDescription,
           java.math.BigInteger totalCopies,
           java.math.BigInteger numberOfRemainingCopies,
           java.lang.Boolean isTrusted,
           flexnet.macrovision.com.AttributeDescriptorType[] customAttributes,
           flexnet.macrovision.com.AttributeDescriptorType[] customHostAttributes,
           java.lang.String migrationId,
           java.lang.String vendorDaemonName,
           flexnet.macrovision.com.LicenseFileDataType[] licenseFiles,
           flexnet.macrovision.com.EntitledProductDataType[] entitledProducts,
           java.lang.String FNPTimeZoneValue,
           flexnet.macrovision.com.ActivationType activationType) {
           this.fulfillmentId = fulfillmentId;
           this.fulfillmentType = fulfillmentType;
           this.state = state;
           this.entitlementId = entitlementId;
           this.lineitemId = lineitemId;
           this.product = product;
           this.productDescription = productDescription;
           this.partNumber = partNumber;
           this.partNumberDescription = partNumberDescription;
           this.licenseTechnology = licenseTechnology;
           this.licenseModel = licenseModel;
           this.soldTo = soldTo;
           this.soldToDisplayName = soldToDisplayName;
           this.shipToEmail = shipToEmail;
           this.shipToAddress = shipToAddress;
           this.licenseHost = licenseHost;
           this.fulfilledCount = fulfilledCount;
           this.overDraftCount = overDraftCount;
           this.fulfillDate = fulfillDate;
           this.fulfillDateTime = fulfillDateTime;
           this.isPermanent = isPermanent;
           this.startDate = startDate;
           this.expirationDate = expirationDate;
           this.versionDate = versionDate;
           this.licenseFileType = licenseFileType;
           this.licenseText = licenseText;
           this.binaryLicense = binaryLicense;
           this.supportAction = supportAction;
           this.lastModifiedDateTime = lastModifiedDateTime;
           this.parentFulfillmentId = parentFulfillmentId;
           this.fulfillmentSource = fulfillmentSource;
           this.orderId = orderId;
           this.orderLineNumber = orderLineNumber;
           this.lineitemDescription = lineitemDescription;
           this.totalCopies = totalCopies;
           this.numberOfRemainingCopies = numberOfRemainingCopies;
           this.isTrusted = isTrusted;
           this.customAttributes = customAttributes;
           this.customHostAttributes = customHostAttributes;
           this.migrationId = migrationId;
           this.vendorDaemonName = vendorDaemonName;
           this.licenseFiles = licenseFiles;
           this.entitledProducts = entitledProducts;
           this.FNPTimeZoneValue = FNPTimeZoneValue;
           this.activationType = activationType;
    }


    /**
     * Gets the fulfillmentId value for this FulfillmentPropertiesType.
     * 
     * @return fulfillmentId
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this FulfillmentPropertiesType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the fulfillmentType value for this FulfillmentPropertiesType.
     * 
     * @return fulfillmentType
     */
    public java.lang.String getFulfillmentType() {
        return fulfillmentType;
    }


    /**
     * Sets the fulfillmentType value for this FulfillmentPropertiesType.
     * 
     * @param fulfillmentType
     */
    public void setFulfillmentType(java.lang.String fulfillmentType) {
        this.fulfillmentType = fulfillmentType;
    }


    /**
     * Gets the state value for this FulfillmentPropertiesType.
     * 
     * @return state
     */
    public flexnet.macrovision.com.StateType getState() {
        return state;
    }


    /**
     * Sets the state value for this FulfillmentPropertiesType.
     * 
     * @param state
     */
    public void setState(flexnet.macrovision.com.StateType state) {
        this.state = state;
    }


    /**
     * Gets the entitlementId value for this FulfillmentPropertiesType.
     * 
     * @return entitlementId
     */
    public flexnet.macrovision.com.EntitlementIdentifierType getEntitlementId() {
        return entitlementId;
    }


    /**
     * Sets the entitlementId value for this FulfillmentPropertiesType.
     * 
     * @param entitlementId
     */
    public void setEntitlementId(flexnet.macrovision.com.EntitlementIdentifierType entitlementId) {
        this.entitlementId = entitlementId;
    }


    /**
     * Gets the lineitemId value for this FulfillmentPropertiesType.
     * 
     * @return lineitemId
     */
    public flexnet.macrovision.com.EntitlementLineItemIdentifierType getLineitemId() {
        return lineitemId;
    }


    /**
     * Sets the lineitemId value for this FulfillmentPropertiesType.
     * 
     * @param lineitemId
     */
    public void setLineitemId(flexnet.macrovision.com.EntitlementLineItemIdentifierType lineitemId) {
        this.lineitemId = lineitemId;
    }


    /**
     * Gets the product value for this FulfillmentPropertiesType.
     * 
     * @return product
     */
    public flexnet.macrovision.com.ProductIdentifierType getProduct() {
        return product;
    }


    /**
     * Sets the product value for this FulfillmentPropertiesType.
     * 
     * @param product
     */
    public void setProduct(flexnet.macrovision.com.ProductIdentifierType product) {
        this.product = product;
    }


    /**
     * Gets the productDescription value for this FulfillmentPropertiesType.
     * 
     * @return productDescription
     */
    public java.lang.String getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this FulfillmentPropertiesType.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.String productDescription) {
        this.productDescription = productDescription;
    }


    /**
     * Gets the partNumber value for this FulfillmentPropertiesType.
     * 
     * @return partNumber
     */
    public flexnet.macrovision.com.PartNumberIdentifierType getPartNumber() {
        return partNumber;
    }


    /**
     * Sets the partNumber value for this FulfillmentPropertiesType.
     * 
     * @param partNumber
     */
    public void setPartNumber(flexnet.macrovision.com.PartNumberIdentifierType partNumber) {
        this.partNumber = partNumber;
    }


    /**
     * Gets the partNumberDescription value for this FulfillmentPropertiesType.
     * 
     * @return partNumberDescription
     */
    public java.lang.String getPartNumberDescription() {
        return partNumberDescription;
    }


    /**
     * Sets the partNumberDescription value for this FulfillmentPropertiesType.
     * 
     * @param partNumberDescription
     */
    public void setPartNumberDescription(java.lang.String partNumberDescription) {
        this.partNumberDescription = partNumberDescription;
    }


    /**
     * Gets the licenseTechnology value for this FulfillmentPropertiesType.
     * 
     * @return licenseTechnology
     */
    public flexnet.macrovision.com.LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }


    /**
     * Sets the licenseTechnology value for this FulfillmentPropertiesType.
     * 
     * @param licenseTechnology
     */
    public void setLicenseTechnology(flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnology) {
        this.licenseTechnology = licenseTechnology;
    }


    /**
     * Gets the licenseModel value for this FulfillmentPropertiesType.
     * 
     * @return licenseModel
     */
    public flexnet.macrovision.com.LicenseModelIdentifierType getLicenseModel() {
        return licenseModel;
    }


    /**
     * Sets the licenseModel value for this FulfillmentPropertiesType.
     * 
     * @param licenseModel
     */
    public void setLicenseModel(flexnet.macrovision.com.LicenseModelIdentifierType licenseModel) {
        this.licenseModel = licenseModel;
    }


    /**
     * Gets the soldTo value for this FulfillmentPropertiesType.
     * 
     * @return soldTo
     */
    public flexnet.macrovision.com.OrganizationIdentifierType getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this FulfillmentPropertiesType.
     * 
     * @param soldTo
     */
    public void setSoldTo(flexnet.macrovision.com.OrganizationIdentifierType soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the soldToDisplayName value for this FulfillmentPropertiesType.
     * 
     * @return soldToDisplayName
     */
    public java.lang.String getSoldToDisplayName() {
        return soldToDisplayName;
    }


    /**
     * Sets the soldToDisplayName value for this FulfillmentPropertiesType.
     * 
     * @param soldToDisplayName
     */
    public void setSoldToDisplayName(java.lang.String soldToDisplayName) {
        this.soldToDisplayName = soldToDisplayName;
    }


    /**
     * Gets the shipToEmail value for this FulfillmentPropertiesType.
     * 
     * @return shipToEmail
     */
    public java.lang.String getShipToEmail() {
        return shipToEmail;
    }


    /**
     * Sets the shipToEmail value for this FulfillmentPropertiesType.
     * 
     * @param shipToEmail
     */
    public void setShipToEmail(java.lang.String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }


    /**
     * Gets the shipToAddress value for this FulfillmentPropertiesType.
     * 
     * @return shipToAddress
     */
    public java.lang.String getShipToAddress() {
        return shipToAddress;
    }


    /**
     * Sets the shipToAddress value for this FulfillmentPropertiesType.
     * 
     * @param shipToAddress
     */
    public void setShipToAddress(java.lang.String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }


    /**
     * Gets the licenseHost value for this FulfillmentPropertiesType.
     * 
     * @return licenseHost
     */
    public java.lang.String getLicenseHost() {
        return licenseHost;
    }


    /**
     * Sets the licenseHost value for this FulfillmentPropertiesType.
     * 
     * @param licenseHost
     */
    public void setLicenseHost(java.lang.String licenseHost) {
        this.licenseHost = licenseHost;
    }


    /**
     * Gets the fulfilledCount value for this FulfillmentPropertiesType.
     * 
     * @return fulfilledCount
     */
    public java.math.BigInteger getFulfilledCount() {
        return fulfilledCount;
    }


    /**
     * Sets the fulfilledCount value for this FulfillmentPropertiesType.
     * 
     * @param fulfilledCount
     */
    public void setFulfilledCount(java.math.BigInteger fulfilledCount) {
        this.fulfilledCount = fulfilledCount;
    }


    /**
     * Gets the overDraftCount value for this FulfillmentPropertiesType.
     * 
     * @return overDraftCount
     */
    public java.math.BigInteger getOverDraftCount() {
        return overDraftCount;
    }


    /**
     * Sets the overDraftCount value for this FulfillmentPropertiesType.
     * 
     * @param overDraftCount
     */
    public void setOverDraftCount(java.math.BigInteger overDraftCount) {
        this.overDraftCount = overDraftCount;
    }


    /**
     * Gets the fulfillDate value for this FulfillmentPropertiesType.
     * 
     * @return fulfillDate
     */
    public java.util.Date getFulfillDate() {
        return fulfillDate;
    }


    /**
     * Sets the fulfillDate value for this FulfillmentPropertiesType.
     * 
     * @param fulfillDate
     */
    public void setFulfillDate(java.util.Date fulfillDate) {
        this.fulfillDate = fulfillDate;
    }


    /**
     * Gets the fulfillDateTime value for this FulfillmentPropertiesType.
     * 
     * @return fulfillDateTime
     */
    public java.util.Calendar getFulfillDateTime() {
        return fulfillDateTime;
    }


    /**
     * Sets the fulfillDateTime value for this FulfillmentPropertiesType.
     * 
     * @param fulfillDateTime
     */
    public void setFulfillDateTime(java.util.Calendar fulfillDateTime) {
        this.fulfillDateTime = fulfillDateTime;
    }


    /**
     * Gets the isPermanent value for this FulfillmentPropertiesType.
     * 
     * @return isPermanent
     */
    public java.lang.Boolean getIsPermanent() {
        return isPermanent;
    }


    /**
     * Sets the isPermanent value for this FulfillmentPropertiesType.
     * 
     * @param isPermanent
     */
    public void setIsPermanent(java.lang.Boolean isPermanent) {
        this.isPermanent = isPermanent;
    }


    /**
     * Gets the startDate value for this FulfillmentPropertiesType.
     * 
     * @return startDate
     */
    public java.util.Date getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this FulfillmentPropertiesType.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the expirationDate value for this FulfillmentPropertiesType.
     * 
     * @return expirationDate
     */
    public java.util.Date getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this FulfillmentPropertiesType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Date expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the versionDate value for this FulfillmentPropertiesType.
     * 
     * @return versionDate
     */
    public java.util.Date getVersionDate() {
        return versionDate;
    }


    /**
     * Sets the versionDate value for this FulfillmentPropertiesType.
     * 
     * @param versionDate
     */
    public void setVersionDate(java.util.Date versionDate) {
        this.versionDate = versionDate;
    }


    /**
     * Gets the licenseFileType value for this FulfillmentPropertiesType.
     * 
     * @return licenseFileType
     */
    public java.lang.String getLicenseFileType() {
        return licenseFileType;
    }


    /**
     * Sets the licenseFileType value for this FulfillmentPropertiesType.
     * 
     * @param licenseFileType
     */
    public void setLicenseFileType(java.lang.String licenseFileType) {
        this.licenseFileType = licenseFileType;
    }


    /**
     * Gets the licenseText value for this FulfillmentPropertiesType.
     * 
     * @return licenseText
     */
    public java.lang.String getLicenseText() {
        return licenseText;
    }


    /**
     * Sets the licenseText value for this FulfillmentPropertiesType.
     * 
     * @param licenseText
     */
    public void setLicenseText(java.lang.String licenseText) {
        this.licenseText = licenseText;
    }


    /**
     * Gets the binaryLicense value for this FulfillmentPropertiesType.
     * 
     * @return binaryLicense
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }


    /**
     * Sets the binaryLicense value for this FulfillmentPropertiesType.
     * 
     * @param binaryLicense
     */
    public void setBinaryLicense(byte[] binaryLicense) {
        this.binaryLicense = binaryLicense;
    }


    /**
     * Gets the supportAction value for this FulfillmentPropertiesType.
     * 
     * @return supportAction
     */
    public flexnet.macrovision.com.SupportLicenseType getSupportAction() {
        return supportAction;
    }


    /**
     * Sets the supportAction value for this FulfillmentPropertiesType.
     * 
     * @param supportAction
     */
    public void setSupportAction(flexnet.macrovision.com.SupportLicenseType supportAction) {
        this.supportAction = supportAction;
    }


    /**
     * Gets the lastModifiedDateTime value for this FulfillmentPropertiesType.
     * 
     * @return lastModifiedDateTime
     */
    public java.util.Calendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }


    /**
     * Sets the lastModifiedDateTime value for this FulfillmentPropertiesType.
     * 
     * @param lastModifiedDateTime
     */
    public void setLastModifiedDateTime(java.util.Calendar lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }


    /**
     * Gets the parentFulfillmentId value for this FulfillmentPropertiesType.
     * 
     * @return parentFulfillmentId
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getParentFulfillmentId() {
        return parentFulfillmentId;
    }


    /**
     * Sets the parentFulfillmentId value for this FulfillmentPropertiesType.
     * 
     * @param parentFulfillmentId
     */
    public void setParentFulfillmentId(flexnet.macrovision.com.FulfillmentIdentifierType parentFulfillmentId) {
        this.parentFulfillmentId = parentFulfillmentId;
    }


    /**
     * Gets the fulfillmentSource value for this FulfillmentPropertiesType.
     * 
     * @return fulfillmentSource
     */
    public flexnet.macrovision.com.FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }


    /**
     * Sets the fulfillmentSource value for this FulfillmentPropertiesType.
     * 
     * @param fulfillmentSource
     */
    public void setFulfillmentSource(flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource) {
        this.fulfillmentSource = fulfillmentSource;
    }


    /**
     * Gets the orderId value for this FulfillmentPropertiesType.
     * 
     * @return orderId
     */
    public java.lang.String getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this FulfillmentPropertiesType.
     * 
     * @param orderId
     */
    public void setOrderId(java.lang.String orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the orderLineNumber value for this FulfillmentPropertiesType.
     * 
     * @return orderLineNumber
     */
    public java.lang.String getOrderLineNumber() {
        return orderLineNumber;
    }


    /**
     * Sets the orderLineNumber value for this FulfillmentPropertiesType.
     * 
     * @param orderLineNumber
     */
    public void setOrderLineNumber(java.lang.String orderLineNumber) {
        this.orderLineNumber = orderLineNumber;
    }


    /**
     * Gets the lineitemDescription value for this FulfillmentPropertiesType.
     * 
     * @return lineitemDescription
     */
    public java.lang.String getLineitemDescription() {
        return lineitemDescription;
    }


    /**
     * Sets the lineitemDescription value for this FulfillmentPropertiesType.
     * 
     * @param lineitemDescription
     */
    public void setLineitemDescription(java.lang.String lineitemDescription) {
        this.lineitemDescription = lineitemDescription;
    }


    /**
     * Gets the totalCopies value for this FulfillmentPropertiesType.
     * 
     * @return totalCopies
     */
    public java.math.BigInteger getTotalCopies() {
        return totalCopies;
    }


    /**
     * Sets the totalCopies value for this FulfillmentPropertiesType.
     * 
     * @param totalCopies
     */
    public void setTotalCopies(java.math.BigInteger totalCopies) {
        this.totalCopies = totalCopies;
    }


    /**
     * Gets the numberOfRemainingCopies value for this FulfillmentPropertiesType.
     * 
     * @return numberOfRemainingCopies
     */
    public java.math.BigInteger getNumberOfRemainingCopies() {
        return numberOfRemainingCopies;
    }


    /**
     * Sets the numberOfRemainingCopies value for this FulfillmentPropertiesType.
     * 
     * @param numberOfRemainingCopies
     */
    public void setNumberOfRemainingCopies(java.math.BigInteger numberOfRemainingCopies) {
        this.numberOfRemainingCopies = numberOfRemainingCopies;
    }


    /**
     * Gets the isTrusted value for this FulfillmentPropertiesType.
     * 
     * @return isTrusted
     */
    public java.lang.Boolean getIsTrusted() {
        return isTrusted;
    }


    /**
     * Sets the isTrusted value for this FulfillmentPropertiesType.
     * 
     * @param isTrusted
     */
    public void setIsTrusted(java.lang.Boolean isTrusted) {
        this.isTrusted = isTrusted;
    }


    /**
     * Gets the customAttributes value for this FulfillmentPropertiesType.
     * 
     * @return customAttributes
     */
    public flexnet.macrovision.com.AttributeDescriptorType[] getCustomAttributes() {
        return customAttributes;
    }


    /**
     * Sets the customAttributes value for this FulfillmentPropertiesType.
     * 
     * @param customAttributes
     */
    public void setCustomAttributes(flexnet.macrovision.com.AttributeDescriptorType[] customAttributes) {
        this.customAttributes = customAttributes;
    }


    /**
     * Gets the customHostAttributes value for this FulfillmentPropertiesType.
     * 
     * @return customHostAttributes
     */
    public flexnet.macrovision.com.AttributeDescriptorType[] getCustomHostAttributes() {
        return customHostAttributes;
    }


    /**
     * Sets the customHostAttributes value for this FulfillmentPropertiesType.
     * 
     * @param customHostAttributes
     */
    public void setCustomHostAttributes(flexnet.macrovision.com.AttributeDescriptorType[] customHostAttributes) {
        this.customHostAttributes = customHostAttributes;
    }


    /**
     * Gets the migrationId value for this FulfillmentPropertiesType.
     * 
     * @return migrationId
     */
    public java.lang.String getMigrationId() {
        return migrationId;
    }


    /**
     * Sets the migrationId value for this FulfillmentPropertiesType.
     * 
     * @param migrationId
     */
    public void setMigrationId(java.lang.String migrationId) {
        this.migrationId = migrationId;
    }


    /**
     * Gets the vendorDaemonName value for this FulfillmentPropertiesType.
     * 
     * @return vendorDaemonName
     */
    public java.lang.String getVendorDaemonName() {
        return vendorDaemonName;
    }


    /**
     * Sets the vendorDaemonName value for this FulfillmentPropertiesType.
     * 
     * @param vendorDaemonName
     */
    public void setVendorDaemonName(java.lang.String vendorDaemonName) {
        this.vendorDaemonName = vendorDaemonName;
    }


    /**
     * Gets the licenseFiles value for this FulfillmentPropertiesType.
     * 
     * @return licenseFiles
     */
    public flexnet.macrovision.com.LicenseFileDataType[] getLicenseFiles() {
        return licenseFiles;
    }


    /**
     * Sets the licenseFiles value for this FulfillmentPropertiesType.
     * 
     * @param licenseFiles
     */
    public void setLicenseFiles(flexnet.macrovision.com.LicenseFileDataType[] licenseFiles) {
        this.licenseFiles = licenseFiles;
    }


    /**
     * Gets the entitledProducts value for this FulfillmentPropertiesType.
     * 
     * @return entitledProducts
     */
    public flexnet.macrovision.com.EntitledProductDataType[] getEntitledProducts() {
        return entitledProducts;
    }


    /**
     * Sets the entitledProducts value for this FulfillmentPropertiesType.
     * 
     * @param entitledProducts
     */
    public void setEntitledProducts(flexnet.macrovision.com.EntitledProductDataType[] entitledProducts) {
        this.entitledProducts = entitledProducts;
    }


    /**
     * Gets the FNPTimeZoneValue value for this FulfillmentPropertiesType.
     * 
     * @return FNPTimeZoneValue
     */
    public java.lang.String getFNPTimeZoneValue() {
        return FNPTimeZoneValue;
    }


    /**
     * Sets the FNPTimeZoneValue value for this FulfillmentPropertiesType.
     * 
     * @param FNPTimeZoneValue
     */
    public void setFNPTimeZoneValue(java.lang.String FNPTimeZoneValue) {
        this.FNPTimeZoneValue = FNPTimeZoneValue;
    }


    /**
     * Gets the activationType value for this FulfillmentPropertiesType.
     * 
     * @return activationType
     */
    public flexnet.macrovision.com.ActivationType getActivationType() {
        return activationType;
    }


    /**
     * Sets the activationType value for this FulfillmentPropertiesType.
     * 
     * @param activationType
     */
    public void setActivationType(flexnet.macrovision.com.ActivationType activationType) {
        this.activationType = activationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FulfillmentPropertiesType)) return false;
        FulfillmentPropertiesType other = (FulfillmentPropertiesType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.fulfillmentType==null && other.getFulfillmentType()==null) || 
             (this.fulfillmentType!=null &&
              this.fulfillmentType.equals(other.getFulfillmentType()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.entitlementId==null && other.getEntitlementId()==null) || 
             (this.entitlementId!=null &&
              this.entitlementId.equals(other.getEntitlementId()))) &&
            ((this.lineitemId==null && other.getLineitemId()==null) || 
             (this.lineitemId!=null &&
              this.lineitemId.equals(other.getLineitemId()))) &&
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              this.product.equals(other.getProduct()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            ((this.partNumber==null && other.getPartNumber()==null) || 
             (this.partNumber!=null &&
              this.partNumber.equals(other.getPartNumber()))) &&
            ((this.partNumberDescription==null && other.getPartNumberDescription()==null) || 
             (this.partNumberDescription!=null &&
              this.partNumberDescription.equals(other.getPartNumberDescription()))) &&
            ((this.licenseTechnology==null && other.getLicenseTechnology()==null) || 
             (this.licenseTechnology!=null &&
              this.licenseTechnology.equals(other.getLicenseTechnology()))) &&
            ((this.licenseModel==null && other.getLicenseModel()==null) || 
             (this.licenseModel!=null &&
              this.licenseModel.equals(other.getLicenseModel()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.soldToDisplayName==null && other.getSoldToDisplayName()==null) || 
             (this.soldToDisplayName!=null &&
              this.soldToDisplayName.equals(other.getSoldToDisplayName()))) &&
            ((this.shipToEmail==null && other.getShipToEmail()==null) || 
             (this.shipToEmail!=null &&
              this.shipToEmail.equals(other.getShipToEmail()))) &&
            ((this.shipToAddress==null && other.getShipToAddress()==null) || 
             (this.shipToAddress!=null &&
              this.shipToAddress.equals(other.getShipToAddress()))) &&
            ((this.licenseHost==null && other.getLicenseHost()==null) || 
             (this.licenseHost!=null &&
              this.licenseHost.equals(other.getLicenseHost()))) &&
            ((this.fulfilledCount==null && other.getFulfilledCount()==null) || 
             (this.fulfilledCount!=null &&
              this.fulfilledCount.equals(other.getFulfilledCount()))) &&
            ((this.overDraftCount==null && other.getOverDraftCount()==null) || 
             (this.overDraftCount!=null &&
              this.overDraftCount.equals(other.getOverDraftCount()))) &&
            ((this.fulfillDate==null && other.getFulfillDate()==null) || 
             (this.fulfillDate!=null &&
              this.fulfillDate.equals(other.getFulfillDate()))) &&
            ((this.fulfillDateTime==null && other.getFulfillDateTime()==null) || 
             (this.fulfillDateTime!=null &&
              this.fulfillDateTime.equals(other.getFulfillDateTime()))) &&
            ((this.isPermanent==null && other.getIsPermanent()==null) || 
             (this.isPermanent!=null &&
              this.isPermanent.equals(other.getIsPermanent()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.versionDate==null && other.getVersionDate()==null) || 
             (this.versionDate!=null &&
              this.versionDate.equals(other.getVersionDate()))) &&
            ((this.licenseFileType==null && other.getLicenseFileType()==null) || 
             (this.licenseFileType!=null &&
              this.licenseFileType.equals(other.getLicenseFileType()))) &&
            ((this.licenseText==null && other.getLicenseText()==null) || 
             (this.licenseText!=null &&
              this.licenseText.equals(other.getLicenseText()))) &&
            ((this.binaryLicense==null && other.getBinaryLicense()==null) || 
             (this.binaryLicense!=null &&
              java.util.Arrays.equals(this.binaryLicense, other.getBinaryLicense()))) &&
            ((this.supportAction==null && other.getSupportAction()==null) || 
             (this.supportAction!=null &&
              this.supportAction.equals(other.getSupportAction()))) &&
            ((this.lastModifiedDateTime==null && other.getLastModifiedDateTime()==null) || 
             (this.lastModifiedDateTime!=null &&
              this.lastModifiedDateTime.equals(other.getLastModifiedDateTime()))) &&
            ((this.parentFulfillmentId==null && other.getParentFulfillmentId()==null) || 
             (this.parentFulfillmentId!=null &&
              this.parentFulfillmentId.equals(other.getParentFulfillmentId()))) &&
            ((this.fulfillmentSource==null && other.getFulfillmentSource()==null) || 
             (this.fulfillmentSource!=null &&
              this.fulfillmentSource.equals(other.getFulfillmentSource()))) &&
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.orderLineNumber==null && other.getOrderLineNumber()==null) || 
             (this.orderLineNumber!=null &&
              this.orderLineNumber.equals(other.getOrderLineNumber()))) &&
            ((this.lineitemDescription==null && other.getLineitemDescription()==null) || 
             (this.lineitemDescription!=null &&
              this.lineitemDescription.equals(other.getLineitemDescription()))) &&
            ((this.totalCopies==null && other.getTotalCopies()==null) || 
             (this.totalCopies!=null &&
              this.totalCopies.equals(other.getTotalCopies()))) &&
            ((this.numberOfRemainingCopies==null && other.getNumberOfRemainingCopies()==null) || 
             (this.numberOfRemainingCopies!=null &&
              this.numberOfRemainingCopies.equals(other.getNumberOfRemainingCopies()))) &&
            ((this.isTrusted==null && other.getIsTrusted()==null) || 
             (this.isTrusted!=null &&
              this.isTrusted.equals(other.getIsTrusted()))) &&
            ((this.customAttributes==null && other.getCustomAttributes()==null) || 
             (this.customAttributes!=null &&
              java.util.Arrays.equals(this.customAttributes, other.getCustomAttributes()))) &&
            ((this.customHostAttributes==null && other.getCustomHostAttributes()==null) || 
             (this.customHostAttributes!=null &&
              java.util.Arrays.equals(this.customHostAttributes, other.getCustomHostAttributes()))) &&
            ((this.migrationId==null && other.getMigrationId()==null) || 
             (this.migrationId!=null &&
              this.migrationId.equals(other.getMigrationId()))) &&
            ((this.vendorDaemonName==null && other.getVendorDaemonName()==null) || 
             (this.vendorDaemonName!=null &&
              this.vendorDaemonName.equals(other.getVendorDaemonName()))) &&
            ((this.licenseFiles==null && other.getLicenseFiles()==null) || 
             (this.licenseFiles!=null &&
              java.util.Arrays.equals(this.licenseFiles, other.getLicenseFiles()))) &&
            ((this.entitledProducts==null && other.getEntitledProducts()==null) || 
             (this.entitledProducts!=null &&
              java.util.Arrays.equals(this.entitledProducts, other.getEntitledProducts()))) &&
            ((this.FNPTimeZoneValue==null && other.getFNPTimeZoneValue()==null) || 
             (this.FNPTimeZoneValue!=null &&
              this.FNPTimeZoneValue.equals(other.getFNPTimeZoneValue()))) &&
            ((this.activationType==null && other.getActivationType()==null) || 
             (this.activationType!=null &&
              this.activationType.equals(other.getActivationType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getFulfillmentType() != null) {
            _hashCode += getFulfillmentType().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getEntitlementId() != null) {
            _hashCode += getEntitlementId().hashCode();
        }
        if (getLineitemId() != null) {
            _hashCode += getLineitemId().hashCode();
        }
        if (getProduct() != null) {
            _hashCode += getProduct().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getPartNumber() != null) {
            _hashCode += getPartNumber().hashCode();
        }
        if (getPartNumberDescription() != null) {
            _hashCode += getPartNumberDescription().hashCode();
        }
        if (getLicenseTechnology() != null) {
            _hashCode += getLicenseTechnology().hashCode();
        }
        if (getLicenseModel() != null) {
            _hashCode += getLicenseModel().hashCode();
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getSoldToDisplayName() != null) {
            _hashCode += getSoldToDisplayName().hashCode();
        }
        if (getShipToEmail() != null) {
            _hashCode += getShipToEmail().hashCode();
        }
        if (getShipToAddress() != null) {
            _hashCode += getShipToAddress().hashCode();
        }
        if (getLicenseHost() != null) {
            _hashCode += getLicenseHost().hashCode();
        }
        if (getFulfilledCount() != null) {
            _hashCode += getFulfilledCount().hashCode();
        }
        if (getOverDraftCount() != null) {
            _hashCode += getOverDraftCount().hashCode();
        }
        if (getFulfillDate() != null) {
            _hashCode += getFulfillDate().hashCode();
        }
        if (getFulfillDateTime() != null) {
            _hashCode += getFulfillDateTime().hashCode();
        }
        if (getIsPermanent() != null) {
            _hashCode += getIsPermanent().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getVersionDate() != null) {
            _hashCode += getVersionDate().hashCode();
        }
        if (getLicenseFileType() != null) {
            _hashCode += getLicenseFileType().hashCode();
        }
        if (getLicenseText() != null) {
            _hashCode += getLicenseText().hashCode();
        }
        if (getBinaryLicense() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBinaryLicense());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBinaryLicense(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSupportAction() != null) {
            _hashCode += getSupportAction().hashCode();
        }
        if (getLastModifiedDateTime() != null) {
            _hashCode += getLastModifiedDateTime().hashCode();
        }
        if (getParentFulfillmentId() != null) {
            _hashCode += getParentFulfillmentId().hashCode();
        }
        if (getFulfillmentSource() != null) {
            _hashCode += getFulfillmentSource().hashCode();
        }
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getOrderLineNumber() != null) {
            _hashCode += getOrderLineNumber().hashCode();
        }
        if (getLineitemDescription() != null) {
            _hashCode += getLineitemDescription().hashCode();
        }
        if (getTotalCopies() != null) {
            _hashCode += getTotalCopies().hashCode();
        }
        if (getNumberOfRemainingCopies() != null) {
            _hashCode += getNumberOfRemainingCopies().hashCode();
        }
        if (getIsTrusted() != null) {
            _hashCode += getIsTrusted().hashCode();
        }
        if (getCustomAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHostAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomHostAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomHostAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMigrationId() != null) {
            _hashCode += getMigrationId().hashCode();
        }
        if (getVendorDaemonName() != null) {
            _hashCode += getVendorDaemonName().hashCode();
        }
        if (getLicenseFiles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseFiles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseFiles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEntitledProducts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEntitledProducts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEntitledProducts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFNPTimeZoneValue() != null) {
            _hashCode += getFNPTimeZoneValue().hashCode();
        }
        if (getActivationType() != null) {
            _hashCode += getActivationType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FulfillmentPropertiesType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentPropertiesType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitlementId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineitemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lineitemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementLineItemIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "product"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "productIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "productDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumberIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partNumberDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumberDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseTechnology");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnology"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnologyIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseModel");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModel"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModelIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "organizationIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldToDisplayName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldToDisplayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfilledCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfilledCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPermanent");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "isPermanent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "versionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFileType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseText");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binaryLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "binaryLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supportAction");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "supportAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SupportLicenseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastModifiedDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lastModifiedDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentFulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "parentFulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentSource");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FulfillmentSourceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "orderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderLineNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "orderLineNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineitemDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lineitemDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCopies");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "totalCopies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfRemainingCopies");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "numberOfRemainingCopies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isTrusted");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "isTrusted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHostAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHostAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("migrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "migrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorDaemonName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "vendorDaemonName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFiles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFiles"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFile"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitledProducts");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProducts"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProductDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProduct"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FNPTimeZoneValue");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FNPTimeZoneValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ActivationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
