/**
 * ReturnFulfillmentDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ReturnFulfillmentDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier;

    private java.math.BigInteger partialCount;

    private java.math.BigInteger overDraftCount;

    private java.lang.Boolean overridePolicy;

    private java.lang.Boolean forceReturnOfThisTrustedFulfillment;

    public ReturnFulfillmentDataType() {
    }

    public ReturnFulfillmentDataType(
           flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier,
           java.math.BigInteger partialCount,
           java.math.BigInteger overDraftCount,
           java.lang.Boolean overridePolicy,
           java.lang.Boolean forceReturnOfThisTrustedFulfillment) {
           this.fulfillmentIdentifier = fulfillmentIdentifier;
           this.partialCount = partialCount;
           this.overDraftCount = overDraftCount;
           this.overridePolicy = overridePolicy;
           this.forceReturnOfThisTrustedFulfillment = forceReturnOfThisTrustedFulfillment;
    }


    /**
     * Gets the fulfillmentIdentifier value for this ReturnFulfillmentDataType.
     * 
     * @return fulfillmentIdentifier
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }


    /**
     * Sets the fulfillmentIdentifier value for this ReturnFulfillmentDataType.
     * 
     * @param fulfillmentIdentifier
     */
    public void setFulfillmentIdentifier(flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier) {
        this.fulfillmentIdentifier = fulfillmentIdentifier;
    }


    /**
     * Gets the partialCount value for this ReturnFulfillmentDataType.
     * 
     * @return partialCount
     */
    public java.math.BigInteger getPartialCount() {
        return partialCount;
    }


    /**
     * Sets the partialCount value for this ReturnFulfillmentDataType.
     * 
     * @param partialCount
     */
    public void setPartialCount(java.math.BigInteger partialCount) {
        this.partialCount = partialCount;
    }


    /**
     * Gets the overDraftCount value for this ReturnFulfillmentDataType.
     * 
     * @return overDraftCount
     */
    public java.math.BigInteger getOverDraftCount() {
        return overDraftCount;
    }


    /**
     * Sets the overDraftCount value for this ReturnFulfillmentDataType.
     * 
     * @param overDraftCount
     */
    public void setOverDraftCount(java.math.BigInteger overDraftCount) {
        this.overDraftCount = overDraftCount;
    }


    /**
     * Gets the overridePolicy value for this ReturnFulfillmentDataType.
     * 
     * @return overridePolicy
     */
    public java.lang.Boolean getOverridePolicy() {
        return overridePolicy;
    }


    /**
     * Sets the overridePolicy value for this ReturnFulfillmentDataType.
     * 
     * @param overridePolicy
     */
    public void setOverridePolicy(java.lang.Boolean overridePolicy) {
        this.overridePolicy = overridePolicy;
    }


    /**
     * Gets the forceReturnOfThisTrustedFulfillment value for this ReturnFulfillmentDataType.
     * 
     * @return forceReturnOfThisTrustedFulfillment
     */
    public java.lang.Boolean getForceReturnOfThisTrustedFulfillment() {
        return forceReturnOfThisTrustedFulfillment;
    }


    /**
     * Sets the forceReturnOfThisTrustedFulfillment value for this ReturnFulfillmentDataType.
     * 
     * @param forceReturnOfThisTrustedFulfillment
     */
    public void setForceReturnOfThisTrustedFulfillment(java.lang.Boolean forceReturnOfThisTrustedFulfillment) {
        this.forceReturnOfThisTrustedFulfillment = forceReturnOfThisTrustedFulfillment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReturnFulfillmentDataType)) return false;
        ReturnFulfillmentDataType other = (ReturnFulfillmentDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentIdentifier==null && other.getFulfillmentIdentifier()==null) || 
             (this.fulfillmentIdentifier!=null &&
              this.fulfillmentIdentifier.equals(other.getFulfillmentIdentifier()))) &&
            ((this.partialCount==null && other.getPartialCount()==null) || 
             (this.partialCount!=null &&
              this.partialCount.equals(other.getPartialCount()))) &&
            ((this.overDraftCount==null && other.getOverDraftCount()==null) || 
             (this.overDraftCount!=null &&
              this.overDraftCount.equals(other.getOverDraftCount()))) &&
            ((this.overridePolicy==null && other.getOverridePolicy()==null) || 
             (this.overridePolicy!=null &&
              this.overridePolicy.equals(other.getOverridePolicy()))) &&
            ((this.forceReturnOfThisTrustedFulfillment==null && other.getForceReturnOfThisTrustedFulfillment()==null) || 
             (this.forceReturnOfThisTrustedFulfillment!=null &&
              this.forceReturnOfThisTrustedFulfillment.equals(other.getForceReturnOfThisTrustedFulfillment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentIdentifier() != null) {
            _hashCode += getFulfillmentIdentifier().hashCode();
        }
        if (getPartialCount() != null) {
            _hashCode += getPartialCount().hashCode();
        }
        if (getOverDraftCount() != null) {
            _hashCode += getOverDraftCount().hashCode();
        }
        if (getOverridePolicy() != null) {
            _hashCode += getOverridePolicy().hashCode();
        }
        if (getForceReturnOfThisTrustedFulfillment() != null) {
            _hashCode += getForceReturnOfThisTrustedFulfillment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReturnFulfillmentDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partialCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partialCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overridePolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overridePolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("forceReturnOfThisTrustedFulfillment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "forceReturnOfThisTrustedFulfillment"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
