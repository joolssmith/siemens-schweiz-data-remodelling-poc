/**
 * FailedPublisherErrorResponseDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FailedPublisherErrorResponseDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.PublisherErrorFulfillmentDataType fulfillment;

    private java.lang.String reason;

    public FailedPublisherErrorResponseDataType() {
    }

    public FailedPublisherErrorResponseDataType(
           flexnet.macrovision.com.PublisherErrorFulfillmentDataType fulfillment,
           java.lang.String reason) {
           this.fulfillment = fulfillment;
           this.reason = reason;
    }


    /**
     * Gets the fulfillment value for this FailedPublisherErrorResponseDataType.
     * 
     * @return fulfillment
     */
    public flexnet.macrovision.com.PublisherErrorFulfillmentDataType getFulfillment() {
        return fulfillment;
    }


    /**
     * Sets the fulfillment value for this FailedPublisherErrorResponseDataType.
     * 
     * @param fulfillment
     */
    public void setFulfillment(flexnet.macrovision.com.PublisherErrorFulfillmentDataType fulfillment) {
        this.fulfillment = fulfillment;
    }


    /**
     * Gets the reason value for this FailedPublisherErrorResponseDataType.
     * 
     * @return reason
     */
    public java.lang.String getReason() {
        return reason;
    }


    /**
     * Sets the reason value for this FailedPublisherErrorResponseDataType.
     * 
     * @param reason
     */
    public void setReason(java.lang.String reason) {
        this.reason = reason;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FailedPublisherErrorResponseDataType)) return false;
        FailedPublisherErrorResponseDataType other = (FailedPublisherErrorResponseDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillment==null && other.getFulfillment()==null) || 
             (this.fulfillment!=null &&
              this.fulfillment.equals(other.getFulfillment()))) &&
            ((this.reason==null && other.getReason()==null) || 
             (this.reason!=null &&
              this.reason.equals(other.getReason())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillment() != null) {
            _hashCode += getFulfillment().hashCode();
        }
        if (getReason() != null) {
            _hashCode += getReason().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FailedPublisherErrorResponseDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedPublisherErrorResponseDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentDataType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reason");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "reason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
