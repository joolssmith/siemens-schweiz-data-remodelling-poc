/**
 * ReturnFulfillmentResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ReturnFulfillmentResponseType  implements java.io.Serializable {
    private flexnet.macrovision.com.StatusInfoType statusInfo;

    private flexnet.macrovision.com.ReturnFulfillmentResponseDataType[] responseData;

    private flexnet.macrovision.com.FailedReturnResponseDataType[] failedData;

    public ReturnFulfillmentResponseType() {
    }

    public ReturnFulfillmentResponseType(
           flexnet.macrovision.com.StatusInfoType statusInfo,
           flexnet.macrovision.com.ReturnFulfillmentResponseDataType[] responseData,
           flexnet.macrovision.com.FailedReturnResponseDataType[] failedData) {
           this.statusInfo = statusInfo;
           this.responseData = responseData;
           this.failedData = failedData;
    }


    /**
     * Gets the statusInfo value for this ReturnFulfillmentResponseType.
     * 
     * @return statusInfo
     */
    public flexnet.macrovision.com.StatusInfoType getStatusInfo() {
        return statusInfo;
    }


    /**
     * Sets the statusInfo value for this ReturnFulfillmentResponseType.
     * 
     * @param statusInfo
     */
    public void setStatusInfo(flexnet.macrovision.com.StatusInfoType statusInfo) {
        this.statusInfo = statusInfo;
    }


    /**
     * Gets the responseData value for this ReturnFulfillmentResponseType.
     * 
     * @return responseData
     */
    public flexnet.macrovision.com.ReturnFulfillmentResponseDataType[] getResponseData() {
        return responseData;
    }


    /**
     * Sets the responseData value for this ReturnFulfillmentResponseType.
     * 
     * @param responseData
     */
    public void setResponseData(flexnet.macrovision.com.ReturnFulfillmentResponseDataType[] responseData) {
        this.responseData = responseData;
    }


    /**
     * Gets the failedData value for this ReturnFulfillmentResponseType.
     * 
     * @return failedData
     */
    public flexnet.macrovision.com.FailedReturnResponseDataType[] getFailedData() {
        return failedData;
    }


    /**
     * Sets the failedData value for this ReturnFulfillmentResponseType.
     * 
     * @param failedData
     */
    public void setFailedData(flexnet.macrovision.com.FailedReturnResponseDataType[] failedData) {
        this.failedData = failedData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReturnFulfillmentResponseType)) return false;
        ReturnFulfillmentResponseType other = (ReturnFulfillmentResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusInfo==null && other.getStatusInfo()==null) || 
             (this.statusInfo!=null &&
              this.statusInfo.equals(other.getStatusInfo()))) &&
            ((this.responseData==null && other.getResponseData()==null) || 
             (this.responseData!=null &&
              java.util.Arrays.equals(this.responseData, other.getResponseData()))) &&
            ((this.failedData==null && other.getFailedData()==null) || 
             (this.failedData!=null &&
              java.util.Arrays.equals(this.failedData, other.getFailedData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusInfo() != null) {
            _hashCode += getStatusInfo().hashCode();
        }
        if (getResponseData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResponseData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResponseData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFailedData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFailedData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFailedData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReturnFulfillmentResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "statusInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StatusInfoType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responseData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "responseData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnFulfillmentResponseDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentData"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("failedData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedReturnResponseDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "failedFulfillment"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
