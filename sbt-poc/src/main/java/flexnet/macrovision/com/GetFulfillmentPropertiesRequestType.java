/**
 * GetFulfillmentPropertiesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetFulfillmentPropertiesRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentsQueryParametersType queryParams;

    private flexnet.macrovision.com.FulfillmentResponseConfigRequestType fulfillmentResponseConfig;

    private java.math.BigInteger batchSize;

    private java.math.BigInteger pageNumber;

    public GetFulfillmentPropertiesRequestType() {
    }

    public GetFulfillmentPropertiesRequestType(
           flexnet.macrovision.com.FulfillmentsQueryParametersType queryParams,
           flexnet.macrovision.com.FulfillmentResponseConfigRequestType fulfillmentResponseConfig,
           java.math.BigInteger batchSize,
           java.math.BigInteger pageNumber) {
           this.queryParams = queryParams;
           this.fulfillmentResponseConfig = fulfillmentResponseConfig;
           this.batchSize = batchSize;
           this.pageNumber = pageNumber;
    }


    /**
     * Gets the queryParams value for this GetFulfillmentPropertiesRequestType.
     * 
     * @return queryParams
     */
    public flexnet.macrovision.com.FulfillmentsQueryParametersType getQueryParams() {
        return queryParams;
    }


    /**
     * Sets the queryParams value for this GetFulfillmentPropertiesRequestType.
     * 
     * @param queryParams
     */
    public void setQueryParams(flexnet.macrovision.com.FulfillmentsQueryParametersType queryParams) {
        this.queryParams = queryParams;
    }


    /**
     * Gets the fulfillmentResponseConfig value for this GetFulfillmentPropertiesRequestType.
     * 
     * @return fulfillmentResponseConfig
     */
    public flexnet.macrovision.com.FulfillmentResponseConfigRequestType getFulfillmentResponseConfig() {
        return fulfillmentResponseConfig;
    }


    /**
     * Sets the fulfillmentResponseConfig value for this GetFulfillmentPropertiesRequestType.
     * 
     * @param fulfillmentResponseConfig
     */
    public void setFulfillmentResponseConfig(flexnet.macrovision.com.FulfillmentResponseConfigRequestType fulfillmentResponseConfig) {
        this.fulfillmentResponseConfig = fulfillmentResponseConfig;
    }


    /**
     * Gets the batchSize value for this GetFulfillmentPropertiesRequestType.
     * 
     * @return batchSize
     */
    public java.math.BigInteger getBatchSize() {
        return batchSize;
    }


    /**
     * Sets the batchSize value for this GetFulfillmentPropertiesRequestType.
     * 
     * @param batchSize
     */
    public void setBatchSize(java.math.BigInteger batchSize) {
        this.batchSize = batchSize;
    }


    /**
     * Gets the pageNumber value for this GetFulfillmentPropertiesRequestType.
     * 
     * @return pageNumber
     */
    public java.math.BigInteger getPageNumber() {
        return pageNumber;
    }


    /**
     * Sets the pageNumber value for this GetFulfillmentPropertiesRequestType.
     * 
     * @param pageNumber
     */
    public void setPageNumber(java.math.BigInteger pageNumber) {
        this.pageNumber = pageNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFulfillmentPropertiesRequestType)) return false;
        GetFulfillmentPropertiesRequestType other = (GetFulfillmentPropertiesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.queryParams==null && other.getQueryParams()==null) || 
             (this.queryParams!=null &&
              this.queryParams.equals(other.getQueryParams()))) &&
            ((this.fulfillmentResponseConfig==null && other.getFulfillmentResponseConfig()==null) || 
             (this.fulfillmentResponseConfig!=null &&
              this.fulfillmentResponseConfig.equals(other.getFulfillmentResponseConfig()))) &&
            ((this.batchSize==null && other.getBatchSize()==null) || 
             (this.batchSize!=null &&
              this.batchSize.equals(other.getBatchSize()))) &&
            ((this.pageNumber==null && other.getPageNumber()==null) || 
             (this.pageNumber!=null &&
              this.pageNumber.equals(other.getPageNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQueryParams() != null) {
            _hashCode += getQueryParams().hashCode();
        }
        if (getFulfillmentResponseConfig() != null) {
            _hashCode += getFulfillmentResponseConfig().hashCode();
        }
        if (getBatchSize() != null) {
            _hashCode += getBatchSize().hashCode();
        }
        if (getPageNumber() != null) {
            _hashCode += getPageNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFulfillmentPropertiesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryParams");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "queryParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentsQueryParametersType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentResponseConfig");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentResponseConfig"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentResponseConfigRequestType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "batchSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "pageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
