/**
 * GetFulfillmentHistoryRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetFulfillmentHistoryRequestType  implements java.io.Serializable {
    private java.lang.String activationId;

    private flexnet.macrovision.com.SimpleQueryType fulfillmentId;

    private flexnet.macrovision.com.SimpleQueryType userId;

    private flexnet.macrovision.com.NumberQueryType count;

    private java.lang.Boolean policyOverridden;

    private flexnet.macrovision.com.DateTimeQueryType actionDateTime;

    private flexnet.macrovision.com.SupportLicenseType lifeCycleAction;

    private flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource;

    private java.math.BigInteger pageNumber;

    private java.math.BigInteger batchSize;

    public GetFulfillmentHistoryRequestType() {
    }

    public GetFulfillmentHistoryRequestType(
           java.lang.String activationId,
           flexnet.macrovision.com.SimpleQueryType fulfillmentId,
           flexnet.macrovision.com.SimpleQueryType userId,
           flexnet.macrovision.com.NumberQueryType count,
           java.lang.Boolean policyOverridden,
           flexnet.macrovision.com.DateTimeQueryType actionDateTime,
           flexnet.macrovision.com.SupportLicenseType lifeCycleAction,
           flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource,
           java.math.BigInteger pageNumber,
           java.math.BigInteger batchSize) {
           this.activationId = activationId;
           this.fulfillmentId = fulfillmentId;
           this.userId = userId;
           this.count = count;
           this.policyOverridden = policyOverridden;
           this.actionDateTime = actionDateTime;
           this.lifeCycleAction = lifeCycleAction;
           this.fulfillmentSource = fulfillmentSource;
           this.pageNumber = pageNumber;
           this.batchSize = batchSize;
    }


    /**
     * Gets the activationId value for this GetFulfillmentHistoryRequestType.
     * 
     * @return activationId
     */
    public java.lang.String getActivationId() {
        return activationId;
    }


    /**
     * Sets the activationId value for this GetFulfillmentHistoryRequestType.
     * 
     * @param activationId
     */
    public void setActivationId(java.lang.String activationId) {
        this.activationId = activationId;
    }


    /**
     * Gets the fulfillmentId value for this GetFulfillmentHistoryRequestType.
     * 
     * @return fulfillmentId
     */
    public flexnet.macrovision.com.SimpleQueryType getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this GetFulfillmentHistoryRequestType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(flexnet.macrovision.com.SimpleQueryType fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the userId value for this GetFulfillmentHistoryRequestType.
     * 
     * @return userId
     */
    public flexnet.macrovision.com.SimpleQueryType getUserId() {
        return userId;
    }


    /**
     * Sets the userId value for this GetFulfillmentHistoryRequestType.
     * 
     * @param userId
     */
    public void setUserId(flexnet.macrovision.com.SimpleQueryType userId) {
        this.userId = userId;
    }


    /**
     * Gets the count value for this GetFulfillmentHistoryRequestType.
     * 
     * @return count
     */
    public flexnet.macrovision.com.NumberQueryType getCount() {
        return count;
    }


    /**
     * Sets the count value for this GetFulfillmentHistoryRequestType.
     * 
     * @param count
     */
    public void setCount(flexnet.macrovision.com.NumberQueryType count) {
        this.count = count;
    }


    /**
     * Gets the policyOverridden value for this GetFulfillmentHistoryRequestType.
     * 
     * @return policyOverridden
     */
    public java.lang.Boolean getPolicyOverridden() {
        return policyOverridden;
    }


    /**
     * Sets the policyOverridden value for this GetFulfillmentHistoryRequestType.
     * 
     * @param policyOverridden
     */
    public void setPolicyOverridden(java.lang.Boolean policyOverridden) {
        this.policyOverridden = policyOverridden;
    }


    /**
     * Gets the actionDateTime value for this GetFulfillmentHistoryRequestType.
     * 
     * @return actionDateTime
     */
    public flexnet.macrovision.com.DateTimeQueryType getActionDateTime() {
        return actionDateTime;
    }


    /**
     * Sets the actionDateTime value for this GetFulfillmentHistoryRequestType.
     * 
     * @param actionDateTime
     */
    public void setActionDateTime(flexnet.macrovision.com.DateTimeQueryType actionDateTime) {
        this.actionDateTime = actionDateTime;
    }


    /**
     * Gets the lifeCycleAction value for this GetFulfillmentHistoryRequestType.
     * 
     * @return lifeCycleAction
     */
    public flexnet.macrovision.com.SupportLicenseType getLifeCycleAction() {
        return lifeCycleAction;
    }


    /**
     * Sets the lifeCycleAction value for this GetFulfillmentHistoryRequestType.
     * 
     * @param lifeCycleAction
     */
    public void setLifeCycleAction(flexnet.macrovision.com.SupportLicenseType lifeCycleAction) {
        this.lifeCycleAction = lifeCycleAction;
    }


    /**
     * Gets the fulfillmentSource value for this GetFulfillmentHistoryRequestType.
     * 
     * @return fulfillmentSource
     */
    public flexnet.macrovision.com.FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }


    /**
     * Sets the fulfillmentSource value for this GetFulfillmentHistoryRequestType.
     * 
     * @param fulfillmentSource
     */
    public void setFulfillmentSource(flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource) {
        this.fulfillmentSource = fulfillmentSource;
    }


    /**
     * Gets the pageNumber value for this GetFulfillmentHistoryRequestType.
     * 
     * @return pageNumber
     */
    public java.math.BigInteger getPageNumber() {
        return pageNumber;
    }


    /**
     * Sets the pageNumber value for this GetFulfillmentHistoryRequestType.
     * 
     * @param pageNumber
     */
    public void setPageNumber(java.math.BigInteger pageNumber) {
        this.pageNumber = pageNumber;
    }


    /**
     * Gets the batchSize value for this GetFulfillmentHistoryRequestType.
     * 
     * @return batchSize
     */
    public java.math.BigInteger getBatchSize() {
        return batchSize;
    }


    /**
     * Sets the batchSize value for this GetFulfillmentHistoryRequestType.
     * 
     * @param batchSize
     */
    public void setBatchSize(java.math.BigInteger batchSize) {
        this.batchSize = batchSize;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFulfillmentHistoryRequestType)) return false;
        GetFulfillmentHistoryRequestType other = (GetFulfillmentHistoryRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.activationId==null && other.getActivationId()==null) || 
             (this.activationId!=null &&
              this.activationId.equals(other.getActivationId()))) &&
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.userId==null && other.getUserId()==null) || 
             (this.userId!=null &&
              this.userId.equals(other.getUserId()))) &&
            ((this.count==null && other.getCount()==null) || 
             (this.count!=null &&
              this.count.equals(other.getCount()))) &&
            ((this.policyOverridden==null && other.getPolicyOverridden()==null) || 
             (this.policyOverridden!=null &&
              this.policyOverridden.equals(other.getPolicyOverridden()))) &&
            ((this.actionDateTime==null && other.getActionDateTime()==null) || 
             (this.actionDateTime!=null &&
              this.actionDateTime.equals(other.getActionDateTime()))) &&
            ((this.lifeCycleAction==null && other.getLifeCycleAction()==null) || 
             (this.lifeCycleAction!=null &&
              this.lifeCycleAction.equals(other.getLifeCycleAction()))) &&
            ((this.fulfillmentSource==null && other.getFulfillmentSource()==null) || 
             (this.fulfillmentSource!=null &&
              this.fulfillmentSource.equals(other.getFulfillmentSource()))) &&
            ((this.pageNumber==null && other.getPageNumber()==null) || 
             (this.pageNumber!=null &&
              this.pageNumber.equals(other.getPageNumber()))) &&
            ((this.batchSize==null && other.getBatchSize()==null) || 
             (this.batchSize!=null &&
              this.batchSize.equals(other.getBatchSize())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getActivationId() != null) {
            _hashCode += getActivationId().hashCode();
        }
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getUserId() != null) {
            _hashCode += getUserId().hashCode();
        }
        if (getCount() != null) {
            _hashCode += getCount().hashCode();
        }
        if (getPolicyOverridden() != null) {
            _hashCode += getPolicyOverridden().hashCode();
        }
        if (getActionDateTime() != null) {
            _hashCode += getActionDateTime().hashCode();
        }
        if (getLifeCycleAction() != null) {
            _hashCode += getLifeCycleAction().hashCode();
        }
        if (getFulfillmentSource() != null) {
            _hashCode += getFulfillmentSource().hashCode();
        }
        if (getPageNumber() != null) {
            _hashCode += getPageNumber().hashCode();
        }
        if (getBatchSize() != null) {
            _hashCode += getBatchSize().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFulfillmentHistoryRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentHistoryRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "userId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("count");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "count"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "NumberQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("policyOverridden");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "policyOverridden"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "actionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "DateTimeQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lifeCycleAction");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lifeCycleAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SupportLicenseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentSource");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FulfillmentSourceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "pageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "batchSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
