/**
 * LicenseService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public interface LicenseService extends javax.xml.rpc.Service {
    public java.lang.String getLicenseServiceAddress();

    public flexnet.macrovision.com.LicenseFulfillmentServiceInterface getLicenseService() throws javax.xml.rpc.ServiceException;

    public flexnet.macrovision.com.LicenseFulfillmentServiceInterface getLicenseService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
