/**
 * LicenseFulfillmentServiceInterface.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public interface LicenseFulfillmentServiceInterface extends java.rmi.Remote {
    public flexnet.macrovision.com.GetFulfillmentCountResponseType getFulfillmentCount(flexnet.macrovision.com.GetFulfillmentCountRequestType getFulfillmentCountRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetFulfillmentsQueryResponseType getFulfillmentsQuery(flexnet.macrovision.com.GetFulfillmentsQueryRequestType getFulfillmentsQueryRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetFulfillmentPropertiesResponseType getFulfillmentPropertiesQuery(flexnet.macrovision.com.GetFulfillmentPropertiesRequestType getFulfillmentPropertiesRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.RehostFulfillmentResponseType rehostLicense(flexnet.macrovision.com.RehostFulfillmentDataType[] rehostLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.ReturnFulfillmentResponseType returnLicense(flexnet.macrovision.com.ReturnFulfillmentDataType[] returnLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.RepairFulfillmentResponseType repairLicense(flexnet.macrovision.com.RepairFulfillmentDataType[] repairLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.EmergencyFulfillmentResponseType emergencyLicense(flexnet.macrovision.com.EmergencyFulfillmentDataType[] emergencyLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.PublisherErrorFulfillmentResponseType publisherErrorLicense(flexnet.macrovision.com.PublisherErrorFulfillmentDataType[] publisherErrorLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.StopGapFulfillmentResponseType stopGapLicense(flexnet.macrovision.com.StopGapFulfillmentDataType[] stopGapLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetFulfillmentAttributesResponseType getFulfillmentAttributesFromModel(flexnet.macrovision.com.GetFulfillmentAttributesRequestType getFulfillmentAttributesRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetHostAttributesResponseType getHostAttributesFromLicenseTechnology(flexnet.macrovision.com.GetHostAttributesRequestType getHostAttributesRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.CreateFulfillmentResponseType verifyCreateLicense(flexnet.macrovision.com.CreateFulfillmentDataType[] verifyCreateLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.CreateFulfillmentResponseType createLicense(flexnet.macrovision.com.CreateFulfillmentDataType[] createLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.ActivateShortCodeResponseType activateShortCode(flexnet.macrovision.com.ActivateShortCodeRequestType activateShortCodeRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.RepairShortCodeResponseType repairShortCode(flexnet.macrovision.com.RepairShortCodeRequestType repairShortCodeRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.ReturnShortCodeResponseType returnShortCode(flexnet.macrovision.com.ReturnShortCodeRequestType returnShortCodeRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.EmailLicenseResponseType emailLicense(flexnet.macrovision.com.EmailLicenseRequestType emailLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.ConsolidateFulfillmentsResponseType consolidateFulfillments(flexnet.macrovision.com.ConsolidateFulfillmentsRequestType consolidateFulfillmentsRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetConsolidatedFulfillmentCountResponseType getConsolidatedFulfillmentCount(flexnet.macrovision.com.GetConsolidatedFulfillmentCountRequestType getConsolidatedFulfillmentCountRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryResponseType getConsolidatedFulfillmentsQuery(flexnet.macrovision.com.GetConsolidatedFulfillmentsQueryRequestType getConsolidatedFulfillmentsQueryRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetFmtAttributesForBatchActivationResponseType getFulfillmentAttributesForBatchActivation(flexnet.macrovision.com.GetFmtAttributesForBatchActivationRequestType getFmtAttributesForBatchActivationRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.CreateLicensesAsBatchResponseType createLicensesAsBatch(flexnet.macrovision.com.CreateLicensesAsBatchRequestType createLicensesAsBatchRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.ConsolidateFulfillmentsResponseType createLicensesAsBatchAndConsolidate(flexnet.macrovision.com.CreateLicensesAsBatchRequestType createLicensesAsBatchAndConsolidateRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.EmailConsolidatedLicensesResponseType emailConsolidatedLicenses(flexnet.macrovision.com.EmailConsolidatedLicensesRequestType emailConsolidatedLicensesRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.TrustedResponseType manualActivation(flexnet.macrovision.com.TrustedRequestType manualActivationRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.TrustedResponseType manualRepair(flexnet.macrovision.com.TrustedRequestType manualRepairRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.TrustedResponseType manualReturn(flexnet.macrovision.com.TrustedRequestType manualReturnRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.GetFulfillmentHistoryResponseType getFulfillmentHistory(flexnet.macrovision.com.GetFulfillmentHistoryRequestType getFulfillmentHistoryRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.CreateChildLineItemFulfillmentResponseType createChildLineItemFulfillment(flexnet.macrovision.com.CreateChildLineItemFulfillmentDataType[] createChildLineItemFulfillmentRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType upgradeFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType upgradeFulfillmentRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType upsellFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType upsellFulfillmentRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.AdvancedFulfillmentLCResponseType renewFulfillment(flexnet.macrovision.com.AdvancedFulfillmentLCRequestType renewFulfillmentRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.SetLicenseResponseType setLicense(flexnet.macrovision.com.SetLicenseRequestType setLicenseRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.DeleteOnholdFulfillmentsResponseType deleteOnholdFulfillments(flexnet.macrovision.com.DeleteOnholdFulfillmentsRequestType deleteOnholdFulfillmentsRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.ActivateLicensesResponseType offlineFNPTrustedStorageActivation(flexnet.macrovision.com.ActivateLicensesRequestType activateLicensesRequest) throws java.rmi.RemoteException;
    public flexnet.macrovision.com.TransferHostResponseType transferHost(flexnet.macrovision.com.TransferHostRequestType transferHostRequest) throws java.rmi.RemoteException;
}
