/**
 * ReturnShortCodeRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ReturnShortCodeRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.ReturnShortCodeDataType shortCodeData;

    private flexnet.macrovision.com.ReturnedShortCodeReturnReason returnReason;

    public ReturnShortCodeRequestType() {
    }

    public ReturnShortCodeRequestType(
           flexnet.macrovision.com.ReturnShortCodeDataType shortCodeData,
           flexnet.macrovision.com.ReturnedShortCodeReturnReason returnReason) {
           this.shortCodeData = shortCodeData;
           this.returnReason = returnReason;
    }


    /**
     * Gets the shortCodeData value for this ReturnShortCodeRequestType.
     * 
     * @return shortCodeData
     */
    public flexnet.macrovision.com.ReturnShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }


    /**
     * Sets the shortCodeData value for this ReturnShortCodeRequestType.
     * 
     * @param shortCodeData
     */
    public void setShortCodeData(flexnet.macrovision.com.ReturnShortCodeDataType shortCodeData) {
        this.shortCodeData = shortCodeData;
    }


    /**
     * Gets the returnReason value for this ReturnShortCodeRequestType.
     * 
     * @return returnReason
     */
    public flexnet.macrovision.com.ReturnedShortCodeReturnReason getReturnReason() {
        return returnReason;
    }


    /**
     * Sets the returnReason value for this ReturnShortCodeRequestType.
     * 
     * @param returnReason
     */
    public void setReturnReason(flexnet.macrovision.com.ReturnedShortCodeReturnReason returnReason) {
        this.returnReason = returnReason;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ReturnShortCodeRequestType)) return false;
        ReturnShortCodeRequestType other = (ReturnShortCodeRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.shortCodeData==null && other.getShortCodeData()==null) || 
             (this.shortCodeData!=null &&
              this.shortCodeData.equals(other.getShortCodeData()))) &&
            ((this.returnReason==null && other.getReturnReason()==null) || 
             (this.returnReason!=null &&
              this.returnReason.equals(other.getReturnReason())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getShortCodeData() != null) {
            _hashCode += getShortCodeData().hashCode();
        }
        if (getReturnReason() != null) {
            _hashCode += getReturnReason().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ReturnShortCodeRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortCodeData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shortCodeData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnShortCodeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("returnReason");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "returnReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ReturnedShortCodeReturnReason"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
