/**
 * OnHoldFmtLicenseDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class OnHoldFmtLicenseDataType  implements java.io.Serializable {
    private java.lang.String fulfillmentId;

    private java.lang.String textLicense;

    private byte[] binaryLicense;

    private flexnet.macrovision.com.LicenseFileDataType[] licenseFiles;

    public OnHoldFmtLicenseDataType() {
    }

    public OnHoldFmtLicenseDataType(
           java.lang.String fulfillmentId,
           java.lang.String textLicense,
           byte[] binaryLicense,
           flexnet.macrovision.com.LicenseFileDataType[] licenseFiles) {
           this.fulfillmentId = fulfillmentId;
           this.textLicense = textLicense;
           this.binaryLicense = binaryLicense;
           this.licenseFiles = licenseFiles;
    }


    /**
     * Gets the fulfillmentId value for this OnHoldFmtLicenseDataType.
     * 
     * @return fulfillmentId
     */
    public java.lang.String getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this OnHoldFmtLicenseDataType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(java.lang.String fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the textLicense value for this OnHoldFmtLicenseDataType.
     * 
     * @return textLicense
     */
    public java.lang.String getTextLicense() {
        return textLicense;
    }


    /**
     * Sets the textLicense value for this OnHoldFmtLicenseDataType.
     * 
     * @param textLicense
     */
    public void setTextLicense(java.lang.String textLicense) {
        this.textLicense = textLicense;
    }


    /**
     * Gets the binaryLicense value for this OnHoldFmtLicenseDataType.
     * 
     * @return binaryLicense
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }


    /**
     * Sets the binaryLicense value for this OnHoldFmtLicenseDataType.
     * 
     * @param binaryLicense
     */
    public void setBinaryLicense(byte[] binaryLicense) {
        this.binaryLicense = binaryLicense;
    }


    /**
     * Gets the licenseFiles value for this OnHoldFmtLicenseDataType.
     * 
     * @return licenseFiles
     */
    public flexnet.macrovision.com.LicenseFileDataType[] getLicenseFiles() {
        return licenseFiles;
    }


    /**
     * Sets the licenseFiles value for this OnHoldFmtLicenseDataType.
     * 
     * @param licenseFiles
     */
    public void setLicenseFiles(flexnet.macrovision.com.LicenseFileDataType[] licenseFiles) {
        this.licenseFiles = licenseFiles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OnHoldFmtLicenseDataType)) return false;
        OnHoldFmtLicenseDataType other = (OnHoldFmtLicenseDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.textLicense==null && other.getTextLicense()==null) || 
             (this.textLicense!=null &&
              this.textLicense.equals(other.getTextLicense()))) &&
            ((this.binaryLicense==null && other.getBinaryLicense()==null) || 
             (this.binaryLicense!=null &&
              java.util.Arrays.equals(this.binaryLicense, other.getBinaryLicense()))) &&
            ((this.licenseFiles==null && other.getLicenseFiles()==null) || 
             (this.licenseFiles!=null &&
              java.util.Arrays.equals(this.licenseFiles, other.getLicenseFiles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getTextLicense() != null) {
            _hashCode += getTextLicense().hashCode();
        }
        if (getBinaryLicense() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBinaryLicense());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBinaryLicense(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLicenseFiles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseFiles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseFiles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OnHoldFmtLicenseDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onHoldFmtLicenseDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("textLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "textLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binaryLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "binaryLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFiles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFiles"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFile"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
