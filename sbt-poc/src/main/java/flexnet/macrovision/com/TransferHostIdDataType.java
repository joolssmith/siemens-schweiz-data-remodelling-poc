/**
 * TransferHostIdDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class TransferHostIdDataType  implements java.io.Serializable {
    private java.lang.String soldTo;

    private flexnet.macrovision.com.ServerIDsType serverIds;

    private java.lang.String nodeId;

    private java.lang.String customHostId;

    private java.lang.String customHostType;

    private java.lang.String customLicenseTechnology;

    public TransferHostIdDataType() {
    }

    public TransferHostIdDataType(
           java.lang.String soldTo,
           flexnet.macrovision.com.ServerIDsType serverIds,
           java.lang.String nodeId,
           java.lang.String customHostId,
           java.lang.String customHostType,
           java.lang.String customLicenseTechnology) {
           this.soldTo = soldTo;
           this.serverIds = serverIds;
           this.nodeId = nodeId;
           this.customHostId = customHostId;
           this.customHostType = customHostType;
           this.customLicenseTechnology = customLicenseTechnology;
    }


    /**
     * Gets the soldTo value for this TransferHostIdDataType.
     * 
     * @return soldTo
     */
    public java.lang.String getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this TransferHostIdDataType.
     * 
     * @param soldTo
     */
    public void setSoldTo(java.lang.String soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the serverIds value for this TransferHostIdDataType.
     * 
     * @return serverIds
     */
    public flexnet.macrovision.com.ServerIDsType getServerIds() {
        return serverIds;
    }


    /**
     * Sets the serverIds value for this TransferHostIdDataType.
     * 
     * @param serverIds
     */
    public void setServerIds(flexnet.macrovision.com.ServerIDsType serverIds) {
        this.serverIds = serverIds;
    }


    /**
     * Gets the nodeId value for this TransferHostIdDataType.
     * 
     * @return nodeId
     */
    public java.lang.String getNodeId() {
        return nodeId;
    }


    /**
     * Sets the nodeId value for this TransferHostIdDataType.
     * 
     * @param nodeId
     */
    public void setNodeId(java.lang.String nodeId) {
        this.nodeId = nodeId;
    }


    /**
     * Gets the customHostId value for this TransferHostIdDataType.
     * 
     * @return customHostId
     */
    public java.lang.String getCustomHostId() {
        return customHostId;
    }


    /**
     * Sets the customHostId value for this TransferHostIdDataType.
     * 
     * @param customHostId
     */
    public void setCustomHostId(java.lang.String customHostId) {
        this.customHostId = customHostId;
    }


    /**
     * Gets the customHostType value for this TransferHostIdDataType.
     * 
     * @return customHostType
     */
    public java.lang.String getCustomHostType() {
        return customHostType;
    }


    /**
     * Sets the customHostType value for this TransferHostIdDataType.
     * 
     * @param customHostType
     */
    public void setCustomHostType(java.lang.String customHostType) {
        this.customHostType = customHostType;
    }


    /**
     * Gets the customLicenseTechnology value for this TransferHostIdDataType.
     * 
     * @return customLicenseTechnology
     */
    public java.lang.String getCustomLicenseTechnology() {
        return customLicenseTechnology;
    }


    /**
     * Sets the customLicenseTechnology value for this TransferHostIdDataType.
     * 
     * @param customLicenseTechnology
     */
    public void setCustomLicenseTechnology(java.lang.String customLicenseTechnology) {
        this.customLicenseTechnology = customLicenseTechnology;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferHostIdDataType)) return false;
        TransferHostIdDataType other = (TransferHostIdDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.serverIds==null && other.getServerIds()==null) || 
             (this.serverIds!=null &&
              this.serverIds.equals(other.getServerIds()))) &&
            ((this.nodeId==null && other.getNodeId()==null) || 
             (this.nodeId!=null &&
              this.nodeId.equals(other.getNodeId()))) &&
            ((this.customHostId==null && other.getCustomHostId()==null) || 
             (this.customHostId!=null &&
              this.customHostId.equals(other.getCustomHostId()))) &&
            ((this.customHostType==null && other.getCustomHostType()==null) || 
             (this.customHostType!=null &&
              this.customHostType.equals(other.getCustomHostType()))) &&
            ((this.customLicenseTechnology==null && other.getCustomLicenseTechnology()==null) || 
             (this.customLicenseTechnology!=null &&
              this.customLicenseTechnology.equals(other.getCustomLicenseTechnology())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getServerIds() != null) {
            _hashCode += getServerIds().hashCode();
        }
        if (getNodeId() != null) {
            _hashCode += getNodeId().hashCode();
        }
        if (getCustomHostId() != null) {
            _hashCode += getCustomHostId().hashCode();
        }
        if (getCustomHostType() != null) {
            _hashCode += getCustomHostType().hashCode();
        }
        if (getCustomLicenseTechnology() != null) {
            _hashCode += getCustomLicenseTechnology().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferHostIdDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostIdDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serverIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "serverIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHostId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHostId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHostType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHostType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customLicenseTechnology");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customLicenseTechnology"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
