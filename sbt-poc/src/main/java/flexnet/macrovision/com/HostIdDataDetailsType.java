/**
 * HostIdDataDetailsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class HostIdDataDetailsType  implements java.io.Serializable {
    private flexnet.macrovision.com.ServerIDsType serverHost;

    private java.lang.String nodeLockHost;

    private java.lang.String[] countedNodeLockHostIds;

    private flexnet.macrovision.com.CustomHostIDType customHost;

    private java.math.BigInteger countFromParent;

    private java.math.BigInteger countFromOwn;

    private java.lang.String parentActivationId;

    public HostIdDataDetailsType() {
    }

    public HostIdDataDetailsType(
           flexnet.macrovision.com.ServerIDsType serverHost,
           java.lang.String nodeLockHost,
           java.lang.String[] countedNodeLockHostIds,
           flexnet.macrovision.com.CustomHostIDType customHost,
           java.math.BigInteger countFromParent,
           java.math.BigInteger countFromOwn,
           java.lang.String parentActivationId) {
           this.serverHost = serverHost;
           this.nodeLockHost = nodeLockHost;
           this.countedNodeLockHostIds = countedNodeLockHostIds;
           this.customHost = customHost;
           this.countFromParent = countFromParent;
           this.countFromOwn = countFromOwn;
           this.parentActivationId = parentActivationId;
    }


    /**
     * Gets the serverHost value for this HostIdDataDetailsType.
     * 
     * @return serverHost
     */
    public flexnet.macrovision.com.ServerIDsType getServerHost() {
        return serverHost;
    }


    /**
     * Sets the serverHost value for this HostIdDataDetailsType.
     * 
     * @param serverHost
     */
    public void setServerHost(flexnet.macrovision.com.ServerIDsType serverHost) {
        this.serverHost = serverHost;
    }


    /**
     * Gets the nodeLockHost value for this HostIdDataDetailsType.
     * 
     * @return nodeLockHost
     */
    public java.lang.String getNodeLockHost() {
        return nodeLockHost;
    }


    /**
     * Sets the nodeLockHost value for this HostIdDataDetailsType.
     * 
     * @param nodeLockHost
     */
    public void setNodeLockHost(java.lang.String nodeLockHost) {
        this.nodeLockHost = nodeLockHost;
    }


    /**
     * Gets the countedNodeLockHostIds value for this HostIdDataDetailsType.
     * 
     * @return countedNodeLockHostIds
     */
    public java.lang.String[] getCountedNodeLockHostIds() {
        return countedNodeLockHostIds;
    }


    /**
     * Sets the countedNodeLockHostIds value for this HostIdDataDetailsType.
     * 
     * @param countedNodeLockHostIds
     */
    public void setCountedNodeLockHostIds(java.lang.String[] countedNodeLockHostIds) {
        this.countedNodeLockHostIds = countedNodeLockHostIds;
    }


    /**
     * Gets the customHost value for this HostIdDataDetailsType.
     * 
     * @return customHost
     */
    public flexnet.macrovision.com.CustomHostIDType getCustomHost() {
        return customHost;
    }


    /**
     * Sets the customHost value for this HostIdDataDetailsType.
     * 
     * @param customHost
     */
    public void setCustomHost(flexnet.macrovision.com.CustomHostIDType customHost) {
        this.customHost = customHost;
    }


    /**
     * Gets the countFromParent value for this HostIdDataDetailsType.
     * 
     * @return countFromParent
     */
    public java.math.BigInteger getCountFromParent() {
        return countFromParent;
    }


    /**
     * Sets the countFromParent value for this HostIdDataDetailsType.
     * 
     * @param countFromParent
     */
    public void setCountFromParent(java.math.BigInteger countFromParent) {
        this.countFromParent = countFromParent;
    }


    /**
     * Gets the countFromOwn value for this HostIdDataDetailsType.
     * 
     * @return countFromOwn
     */
    public java.math.BigInteger getCountFromOwn() {
        return countFromOwn;
    }


    /**
     * Sets the countFromOwn value for this HostIdDataDetailsType.
     * 
     * @param countFromOwn
     */
    public void setCountFromOwn(java.math.BigInteger countFromOwn) {
        this.countFromOwn = countFromOwn;
    }


    /**
     * Gets the parentActivationId value for this HostIdDataDetailsType.
     * 
     * @return parentActivationId
     */
    public java.lang.String getParentActivationId() {
        return parentActivationId;
    }


    /**
     * Sets the parentActivationId value for this HostIdDataDetailsType.
     * 
     * @param parentActivationId
     */
    public void setParentActivationId(java.lang.String parentActivationId) {
        this.parentActivationId = parentActivationId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof HostIdDataDetailsType)) return false;
        HostIdDataDetailsType other = (HostIdDataDetailsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.serverHost==null && other.getServerHost()==null) || 
             (this.serverHost!=null &&
              this.serverHost.equals(other.getServerHost()))) &&
            ((this.nodeLockHost==null && other.getNodeLockHost()==null) || 
             (this.nodeLockHost!=null &&
              this.nodeLockHost.equals(other.getNodeLockHost()))) &&
            ((this.countedNodeLockHostIds==null && other.getCountedNodeLockHostIds()==null) || 
             (this.countedNodeLockHostIds!=null &&
              java.util.Arrays.equals(this.countedNodeLockHostIds, other.getCountedNodeLockHostIds()))) &&
            ((this.customHost==null && other.getCustomHost()==null) || 
             (this.customHost!=null &&
              this.customHost.equals(other.getCustomHost()))) &&
            ((this.countFromParent==null && other.getCountFromParent()==null) || 
             (this.countFromParent!=null &&
              this.countFromParent.equals(other.getCountFromParent()))) &&
            ((this.countFromOwn==null && other.getCountFromOwn()==null) || 
             (this.countFromOwn!=null &&
              this.countFromOwn.equals(other.getCountFromOwn()))) &&
            ((this.parentActivationId==null && other.getParentActivationId()==null) || 
             (this.parentActivationId!=null &&
              this.parentActivationId.equals(other.getParentActivationId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServerHost() != null) {
            _hashCode += getServerHost().hashCode();
        }
        if (getNodeLockHost() != null) {
            _hashCode += getNodeLockHost().hashCode();
        }
        if (getCountedNodeLockHostIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCountedNodeLockHostIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCountedNodeLockHostIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHost() != null) {
            _hashCode += getCustomHost().hashCode();
        }
        if (getCountFromParent() != null) {
            _hashCode += getCountFromParent().hashCode();
        }
        if (getCountFromOwn() != null) {
            _hashCode += getCountFromOwn().hashCode();
        }
        if (getParentActivationId() != null) {
            _hashCode += getParentActivationId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(HostIdDataDetailsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdDataDetailsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serverHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "serverHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeLockHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeLockHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countedNodeLockHostIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countedNodeLockHostIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countFromParent");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countFromParent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countFromOwn");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countFromOwn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentActivationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "parentActivationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
