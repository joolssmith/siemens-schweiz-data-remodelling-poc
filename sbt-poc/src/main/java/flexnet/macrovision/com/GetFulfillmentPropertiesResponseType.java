/**
 * GetFulfillmentPropertiesResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetFulfillmentPropertiesResponseType  implements java.io.Serializable {
    private flexnet.macrovision.com.StatusInfoType statusInfo;

    private flexnet.macrovision.com.FulfillmentPropertiesType[] fulfillment;

    public GetFulfillmentPropertiesResponseType() {
    }

    public GetFulfillmentPropertiesResponseType(
           flexnet.macrovision.com.StatusInfoType statusInfo,
           flexnet.macrovision.com.FulfillmentPropertiesType[] fulfillment) {
           this.statusInfo = statusInfo;
           this.fulfillment = fulfillment;
    }


    /**
     * Gets the statusInfo value for this GetFulfillmentPropertiesResponseType.
     * 
     * @return statusInfo
     */
    public flexnet.macrovision.com.StatusInfoType getStatusInfo() {
        return statusInfo;
    }


    /**
     * Sets the statusInfo value for this GetFulfillmentPropertiesResponseType.
     * 
     * @param statusInfo
     */
    public void setStatusInfo(flexnet.macrovision.com.StatusInfoType statusInfo) {
        this.statusInfo = statusInfo;
    }


    /**
     * Gets the fulfillment value for this GetFulfillmentPropertiesResponseType.
     * 
     * @return fulfillment
     */
    public flexnet.macrovision.com.FulfillmentPropertiesType[] getFulfillment() {
        return fulfillment;
    }


    /**
     * Sets the fulfillment value for this GetFulfillmentPropertiesResponseType.
     * 
     * @param fulfillment
     */
    public void setFulfillment(flexnet.macrovision.com.FulfillmentPropertiesType[] fulfillment) {
        this.fulfillment = fulfillment;
    }

    public flexnet.macrovision.com.FulfillmentPropertiesType getFulfillment(int i) {
        return this.fulfillment[i];
    }

    public void setFulfillment(int i, flexnet.macrovision.com.FulfillmentPropertiesType _value) {
        this.fulfillment[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFulfillmentPropertiesResponseType)) return false;
        GetFulfillmentPropertiesResponseType other = (GetFulfillmentPropertiesResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusInfo==null && other.getStatusInfo()==null) || 
             (this.statusInfo!=null &&
              this.statusInfo.equals(other.getStatusInfo()))) &&
            ((this.fulfillment==null && other.getFulfillment()==null) || 
             (this.fulfillment!=null &&
              java.util.Arrays.equals(this.fulfillment, other.getFulfillment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusInfo() != null) {
            _hashCode += getStatusInfo().hashCode();
        }
        if (getFulfillment() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFulfillment());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFulfillment(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFulfillmentPropertiesResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentPropertiesResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "statusInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StatusInfoType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentPropertiesType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
