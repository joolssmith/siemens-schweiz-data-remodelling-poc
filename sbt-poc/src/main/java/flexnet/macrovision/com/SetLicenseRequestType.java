/**
 * SetLicenseRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class SetLicenseRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.OnHoldFmtLicenseDataType[] onholdFulfillmentList;

    public SetLicenseRequestType() {
    }

    public SetLicenseRequestType(
           flexnet.macrovision.com.OnHoldFmtLicenseDataType[] onholdFulfillmentList) {
           this.onholdFulfillmentList = onholdFulfillmentList;
    }


    /**
     * Gets the onholdFulfillmentList value for this SetLicenseRequestType.
     * 
     * @return onholdFulfillmentList
     */
    public flexnet.macrovision.com.OnHoldFmtLicenseDataType[] getOnholdFulfillmentList() {
        return onholdFulfillmentList;
    }


    /**
     * Sets the onholdFulfillmentList value for this SetLicenseRequestType.
     * 
     * @param onholdFulfillmentList
     */
    public void setOnholdFulfillmentList(flexnet.macrovision.com.OnHoldFmtLicenseDataType[] onholdFulfillmentList) {
        this.onholdFulfillmentList = onholdFulfillmentList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SetLicenseRequestType)) return false;
        SetLicenseRequestType other = (SetLicenseRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.onholdFulfillmentList==null && other.getOnholdFulfillmentList()==null) || 
             (this.onholdFulfillmentList!=null &&
              java.util.Arrays.equals(this.onholdFulfillmentList, other.getOnholdFulfillmentList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOnholdFulfillmentList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOnholdFulfillmentList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOnholdFulfillmentList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SetLicenseRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "setLicenseRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onholdFulfillmentList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onholdFulfillmentList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onHoldFmtLicenseDataType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "onholdFmtLicenseData"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
