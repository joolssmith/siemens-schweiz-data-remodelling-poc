/**
 * DeleteOnholdFulfillmentsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class DeleteOnholdFulfillmentsRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType[] fulfillmentIdList;

    public DeleteOnholdFulfillmentsRequestType() {
    }

    public DeleteOnholdFulfillmentsRequestType(
           flexnet.macrovision.com.FulfillmentIdentifierType[] fulfillmentIdList) {
           this.fulfillmentIdList = fulfillmentIdList;
    }


    /**
     * Gets the fulfillmentIdList value for this DeleteOnholdFulfillmentsRequestType.
     * 
     * @return fulfillmentIdList
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType[] getFulfillmentIdList() {
        return fulfillmentIdList;
    }


    /**
     * Sets the fulfillmentIdList value for this DeleteOnholdFulfillmentsRequestType.
     * 
     * @param fulfillmentIdList
     */
    public void setFulfillmentIdList(flexnet.macrovision.com.FulfillmentIdentifierType[] fulfillmentIdList) {
        this.fulfillmentIdList = fulfillmentIdList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof DeleteOnholdFulfillmentsRequestType)) return false;
        DeleteOnholdFulfillmentsRequestType other = (DeleteOnholdFulfillmentsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentIdList==null && other.getFulfillmentIdList()==null) || 
             (this.fulfillmentIdList!=null &&
              java.util.Arrays.equals(this.fulfillmentIdList, other.getFulfillmentIdList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentIdList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFulfillmentIdList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFulfillmentIdList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(DeleteOnholdFulfillmentsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "deleteOnholdFulfillmentsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentIdList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdList"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
