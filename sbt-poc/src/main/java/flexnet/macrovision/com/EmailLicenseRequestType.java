/**
 * EmailLicenseRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class EmailLicenseRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier;

    private java.lang.Boolean ignoreExistingEmailList;

    private java.lang.Boolean validateEmailAddresses;

    private java.lang.String[] emailIdList;

    private java.lang.String locale;

    public EmailLicenseRequestType() {
    }

    public EmailLicenseRequestType(
           flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier,
           java.lang.Boolean ignoreExistingEmailList,
           java.lang.Boolean validateEmailAddresses,
           java.lang.String[] emailIdList,
           java.lang.String locale) {
           this.fulfillmentIdentifier = fulfillmentIdentifier;
           this.ignoreExistingEmailList = ignoreExistingEmailList;
           this.validateEmailAddresses = validateEmailAddresses;
           this.emailIdList = emailIdList;
           this.locale = locale;
    }


    /**
     * Gets the fulfillmentIdentifier value for this EmailLicenseRequestType.
     * 
     * @return fulfillmentIdentifier
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }


    /**
     * Sets the fulfillmentIdentifier value for this EmailLicenseRequestType.
     * 
     * @param fulfillmentIdentifier
     */
    public void setFulfillmentIdentifier(flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier) {
        this.fulfillmentIdentifier = fulfillmentIdentifier;
    }


    /**
     * Gets the ignoreExistingEmailList value for this EmailLicenseRequestType.
     * 
     * @return ignoreExistingEmailList
     */
    public java.lang.Boolean getIgnoreExistingEmailList() {
        return ignoreExistingEmailList;
    }


    /**
     * Sets the ignoreExistingEmailList value for this EmailLicenseRequestType.
     * 
     * @param ignoreExistingEmailList
     */
    public void setIgnoreExistingEmailList(java.lang.Boolean ignoreExistingEmailList) {
        this.ignoreExistingEmailList = ignoreExistingEmailList;
    }


    /**
     * Gets the validateEmailAddresses value for this EmailLicenseRequestType.
     * 
     * @return validateEmailAddresses
     */
    public java.lang.Boolean getValidateEmailAddresses() {
        return validateEmailAddresses;
    }


    /**
     * Sets the validateEmailAddresses value for this EmailLicenseRequestType.
     * 
     * @param validateEmailAddresses
     */
    public void setValidateEmailAddresses(java.lang.Boolean validateEmailAddresses) {
        this.validateEmailAddresses = validateEmailAddresses;
    }


    /**
     * Gets the emailIdList value for this EmailLicenseRequestType.
     * 
     * @return emailIdList
     */
    public java.lang.String[] getEmailIdList() {
        return emailIdList;
    }


    /**
     * Sets the emailIdList value for this EmailLicenseRequestType.
     * 
     * @param emailIdList
     */
    public void setEmailIdList(java.lang.String[] emailIdList) {
        this.emailIdList = emailIdList;
    }


    /**
     * Gets the locale value for this EmailLicenseRequestType.
     * 
     * @return locale
     */
    public java.lang.String getLocale() {
        return locale;
    }


    /**
     * Sets the locale value for this EmailLicenseRequestType.
     * 
     * @param locale
     */
    public void setLocale(java.lang.String locale) {
        this.locale = locale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmailLicenseRequestType)) return false;
        EmailLicenseRequestType other = (EmailLicenseRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentIdentifier==null && other.getFulfillmentIdentifier()==null) || 
             (this.fulfillmentIdentifier!=null &&
              this.fulfillmentIdentifier.equals(other.getFulfillmentIdentifier()))) &&
            ((this.ignoreExistingEmailList==null && other.getIgnoreExistingEmailList()==null) || 
             (this.ignoreExistingEmailList!=null &&
              this.ignoreExistingEmailList.equals(other.getIgnoreExistingEmailList()))) &&
            ((this.validateEmailAddresses==null && other.getValidateEmailAddresses()==null) || 
             (this.validateEmailAddresses!=null &&
              this.validateEmailAddresses.equals(other.getValidateEmailAddresses()))) &&
            ((this.emailIdList==null && other.getEmailIdList()==null) || 
             (this.emailIdList!=null &&
              java.util.Arrays.equals(this.emailIdList, other.getEmailIdList()))) &&
            ((this.locale==null && other.getLocale()==null) || 
             (this.locale!=null &&
              this.locale.equals(other.getLocale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentIdentifier() != null) {
            _hashCode += getFulfillmentIdentifier().hashCode();
        }
        if (getIgnoreExistingEmailList() != null) {
            _hashCode += getIgnoreExistingEmailList().hashCode();
        }
        if (getValidateEmailAddresses() != null) {
            _hashCode += getValidateEmailAddresses().hashCode();
        }
        if (getEmailIdList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEmailIdList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEmailIdList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLocale() != null) {
            _hashCode += getLocale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmailLicenseRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailLicenseRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ignoreExistingEmailList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ignoreExistingEmailList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validateEmailAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "validateEmailAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailIdList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailIdList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locale");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "locale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
