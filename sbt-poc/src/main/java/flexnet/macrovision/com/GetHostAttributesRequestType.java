/**
 * GetHostAttributesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetHostAttributesRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnologyIdentifier;

    private flexnet.macrovision.com.HostTypePKType hostType;

    public GetHostAttributesRequestType() {
    }

    public GetHostAttributesRequestType(
           flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnologyIdentifier,
           flexnet.macrovision.com.HostTypePKType hostType) {
           this.licenseTechnologyIdentifier = licenseTechnologyIdentifier;
           this.hostType = hostType;
    }


    /**
     * Gets the licenseTechnologyIdentifier value for this GetHostAttributesRequestType.
     * 
     * @return licenseTechnologyIdentifier
     */
    public flexnet.macrovision.com.LicenseTechnologyIdentifierType getLicenseTechnologyIdentifier() {
        return licenseTechnologyIdentifier;
    }


    /**
     * Sets the licenseTechnologyIdentifier value for this GetHostAttributesRequestType.
     * 
     * @param licenseTechnologyIdentifier
     */
    public void setLicenseTechnologyIdentifier(flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnologyIdentifier) {
        this.licenseTechnologyIdentifier = licenseTechnologyIdentifier;
    }


    /**
     * Gets the hostType value for this GetHostAttributesRequestType.
     * 
     * @return hostType
     */
    public flexnet.macrovision.com.HostTypePKType getHostType() {
        return hostType;
    }


    /**
     * Sets the hostType value for this GetHostAttributesRequestType.
     * 
     * @param hostType
     */
    public void setHostType(flexnet.macrovision.com.HostTypePKType hostType) {
        this.hostType = hostType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetHostAttributesRequestType)) return false;
        GetHostAttributesRequestType other = (GetHostAttributesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.licenseTechnologyIdentifier==null && other.getLicenseTechnologyIdentifier()==null) || 
             (this.licenseTechnologyIdentifier!=null &&
              this.licenseTechnologyIdentifier.equals(other.getLicenseTechnologyIdentifier()))) &&
            ((this.hostType==null && other.getHostType()==null) || 
             (this.hostType!=null &&
              this.hostType.equals(other.getHostType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLicenseTechnologyIdentifier() != null) {
            _hashCode += getLicenseTechnologyIdentifier().hashCode();
        }
        if (getHostType() != null) {
            _hashCode += getHostType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetHostAttributesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getHostAttributesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseTechnologyIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnologyIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnologyIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostTypePKType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
