/**
 * AdvancedFmtLCResponseDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class AdvancedFmtLCResponseDataType  implements java.io.Serializable {
    private java.math.BigInteger recordRefNo;

    private flexnet.macrovision.com.AdvancedFulfillmentLCInfoType[] upgradedFulfillmentInfo;

    public AdvancedFmtLCResponseDataType() {
    }

    public AdvancedFmtLCResponseDataType(
           java.math.BigInteger recordRefNo,
           flexnet.macrovision.com.AdvancedFulfillmentLCInfoType[] upgradedFulfillmentInfo) {
           this.recordRefNo = recordRefNo;
           this.upgradedFulfillmentInfo = upgradedFulfillmentInfo;
    }


    /**
     * Gets the recordRefNo value for this AdvancedFmtLCResponseDataType.
     * 
     * @return recordRefNo
     */
    public java.math.BigInteger getRecordRefNo() {
        return recordRefNo;
    }


    /**
     * Sets the recordRefNo value for this AdvancedFmtLCResponseDataType.
     * 
     * @param recordRefNo
     */
    public void setRecordRefNo(java.math.BigInteger recordRefNo) {
        this.recordRefNo = recordRefNo;
    }


    /**
     * Gets the upgradedFulfillmentInfo value for this AdvancedFmtLCResponseDataType.
     * 
     * @return upgradedFulfillmentInfo
     */
    public flexnet.macrovision.com.AdvancedFulfillmentLCInfoType[] getUpgradedFulfillmentInfo() {
        return upgradedFulfillmentInfo;
    }


    /**
     * Sets the upgradedFulfillmentInfo value for this AdvancedFmtLCResponseDataType.
     * 
     * @param upgradedFulfillmentInfo
     */
    public void setUpgradedFulfillmentInfo(flexnet.macrovision.com.AdvancedFulfillmentLCInfoType[] upgradedFulfillmentInfo) {
        this.upgradedFulfillmentInfo = upgradedFulfillmentInfo;
    }

    public flexnet.macrovision.com.AdvancedFulfillmentLCInfoType getUpgradedFulfillmentInfo(int i) {
        return this.upgradedFulfillmentInfo[i];
    }

    public void setUpgradedFulfillmentInfo(int i, flexnet.macrovision.com.AdvancedFulfillmentLCInfoType _value) {
        this.upgradedFulfillmentInfo[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AdvancedFmtLCResponseDataType)) return false;
        AdvancedFmtLCResponseDataType other = (AdvancedFmtLCResponseDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.recordRefNo==null && other.getRecordRefNo()==null) || 
             (this.recordRefNo!=null &&
              this.recordRefNo.equals(other.getRecordRefNo()))) &&
            ((this.upgradedFulfillmentInfo==null && other.getUpgradedFulfillmentInfo()==null) || 
             (this.upgradedFulfillmentInfo!=null &&
              java.util.Arrays.equals(this.upgradedFulfillmentInfo, other.getUpgradedFulfillmentInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRecordRefNo() != null) {
            _hashCode += getRecordRefNo().hashCode();
        }
        if (getUpgradedFulfillmentInfo() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getUpgradedFulfillmentInfo());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getUpgradedFulfillmentInfo(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AdvancedFmtLCResponseDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFmtLCResponseDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recordRefNo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "recordRefNo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("upgradedFulfillmentInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "upgradedFulfillmentInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "advancedFulfillmentLCInfoType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
