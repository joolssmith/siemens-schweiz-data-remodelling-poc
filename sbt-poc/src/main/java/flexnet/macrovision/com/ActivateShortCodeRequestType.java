/**
 * ActivateShortCodeRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ActivateShortCodeRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.CreateShortCodeDataType shortCodeData;

    public ActivateShortCodeRequestType() {
    }

    public ActivateShortCodeRequestType(
           flexnet.macrovision.com.CreateShortCodeDataType shortCodeData) {
           this.shortCodeData = shortCodeData;
    }


    /**
     * Gets the shortCodeData value for this ActivateShortCodeRequestType.
     * 
     * @return shortCodeData
     */
    public flexnet.macrovision.com.CreateShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }


    /**
     * Sets the shortCodeData value for this ActivateShortCodeRequestType.
     * 
     * @param shortCodeData
     */
    public void setShortCodeData(flexnet.macrovision.com.CreateShortCodeDataType shortCodeData) {
        this.shortCodeData = shortCodeData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ActivateShortCodeRequestType)) return false;
        ActivateShortCodeRequestType other = (ActivateShortCodeRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.shortCodeData==null && other.getShortCodeData()==null) || 
             (this.shortCodeData!=null &&
              this.shortCodeData.equals(other.getShortCodeData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getShortCodeData() != null) {
            _hashCode += getShortCodeData().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ActivateShortCodeRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activateShortCodeRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortCodeData");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shortCodeData"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createShortCodeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
