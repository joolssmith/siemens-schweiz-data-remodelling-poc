/**
 * PublisherErrorFulfillmentDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class PublisherErrorFulfillmentDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier;

    private flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes;

    private java.lang.String FNPTimeZoneValue;

    private java.lang.String shipToEmail;

    private java.lang.String shipToAddress;

    private flexnet.macrovision.com.ServerIDsType serverIds;

    private java.lang.String[] nodeIds;

    private flexnet.macrovision.com.CustomHostIDType customHost;

    private java.math.BigInteger fulfillCount;

    private java.lang.Boolean overridePolicy;

    public PublisherErrorFulfillmentDataType() {
    }

    public PublisherErrorFulfillmentDataType(
           flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier,
           flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes,
           java.lang.String FNPTimeZoneValue,
           java.lang.String shipToEmail,
           java.lang.String shipToAddress,
           flexnet.macrovision.com.ServerIDsType serverIds,
           java.lang.String[] nodeIds,
           flexnet.macrovision.com.CustomHostIDType customHost,
           java.math.BigInteger fulfillCount,
           java.lang.Boolean overridePolicy) {
           this.fulfillmentIdentifier = fulfillmentIdentifier;
           this.licenseModelAttributes = licenseModelAttributes;
           this.FNPTimeZoneValue = FNPTimeZoneValue;
           this.shipToEmail = shipToEmail;
           this.shipToAddress = shipToAddress;
           this.serverIds = serverIds;
           this.nodeIds = nodeIds;
           this.customHost = customHost;
           this.fulfillCount = fulfillCount;
           this.overridePolicy = overridePolicy;
    }


    /**
     * Gets the fulfillmentIdentifier value for this PublisherErrorFulfillmentDataType.
     * 
     * @return fulfillmentIdentifier
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }


    /**
     * Sets the fulfillmentIdentifier value for this PublisherErrorFulfillmentDataType.
     * 
     * @param fulfillmentIdentifier
     */
    public void setFulfillmentIdentifier(flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier) {
        this.fulfillmentIdentifier = fulfillmentIdentifier;
    }


    /**
     * Gets the licenseModelAttributes value for this PublisherErrorFulfillmentDataType.
     * 
     * @return licenseModelAttributes
     */
    public flexnet.macrovision.com.AttributeDescriptorType[] getLicenseModelAttributes() {
        return licenseModelAttributes;
    }


    /**
     * Sets the licenseModelAttributes value for this PublisherErrorFulfillmentDataType.
     * 
     * @param licenseModelAttributes
     */
    public void setLicenseModelAttributes(flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes) {
        this.licenseModelAttributes = licenseModelAttributes;
    }


    /**
     * Gets the FNPTimeZoneValue value for this PublisherErrorFulfillmentDataType.
     * 
     * @return FNPTimeZoneValue
     */
    public java.lang.String getFNPTimeZoneValue() {
        return FNPTimeZoneValue;
    }


    /**
     * Sets the FNPTimeZoneValue value for this PublisherErrorFulfillmentDataType.
     * 
     * @param FNPTimeZoneValue
     */
    public void setFNPTimeZoneValue(java.lang.String FNPTimeZoneValue) {
        this.FNPTimeZoneValue = FNPTimeZoneValue;
    }


    /**
     * Gets the shipToEmail value for this PublisherErrorFulfillmentDataType.
     * 
     * @return shipToEmail
     */
    public java.lang.String getShipToEmail() {
        return shipToEmail;
    }


    /**
     * Sets the shipToEmail value for this PublisherErrorFulfillmentDataType.
     * 
     * @param shipToEmail
     */
    public void setShipToEmail(java.lang.String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }


    /**
     * Gets the shipToAddress value for this PublisherErrorFulfillmentDataType.
     * 
     * @return shipToAddress
     */
    public java.lang.String getShipToAddress() {
        return shipToAddress;
    }


    /**
     * Sets the shipToAddress value for this PublisherErrorFulfillmentDataType.
     * 
     * @param shipToAddress
     */
    public void setShipToAddress(java.lang.String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }


    /**
     * Gets the serverIds value for this PublisherErrorFulfillmentDataType.
     * 
     * @return serverIds
     */
    public flexnet.macrovision.com.ServerIDsType getServerIds() {
        return serverIds;
    }


    /**
     * Sets the serverIds value for this PublisherErrorFulfillmentDataType.
     * 
     * @param serverIds
     */
    public void setServerIds(flexnet.macrovision.com.ServerIDsType serverIds) {
        this.serverIds = serverIds;
    }


    /**
     * Gets the nodeIds value for this PublisherErrorFulfillmentDataType.
     * 
     * @return nodeIds
     */
    public java.lang.String[] getNodeIds() {
        return nodeIds;
    }


    /**
     * Sets the nodeIds value for this PublisherErrorFulfillmentDataType.
     * 
     * @param nodeIds
     */
    public void setNodeIds(java.lang.String[] nodeIds) {
        this.nodeIds = nodeIds;
    }


    /**
     * Gets the customHost value for this PublisherErrorFulfillmentDataType.
     * 
     * @return customHost
     */
    public flexnet.macrovision.com.CustomHostIDType getCustomHost() {
        return customHost;
    }


    /**
     * Sets the customHost value for this PublisherErrorFulfillmentDataType.
     * 
     * @param customHost
     */
    public void setCustomHost(flexnet.macrovision.com.CustomHostIDType customHost) {
        this.customHost = customHost;
    }


    /**
     * Gets the fulfillCount value for this PublisherErrorFulfillmentDataType.
     * 
     * @return fulfillCount
     */
    public java.math.BigInteger getFulfillCount() {
        return fulfillCount;
    }


    /**
     * Sets the fulfillCount value for this PublisherErrorFulfillmentDataType.
     * 
     * @param fulfillCount
     */
    public void setFulfillCount(java.math.BigInteger fulfillCount) {
        this.fulfillCount = fulfillCount;
    }


    /**
     * Gets the overridePolicy value for this PublisherErrorFulfillmentDataType.
     * 
     * @return overridePolicy
     */
    public java.lang.Boolean getOverridePolicy() {
        return overridePolicy;
    }


    /**
     * Sets the overridePolicy value for this PublisherErrorFulfillmentDataType.
     * 
     * @param overridePolicy
     */
    public void setOverridePolicy(java.lang.Boolean overridePolicy) {
        this.overridePolicy = overridePolicy;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PublisherErrorFulfillmentDataType)) return false;
        PublisherErrorFulfillmentDataType other = (PublisherErrorFulfillmentDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentIdentifier==null && other.getFulfillmentIdentifier()==null) || 
             (this.fulfillmentIdentifier!=null &&
              this.fulfillmentIdentifier.equals(other.getFulfillmentIdentifier()))) &&
            ((this.licenseModelAttributes==null && other.getLicenseModelAttributes()==null) || 
             (this.licenseModelAttributes!=null &&
              java.util.Arrays.equals(this.licenseModelAttributes, other.getLicenseModelAttributes()))) &&
            ((this.FNPTimeZoneValue==null && other.getFNPTimeZoneValue()==null) || 
             (this.FNPTimeZoneValue!=null &&
              this.FNPTimeZoneValue.equals(other.getFNPTimeZoneValue()))) &&
            ((this.shipToEmail==null && other.getShipToEmail()==null) || 
             (this.shipToEmail!=null &&
              this.shipToEmail.equals(other.getShipToEmail()))) &&
            ((this.shipToAddress==null && other.getShipToAddress()==null) || 
             (this.shipToAddress!=null &&
              this.shipToAddress.equals(other.getShipToAddress()))) &&
            ((this.serverIds==null && other.getServerIds()==null) || 
             (this.serverIds!=null &&
              this.serverIds.equals(other.getServerIds()))) &&
            ((this.nodeIds==null && other.getNodeIds()==null) || 
             (this.nodeIds!=null &&
              java.util.Arrays.equals(this.nodeIds, other.getNodeIds()))) &&
            ((this.customHost==null && other.getCustomHost()==null) || 
             (this.customHost!=null &&
              this.customHost.equals(other.getCustomHost()))) &&
            ((this.fulfillCount==null && other.getFulfillCount()==null) || 
             (this.fulfillCount!=null &&
              this.fulfillCount.equals(other.getFulfillCount()))) &&
            ((this.overridePolicy==null && other.getOverridePolicy()==null) || 
             (this.overridePolicy!=null &&
              this.overridePolicy.equals(other.getOverridePolicy())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentIdentifier() != null) {
            _hashCode += getFulfillmentIdentifier().hashCode();
        }
        if (getLicenseModelAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseModelAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseModelAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFNPTimeZoneValue() != null) {
            _hashCode += getFNPTimeZoneValue().hashCode();
        }
        if (getShipToEmail() != null) {
            _hashCode += getShipToEmail().hashCode();
        }
        if (getShipToAddress() != null) {
            _hashCode += getShipToAddress().hashCode();
        }
        if (getServerIds() != null) {
            _hashCode += getServerIds().hashCode();
        }
        if (getNodeIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNodeIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNodeIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHost() != null) {
            _hashCode += getCustomHost().hashCode();
        }
        if (getFulfillCount() != null) {
            _hashCode += getFulfillCount().hashCode();
        }
        if (getOverridePolicy() != null) {
            _hashCode += getOverridePolicy().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PublisherErrorFulfillmentDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherErrorFulfillmentDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseModelAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModelAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FNPTimeZoneValue");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FNPTimeZoneValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serverIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "serverIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overridePolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overridePolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
