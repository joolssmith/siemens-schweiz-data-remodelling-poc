/**
 * GetFulfillmentAttributesDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetFulfillmentAttributesDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.AttributeMetaDescriptorType[] fulfillmentAttributes;

    private java.lang.Boolean needTimeZone;

    public GetFulfillmentAttributesDataType() {
    }

    public GetFulfillmentAttributesDataType(
           flexnet.macrovision.com.AttributeMetaDescriptorType[] fulfillmentAttributes,
           java.lang.Boolean needTimeZone) {
           this.fulfillmentAttributes = fulfillmentAttributes;
           this.needTimeZone = needTimeZone;
    }


    /**
     * Gets the fulfillmentAttributes value for this GetFulfillmentAttributesDataType.
     * 
     * @return fulfillmentAttributes
     */
    public flexnet.macrovision.com.AttributeMetaDescriptorType[] getFulfillmentAttributes() {
        return fulfillmentAttributes;
    }


    /**
     * Sets the fulfillmentAttributes value for this GetFulfillmentAttributesDataType.
     * 
     * @param fulfillmentAttributes
     */
    public void setFulfillmentAttributes(flexnet.macrovision.com.AttributeMetaDescriptorType[] fulfillmentAttributes) {
        this.fulfillmentAttributes = fulfillmentAttributes;
    }


    /**
     * Gets the needTimeZone value for this GetFulfillmentAttributesDataType.
     * 
     * @return needTimeZone
     */
    public java.lang.Boolean getNeedTimeZone() {
        return needTimeZone;
    }


    /**
     * Sets the needTimeZone value for this GetFulfillmentAttributesDataType.
     * 
     * @param needTimeZone
     */
    public void setNeedTimeZone(java.lang.Boolean needTimeZone) {
        this.needTimeZone = needTimeZone;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetFulfillmentAttributesDataType)) return false;
        GetFulfillmentAttributesDataType other = (GetFulfillmentAttributesDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentAttributes==null && other.getFulfillmentAttributes()==null) || 
             (this.fulfillmentAttributes!=null &&
              java.util.Arrays.equals(this.fulfillmentAttributes, other.getFulfillmentAttributes()))) &&
            ((this.needTimeZone==null && other.getNeedTimeZone()==null) || 
             (this.needTimeZone!=null &&
              this.needTimeZone.equals(other.getNeedTimeZone())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFulfillmentAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFulfillmentAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNeedTimeZone() != null) {
            _hashCode += getNeedTimeZone().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetFulfillmentAttributesDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getFulfillmentAttributesDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeMetaDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("needTimeZone");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "needTimeZone"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
