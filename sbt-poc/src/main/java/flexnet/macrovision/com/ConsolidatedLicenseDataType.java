/**
 * ConsolidatedLicenseDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ConsolidatedLicenseDataType  implements java.io.Serializable {
    private java.lang.String consolidatedLicenseId;

    private java.lang.String licenseText;

    private byte[] binaryLicense;

    private java.lang.String soldTo;

    private java.lang.String criteria;

    private flexnet.macrovision.com.StateType state;

    private flexnet.macrovision.com.FulfillmentIdentifierType[] consolidatedFulfillments;

    private flexnet.macrovision.com.LicenseFileDataType[] licenseFiles;

    public ConsolidatedLicenseDataType() {
    }

    public ConsolidatedLicenseDataType(
           java.lang.String consolidatedLicenseId,
           java.lang.String licenseText,
           byte[] binaryLicense,
           java.lang.String soldTo,
           java.lang.String criteria,
           flexnet.macrovision.com.StateType state,
           flexnet.macrovision.com.FulfillmentIdentifierType[] consolidatedFulfillments,
           flexnet.macrovision.com.LicenseFileDataType[] licenseFiles) {
           this.consolidatedLicenseId = consolidatedLicenseId;
           this.licenseText = licenseText;
           this.binaryLicense = binaryLicense;
           this.soldTo = soldTo;
           this.criteria = criteria;
           this.state = state;
           this.consolidatedFulfillments = consolidatedFulfillments;
           this.licenseFiles = licenseFiles;
    }


    /**
     * Gets the consolidatedLicenseId value for this ConsolidatedLicenseDataType.
     * 
     * @return consolidatedLicenseId
     */
    public java.lang.String getConsolidatedLicenseId() {
        return consolidatedLicenseId;
    }


    /**
     * Sets the consolidatedLicenseId value for this ConsolidatedLicenseDataType.
     * 
     * @param consolidatedLicenseId
     */
    public void setConsolidatedLicenseId(java.lang.String consolidatedLicenseId) {
        this.consolidatedLicenseId = consolidatedLicenseId;
    }


    /**
     * Gets the licenseText value for this ConsolidatedLicenseDataType.
     * 
     * @return licenseText
     */
    public java.lang.String getLicenseText() {
        return licenseText;
    }


    /**
     * Sets the licenseText value for this ConsolidatedLicenseDataType.
     * 
     * @param licenseText
     */
    public void setLicenseText(java.lang.String licenseText) {
        this.licenseText = licenseText;
    }


    /**
     * Gets the binaryLicense value for this ConsolidatedLicenseDataType.
     * 
     * @return binaryLicense
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }


    /**
     * Sets the binaryLicense value for this ConsolidatedLicenseDataType.
     * 
     * @param binaryLicense
     */
    public void setBinaryLicense(byte[] binaryLicense) {
        this.binaryLicense = binaryLicense;
    }


    /**
     * Gets the soldTo value for this ConsolidatedLicenseDataType.
     * 
     * @return soldTo
     */
    public java.lang.String getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this ConsolidatedLicenseDataType.
     * 
     * @param soldTo
     */
    public void setSoldTo(java.lang.String soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the criteria value for this ConsolidatedLicenseDataType.
     * 
     * @return criteria
     */
    public java.lang.String getCriteria() {
        return criteria;
    }


    /**
     * Sets the criteria value for this ConsolidatedLicenseDataType.
     * 
     * @param criteria
     */
    public void setCriteria(java.lang.String criteria) {
        this.criteria = criteria;
    }


    /**
     * Gets the state value for this ConsolidatedLicenseDataType.
     * 
     * @return state
     */
    public flexnet.macrovision.com.StateType getState() {
        return state;
    }


    /**
     * Sets the state value for this ConsolidatedLicenseDataType.
     * 
     * @param state
     */
    public void setState(flexnet.macrovision.com.StateType state) {
        this.state = state;
    }


    /**
     * Gets the consolidatedFulfillments value for this ConsolidatedLicenseDataType.
     * 
     * @return consolidatedFulfillments
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType[] getConsolidatedFulfillments() {
        return consolidatedFulfillments;
    }


    /**
     * Sets the consolidatedFulfillments value for this ConsolidatedLicenseDataType.
     * 
     * @param consolidatedFulfillments
     */
    public void setConsolidatedFulfillments(flexnet.macrovision.com.FulfillmentIdentifierType[] consolidatedFulfillments) {
        this.consolidatedFulfillments = consolidatedFulfillments;
    }


    /**
     * Gets the licenseFiles value for this ConsolidatedLicenseDataType.
     * 
     * @return licenseFiles
     */
    public flexnet.macrovision.com.LicenseFileDataType[] getLicenseFiles() {
        return licenseFiles;
    }


    /**
     * Sets the licenseFiles value for this ConsolidatedLicenseDataType.
     * 
     * @param licenseFiles
     */
    public void setLicenseFiles(flexnet.macrovision.com.LicenseFileDataType[] licenseFiles) {
        this.licenseFiles = licenseFiles;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsolidatedLicenseDataType)) return false;
        ConsolidatedLicenseDataType other = (ConsolidatedLicenseDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consolidatedLicenseId==null && other.getConsolidatedLicenseId()==null) || 
             (this.consolidatedLicenseId!=null &&
              this.consolidatedLicenseId.equals(other.getConsolidatedLicenseId()))) &&
            ((this.licenseText==null && other.getLicenseText()==null) || 
             (this.licenseText!=null &&
              this.licenseText.equals(other.getLicenseText()))) &&
            ((this.binaryLicense==null && other.getBinaryLicense()==null) || 
             (this.binaryLicense!=null &&
              java.util.Arrays.equals(this.binaryLicense, other.getBinaryLicense()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.criteria==null && other.getCriteria()==null) || 
             (this.criteria!=null &&
              this.criteria.equals(other.getCriteria()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.consolidatedFulfillments==null && other.getConsolidatedFulfillments()==null) || 
             (this.consolidatedFulfillments!=null &&
              java.util.Arrays.equals(this.consolidatedFulfillments, other.getConsolidatedFulfillments()))) &&
            ((this.licenseFiles==null && other.getLicenseFiles()==null) || 
             (this.licenseFiles!=null &&
              java.util.Arrays.equals(this.licenseFiles, other.getLicenseFiles())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsolidatedLicenseId() != null) {
            _hashCode += getConsolidatedLicenseId().hashCode();
        }
        if (getLicenseText() != null) {
            _hashCode += getLicenseText().hashCode();
        }
        if (getBinaryLicense() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBinaryLicense());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBinaryLicense(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getCriteria() != null) {
            _hashCode += getCriteria().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getConsolidatedFulfillments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsolidatedFulfillments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsolidatedFulfillments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLicenseFiles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseFiles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseFiles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsolidatedLicenseDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consolidatedLicenseId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseText");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binaryLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "binaryLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criteria");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "criteria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consolidatedFulfillments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedFulfillments"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFiles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFiles"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFile"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
