/**
 * ServerIDsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ServerIDsType  implements java.io.Serializable {
    private java.lang.String server1;

    private java.lang.String server2;

    private java.lang.String server3;

    public ServerIDsType() {
    }

    public ServerIDsType(
           java.lang.String server1,
           java.lang.String server2,
           java.lang.String server3) {
           this.server1 = server1;
           this.server2 = server2;
           this.server3 = server3;
    }


    /**
     * Gets the server1 value for this ServerIDsType.
     * 
     * @return server1
     */
    public java.lang.String getServer1() {
        return server1;
    }


    /**
     * Sets the server1 value for this ServerIDsType.
     * 
     * @param server1
     */
    public void setServer1(java.lang.String server1) {
        this.server1 = server1;
    }


    /**
     * Gets the server2 value for this ServerIDsType.
     * 
     * @return server2
     */
    public java.lang.String getServer2() {
        return server2;
    }


    /**
     * Sets the server2 value for this ServerIDsType.
     * 
     * @param server2
     */
    public void setServer2(java.lang.String server2) {
        this.server2 = server2;
    }


    /**
     * Gets the server3 value for this ServerIDsType.
     * 
     * @return server3
     */
    public java.lang.String getServer3() {
        return server3;
    }


    /**
     * Sets the server3 value for this ServerIDsType.
     * 
     * @param server3
     */
    public void setServer3(java.lang.String server3) {
        this.server3 = server3;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ServerIDsType)) return false;
        ServerIDsType other = (ServerIDsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.server1==null && other.getServer1()==null) || 
             (this.server1!=null &&
              this.server1.equals(other.getServer1()))) &&
            ((this.server2==null && other.getServer2()==null) || 
             (this.server2!=null &&
              this.server2.equals(other.getServer2()))) &&
            ((this.server3==null && other.getServer3()==null) || 
             (this.server3!=null &&
              this.server3.equals(other.getServer3())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getServer1() != null) {
            _hashCode += getServer1().hashCode();
        }
        if (getServer2() != null) {
            _hashCode += getServer2().hashCode();
        }
        if (getServer3() != null) {
            _hashCode += getServer3().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ServerIDsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("server1");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "server1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("server2");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "server2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("server3");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "server3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
