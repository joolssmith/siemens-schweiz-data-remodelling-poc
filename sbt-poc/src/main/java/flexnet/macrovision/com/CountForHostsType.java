/**
 * CountForHostsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class CountForHostsType  implements java.io.Serializable {
    private java.lang.String hostDataRefId;

    private java.math.BigInteger fulfillCount;

    private java.math.BigInteger overDraftCount;

    public CountForHostsType() {
    }

    public CountForHostsType(
           java.lang.String hostDataRefId,
           java.math.BigInteger fulfillCount,
           java.math.BigInteger overDraftCount) {
           this.hostDataRefId = hostDataRefId;
           this.fulfillCount = fulfillCount;
           this.overDraftCount = overDraftCount;
    }


    /**
     * Gets the hostDataRefId value for this CountForHostsType.
     * 
     * @return hostDataRefId
     */
    public java.lang.String getHostDataRefId() {
        return hostDataRefId;
    }


    /**
     * Sets the hostDataRefId value for this CountForHostsType.
     * 
     * @param hostDataRefId
     */
    public void setHostDataRefId(java.lang.String hostDataRefId) {
        this.hostDataRefId = hostDataRefId;
    }


    /**
     * Gets the fulfillCount value for this CountForHostsType.
     * 
     * @return fulfillCount
     */
    public java.math.BigInteger getFulfillCount() {
        return fulfillCount;
    }


    /**
     * Sets the fulfillCount value for this CountForHostsType.
     * 
     * @param fulfillCount
     */
    public void setFulfillCount(java.math.BigInteger fulfillCount) {
        this.fulfillCount = fulfillCount;
    }


    /**
     * Gets the overDraftCount value for this CountForHostsType.
     * 
     * @return overDraftCount
     */
    public java.math.BigInteger getOverDraftCount() {
        return overDraftCount;
    }


    /**
     * Sets the overDraftCount value for this CountForHostsType.
     * 
     * @param overDraftCount
     */
    public void setOverDraftCount(java.math.BigInteger overDraftCount) {
        this.overDraftCount = overDraftCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CountForHostsType)) return false;
        CountForHostsType other = (CountForHostsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.hostDataRefId==null && other.getHostDataRefId()==null) || 
             (this.hostDataRefId!=null &&
              this.hostDataRefId.equals(other.getHostDataRefId()))) &&
            ((this.fulfillCount==null && other.getFulfillCount()==null) || 
             (this.fulfillCount!=null &&
              this.fulfillCount.equals(other.getFulfillCount()))) &&
            ((this.overDraftCount==null && other.getOverDraftCount()==null) || 
             (this.overDraftCount!=null &&
              this.overDraftCount.equals(other.getOverDraftCount())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHostDataRefId() != null) {
            _hashCode += getHostDataRefId().hashCode();
        }
        if (getFulfillCount() != null) {
            _hashCode += getFulfillCount().hashCode();
        }
        if (getOverDraftCount() != null) {
            _hashCode += getOverDraftCount().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CountForHostsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "countForHostsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostDataRefId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostDataRefId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
