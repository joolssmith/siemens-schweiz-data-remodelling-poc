/**
 * SupportLicenseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class SupportLicenseType implements java.io.Serializable {
    private org.apache.axis.types.NMToken _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected SupportLicenseType(org.apache.axis.types.NMToken value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final org.apache.axis.types.NMToken _MASTER = new org.apache.axis.types.NMToken("MASTER");
    public static final org.apache.axis.types.NMToken _REHOST = new org.apache.axis.types.NMToken("REHOST");
    public static final org.apache.axis.types.NMToken _RETURN = new org.apache.axis.types.NMToken("RETURN");
    public static final org.apache.axis.types.NMToken _REPAIR = new org.apache.axis.types.NMToken("REPAIR");
    public static final org.apache.axis.types.NMToken _STOPGAP = new org.apache.axis.types.NMToken("STOPGAP");
    public static final org.apache.axis.types.NMToken _EMERGENCY = new org.apache.axis.types.NMToken("EMERGENCY");
    public static final org.apache.axis.types.NMToken _PUBLISHER_ERROR = new org.apache.axis.types.NMToken("PUBLISHER_ERROR");
    public static final org.apache.axis.types.NMToken _TRANSFER = new org.apache.axis.types.NMToken("TRANSFER");
    public static final org.apache.axis.types.NMToken _UPGRADE = new org.apache.axis.types.NMToken("UPGRADE");
    public static final org.apache.axis.types.NMToken _UPSELL = new org.apache.axis.types.NMToken("UPSELL");
    public static final org.apache.axis.types.NMToken _REGENERATE = new org.apache.axis.types.NMToken("REGENERATE");
    public static final org.apache.axis.types.NMToken _REINSTALL = new org.apache.axis.types.NMToken("REINSTALL");
    public static final org.apache.axis.types.NMToken _RENEW = new org.apache.axis.types.NMToken("RENEW");
    public static final SupportLicenseType MASTER = new SupportLicenseType(_MASTER);
    public static final SupportLicenseType REHOST = new SupportLicenseType(_REHOST);
    public static final SupportLicenseType RETURN = new SupportLicenseType(_RETURN);
    public static final SupportLicenseType REPAIR = new SupportLicenseType(_REPAIR);
    public static final SupportLicenseType STOPGAP = new SupportLicenseType(_STOPGAP);
    public static final SupportLicenseType EMERGENCY = new SupportLicenseType(_EMERGENCY);
    public static final SupportLicenseType PUBLISHER_ERROR = new SupportLicenseType(_PUBLISHER_ERROR);
    public static final SupportLicenseType TRANSFER = new SupportLicenseType(_TRANSFER);
    public static final SupportLicenseType UPGRADE = new SupportLicenseType(_UPGRADE);
    public static final SupportLicenseType UPSELL = new SupportLicenseType(_UPSELL);
    public static final SupportLicenseType REGENERATE = new SupportLicenseType(_REGENERATE);
    public static final SupportLicenseType REINSTALL = new SupportLicenseType(_REINSTALL);
    public static final SupportLicenseType RENEW = new SupportLicenseType(_RENEW);
    public org.apache.axis.types.NMToken getValue() { return _value_;}
    public static SupportLicenseType fromValue(org.apache.axis.types.NMToken value)
          throws java.lang.IllegalArgumentException {
        SupportLicenseType enumeration = (SupportLicenseType)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static SupportLicenseType fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        try {
            return fromValue(new org.apache.axis.types.NMToken(value));
        } catch (Exception e) {
            throw new java.lang.IllegalArgumentException();
        }
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_.toString();}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SupportLicenseType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SupportLicenseType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
