/**
 * GetConsolidatedFulfillmentCountRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetConsolidatedFulfillmentCountRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.ConsolidatedFulfillmentsQPType queryParams;

    public GetConsolidatedFulfillmentCountRequestType() {
    }

    public GetConsolidatedFulfillmentCountRequestType(
           flexnet.macrovision.com.ConsolidatedFulfillmentsQPType queryParams) {
           this.queryParams = queryParams;
    }


    /**
     * Gets the queryParams value for this GetConsolidatedFulfillmentCountRequestType.
     * 
     * @return queryParams
     */
    public flexnet.macrovision.com.ConsolidatedFulfillmentsQPType getQueryParams() {
        return queryParams;
    }


    /**
     * Sets the queryParams value for this GetConsolidatedFulfillmentCountRequestType.
     * 
     * @param queryParams
     */
    public void setQueryParams(flexnet.macrovision.com.ConsolidatedFulfillmentsQPType queryParams) {
        this.queryParams = queryParams;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetConsolidatedFulfillmentCountRequestType)) return false;
        GetConsolidatedFulfillmentCountRequestType other = (GetConsolidatedFulfillmentCountRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.queryParams==null && other.getQueryParams()==null) || 
             (this.queryParams!=null &&
              this.queryParams.equals(other.getQueryParams())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQueryParams() != null) {
            _hashCode += getQueryParams().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetConsolidatedFulfillmentCountRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentCountRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryParams");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "queryParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedFulfillmentsQPType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
