/**
 * GetConsolidatedFulfillmentsQueryRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class GetConsolidatedFulfillmentsQueryRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.ConsolidatedFulfillmentsQPType queryParams;

    private java.math.BigInteger pageNumber;

    private java.math.BigInteger batchSize;

    private java.lang.Boolean includeLicenseText;

    public GetConsolidatedFulfillmentsQueryRequestType() {
    }

    public GetConsolidatedFulfillmentsQueryRequestType(
           flexnet.macrovision.com.ConsolidatedFulfillmentsQPType queryParams,
           java.math.BigInteger pageNumber,
           java.math.BigInteger batchSize,
           java.lang.Boolean includeLicenseText) {
           this.queryParams = queryParams;
           this.pageNumber = pageNumber;
           this.batchSize = batchSize;
           this.includeLicenseText = includeLicenseText;
    }


    /**
     * Gets the queryParams value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @return queryParams
     */
    public flexnet.macrovision.com.ConsolidatedFulfillmentsQPType getQueryParams() {
        return queryParams;
    }


    /**
     * Sets the queryParams value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @param queryParams
     */
    public void setQueryParams(flexnet.macrovision.com.ConsolidatedFulfillmentsQPType queryParams) {
        this.queryParams = queryParams;
    }


    /**
     * Gets the pageNumber value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @return pageNumber
     */
    public java.math.BigInteger getPageNumber() {
        return pageNumber;
    }


    /**
     * Sets the pageNumber value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @param pageNumber
     */
    public void setPageNumber(java.math.BigInteger pageNumber) {
        this.pageNumber = pageNumber;
    }


    /**
     * Gets the batchSize value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @return batchSize
     */
    public java.math.BigInteger getBatchSize() {
        return batchSize;
    }


    /**
     * Sets the batchSize value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @param batchSize
     */
    public void setBatchSize(java.math.BigInteger batchSize) {
        this.batchSize = batchSize;
    }


    /**
     * Gets the includeLicenseText value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @return includeLicenseText
     */
    public java.lang.Boolean getIncludeLicenseText() {
        return includeLicenseText;
    }


    /**
     * Sets the includeLicenseText value for this GetConsolidatedFulfillmentsQueryRequestType.
     * 
     * @param includeLicenseText
     */
    public void setIncludeLicenseText(java.lang.Boolean includeLicenseText) {
        this.includeLicenseText = includeLicenseText;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetConsolidatedFulfillmentsQueryRequestType)) return false;
        GetConsolidatedFulfillmentsQueryRequestType other = (GetConsolidatedFulfillmentsQueryRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.queryParams==null && other.getQueryParams()==null) || 
             (this.queryParams!=null &&
              this.queryParams.equals(other.getQueryParams()))) &&
            ((this.pageNumber==null && other.getPageNumber()==null) || 
             (this.pageNumber!=null &&
              this.pageNumber.equals(other.getPageNumber()))) &&
            ((this.batchSize==null && other.getBatchSize()==null) || 
             (this.batchSize!=null &&
              this.batchSize.equals(other.getBatchSize()))) &&
            ((this.includeLicenseText==null && other.getIncludeLicenseText()==null) || 
             (this.includeLicenseText!=null &&
              this.includeLicenseText.equals(other.getIncludeLicenseText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQueryParams() != null) {
            _hashCode += getQueryParams().hashCode();
        }
        if (getPageNumber() != null) {
            _hashCode += getPageNumber().hashCode();
        }
        if (getBatchSize() != null) {
            _hashCode += getBatchSize().hashCode();
        }
        if (getIncludeLicenseText() != null) {
            _hashCode += getIncludeLicenseText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetConsolidatedFulfillmentsQueryRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "getConsolidatedFulfillmentsQueryRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryParams");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "queryParams"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedFulfillmentsQPType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "pageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchSize");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "batchSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeLicenseText");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "includeLicenseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
