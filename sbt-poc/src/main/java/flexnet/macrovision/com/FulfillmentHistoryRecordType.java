/**
 * FulfillmentHistoryRecordType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FulfillmentHistoryRecordType  implements java.io.Serializable {
    private java.lang.String fulfillmentId;

    private java.lang.String action;

    private java.util.Date actionDate;

    private java.util.Calendar actionDateTime;

    private java.lang.String actionPerformedBy;

    private boolean isPolicyOverridden;

    private java.math.BigInteger count;

    private java.lang.String fulfillmentSource;

    public FulfillmentHistoryRecordType() {
    }

    public FulfillmentHistoryRecordType(
           java.lang.String fulfillmentId,
           java.lang.String action,
           java.util.Date actionDate,
           java.util.Calendar actionDateTime,
           java.lang.String actionPerformedBy,
           boolean isPolicyOverridden,
           java.math.BigInteger count,
           java.lang.String fulfillmentSource) {
           this.fulfillmentId = fulfillmentId;
           this.action = action;
           this.actionDate = actionDate;
           this.actionDateTime = actionDateTime;
           this.actionPerformedBy = actionPerformedBy;
           this.isPolicyOverridden = isPolicyOverridden;
           this.count = count;
           this.fulfillmentSource = fulfillmentSource;
    }


    /**
     * Gets the fulfillmentId value for this FulfillmentHistoryRecordType.
     * 
     * @return fulfillmentId
     */
    public java.lang.String getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this FulfillmentHistoryRecordType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(java.lang.String fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the action value for this FulfillmentHistoryRecordType.
     * 
     * @return action
     */
    public java.lang.String getAction() {
        return action;
    }


    /**
     * Sets the action value for this FulfillmentHistoryRecordType.
     * 
     * @param action
     */
    public void setAction(java.lang.String action) {
        this.action = action;
    }


    /**
     * Gets the actionDate value for this FulfillmentHistoryRecordType.
     * 
     * @return actionDate
     */
    public java.util.Date getActionDate() {
        return actionDate;
    }


    /**
     * Sets the actionDate value for this FulfillmentHistoryRecordType.
     * 
     * @param actionDate
     */
    public void setActionDate(java.util.Date actionDate) {
        this.actionDate = actionDate;
    }


    /**
     * Gets the actionDateTime value for this FulfillmentHistoryRecordType.
     * 
     * @return actionDateTime
     */
    public java.util.Calendar getActionDateTime() {
        return actionDateTime;
    }


    /**
     * Sets the actionDateTime value for this FulfillmentHistoryRecordType.
     * 
     * @param actionDateTime
     */
    public void setActionDateTime(java.util.Calendar actionDateTime) {
        this.actionDateTime = actionDateTime;
    }


    /**
     * Gets the actionPerformedBy value for this FulfillmentHistoryRecordType.
     * 
     * @return actionPerformedBy
     */
    public java.lang.String getActionPerformedBy() {
        return actionPerformedBy;
    }


    /**
     * Sets the actionPerformedBy value for this FulfillmentHistoryRecordType.
     * 
     * @param actionPerformedBy
     */
    public void setActionPerformedBy(java.lang.String actionPerformedBy) {
        this.actionPerformedBy = actionPerformedBy;
    }


    /**
     * Gets the isPolicyOverridden value for this FulfillmentHistoryRecordType.
     * 
     * @return isPolicyOverridden
     */
    public boolean isIsPolicyOverridden() {
        return isPolicyOverridden;
    }


    /**
     * Sets the isPolicyOverridden value for this FulfillmentHistoryRecordType.
     * 
     * @param isPolicyOverridden
     */
    public void setIsPolicyOverridden(boolean isPolicyOverridden) {
        this.isPolicyOverridden = isPolicyOverridden;
    }


    /**
     * Gets the count value for this FulfillmentHistoryRecordType.
     * 
     * @return count
     */
    public java.math.BigInteger getCount() {
        return count;
    }


    /**
     * Sets the count value for this FulfillmentHistoryRecordType.
     * 
     * @param count
     */
    public void setCount(java.math.BigInteger count) {
        this.count = count;
    }


    /**
     * Gets the fulfillmentSource value for this FulfillmentHistoryRecordType.
     * 
     * @return fulfillmentSource
     */
    public java.lang.String getFulfillmentSource() {
        return fulfillmentSource;
    }


    /**
     * Sets the fulfillmentSource value for this FulfillmentHistoryRecordType.
     * 
     * @param fulfillmentSource
     */
    public void setFulfillmentSource(java.lang.String fulfillmentSource) {
        this.fulfillmentSource = fulfillmentSource;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FulfillmentHistoryRecordType)) return false;
        FulfillmentHistoryRecordType other = (FulfillmentHistoryRecordType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.action==null && other.getAction()==null) || 
             (this.action!=null &&
              this.action.equals(other.getAction()))) &&
            ((this.actionDate==null && other.getActionDate()==null) || 
             (this.actionDate!=null &&
              this.actionDate.equals(other.getActionDate()))) &&
            ((this.actionDateTime==null && other.getActionDateTime()==null) || 
             (this.actionDateTime!=null &&
              this.actionDateTime.equals(other.getActionDateTime()))) &&
            ((this.actionPerformedBy==null && other.getActionPerformedBy()==null) || 
             (this.actionPerformedBy!=null &&
              this.actionPerformedBy.equals(other.getActionPerformedBy()))) &&
            this.isPolicyOverridden == other.isIsPolicyOverridden() &&
            ((this.count==null && other.getCount()==null) || 
             (this.count!=null &&
              this.count.equals(other.getCount()))) &&
            ((this.fulfillmentSource==null && other.getFulfillmentSource()==null) || 
             (this.fulfillmentSource!=null &&
              this.fulfillmentSource.equals(other.getFulfillmentSource())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getAction() != null) {
            _hashCode += getAction().hashCode();
        }
        if (getActionDate() != null) {
            _hashCode += getActionDate().hashCode();
        }
        if (getActionDateTime() != null) {
            _hashCode += getActionDateTime().hashCode();
        }
        if (getActionPerformedBy() != null) {
            _hashCode += getActionPerformedBy().hashCode();
        }
        _hashCode += (isIsPolicyOverridden() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCount() != null) {
            _hashCode += getCount().hashCode();
        }
        if (getFulfillmentSource() != null) {
            _hashCode += getFulfillmentSource().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FulfillmentHistoryRecordType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentHistoryRecordType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("action");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "action"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "actionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "actionDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("actionPerformedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "actionPerformedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPolicyOverridden");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "isPolicyOverridden"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("count");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "count"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentSource");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
