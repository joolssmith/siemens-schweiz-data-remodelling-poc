/**
 * FulfillmentDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FulfillmentDataType  implements java.io.Serializable {
    private flexnet.macrovision.com.EntitlementIdentifierType entitlementIdentifier;

    private flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier;

    private java.lang.String fulfillmentType;

    private flexnet.macrovision.com.EntitlementLineItemIdentifierType lineItem;

    private flexnet.macrovision.com.ProductIdentifierType product;

    private java.lang.String soldTo;

    private java.lang.String shipToEmail;

    private java.lang.String shipToAddress;

    private flexnet.macrovision.com.ServerIDsType serverIds;

    private java.lang.String[] nodeIds;

    private flexnet.macrovision.com.CustomHostIDType customHost;

    private java.lang.String fulfilledCount;

    private java.lang.String overDraftCount;

    private java.util.Date fulfillDate;

    private java.util.Calendar fulfillDateTime;

    private boolean isPermanent;

    private java.util.Date startDate;

    private java.util.Date expirationDate;

    private java.lang.String licenseText;

    private byte[] binaryLicense;

    private java.lang.String[] consolidatedHostLicense;

    private flexnet.macrovision.com.SupportLicenseType supportAction;

    private java.util.Calendar lastModifiedDateTime;

    private flexnet.macrovision.com.FulfillmentIdentifierType parentFulfillmentId;

    private flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnology;

    private flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes;

    private flexnet.macrovision.com.StateType state;

    private flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource;

    private flexnet.macrovision.com.LicenseFileDataType[] licenseFiles;

    private flexnet.macrovision.com.EntitledProductDataType[] entitledProducts;

    private flexnet.macrovision.com.ActivationType activationType;

    public FulfillmentDataType() {
    }

    public FulfillmentDataType(
           flexnet.macrovision.com.EntitlementIdentifierType entitlementIdentifier,
           flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier,
           java.lang.String fulfillmentType,
           flexnet.macrovision.com.EntitlementLineItemIdentifierType lineItem,
           flexnet.macrovision.com.ProductIdentifierType product,
           java.lang.String soldTo,
           java.lang.String shipToEmail,
           java.lang.String shipToAddress,
           flexnet.macrovision.com.ServerIDsType serverIds,
           java.lang.String[] nodeIds,
           flexnet.macrovision.com.CustomHostIDType customHost,
           java.lang.String fulfilledCount,
           java.lang.String overDraftCount,
           java.util.Date fulfillDate,
           java.util.Calendar fulfillDateTime,
           boolean isPermanent,
           java.util.Date startDate,
           java.util.Date expirationDate,
           java.lang.String licenseText,
           byte[] binaryLicense,
           java.lang.String[] consolidatedHostLicense,
           flexnet.macrovision.com.SupportLicenseType supportAction,
           java.util.Calendar lastModifiedDateTime,
           flexnet.macrovision.com.FulfillmentIdentifierType parentFulfillmentId,
           flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnology,
           flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes,
           flexnet.macrovision.com.StateType state,
           flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource,
           flexnet.macrovision.com.LicenseFileDataType[] licenseFiles,
           flexnet.macrovision.com.EntitledProductDataType[] entitledProducts,
           flexnet.macrovision.com.ActivationType activationType) {
           this.entitlementIdentifier = entitlementIdentifier;
           this.fulfillmentIdentifier = fulfillmentIdentifier;
           this.fulfillmentType = fulfillmentType;
           this.lineItem = lineItem;
           this.product = product;
           this.soldTo = soldTo;
           this.shipToEmail = shipToEmail;
           this.shipToAddress = shipToAddress;
           this.serverIds = serverIds;
           this.nodeIds = nodeIds;
           this.customHost = customHost;
           this.fulfilledCount = fulfilledCount;
           this.overDraftCount = overDraftCount;
           this.fulfillDate = fulfillDate;
           this.fulfillDateTime = fulfillDateTime;
           this.isPermanent = isPermanent;
           this.startDate = startDate;
           this.expirationDate = expirationDate;
           this.licenseText = licenseText;
           this.binaryLicense = binaryLicense;
           this.consolidatedHostLicense = consolidatedHostLicense;
           this.supportAction = supportAction;
           this.lastModifiedDateTime = lastModifiedDateTime;
           this.parentFulfillmentId = parentFulfillmentId;
           this.licenseTechnology = licenseTechnology;
           this.licenseModelAttributes = licenseModelAttributes;
           this.state = state;
           this.fulfillmentSource = fulfillmentSource;
           this.licenseFiles = licenseFiles;
           this.entitledProducts = entitledProducts;
           this.activationType = activationType;
    }


    /**
     * Gets the entitlementIdentifier value for this FulfillmentDataType.
     * 
     * @return entitlementIdentifier
     */
    public flexnet.macrovision.com.EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }


    /**
     * Sets the entitlementIdentifier value for this FulfillmentDataType.
     * 
     * @param entitlementIdentifier
     */
    public void setEntitlementIdentifier(flexnet.macrovision.com.EntitlementIdentifierType entitlementIdentifier) {
        this.entitlementIdentifier = entitlementIdentifier;
    }


    /**
     * Gets the fulfillmentIdentifier value for this FulfillmentDataType.
     * 
     * @return fulfillmentIdentifier
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }


    /**
     * Sets the fulfillmentIdentifier value for this FulfillmentDataType.
     * 
     * @param fulfillmentIdentifier
     */
    public void setFulfillmentIdentifier(flexnet.macrovision.com.FulfillmentIdentifierType fulfillmentIdentifier) {
        this.fulfillmentIdentifier = fulfillmentIdentifier;
    }


    /**
     * Gets the fulfillmentType value for this FulfillmentDataType.
     * 
     * @return fulfillmentType
     */
    public java.lang.String getFulfillmentType() {
        return fulfillmentType;
    }


    /**
     * Sets the fulfillmentType value for this FulfillmentDataType.
     * 
     * @param fulfillmentType
     */
    public void setFulfillmentType(java.lang.String fulfillmentType) {
        this.fulfillmentType = fulfillmentType;
    }


    /**
     * Gets the lineItem value for this FulfillmentDataType.
     * 
     * @return lineItem
     */
    public flexnet.macrovision.com.EntitlementLineItemIdentifierType getLineItem() {
        return lineItem;
    }


    /**
     * Sets the lineItem value for this FulfillmentDataType.
     * 
     * @param lineItem
     */
    public void setLineItem(flexnet.macrovision.com.EntitlementLineItemIdentifierType lineItem) {
        this.lineItem = lineItem;
    }


    /**
     * Gets the product value for this FulfillmentDataType.
     * 
     * @return product
     */
    public flexnet.macrovision.com.ProductIdentifierType getProduct() {
        return product;
    }


    /**
     * Sets the product value for this FulfillmentDataType.
     * 
     * @param product
     */
    public void setProduct(flexnet.macrovision.com.ProductIdentifierType product) {
        this.product = product;
    }


    /**
     * Gets the soldTo value for this FulfillmentDataType.
     * 
     * @return soldTo
     */
    public java.lang.String getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this FulfillmentDataType.
     * 
     * @param soldTo
     */
    public void setSoldTo(java.lang.String soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the shipToEmail value for this FulfillmentDataType.
     * 
     * @return shipToEmail
     */
    public java.lang.String getShipToEmail() {
        return shipToEmail;
    }


    /**
     * Sets the shipToEmail value for this FulfillmentDataType.
     * 
     * @param shipToEmail
     */
    public void setShipToEmail(java.lang.String shipToEmail) {
        this.shipToEmail = shipToEmail;
    }


    /**
     * Gets the shipToAddress value for this FulfillmentDataType.
     * 
     * @return shipToAddress
     */
    public java.lang.String getShipToAddress() {
        return shipToAddress;
    }


    /**
     * Sets the shipToAddress value for this FulfillmentDataType.
     * 
     * @param shipToAddress
     */
    public void setShipToAddress(java.lang.String shipToAddress) {
        this.shipToAddress = shipToAddress;
    }


    /**
     * Gets the serverIds value for this FulfillmentDataType.
     * 
     * @return serverIds
     */
    public flexnet.macrovision.com.ServerIDsType getServerIds() {
        return serverIds;
    }


    /**
     * Sets the serverIds value for this FulfillmentDataType.
     * 
     * @param serverIds
     */
    public void setServerIds(flexnet.macrovision.com.ServerIDsType serverIds) {
        this.serverIds = serverIds;
    }


    /**
     * Gets the nodeIds value for this FulfillmentDataType.
     * 
     * @return nodeIds
     */
    public java.lang.String[] getNodeIds() {
        return nodeIds;
    }


    /**
     * Sets the nodeIds value for this FulfillmentDataType.
     * 
     * @param nodeIds
     */
    public void setNodeIds(java.lang.String[] nodeIds) {
        this.nodeIds = nodeIds;
    }


    /**
     * Gets the customHost value for this FulfillmentDataType.
     * 
     * @return customHost
     */
    public flexnet.macrovision.com.CustomHostIDType getCustomHost() {
        return customHost;
    }


    /**
     * Sets the customHost value for this FulfillmentDataType.
     * 
     * @param customHost
     */
    public void setCustomHost(flexnet.macrovision.com.CustomHostIDType customHost) {
        this.customHost = customHost;
    }


    /**
     * Gets the fulfilledCount value for this FulfillmentDataType.
     * 
     * @return fulfilledCount
     */
    public java.lang.String getFulfilledCount() {
        return fulfilledCount;
    }


    /**
     * Sets the fulfilledCount value for this FulfillmentDataType.
     * 
     * @param fulfilledCount
     */
    public void setFulfilledCount(java.lang.String fulfilledCount) {
        this.fulfilledCount = fulfilledCount;
    }


    /**
     * Gets the overDraftCount value for this FulfillmentDataType.
     * 
     * @return overDraftCount
     */
    public java.lang.String getOverDraftCount() {
        return overDraftCount;
    }


    /**
     * Sets the overDraftCount value for this FulfillmentDataType.
     * 
     * @param overDraftCount
     */
    public void setOverDraftCount(java.lang.String overDraftCount) {
        this.overDraftCount = overDraftCount;
    }


    /**
     * Gets the fulfillDate value for this FulfillmentDataType.
     * 
     * @return fulfillDate
     */
    public java.util.Date getFulfillDate() {
        return fulfillDate;
    }


    /**
     * Sets the fulfillDate value for this FulfillmentDataType.
     * 
     * @param fulfillDate
     */
    public void setFulfillDate(java.util.Date fulfillDate) {
        this.fulfillDate = fulfillDate;
    }


    /**
     * Gets the fulfillDateTime value for this FulfillmentDataType.
     * 
     * @return fulfillDateTime
     */
    public java.util.Calendar getFulfillDateTime() {
        return fulfillDateTime;
    }


    /**
     * Sets the fulfillDateTime value for this FulfillmentDataType.
     * 
     * @param fulfillDateTime
     */
    public void setFulfillDateTime(java.util.Calendar fulfillDateTime) {
        this.fulfillDateTime = fulfillDateTime;
    }


    /**
     * Gets the isPermanent value for this FulfillmentDataType.
     * 
     * @return isPermanent
     */
    public boolean isIsPermanent() {
        return isPermanent;
    }


    /**
     * Sets the isPermanent value for this FulfillmentDataType.
     * 
     * @param isPermanent
     */
    public void setIsPermanent(boolean isPermanent) {
        this.isPermanent = isPermanent;
    }


    /**
     * Gets the startDate value for this FulfillmentDataType.
     * 
     * @return startDate
     */
    public java.util.Date getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this FulfillmentDataType.
     * 
     * @param startDate
     */
    public void setStartDate(java.util.Date startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the expirationDate value for this FulfillmentDataType.
     * 
     * @return expirationDate
     */
    public java.util.Date getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this FulfillmentDataType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.util.Date expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the licenseText value for this FulfillmentDataType.
     * 
     * @return licenseText
     */
    public java.lang.String getLicenseText() {
        return licenseText;
    }


    /**
     * Sets the licenseText value for this FulfillmentDataType.
     * 
     * @param licenseText
     */
    public void setLicenseText(java.lang.String licenseText) {
        this.licenseText = licenseText;
    }


    /**
     * Gets the binaryLicense value for this FulfillmentDataType.
     * 
     * @return binaryLicense
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }


    /**
     * Sets the binaryLicense value for this FulfillmentDataType.
     * 
     * @param binaryLicense
     */
    public void setBinaryLicense(byte[] binaryLicense) {
        this.binaryLicense = binaryLicense;
    }


    /**
     * Gets the consolidatedHostLicense value for this FulfillmentDataType.
     * 
     * @return consolidatedHostLicense
     */
    public java.lang.String[] getConsolidatedHostLicense() {
        return consolidatedHostLicense;
    }


    /**
     * Sets the consolidatedHostLicense value for this FulfillmentDataType.
     * 
     * @param consolidatedHostLicense
     */
    public void setConsolidatedHostLicense(java.lang.String[] consolidatedHostLicense) {
        this.consolidatedHostLicense = consolidatedHostLicense;
    }


    /**
     * Gets the supportAction value for this FulfillmentDataType.
     * 
     * @return supportAction
     */
    public flexnet.macrovision.com.SupportLicenseType getSupportAction() {
        return supportAction;
    }


    /**
     * Sets the supportAction value for this FulfillmentDataType.
     * 
     * @param supportAction
     */
    public void setSupportAction(flexnet.macrovision.com.SupportLicenseType supportAction) {
        this.supportAction = supportAction;
    }


    /**
     * Gets the lastModifiedDateTime value for this FulfillmentDataType.
     * 
     * @return lastModifiedDateTime
     */
    public java.util.Calendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }


    /**
     * Sets the lastModifiedDateTime value for this FulfillmentDataType.
     * 
     * @param lastModifiedDateTime
     */
    public void setLastModifiedDateTime(java.util.Calendar lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }


    /**
     * Gets the parentFulfillmentId value for this FulfillmentDataType.
     * 
     * @return parentFulfillmentId
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getParentFulfillmentId() {
        return parentFulfillmentId;
    }


    /**
     * Sets the parentFulfillmentId value for this FulfillmentDataType.
     * 
     * @param parentFulfillmentId
     */
    public void setParentFulfillmentId(flexnet.macrovision.com.FulfillmentIdentifierType parentFulfillmentId) {
        this.parentFulfillmentId = parentFulfillmentId;
    }


    /**
     * Gets the licenseTechnology value for this FulfillmentDataType.
     * 
     * @return licenseTechnology
     */
    public flexnet.macrovision.com.LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }


    /**
     * Sets the licenseTechnology value for this FulfillmentDataType.
     * 
     * @param licenseTechnology
     */
    public void setLicenseTechnology(flexnet.macrovision.com.LicenseTechnologyIdentifierType licenseTechnology) {
        this.licenseTechnology = licenseTechnology;
    }


    /**
     * Gets the licenseModelAttributes value for this FulfillmentDataType.
     * 
     * @return licenseModelAttributes
     */
    public flexnet.macrovision.com.AttributeDescriptorType[] getLicenseModelAttributes() {
        return licenseModelAttributes;
    }


    /**
     * Sets the licenseModelAttributes value for this FulfillmentDataType.
     * 
     * @param licenseModelAttributes
     */
    public void setLicenseModelAttributes(flexnet.macrovision.com.AttributeDescriptorType[] licenseModelAttributes) {
        this.licenseModelAttributes = licenseModelAttributes;
    }


    /**
     * Gets the state value for this FulfillmentDataType.
     * 
     * @return state
     */
    public flexnet.macrovision.com.StateType getState() {
        return state;
    }


    /**
     * Sets the state value for this FulfillmentDataType.
     * 
     * @param state
     */
    public void setState(flexnet.macrovision.com.StateType state) {
        this.state = state;
    }


    /**
     * Gets the fulfillmentSource value for this FulfillmentDataType.
     * 
     * @return fulfillmentSource
     */
    public flexnet.macrovision.com.FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }


    /**
     * Sets the fulfillmentSource value for this FulfillmentDataType.
     * 
     * @param fulfillmentSource
     */
    public void setFulfillmentSource(flexnet.macrovision.com.FulfillmentSourceType fulfillmentSource) {
        this.fulfillmentSource = fulfillmentSource;
    }


    /**
     * Gets the licenseFiles value for this FulfillmentDataType.
     * 
     * @return licenseFiles
     */
    public flexnet.macrovision.com.LicenseFileDataType[] getLicenseFiles() {
        return licenseFiles;
    }


    /**
     * Sets the licenseFiles value for this FulfillmentDataType.
     * 
     * @param licenseFiles
     */
    public void setLicenseFiles(flexnet.macrovision.com.LicenseFileDataType[] licenseFiles) {
        this.licenseFiles = licenseFiles;
    }


    /**
     * Gets the entitledProducts value for this FulfillmentDataType.
     * 
     * @return entitledProducts
     */
    public flexnet.macrovision.com.EntitledProductDataType[] getEntitledProducts() {
        return entitledProducts;
    }


    /**
     * Sets the entitledProducts value for this FulfillmentDataType.
     * 
     * @param entitledProducts
     */
    public void setEntitledProducts(flexnet.macrovision.com.EntitledProductDataType[] entitledProducts) {
        this.entitledProducts = entitledProducts;
    }


    /**
     * Gets the activationType value for this FulfillmentDataType.
     * 
     * @return activationType
     */
    public flexnet.macrovision.com.ActivationType getActivationType() {
        return activationType;
    }


    /**
     * Sets the activationType value for this FulfillmentDataType.
     * 
     * @param activationType
     */
    public void setActivationType(flexnet.macrovision.com.ActivationType activationType) {
        this.activationType = activationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FulfillmentDataType)) return false;
        FulfillmentDataType other = (FulfillmentDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.entitlementIdentifier==null && other.getEntitlementIdentifier()==null) || 
             (this.entitlementIdentifier!=null &&
              this.entitlementIdentifier.equals(other.getEntitlementIdentifier()))) &&
            ((this.fulfillmentIdentifier==null && other.getFulfillmentIdentifier()==null) || 
             (this.fulfillmentIdentifier!=null &&
              this.fulfillmentIdentifier.equals(other.getFulfillmentIdentifier()))) &&
            ((this.fulfillmentType==null && other.getFulfillmentType()==null) || 
             (this.fulfillmentType!=null &&
              this.fulfillmentType.equals(other.getFulfillmentType()))) &&
            ((this.lineItem==null && other.getLineItem()==null) || 
             (this.lineItem!=null &&
              this.lineItem.equals(other.getLineItem()))) &&
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              this.product.equals(other.getProduct()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.shipToEmail==null && other.getShipToEmail()==null) || 
             (this.shipToEmail!=null &&
              this.shipToEmail.equals(other.getShipToEmail()))) &&
            ((this.shipToAddress==null && other.getShipToAddress()==null) || 
             (this.shipToAddress!=null &&
              this.shipToAddress.equals(other.getShipToAddress()))) &&
            ((this.serverIds==null && other.getServerIds()==null) || 
             (this.serverIds!=null &&
              this.serverIds.equals(other.getServerIds()))) &&
            ((this.nodeIds==null && other.getNodeIds()==null) || 
             (this.nodeIds!=null &&
              java.util.Arrays.equals(this.nodeIds, other.getNodeIds()))) &&
            ((this.customHost==null && other.getCustomHost()==null) || 
             (this.customHost!=null &&
              this.customHost.equals(other.getCustomHost()))) &&
            ((this.fulfilledCount==null && other.getFulfilledCount()==null) || 
             (this.fulfilledCount!=null &&
              this.fulfilledCount.equals(other.getFulfilledCount()))) &&
            ((this.overDraftCount==null && other.getOverDraftCount()==null) || 
             (this.overDraftCount!=null &&
              this.overDraftCount.equals(other.getOverDraftCount()))) &&
            ((this.fulfillDate==null && other.getFulfillDate()==null) || 
             (this.fulfillDate!=null &&
              this.fulfillDate.equals(other.getFulfillDate()))) &&
            ((this.fulfillDateTime==null && other.getFulfillDateTime()==null) || 
             (this.fulfillDateTime!=null &&
              this.fulfillDateTime.equals(other.getFulfillDateTime()))) &&
            this.isPermanent == other.isIsPermanent() &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.licenseText==null && other.getLicenseText()==null) || 
             (this.licenseText!=null &&
              this.licenseText.equals(other.getLicenseText()))) &&
            ((this.binaryLicense==null && other.getBinaryLicense()==null) || 
             (this.binaryLicense!=null &&
              java.util.Arrays.equals(this.binaryLicense, other.getBinaryLicense()))) &&
            ((this.consolidatedHostLicense==null && other.getConsolidatedHostLicense()==null) || 
             (this.consolidatedHostLicense!=null &&
              java.util.Arrays.equals(this.consolidatedHostLicense, other.getConsolidatedHostLicense()))) &&
            ((this.supportAction==null && other.getSupportAction()==null) || 
             (this.supportAction!=null &&
              this.supportAction.equals(other.getSupportAction()))) &&
            ((this.lastModifiedDateTime==null && other.getLastModifiedDateTime()==null) || 
             (this.lastModifiedDateTime!=null &&
              this.lastModifiedDateTime.equals(other.getLastModifiedDateTime()))) &&
            ((this.parentFulfillmentId==null && other.getParentFulfillmentId()==null) || 
             (this.parentFulfillmentId!=null &&
              this.parentFulfillmentId.equals(other.getParentFulfillmentId()))) &&
            ((this.licenseTechnology==null && other.getLicenseTechnology()==null) || 
             (this.licenseTechnology!=null &&
              this.licenseTechnology.equals(other.getLicenseTechnology()))) &&
            ((this.licenseModelAttributes==null && other.getLicenseModelAttributes()==null) || 
             (this.licenseModelAttributes!=null &&
              java.util.Arrays.equals(this.licenseModelAttributes, other.getLicenseModelAttributes()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.fulfillmentSource==null && other.getFulfillmentSource()==null) || 
             (this.fulfillmentSource!=null &&
              this.fulfillmentSource.equals(other.getFulfillmentSource()))) &&
            ((this.licenseFiles==null && other.getLicenseFiles()==null) || 
             (this.licenseFiles!=null &&
              java.util.Arrays.equals(this.licenseFiles, other.getLicenseFiles()))) &&
            ((this.entitledProducts==null && other.getEntitledProducts()==null) || 
             (this.entitledProducts!=null &&
              java.util.Arrays.equals(this.entitledProducts, other.getEntitledProducts()))) &&
            ((this.activationType==null && other.getActivationType()==null) || 
             (this.activationType!=null &&
              this.activationType.equals(other.getActivationType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEntitlementIdentifier() != null) {
            _hashCode += getEntitlementIdentifier().hashCode();
        }
        if (getFulfillmentIdentifier() != null) {
            _hashCode += getFulfillmentIdentifier().hashCode();
        }
        if (getFulfillmentType() != null) {
            _hashCode += getFulfillmentType().hashCode();
        }
        if (getLineItem() != null) {
            _hashCode += getLineItem().hashCode();
        }
        if (getProduct() != null) {
            _hashCode += getProduct().hashCode();
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getShipToEmail() != null) {
            _hashCode += getShipToEmail().hashCode();
        }
        if (getShipToAddress() != null) {
            _hashCode += getShipToAddress().hashCode();
        }
        if (getServerIds() != null) {
            _hashCode += getServerIds().hashCode();
        }
        if (getNodeIds() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNodeIds());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNodeIds(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHost() != null) {
            _hashCode += getCustomHost().hashCode();
        }
        if (getFulfilledCount() != null) {
            _hashCode += getFulfilledCount().hashCode();
        }
        if (getOverDraftCount() != null) {
            _hashCode += getOverDraftCount().hashCode();
        }
        if (getFulfillDate() != null) {
            _hashCode += getFulfillDate().hashCode();
        }
        if (getFulfillDateTime() != null) {
            _hashCode += getFulfillDateTime().hashCode();
        }
        _hashCode += (isIsPermanent() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getLicenseText() != null) {
            _hashCode += getLicenseText().hashCode();
        }
        if (getBinaryLicense() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getBinaryLicense());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getBinaryLicense(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getConsolidatedHostLicense() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsolidatedHostLicense());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsolidatedHostLicense(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSupportAction() != null) {
            _hashCode += getSupportAction().hashCode();
        }
        if (getLastModifiedDateTime() != null) {
            _hashCode += getLastModifiedDateTime().hashCode();
        }
        if (getParentFulfillmentId() != null) {
            _hashCode += getParentFulfillmentId().hashCode();
        }
        if (getLicenseTechnology() != null) {
            _hashCode += getLicenseTechnology().hashCode();
        }
        if (getLicenseModelAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseModelAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseModelAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getFulfillmentSource() != null) {
            _hashCode += getFulfillmentSource().hashCode();
        }
        if (getLicenseFiles() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getLicenseFiles());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getLicenseFiles(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getEntitledProducts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEntitledProducts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEntitledProducts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getActivationType() != null) {
            _hashCode += getActivationType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FulfillmentDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitlementIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineItem");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lineItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementLineItemIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "product"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "productIdentifierType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serverIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "serverIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ServerIDsType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nodeIds");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeIds"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "nodeId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfilledCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfilledCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPermanent");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "isPermanent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "date"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseText");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binaryLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "binaryLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consolidatedHostLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedHostLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "license"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supportAction");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "supportAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SupportLicenseType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastModifiedDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lastModifiedDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentFulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "parentFulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseTechnology");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnology"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnologyIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseModelAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModelAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentSource");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FulfillmentSourceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFiles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFiles"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFile"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitledProducts");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProducts"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProductDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitledProduct"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ActivationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
