/**
 * FulfillmentResponseConfigRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class FulfillmentResponseConfigRequestType  implements java.io.Serializable {
    private java.lang.Boolean fulfillmentId;

    private java.lang.Boolean fulfillmentType;

    private java.lang.Boolean state;

    private java.lang.Boolean entitlementId;

    private java.lang.Boolean lineitemId;

    private java.lang.Boolean product;

    private java.lang.Boolean productDescription;

    private java.lang.Boolean partNumber;

    private java.lang.Boolean partNumberDescription;

    private java.lang.Boolean licenseTechnology;

    private java.lang.Boolean licenseModel;

    private java.lang.Boolean soldTo;

    private java.lang.Boolean soldToDisplayName;

    private java.lang.Boolean shipToEmail;

    private java.lang.Boolean shipToAddress;

    private java.lang.Boolean licenseHost;

    private java.lang.Boolean fulfilledCount;

    private java.lang.Boolean overDraftCount;

    private java.lang.Boolean fulfillDate;

    private java.lang.Boolean fulfillDateTime;

    private java.lang.Boolean isPermanent;

    private java.lang.Boolean startDate;

    private java.lang.Boolean expirationDate;

    private java.lang.Boolean versionDate;

    private java.lang.Boolean licenseFileType;

    private java.lang.Boolean licenseText;

    private java.lang.Boolean binaryLicense;

    private java.lang.Boolean supportAction;

    private java.lang.Boolean lastModifiedDateTime;

    private java.lang.Boolean parentFulfillmentId;

    private java.lang.Boolean fulfillmentSource;

    private java.lang.Boolean orderId;

    private java.lang.Boolean orderLineNumber;

    private java.lang.Boolean lineitemDescription;

    private java.lang.Boolean totalCopies;

    private java.lang.Boolean numberOfRemainingCopies;

    private java.lang.Boolean isTrusted;

    private flexnet.macrovision.com.CustomAttributeDescriptorType[] customAttributes;

    private flexnet.macrovision.com.CustomAttributeDescriptorType[] customHostAttributes;

    private java.lang.Boolean migrationId;

    private java.lang.Boolean vendorDaemonName;

    private java.lang.Boolean licenseFiles;

    private java.lang.Boolean FNPTimeZoneValue;

    private java.lang.Boolean activationType;

    public FulfillmentResponseConfigRequestType() {
    }

    public FulfillmentResponseConfigRequestType(
           java.lang.Boolean fulfillmentId,
           java.lang.Boolean fulfillmentType,
           java.lang.Boolean state,
           java.lang.Boolean entitlementId,
           java.lang.Boolean lineitemId,
           java.lang.Boolean product,
           java.lang.Boolean productDescription,
           java.lang.Boolean partNumber,
           java.lang.Boolean partNumberDescription,
           java.lang.Boolean licenseTechnology,
           java.lang.Boolean licenseModel,
           java.lang.Boolean soldTo,
           java.lang.Boolean soldToDisplayName,
           java.lang.Boolean shipToEmail,
           java.lang.Boolean shipToAddress,
           java.lang.Boolean licenseHost,
           java.lang.Boolean fulfilledCount,
           java.lang.Boolean overDraftCount,
           java.lang.Boolean fulfillDate,
           java.lang.Boolean fulfillDateTime,
           java.lang.Boolean isPermanent,
           java.lang.Boolean startDate,
           java.lang.Boolean expirationDate,
           java.lang.Boolean versionDate,
           java.lang.Boolean licenseFileType,
           java.lang.Boolean licenseText,
           java.lang.Boolean binaryLicense,
           java.lang.Boolean supportAction,
           java.lang.Boolean lastModifiedDateTime,
           java.lang.Boolean parentFulfillmentId,
           java.lang.Boolean fulfillmentSource,
           java.lang.Boolean orderId,
           java.lang.Boolean orderLineNumber,
           java.lang.Boolean lineitemDescription,
           java.lang.Boolean totalCopies,
           java.lang.Boolean numberOfRemainingCopies,
           java.lang.Boolean isTrusted,
           flexnet.macrovision.com.CustomAttributeDescriptorType[] customAttributes,
           flexnet.macrovision.com.CustomAttributeDescriptorType[] customHostAttributes,
           java.lang.Boolean migrationId,
           java.lang.Boolean vendorDaemonName,
           java.lang.Boolean licenseFiles,
           java.lang.Boolean FNPTimeZoneValue,
           java.lang.Boolean activationType) {
           this.fulfillmentId = fulfillmentId;
           this.fulfillmentType = fulfillmentType;
           this.state = state;
           this.entitlementId = entitlementId;
           this.lineitemId = lineitemId;
           this.product = product;
           this.productDescription = productDescription;
           this.partNumber = partNumber;
           this.partNumberDescription = partNumberDescription;
           this.licenseTechnology = licenseTechnology;
           this.licenseModel = licenseModel;
           this.soldTo = soldTo;
           this.soldToDisplayName = soldToDisplayName;
           this.shipToEmail = shipToEmail;
           this.shipToAddress = shipToAddress;
           this.licenseHost = licenseHost;
           this.fulfilledCount = fulfilledCount;
           this.overDraftCount = overDraftCount;
           this.fulfillDate = fulfillDate;
           this.fulfillDateTime = fulfillDateTime;
           this.isPermanent = isPermanent;
           this.startDate = startDate;
           this.expirationDate = expirationDate;
           this.versionDate = versionDate;
           this.licenseFileType = licenseFileType;
           this.licenseText = licenseText;
           this.binaryLicense = binaryLicense;
           this.supportAction = supportAction;
           this.lastModifiedDateTime = lastModifiedDateTime;
           this.parentFulfillmentId = parentFulfillmentId;
           this.fulfillmentSource = fulfillmentSource;
           this.orderId = orderId;
           this.orderLineNumber = orderLineNumber;
           this.lineitemDescription = lineitemDescription;
           this.totalCopies = totalCopies;
           this.numberOfRemainingCopies = numberOfRemainingCopies;
           this.isTrusted = isTrusted;
           this.customAttributes = customAttributes;
           this.customHostAttributes = customHostAttributes;
           this.migrationId = migrationId;
           this.vendorDaemonName = vendorDaemonName;
           this.licenseFiles = licenseFiles;
           this.FNPTimeZoneValue = FNPTimeZoneValue;
           this.activationType = activationType;
    }


    /**
     * Gets the fulfillmentId value for this FulfillmentResponseConfigRequestType.
     * 
     * @return fulfillmentId
     */
    public java.lang.Boolean getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this FulfillmentResponseConfigRequestType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(java.lang.Boolean fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the fulfillmentType value for this FulfillmentResponseConfigRequestType.
     * 
     * @return fulfillmentType
     */
    public java.lang.Boolean getFulfillmentType() {
        return fulfillmentType;
    }


    /**
     * Sets the fulfillmentType value for this FulfillmentResponseConfigRequestType.
     * 
     * @param fulfillmentType
     */
    public void setFulfillmentType(java.lang.Boolean fulfillmentType) {
        this.fulfillmentType = fulfillmentType;
    }


    /**
     * Gets the state value for this FulfillmentResponseConfigRequestType.
     * 
     * @return state
     */
    public java.lang.Boolean getState() {
        return state;
    }


    /**
     * Sets the state value for this FulfillmentResponseConfigRequestType.
     * 
     * @param state
     */
    public void setState(java.lang.Boolean state) {
        this.state = state;
    }


    /**
     * Gets the entitlementId value for this FulfillmentResponseConfigRequestType.
     * 
     * @return entitlementId
     */
    public java.lang.Boolean getEntitlementId() {
        return entitlementId;
    }


    /**
     * Sets the entitlementId value for this FulfillmentResponseConfigRequestType.
     * 
     * @param entitlementId
     */
    public void setEntitlementId(java.lang.Boolean entitlementId) {
        this.entitlementId = entitlementId;
    }


    /**
     * Gets the lineitemId value for this FulfillmentResponseConfigRequestType.
     * 
     * @return lineitemId
     */
    public java.lang.Boolean getLineitemId() {
        return lineitemId;
    }


    /**
     * Sets the lineitemId value for this FulfillmentResponseConfigRequestType.
     * 
     * @param lineitemId
     */
    public void setLineitemId(java.lang.Boolean lineitemId) {
        this.lineitemId = lineitemId;
    }


    /**
     * Gets the product value for this FulfillmentResponseConfigRequestType.
     * 
     * @return product
     */
    public java.lang.Boolean getProduct() {
        return product;
    }


    /**
     * Sets the product value for this FulfillmentResponseConfigRequestType.
     * 
     * @param product
     */
    public void setProduct(java.lang.Boolean product) {
        this.product = product;
    }


    /**
     * Gets the productDescription value for this FulfillmentResponseConfigRequestType.
     * 
     * @return productDescription
     */
    public java.lang.Boolean getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this FulfillmentResponseConfigRequestType.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.Boolean productDescription) {
        this.productDescription = productDescription;
    }


    /**
     * Gets the partNumber value for this FulfillmentResponseConfigRequestType.
     * 
     * @return partNumber
     */
    public java.lang.Boolean getPartNumber() {
        return partNumber;
    }


    /**
     * Sets the partNumber value for this FulfillmentResponseConfigRequestType.
     * 
     * @param partNumber
     */
    public void setPartNumber(java.lang.Boolean partNumber) {
        this.partNumber = partNumber;
    }


    /**
     * Gets the partNumberDescription value for this FulfillmentResponseConfigRequestType.
     * 
     * @return partNumberDescription
     */
    public java.lang.Boolean getPartNumberDescription() {
        return partNumberDescription;
    }


    /**
     * Sets the partNumberDescription value for this FulfillmentResponseConfigRequestType.
     * 
     * @param partNumberDescription
     */
    public void setPartNumberDescription(java.lang.Boolean partNumberDescription) {
        this.partNumberDescription = partNumberDescription;
    }


    /**
     * Gets the licenseTechnology value for this FulfillmentResponseConfigRequestType.
     * 
     * @return licenseTechnology
     */
    public java.lang.Boolean getLicenseTechnology() {
        return licenseTechnology;
    }


    /**
     * Sets the licenseTechnology value for this FulfillmentResponseConfigRequestType.
     * 
     * @param licenseTechnology
     */
    public void setLicenseTechnology(java.lang.Boolean licenseTechnology) {
        this.licenseTechnology = licenseTechnology;
    }


    /**
     * Gets the licenseModel value for this FulfillmentResponseConfigRequestType.
     * 
     * @return licenseModel
     */
    public java.lang.Boolean getLicenseModel() {
        return licenseModel;
    }


    /**
     * Sets the licenseModel value for this FulfillmentResponseConfigRequestType.
     * 
     * @param licenseModel
     */
    public void setLicenseModel(java.lang.Boolean licenseModel) {
        this.licenseModel = licenseModel;
    }


    /**
     * Gets the soldTo value for this FulfillmentResponseConfigRequestType.
     * 
     * @return soldTo
     */
    public java.lang.Boolean getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this FulfillmentResponseConfigRequestType.
     * 
     * @param soldTo
     */
    public void setSoldTo(java.lang.Boolean soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the soldToDisplayName value for this FulfillmentResponseConfigRequestType.
     * 
     * @return soldToDisplayName
     */
    public java.lang.Boolean getSoldToDisplayName() {
        return soldToDisplayName;
    }


    /**
     * Sets the soldToDisplayName value for this FulfillmentResponseConfigRequestType.
     * 
     * @param soldToDisplayName
     */
    public void setSoldToDisplayName(java.lang.Boolean soldToDisplayName) {
        this.soldToDisplayName = soldToDisplayName;
    }


    /**
     * Gets the shipToEmail value for this FulfillmentResponseConfigRequestType.
     * 
     * @return shipToEmail
     */
    public java.lang.Boolean getShipToEmail() {
        return shipToEmail;
    }


    /**
     * Sets the shipToEmail value for this FulfillmentResponseConfigRequestType.
     * 
     * @param shipToEmail
     */
    public void setShipToEmail(java.lang.Boolean shipToEmail) {
        this.shipToEmail = shipToEmail;
    }


    /**
     * Gets the shipToAddress value for this FulfillmentResponseConfigRequestType.
     * 
     * @return shipToAddress
     */
    public java.lang.Boolean getShipToAddress() {
        return shipToAddress;
    }


    /**
     * Sets the shipToAddress value for this FulfillmentResponseConfigRequestType.
     * 
     * @param shipToAddress
     */
    public void setShipToAddress(java.lang.Boolean shipToAddress) {
        this.shipToAddress = shipToAddress;
    }


    /**
     * Gets the licenseHost value for this FulfillmentResponseConfigRequestType.
     * 
     * @return licenseHost
     */
    public java.lang.Boolean getLicenseHost() {
        return licenseHost;
    }


    /**
     * Sets the licenseHost value for this FulfillmentResponseConfigRequestType.
     * 
     * @param licenseHost
     */
    public void setLicenseHost(java.lang.Boolean licenseHost) {
        this.licenseHost = licenseHost;
    }


    /**
     * Gets the fulfilledCount value for this FulfillmentResponseConfigRequestType.
     * 
     * @return fulfilledCount
     */
    public java.lang.Boolean getFulfilledCount() {
        return fulfilledCount;
    }


    /**
     * Sets the fulfilledCount value for this FulfillmentResponseConfigRequestType.
     * 
     * @param fulfilledCount
     */
    public void setFulfilledCount(java.lang.Boolean fulfilledCount) {
        this.fulfilledCount = fulfilledCount;
    }


    /**
     * Gets the overDraftCount value for this FulfillmentResponseConfigRequestType.
     * 
     * @return overDraftCount
     */
    public java.lang.Boolean getOverDraftCount() {
        return overDraftCount;
    }


    /**
     * Sets the overDraftCount value for this FulfillmentResponseConfigRequestType.
     * 
     * @param overDraftCount
     */
    public void setOverDraftCount(java.lang.Boolean overDraftCount) {
        this.overDraftCount = overDraftCount;
    }


    /**
     * Gets the fulfillDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @return fulfillDate
     */
    public java.lang.Boolean getFulfillDate() {
        return fulfillDate;
    }


    /**
     * Sets the fulfillDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @param fulfillDate
     */
    public void setFulfillDate(java.lang.Boolean fulfillDate) {
        this.fulfillDate = fulfillDate;
    }


    /**
     * Gets the fulfillDateTime value for this FulfillmentResponseConfigRequestType.
     * 
     * @return fulfillDateTime
     */
    public java.lang.Boolean getFulfillDateTime() {
        return fulfillDateTime;
    }


    /**
     * Sets the fulfillDateTime value for this FulfillmentResponseConfigRequestType.
     * 
     * @param fulfillDateTime
     */
    public void setFulfillDateTime(java.lang.Boolean fulfillDateTime) {
        this.fulfillDateTime = fulfillDateTime;
    }


    /**
     * Gets the isPermanent value for this FulfillmentResponseConfigRequestType.
     * 
     * @return isPermanent
     */
    public java.lang.Boolean getIsPermanent() {
        return isPermanent;
    }


    /**
     * Sets the isPermanent value for this FulfillmentResponseConfigRequestType.
     * 
     * @param isPermanent
     */
    public void setIsPermanent(java.lang.Boolean isPermanent) {
        this.isPermanent = isPermanent;
    }


    /**
     * Gets the startDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @return startDate
     */
    public java.lang.Boolean getStartDate() {
        return startDate;
    }


    /**
     * Sets the startDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @param startDate
     */
    public void setStartDate(java.lang.Boolean startDate) {
        this.startDate = startDate;
    }


    /**
     * Gets the expirationDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @return expirationDate
     */
    public java.lang.Boolean getExpirationDate() {
        return expirationDate;
    }


    /**
     * Sets the expirationDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @param expirationDate
     */
    public void setExpirationDate(java.lang.Boolean expirationDate) {
        this.expirationDate = expirationDate;
    }


    /**
     * Gets the versionDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @return versionDate
     */
    public java.lang.Boolean getVersionDate() {
        return versionDate;
    }


    /**
     * Sets the versionDate value for this FulfillmentResponseConfigRequestType.
     * 
     * @param versionDate
     */
    public void setVersionDate(java.lang.Boolean versionDate) {
        this.versionDate = versionDate;
    }


    /**
     * Gets the licenseFileType value for this FulfillmentResponseConfigRequestType.
     * 
     * @return licenseFileType
     */
    public java.lang.Boolean getLicenseFileType() {
        return licenseFileType;
    }


    /**
     * Sets the licenseFileType value for this FulfillmentResponseConfigRequestType.
     * 
     * @param licenseFileType
     */
    public void setLicenseFileType(java.lang.Boolean licenseFileType) {
        this.licenseFileType = licenseFileType;
    }


    /**
     * Gets the licenseText value for this FulfillmentResponseConfigRequestType.
     * 
     * @return licenseText
     */
    public java.lang.Boolean getLicenseText() {
        return licenseText;
    }


    /**
     * Sets the licenseText value for this FulfillmentResponseConfigRequestType.
     * 
     * @param licenseText
     */
    public void setLicenseText(java.lang.Boolean licenseText) {
        this.licenseText = licenseText;
    }


    /**
     * Gets the binaryLicense value for this FulfillmentResponseConfigRequestType.
     * 
     * @return binaryLicense
     */
    public java.lang.Boolean getBinaryLicense() {
        return binaryLicense;
    }


    /**
     * Sets the binaryLicense value for this FulfillmentResponseConfigRequestType.
     * 
     * @param binaryLicense
     */
    public void setBinaryLicense(java.lang.Boolean binaryLicense) {
        this.binaryLicense = binaryLicense;
    }


    /**
     * Gets the supportAction value for this FulfillmentResponseConfigRequestType.
     * 
     * @return supportAction
     */
    public java.lang.Boolean getSupportAction() {
        return supportAction;
    }


    /**
     * Sets the supportAction value for this FulfillmentResponseConfigRequestType.
     * 
     * @param supportAction
     */
    public void setSupportAction(java.lang.Boolean supportAction) {
        this.supportAction = supportAction;
    }


    /**
     * Gets the lastModifiedDateTime value for this FulfillmentResponseConfigRequestType.
     * 
     * @return lastModifiedDateTime
     */
    public java.lang.Boolean getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }


    /**
     * Sets the lastModifiedDateTime value for this FulfillmentResponseConfigRequestType.
     * 
     * @param lastModifiedDateTime
     */
    public void setLastModifiedDateTime(java.lang.Boolean lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }


    /**
     * Gets the parentFulfillmentId value for this FulfillmentResponseConfigRequestType.
     * 
     * @return parentFulfillmentId
     */
    public java.lang.Boolean getParentFulfillmentId() {
        return parentFulfillmentId;
    }


    /**
     * Sets the parentFulfillmentId value for this FulfillmentResponseConfigRequestType.
     * 
     * @param parentFulfillmentId
     */
    public void setParentFulfillmentId(java.lang.Boolean parentFulfillmentId) {
        this.parentFulfillmentId = parentFulfillmentId;
    }


    /**
     * Gets the fulfillmentSource value for this FulfillmentResponseConfigRequestType.
     * 
     * @return fulfillmentSource
     */
    public java.lang.Boolean getFulfillmentSource() {
        return fulfillmentSource;
    }


    /**
     * Sets the fulfillmentSource value for this FulfillmentResponseConfigRequestType.
     * 
     * @param fulfillmentSource
     */
    public void setFulfillmentSource(java.lang.Boolean fulfillmentSource) {
        this.fulfillmentSource = fulfillmentSource;
    }


    /**
     * Gets the orderId value for this FulfillmentResponseConfigRequestType.
     * 
     * @return orderId
     */
    public java.lang.Boolean getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this FulfillmentResponseConfigRequestType.
     * 
     * @param orderId
     */
    public void setOrderId(java.lang.Boolean orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the orderLineNumber value for this FulfillmentResponseConfigRequestType.
     * 
     * @return orderLineNumber
     */
    public java.lang.Boolean getOrderLineNumber() {
        return orderLineNumber;
    }


    /**
     * Sets the orderLineNumber value for this FulfillmentResponseConfigRequestType.
     * 
     * @param orderLineNumber
     */
    public void setOrderLineNumber(java.lang.Boolean orderLineNumber) {
        this.orderLineNumber = orderLineNumber;
    }


    /**
     * Gets the lineitemDescription value for this FulfillmentResponseConfigRequestType.
     * 
     * @return lineitemDescription
     */
    public java.lang.Boolean getLineitemDescription() {
        return lineitemDescription;
    }


    /**
     * Sets the lineitemDescription value for this FulfillmentResponseConfigRequestType.
     * 
     * @param lineitemDescription
     */
    public void setLineitemDescription(java.lang.Boolean lineitemDescription) {
        this.lineitemDescription = lineitemDescription;
    }


    /**
     * Gets the totalCopies value for this FulfillmentResponseConfigRequestType.
     * 
     * @return totalCopies
     */
    public java.lang.Boolean getTotalCopies() {
        return totalCopies;
    }


    /**
     * Sets the totalCopies value for this FulfillmentResponseConfigRequestType.
     * 
     * @param totalCopies
     */
    public void setTotalCopies(java.lang.Boolean totalCopies) {
        this.totalCopies = totalCopies;
    }


    /**
     * Gets the numberOfRemainingCopies value for this FulfillmentResponseConfigRequestType.
     * 
     * @return numberOfRemainingCopies
     */
    public java.lang.Boolean getNumberOfRemainingCopies() {
        return numberOfRemainingCopies;
    }


    /**
     * Sets the numberOfRemainingCopies value for this FulfillmentResponseConfigRequestType.
     * 
     * @param numberOfRemainingCopies
     */
    public void setNumberOfRemainingCopies(java.lang.Boolean numberOfRemainingCopies) {
        this.numberOfRemainingCopies = numberOfRemainingCopies;
    }


    /**
     * Gets the isTrusted value for this FulfillmentResponseConfigRequestType.
     * 
     * @return isTrusted
     */
    public java.lang.Boolean getIsTrusted() {
        return isTrusted;
    }


    /**
     * Sets the isTrusted value for this FulfillmentResponseConfigRequestType.
     * 
     * @param isTrusted
     */
    public void setIsTrusted(java.lang.Boolean isTrusted) {
        this.isTrusted = isTrusted;
    }


    /**
     * Gets the customAttributes value for this FulfillmentResponseConfigRequestType.
     * 
     * @return customAttributes
     */
    public flexnet.macrovision.com.CustomAttributeDescriptorType[] getCustomAttributes() {
        return customAttributes;
    }


    /**
     * Sets the customAttributes value for this FulfillmentResponseConfigRequestType.
     * 
     * @param customAttributes
     */
    public void setCustomAttributes(flexnet.macrovision.com.CustomAttributeDescriptorType[] customAttributes) {
        this.customAttributes = customAttributes;
    }


    /**
     * Gets the customHostAttributes value for this FulfillmentResponseConfigRequestType.
     * 
     * @return customHostAttributes
     */
    public flexnet.macrovision.com.CustomAttributeDescriptorType[] getCustomHostAttributes() {
        return customHostAttributes;
    }


    /**
     * Sets the customHostAttributes value for this FulfillmentResponseConfigRequestType.
     * 
     * @param customHostAttributes
     */
    public void setCustomHostAttributes(flexnet.macrovision.com.CustomAttributeDescriptorType[] customHostAttributes) {
        this.customHostAttributes = customHostAttributes;
    }


    /**
     * Gets the migrationId value for this FulfillmentResponseConfigRequestType.
     * 
     * @return migrationId
     */
    public java.lang.Boolean getMigrationId() {
        return migrationId;
    }


    /**
     * Sets the migrationId value for this FulfillmentResponseConfigRequestType.
     * 
     * @param migrationId
     */
    public void setMigrationId(java.lang.Boolean migrationId) {
        this.migrationId = migrationId;
    }


    /**
     * Gets the vendorDaemonName value for this FulfillmentResponseConfigRequestType.
     * 
     * @return vendorDaemonName
     */
    public java.lang.Boolean getVendorDaemonName() {
        return vendorDaemonName;
    }


    /**
     * Sets the vendorDaemonName value for this FulfillmentResponseConfigRequestType.
     * 
     * @param vendorDaemonName
     */
    public void setVendorDaemonName(java.lang.Boolean vendorDaemonName) {
        this.vendorDaemonName = vendorDaemonName;
    }


    /**
     * Gets the licenseFiles value for this FulfillmentResponseConfigRequestType.
     * 
     * @return licenseFiles
     */
    public java.lang.Boolean getLicenseFiles() {
        return licenseFiles;
    }


    /**
     * Sets the licenseFiles value for this FulfillmentResponseConfigRequestType.
     * 
     * @param licenseFiles
     */
    public void setLicenseFiles(java.lang.Boolean licenseFiles) {
        this.licenseFiles = licenseFiles;
    }


    /**
     * Gets the FNPTimeZoneValue value for this FulfillmentResponseConfigRequestType.
     * 
     * @return FNPTimeZoneValue
     */
    public java.lang.Boolean getFNPTimeZoneValue() {
        return FNPTimeZoneValue;
    }


    /**
     * Sets the FNPTimeZoneValue value for this FulfillmentResponseConfigRequestType.
     * 
     * @param FNPTimeZoneValue
     */
    public void setFNPTimeZoneValue(java.lang.Boolean FNPTimeZoneValue) {
        this.FNPTimeZoneValue = FNPTimeZoneValue;
    }


    /**
     * Gets the activationType value for this FulfillmentResponseConfigRequestType.
     * 
     * @return activationType
     */
    public java.lang.Boolean getActivationType() {
        return activationType;
    }


    /**
     * Sets the activationType value for this FulfillmentResponseConfigRequestType.
     * 
     * @param activationType
     */
    public void setActivationType(java.lang.Boolean activationType) {
        this.activationType = activationType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof FulfillmentResponseConfigRequestType)) return false;
        FulfillmentResponseConfigRequestType other = (FulfillmentResponseConfigRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.fulfillmentType==null && other.getFulfillmentType()==null) || 
             (this.fulfillmentType!=null &&
              this.fulfillmentType.equals(other.getFulfillmentType()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.entitlementId==null && other.getEntitlementId()==null) || 
             (this.entitlementId!=null &&
              this.entitlementId.equals(other.getEntitlementId()))) &&
            ((this.lineitemId==null && other.getLineitemId()==null) || 
             (this.lineitemId!=null &&
              this.lineitemId.equals(other.getLineitemId()))) &&
            ((this.product==null && other.getProduct()==null) || 
             (this.product!=null &&
              this.product.equals(other.getProduct()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            ((this.partNumber==null && other.getPartNumber()==null) || 
             (this.partNumber!=null &&
              this.partNumber.equals(other.getPartNumber()))) &&
            ((this.partNumberDescription==null && other.getPartNumberDescription()==null) || 
             (this.partNumberDescription!=null &&
              this.partNumberDescription.equals(other.getPartNumberDescription()))) &&
            ((this.licenseTechnology==null && other.getLicenseTechnology()==null) || 
             (this.licenseTechnology!=null &&
              this.licenseTechnology.equals(other.getLicenseTechnology()))) &&
            ((this.licenseModel==null && other.getLicenseModel()==null) || 
             (this.licenseModel!=null &&
              this.licenseModel.equals(other.getLicenseModel()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.soldToDisplayName==null && other.getSoldToDisplayName()==null) || 
             (this.soldToDisplayName!=null &&
              this.soldToDisplayName.equals(other.getSoldToDisplayName()))) &&
            ((this.shipToEmail==null && other.getShipToEmail()==null) || 
             (this.shipToEmail!=null &&
              this.shipToEmail.equals(other.getShipToEmail()))) &&
            ((this.shipToAddress==null && other.getShipToAddress()==null) || 
             (this.shipToAddress!=null &&
              this.shipToAddress.equals(other.getShipToAddress()))) &&
            ((this.licenseHost==null && other.getLicenseHost()==null) || 
             (this.licenseHost!=null &&
              this.licenseHost.equals(other.getLicenseHost()))) &&
            ((this.fulfilledCount==null && other.getFulfilledCount()==null) || 
             (this.fulfilledCount!=null &&
              this.fulfilledCount.equals(other.getFulfilledCount()))) &&
            ((this.overDraftCount==null && other.getOverDraftCount()==null) || 
             (this.overDraftCount!=null &&
              this.overDraftCount.equals(other.getOverDraftCount()))) &&
            ((this.fulfillDate==null && other.getFulfillDate()==null) || 
             (this.fulfillDate!=null &&
              this.fulfillDate.equals(other.getFulfillDate()))) &&
            ((this.fulfillDateTime==null && other.getFulfillDateTime()==null) || 
             (this.fulfillDateTime!=null &&
              this.fulfillDateTime.equals(other.getFulfillDateTime()))) &&
            ((this.isPermanent==null && other.getIsPermanent()==null) || 
             (this.isPermanent!=null &&
              this.isPermanent.equals(other.getIsPermanent()))) &&
            ((this.startDate==null && other.getStartDate()==null) || 
             (this.startDate!=null &&
              this.startDate.equals(other.getStartDate()))) &&
            ((this.expirationDate==null && other.getExpirationDate()==null) || 
             (this.expirationDate!=null &&
              this.expirationDate.equals(other.getExpirationDate()))) &&
            ((this.versionDate==null && other.getVersionDate()==null) || 
             (this.versionDate!=null &&
              this.versionDate.equals(other.getVersionDate()))) &&
            ((this.licenseFileType==null && other.getLicenseFileType()==null) || 
             (this.licenseFileType!=null &&
              this.licenseFileType.equals(other.getLicenseFileType()))) &&
            ((this.licenseText==null && other.getLicenseText()==null) || 
             (this.licenseText!=null &&
              this.licenseText.equals(other.getLicenseText()))) &&
            ((this.binaryLicense==null && other.getBinaryLicense()==null) || 
             (this.binaryLicense!=null &&
              this.binaryLicense.equals(other.getBinaryLicense()))) &&
            ((this.supportAction==null && other.getSupportAction()==null) || 
             (this.supportAction!=null &&
              this.supportAction.equals(other.getSupportAction()))) &&
            ((this.lastModifiedDateTime==null && other.getLastModifiedDateTime()==null) || 
             (this.lastModifiedDateTime!=null &&
              this.lastModifiedDateTime.equals(other.getLastModifiedDateTime()))) &&
            ((this.parentFulfillmentId==null && other.getParentFulfillmentId()==null) || 
             (this.parentFulfillmentId!=null &&
              this.parentFulfillmentId.equals(other.getParentFulfillmentId()))) &&
            ((this.fulfillmentSource==null && other.getFulfillmentSource()==null) || 
             (this.fulfillmentSource!=null &&
              this.fulfillmentSource.equals(other.getFulfillmentSource()))) &&
            ((this.orderId==null && other.getOrderId()==null) || 
             (this.orderId!=null &&
              this.orderId.equals(other.getOrderId()))) &&
            ((this.orderLineNumber==null && other.getOrderLineNumber()==null) || 
             (this.orderLineNumber!=null &&
              this.orderLineNumber.equals(other.getOrderLineNumber()))) &&
            ((this.lineitemDescription==null && other.getLineitemDescription()==null) || 
             (this.lineitemDescription!=null &&
              this.lineitemDescription.equals(other.getLineitemDescription()))) &&
            ((this.totalCopies==null && other.getTotalCopies()==null) || 
             (this.totalCopies!=null &&
              this.totalCopies.equals(other.getTotalCopies()))) &&
            ((this.numberOfRemainingCopies==null && other.getNumberOfRemainingCopies()==null) || 
             (this.numberOfRemainingCopies!=null &&
              this.numberOfRemainingCopies.equals(other.getNumberOfRemainingCopies()))) &&
            ((this.isTrusted==null && other.getIsTrusted()==null) || 
             (this.isTrusted!=null &&
              this.isTrusted.equals(other.getIsTrusted()))) &&
            ((this.customAttributes==null && other.getCustomAttributes()==null) || 
             (this.customAttributes!=null &&
              java.util.Arrays.equals(this.customAttributes, other.getCustomAttributes()))) &&
            ((this.customHostAttributes==null && other.getCustomHostAttributes()==null) || 
             (this.customHostAttributes!=null &&
              java.util.Arrays.equals(this.customHostAttributes, other.getCustomHostAttributes()))) &&
            ((this.migrationId==null && other.getMigrationId()==null) || 
             (this.migrationId!=null &&
              this.migrationId.equals(other.getMigrationId()))) &&
            ((this.vendorDaemonName==null && other.getVendorDaemonName()==null) || 
             (this.vendorDaemonName!=null &&
              this.vendorDaemonName.equals(other.getVendorDaemonName()))) &&
            ((this.licenseFiles==null && other.getLicenseFiles()==null) || 
             (this.licenseFiles!=null &&
              this.licenseFiles.equals(other.getLicenseFiles()))) &&
            ((this.FNPTimeZoneValue==null && other.getFNPTimeZoneValue()==null) || 
             (this.FNPTimeZoneValue!=null &&
              this.FNPTimeZoneValue.equals(other.getFNPTimeZoneValue()))) &&
            ((this.activationType==null && other.getActivationType()==null) || 
             (this.activationType!=null &&
              this.activationType.equals(other.getActivationType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getFulfillmentType() != null) {
            _hashCode += getFulfillmentType().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getEntitlementId() != null) {
            _hashCode += getEntitlementId().hashCode();
        }
        if (getLineitemId() != null) {
            _hashCode += getLineitemId().hashCode();
        }
        if (getProduct() != null) {
            _hashCode += getProduct().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getPartNumber() != null) {
            _hashCode += getPartNumber().hashCode();
        }
        if (getPartNumberDescription() != null) {
            _hashCode += getPartNumberDescription().hashCode();
        }
        if (getLicenseTechnology() != null) {
            _hashCode += getLicenseTechnology().hashCode();
        }
        if (getLicenseModel() != null) {
            _hashCode += getLicenseModel().hashCode();
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getSoldToDisplayName() != null) {
            _hashCode += getSoldToDisplayName().hashCode();
        }
        if (getShipToEmail() != null) {
            _hashCode += getShipToEmail().hashCode();
        }
        if (getShipToAddress() != null) {
            _hashCode += getShipToAddress().hashCode();
        }
        if (getLicenseHost() != null) {
            _hashCode += getLicenseHost().hashCode();
        }
        if (getFulfilledCount() != null) {
            _hashCode += getFulfilledCount().hashCode();
        }
        if (getOverDraftCount() != null) {
            _hashCode += getOverDraftCount().hashCode();
        }
        if (getFulfillDate() != null) {
            _hashCode += getFulfillDate().hashCode();
        }
        if (getFulfillDateTime() != null) {
            _hashCode += getFulfillDateTime().hashCode();
        }
        if (getIsPermanent() != null) {
            _hashCode += getIsPermanent().hashCode();
        }
        if (getStartDate() != null) {
            _hashCode += getStartDate().hashCode();
        }
        if (getExpirationDate() != null) {
            _hashCode += getExpirationDate().hashCode();
        }
        if (getVersionDate() != null) {
            _hashCode += getVersionDate().hashCode();
        }
        if (getLicenseFileType() != null) {
            _hashCode += getLicenseFileType().hashCode();
        }
        if (getLicenseText() != null) {
            _hashCode += getLicenseText().hashCode();
        }
        if (getBinaryLicense() != null) {
            _hashCode += getBinaryLicense().hashCode();
        }
        if (getSupportAction() != null) {
            _hashCode += getSupportAction().hashCode();
        }
        if (getLastModifiedDateTime() != null) {
            _hashCode += getLastModifiedDateTime().hashCode();
        }
        if (getParentFulfillmentId() != null) {
            _hashCode += getParentFulfillmentId().hashCode();
        }
        if (getFulfillmentSource() != null) {
            _hashCode += getFulfillmentSource().hashCode();
        }
        if (getOrderId() != null) {
            _hashCode += getOrderId().hashCode();
        }
        if (getOrderLineNumber() != null) {
            _hashCode += getOrderLineNumber().hashCode();
        }
        if (getLineitemDescription() != null) {
            _hashCode += getLineitemDescription().hashCode();
        }
        if (getTotalCopies() != null) {
            _hashCode += getTotalCopies().hashCode();
        }
        if (getNumberOfRemainingCopies() != null) {
            _hashCode += getNumberOfRemainingCopies().hashCode();
        }
        if (getIsTrusted() != null) {
            _hashCode += getIsTrusted().hashCode();
        }
        if (getCustomAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getCustomHostAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomHostAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomHostAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMigrationId() != null) {
            _hashCode += getMigrationId().hashCode();
        }
        if (getVendorDaemonName() != null) {
            _hashCode += getVendorDaemonName().hashCode();
        }
        if (getLicenseFiles() != null) {
            _hashCode += getLicenseFiles().hashCode();
        }
        if (getFNPTimeZoneValue() != null) {
            _hashCode += getFNPTimeZoneValue().hashCode();
        }
        if (getActivationType() != null) {
            _hashCode += getActivationType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(FulfillmentResponseConfigRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentResponseConfigRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitlementId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineitemId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lineitemId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("product");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "product"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "productDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partNumberDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "partNumberDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseTechnology");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseTechnology"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseModel");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseModel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldToDisplayName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldToDisplayName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToEmail");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToEmail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shipToAddress");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shipToAddress"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseHost");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfilledCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfilledCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overDraftCount");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overDraftCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPermanent");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "isPermanent"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "startDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("expirationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "expirationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("versionDate");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "versionDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFileType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFileType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseText");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("binaryLicense");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "binaryLicense"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("supportAction");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "supportAction"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastModifiedDateTime");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lastModifiedDateTime"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parentFulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "parentFulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentSource");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentSource"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "orderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderLineNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "orderLineNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lineitemDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "lineitemDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalCopies");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "totalCopies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfRemainingCopies");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "numberOfRemainingCopies"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isTrusted");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "isTrusted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customHostAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customHostAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "customAttributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("migrationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "migrationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vendorDaemonName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "vendorDaemonName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("licenseFiles");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "licenseFiles"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("FNPTimeZoneValue");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "FNPTimeZoneValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
