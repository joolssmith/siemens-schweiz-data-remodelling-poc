/**
 * CustomHostIDType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class CustomHostIDType  implements java.io.Serializable {
    private java.lang.String hostId;

    private flexnet.macrovision.com.AttributeDescriptorType[] hostAttributes;

    private flexnet.macrovision.com.HostTypePKType hostType;

    public CustomHostIDType() {
    }

    public CustomHostIDType(
           java.lang.String hostId,
           flexnet.macrovision.com.AttributeDescriptorType[] hostAttributes,
           flexnet.macrovision.com.HostTypePKType hostType) {
           this.hostId = hostId;
           this.hostAttributes = hostAttributes;
           this.hostType = hostType;
    }


    /**
     * Gets the hostId value for this CustomHostIDType.
     * 
     * @return hostId
     */
    public java.lang.String getHostId() {
        return hostId;
    }


    /**
     * Sets the hostId value for this CustomHostIDType.
     * 
     * @param hostId
     */
    public void setHostId(java.lang.String hostId) {
        this.hostId = hostId;
    }


    /**
     * Gets the hostAttributes value for this CustomHostIDType.
     * 
     * @return hostAttributes
     */
    public flexnet.macrovision.com.AttributeDescriptorType[] getHostAttributes() {
        return hostAttributes;
    }


    /**
     * Sets the hostAttributes value for this CustomHostIDType.
     * 
     * @param hostAttributes
     */
    public void setHostAttributes(flexnet.macrovision.com.AttributeDescriptorType[] hostAttributes) {
        this.hostAttributes = hostAttributes;
    }


    /**
     * Gets the hostType value for this CustomHostIDType.
     * 
     * @return hostType
     */
    public flexnet.macrovision.com.HostTypePKType getHostType() {
        return hostType;
    }


    /**
     * Sets the hostType value for this CustomHostIDType.
     * 
     * @param hostType
     */
    public void setHostType(flexnet.macrovision.com.HostTypePKType hostType) {
        this.hostType = hostType;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CustomHostIDType)) return false;
        CustomHostIDType other = (CustomHostIDType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.hostId==null && other.getHostId()==null) || 
             (this.hostId!=null &&
              this.hostId.equals(other.getHostId()))) &&
            ((this.hostAttributes==null && other.getHostAttributes()==null) || 
             (this.hostAttributes!=null &&
              java.util.Arrays.equals(this.hostAttributes, other.getHostAttributes()))) &&
            ((this.hostType==null && other.getHostType()==null) || 
             (this.hostType!=null &&
              this.hostType.equals(other.getHostType())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHostId() != null) {
            _hashCode += getHostId().hashCode();
        }
        if (getHostAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getHostAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getHostAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getHostType() != null) {
            _hashCode += getHostType().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CustomHostIDType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "CustomHostIDType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attributeDescriptorType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hostType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostTypePKType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
