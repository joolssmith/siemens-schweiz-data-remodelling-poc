/**
 * ConsolidatedFulfillmentsQPType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ConsolidatedFulfillmentsQPType  implements java.io.Serializable {
    private flexnet.macrovision.com.SimpleQueryType consolidatedLicenseId;

    private flexnet.macrovision.com.SimpleQueryType fulfillmentId;

    private flexnet.macrovision.com.SimpleQueryType activationId;

    private flexnet.macrovision.com.SimpleQueryType entitlementId;

    private flexnet.macrovision.com.SimpleQueryType soldTo;

    private flexnet.macrovision.com.SimpleQueryType criteria;

    private flexnet.macrovision.com.StateQueryType state;

    public ConsolidatedFulfillmentsQPType() {
    }

    public ConsolidatedFulfillmentsQPType(
           flexnet.macrovision.com.SimpleQueryType consolidatedLicenseId,
           flexnet.macrovision.com.SimpleQueryType fulfillmentId,
           flexnet.macrovision.com.SimpleQueryType activationId,
           flexnet.macrovision.com.SimpleQueryType entitlementId,
           flexnet.macrovision.com.SimpleQueryType soldTo,
           flexnet.macrovision.com.SimpleQueryType criteria,
           flexnet.macrovision.com.StateQueryType state) {
           this.consolidatedLicenseId = consolidatedLicenseId;
           this.fulfillmentId = fulfillmentId;
           this.activationId = activationId;
           this.entitlementId = entitlementId;
           this.soldTo = soldTo;
           this.criteria = criteria;
           this.state = state;
    }


    /**
     * Gets the consolidatedLicenseId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return consolidatedLicenseId
     */
    public flexnet.macrovision.com.SimpleQueryType getConsolidatedLicenseId() {
        return consolidatedLicenseId;
    }


    /**
     * Sets the consolidatedLicenseId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param consolidatedLicenseId
     */
    public void setConsolidatedLicenseId(flexnet.macrovision.com.SimpleQueryType consolidatedLicenseId) {
        this.consolidatedLicenseId = consolidatedLicenseId;
    }


    /**
     * Gets the fulfillmentId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return fulfillmentId
     */
    public flexnet.macrovision.com.SimpleQueryType getFulfillmentId() {
        return fulfillmentId;
    }


    /**
     * Sets the fulfillmentId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param fulfillmentId
     */
    public void setFulfillmentId(flexnet.macrovision.com.SimpleQueryType fulfillmentId) {
        this.fulfillmentId = fulfillmentId;
    }


    /**
     * Gets the activationId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return activationId
     */
    public flexnet.macrovision.com.SimpleQueryType getActivationId() {
        return activationId;
    }


    /**
     * Sets the activationId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param activationId
     */
    public void setActivationId(flexnet.macrovision.com.SimpleQueryType activationId) {
        this.activationId = activationId;
    }


    /**
     * Gets the entitlementId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return entitlementId
     */
    public flexnet.macrovision.com.SimpleQueryType getEntitlementId() {
        return entitlementId;
    }


    /**
     * Sets the entitlementId value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param entitlementId
     */
    public void setEntitlementId(flexnet.macrovision.com.SimpleQueryType entitlementId) {
        this.entitlementId = entitlementId;
    }


    /**
     * Gets the soldTo value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return soldTo
     */
    public flexnet.macrovision.com.SimpleQueryType getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param soldTo
     */
    public void setSoldTo(flexnet.macrovision.com.SimpleQueryType soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the criteria value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return criteria
     */
    public flexnet.macrovision.com.SimpleQueryType getCriteria() {
        return criteria;
    }


    /**
     * Sets the criteria value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param criteria
     */
    public void setCriteria(flexnet.macrovision.com.SimpleQueryType criteria) {
        this.criteria = criteria;
    }


    /**
     * Gets the state value for this ConsolidatedFulfillmentsQPType.
     * 
     * @return state
     */
    public flexnet.macrovision.com.StateQueryType getState() {
        return state;
    }


    /**
     * Sets the state value for this ConsolidatedFulfillmentsQPType.
     * 
     * @param state
     */
    public void setState(flexnet.macrovision.com.StateQueryType state) {
        this.state = state;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsolidatedFulfillmentsQPType)) return false;
        ConsolidatedFulfillmentsQPType other = (ConsolidatedFulfillmentsQPType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consolidatedLicenseId==null && other.getConsolidatedLicenseId()==null) || 
             (this.consolidatedLicenseId!=null &&
              this.consolidatedLicenseId.equals(other.getConsolidatedLicenseId()))) &&
            ((this.fulfillmentId==null && other.getFulfillmentId()==null) || 
             (this.fulfillmentId!=null &&
              this.fulfillmentId.equals(other.getFulfillmentId()))) &&
            ((this.activationId==null && other.getActivationId()==null) || 
             (this.activationId!=null &&
              this.activationId.equals(other.getActivationId()))) &&
            ((this.entitlementId==null && other.getEntitlementId()==null) || 
             (this.entitlementId!=null &&
              this.entitlementId.equals(other.getEntitlementId()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.criteria==null && other.getCriteria()==null) || 
             (this.criteria!=null &&
              this.criteria.equals(other.getCriteria()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsolidatedLicenseId() != null) {
            _hashCode += getConsolidatedLicenseId().hashCode();
        }
        if (getFulfillmentId() != null) {
            _hashCode += getFulfillmentId().hashCode();
        }
        if (getActivationId() != null) {
            _hashCode += getActivationId().hashCode();
        }
        if (getEntitlementId() != null) {
            _hashCode += getEntitlementId().hashCode();
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getCriteria() != null) {
            _hashCode += getCriteria().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsolidatedFulfillmentsQPType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedFulfillmentsQPType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consolidatedLicenseId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillmentId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("activationId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "activationId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("entitlementId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "entitlementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("criteria");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "criteria"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "SimpleQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "StateQueryType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
