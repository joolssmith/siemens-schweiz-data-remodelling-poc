/**
 * TransferHostRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class TransferHostRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.TransferHostIdDataType[] sourceHosts;

    private java.lang.String soldTo;

    private java.lang.Boolean poolEntitlements;

    public TransferHostRequestType() {
    }

    public TransferHostRequestType(
           flexnet.macrovision.com.TransferHostIdDataType[] sourceHosts,
           java.lang.String soldTo,
           java.lang.Boolean poolEntitlements) {
           this.sourceHosts = sourceHosts;
           this.soldTo = soldTo;
           this.poolEntitlements = poolEntitlements;
    }


    /**
     * Gets the sourceHosts value for this TransferHostRequestType.
     * 
     * @return sourceHosts
     */
    public flexnet.macrovision.com.TransferHostIdDataType[] getSourceHosts() {
        return sourceHosts;
    }


    /**
     * Sets the sourceHosts value for this TransferHostRequestType.
     * 
     * @param sourceHosts
     */
    public void setSourceHosts(flexnet.macrovision.com.TransferHostIdDataType[] sourceHosts) {
        this.sourceHosts = sourceHosts;
    }


    /**
     * Gets the soldTo value for this TransferHostRequestType.
     * 
     * @return soldTo
     */
    public java.lang.String getSoldTo() {
        return soldTo;
    }


    /**
     * Sets the soldTo value for this TransferHostRequestType.
     * 
     * @param soldTo
     */
    public void setSoldTo(java.lang.String soldTo) {
        this.soldTo = soldTo;
    }


    /**
     * Gets the poolEntitlements value for this TransferHostRequestType.
     * 
     * @return poolEntitlements
     */
    public java.lang.Boolean getPoolEntitlements() {
        return poolEntitlements;
    }


    /**
     * Sets the poolEntitlements value for this TransferHostRequestType.
     * 
     * @param poolEntitlements
     */
    public void setPoolEntitlements(java.lang.Boolean poolEntitlements) {
        this.poolEntitlements = poolEntitlements;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TransferHostRequestType)) return false;
        TransferHostRequestType other = (TransferHostRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sourceHosts==null && other.getSourceHosts()==null) || 
             (this.sourceHosts!=null &&
              java.util.Arrays.equals(this.sourceHosts, other.getSourceHosts()))) &&
            ((this.soldTo==null && other.getSoldTo()==null) || 
             (this.soldTo!=null &&
              this.soldTo.equals(other.getSoldTo()))) &&
            ((this.poolEntitlements==null && other.getPoolEntitlements()==null) || 
             (this.poolEntitlements!=null &&
              this.poolEntitlements.equals(other.getPoolEntitlements())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSourceHosts() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSourceHosts());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSourceHosts(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSoldTo() != null) {
            _hashCode += getSoldTo().hashCode();
        }
        if (getPoolEntitlements() != null) {
            _hashCode += getPoolEntitlements().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TransferHostRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sourceHosts");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "sourceHosts"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "transferHostIdDataType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "hostIdentifier"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("soldTo");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "soldTo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("poolEntitlements");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "poolEntitlements"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
