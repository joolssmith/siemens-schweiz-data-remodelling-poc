/**
 * EmailConsolidatedLicensesRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class EmailConsolidatedLicensesRequestType  implements java.io.Serializable {
    private java.lang.String[] consolidatedLicenseIdList;

    private java.lang.Boolean validateEmailAddresses;

    private java.lang.String[] emailIdList;

    private java.lang.String locale;

    public EmailConsolidatedLicensesRequestType() {
    }

    public EmailConsolidatedLicensesRequestType(
           java.lang.String[] consolidatedLicenseIdList,
           java.lang.Boolean validateEmailAddresses,
           java.lang.String[] emailIdList,
           java.lang.String locale) {
           this.consolidatedLicenseIdList = consolidatedLicenseIdList;
           this.validateEmailAddresses = validateEmailAddresses;
           this.emailIdList = emailIdList;
           this.locale = locale;
    }


    /**
     * Gets the consolidatedLicenseIdList value for this EmailConsolidatedLicensesRequestType.
     * 
     * @return consolidatedLicenseIdList
     */
    public java.lang.String[] getConsolidatedLicenseIdList() {
        return consolidatedLicenseIdList;
    }


    /**
     * Sets the consolidatedLicenseIdList value for this EmailConsolidatedLicensesRequestType.
     * 
     * @param consolidatedLicenseIdList
     */
    public void setConsolidatedLicenseIdList(java.lang.String[] consolidatedLicenseIdList) {
        this.consolidatedLicenseIdList = consolidatedLicenseIdList;
    }


    /**
     * Gets the validateEmailAddresses value for this EmailConsolidatedLicensesRequestType.
     * 
     * @return validateEmailAddresses
     */
    public java.lang.Boolean getValidateEmailAddresses() {
        return validateEmailAddresses;
    }


    /**
     * Sets the validateEmailAddresses value for this EmailConsolidatedLicensesRequestType.
     * 
     * @param validateEmailAddresses
     */
    public void setValidateEmailAddresses(java.lang.Boolean validateEmailAddresses) {
        this.validateEmailAddresses = validateEmailAddresses;
    }


    /**
     * Gets the emailIdList value for this EmailConsolidatedLicensesRequestType.
     * 
     * @return emailIdList
     */
    public java.lang.String[] getEmailIdList() {
        return emailIdList;
    }


    /**
     * Sets the emailIdList value for this EmailConsolidatedLicensesRequestType.
     * 
     * @param emailIdList
     */
    public void setEmailIdList(java.lang.String[] emailIdList) {
        this.emailIdList = emailIdList;
    }


    /**
     * Gets the locale value for this EmailConsolidatedLicensesRequestType.
     * 
     * @return locale
     */
    public java.lang.String getLocale() {
        return locale;
    }


    /**
     * Sets the locale value for this EmailConsolidatedLicensesRequestType.
     * 
     * @param locale
     */
    public void setLocale(java.lang.String locale) {
        this.locale = locale;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EmailConsolidatedLicensesRequestType)) return false;
        EmailConsolidatedLicensesRequestType other = (EmailConsolidatedLicensesRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.consolidatedLicenseIdList==null && other.getConsolidatedLicenseIdList()==null) || 
             (this.consolidatedLicenseIdList!=null &&
              java.util.Arrays.equals(this.consolidatedLicenseIdList, other.getConsolidatedLicenseIdList()))) &&
            ((this.validateEmailAddresses==null && other.getValidateEmailAddresses()==null) || 
             (this.validateEmailAddresses!=null &&
              this.validateEmailAddresses.equals(other.getValidateEmailAddresses()))) &&
            ((this.emailIdList==null && other.getEmailIdList()==null) || 
             (this.emailIdList!=null &&
              java.util.Arrays.equals(this.emailIdList, other.getEmailIdList()))) &&
            ((this.locale==null && other.getLocale()==null) || 
             (this.locale!=null &&
              this.locale.equals(other.getLocale())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getConsolidatedLicenseIdList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getConsolidatedLicenseIdList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getConsolidatedLicenseIdList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getValidateEmailAddresses() != null) {
            _hashCode += getValidateEmailAddresses().hashCode();
        }
        if (getEmailIdList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEmailIdList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEmailIdList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getLocale() != null) {
            _hashCode += getLocale().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EmailConsolidatedLicensesRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailConsolidatedLicensesRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("consolidatedLicenseIdList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseIdList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidatedLicenseId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validateEmailAddresses");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "validateEmailAddresses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("emailIdList");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailIdList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "emailId"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("locale");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "locale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
