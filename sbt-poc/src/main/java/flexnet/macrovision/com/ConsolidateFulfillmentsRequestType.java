/**
 * ConsolidateFulfillmentsRequestType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class ConsolidateFulfillmentsRequestType  implements java.io.Serializable {
    private flexnet.macrovision.com.FulfillmentIdentifierType[] fulfillments;

    public ConsolidateFulfillmentsRequestType() {
    }

    public ConsolidateFulfillmentsRequestType(
           flexnet.macrovision.com.FulfillmentIdentifierType[] fulfillments) {
           this.fulfillments = fulfillments;
    }


    /**
     * Gets the fulfillments value for this ConsolidateFulfillmentsRequestType.
     * 
     * @return fulfillments
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType[] getFulfillments() {
        return fulfillments;
    }


    /**
     * Sets the fulfillments value for this ConsolidateFulfillmentsRequestType.
     * 
     * @param fulfillments
     */
    public void setFulfillments(flexnet.macrovision.com.FulfillmentIdentifierType[] fulfillments) {
        this.fulfillments = fulfillments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ConsolidateFulfillmentsRequestType)) return false;
        ConsolidateFulfillmentsRequestType other = (ConsolidateFulfillmentsRequestType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.fulfillments==null && other.getFulfillments()==null) || 
             (this.fulfillments!=null &&
              java.util.Arrays.equals(this.fulfillments, other.getFulfillments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFulfillments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFulfillments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFulfillments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ConsolidateFulfillmentsRequestType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "consolidateFulfillmentsRequestType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fulfillments");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillments"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifier"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
