/**
 * CreateShortCodeDataType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package flexnet.macrovision.com;

public class CreateShortCodeDataType  implements java.io.Serializable {
    private java.lang.String bulkEntitlementId;

    private java.lang.String webRegkey;

    private java.lang.String shortCode;

    private flexnet.macrovision.com.SimpleAttributeDataType[] publisherAttributes;

    private java.lang.Boolean overridePolicy;

    private flexnet.macrovision.com.ShortCodeActivationType shortCodeActivationType;

    private flexnet.macrovision.com.FulfillmentIdentifierType reinstallFulfillment;

    public CreateShortCodeDataType() {
    }

    public CreateShortCodeDataType(
           java.lang.String bulkEntitlementId,
           java.lang.String webRegkey,
           java.lang.String shortCode,
           flexnet.macrovision.com.SimpleAttributeDataType[] publisherAttributes,
           java.lang.Boolean overridePolicy,
           flexnet.macrovision.com.ShortCodeActivationType shortCodeActivationType,
           flexnet.macrovision.com.FulfillmentIdentifierType reinstallFulfillment) {
           this.bulkEntitlementId = bulkEntitlementId;
           this.webRegkey = webRegkey;
           this.shortCode = shortCode;
           this.publisherAttributes = publisherAttributes;
           this.overridePolicy = overridePolicy;
           this.shortCodeActivationType = shortCodeActivationType;
           this.reinstallFulfillment = reinstallFulfillment;
    }


    /**
     * Gets the bulkEntitlementId value for this CreateShortCodeDataType.
     * 
     * @return bulkEntitlementId
     */
    public java.lang.String getBulkEntitlementId() {
        return bulkEntitlementId;
    }


    /**
     * Sets the bulkEntitlementId value for this CreateShortCodeDataType.
     * 
     * @param bulkEntitlementId
     */
    public void setBulkEntitlementId(java.lang.String bulkEntitlementId) {
        this.bulkEntitlementId = bulkEntitlementId;
    }


    /**
     * Gets the webRegkey value for this CreateShortCodeDataType.
     * 
     * @return webRegkey
     */
    public java.lang.String getWebRegkey() {
        return webRegkey;
    }


    /**
     * Sets the webRegkey value for this CreateShortCodeDataType.
     * 
     * @param webRegkey
     */
    public void setWebRegkey(java.lang.String webRegkey) {
        this.webRegkey = webRegkey;
    }


    /**
     * Gets the shortCode value for this CreateShortCodeDataType.
     * 
     * @return shortCode
     */
    public java.lang.String getShortCode() {
        return shortCode;
    }


    /**
     * Sets the shortCode value for this CreateShortCodeDataType.
     * 
     * @param shortCode
     */
    public void setShortCode(java.lang.String shortCode) {
        this.shortCode = shortCode;
    }


    /**
     * Gets the publisherAttributes value for this CreateShortCodeDataType.
     * 
     * @return publisherAttributes
     */
    public flexnet.macrovision.com.SimpleAttributeDataType[] getPublisherAttributes() {
        return publisherAttributes;
    }


    /**
     * Sets the publisherAttributes value for this CreateShortCodeDataType.
     * 
     * @param publisherAttributes
     */
    public void setPublisherAttributes(flexnet.macrovision.com.SimpleAttributeDataType[] publisherAttributes) {
        this.publisherAttributes = publisherAttributes;
    }


    /**
     * Gets the overridePolicy value for this CreateShortCodeDataType.
     * 
     * @return overridePolicy
     */
    public java.lang.Boolean getOverridePolicy() {
        return overridePolicy;
    }


    /**
     * Sets the overridePolicy value for this CreateShortCodeDataType.
     * 
     * @param overridePolicy
     */
    public void setOverridePolicy(java.lang.Boolean overridePolicy) {
        this.overridePolicy = overridePolicy;
    }


    /**
     * Gets the shortCodeActivationType value for this CreateShortCodeDataType.
     * 
     * @return shortCodeActivationType
     */
    public flexnet.macrovision.com.ShortCodeActivationType getShortCodeActivationType() {
        return shortCodeActivationType;
    }


    /**
     * Sets the shortCodeActivationType value for this CreateShortCodeDataType.
     * 
     * @param shortCodeActivationType
     */
    public void setShortCodeActivationType(flexnet.macrovision.com.ShortCodeActivationType shortCodeActivationType) {
        this.shortCodeActivationType = shortCodeActivationType;
    }


    /**
     * Gets the reinstallFulfillment value for this CreateShortCodeDataType.
     * 
     * @return reinstallFulfillment
     */
    public flexnet.macrovision.com.FulfillmentIdentifierType getReinstallFulfillment() {
        return reinstallFulfillment;
    }


    /**
     * Sets the reinstallFulfillment value for this CreateShortCodeDataType.
     * 
     * @param reinstallFulfillment
     */
    public void setReinstallFulfillment(flexnet.macrovision.com.FulfillmentIdentifierType reinstallFulfillment) {
        this.reinstallFulfillment = reinstallFulfillment;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CreateShortCodeDataType)) return false;
        CreateShortCodeDataType other = (CreateShortCodeDataType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.bulkEntitlementId==null && other.getBulkEntitlementId()==null) || 
             (this.bulkEntitlementId!=null &&
              this.bulkEntitlementId.equals(other.getBulkEntitlementId()))) &&
            ((this.webRegkey==null && other.getWebRegkey()==null) || 
             (this.webRegkey!=null &&
              this.webRegkey.equals(other.getWebRegkey()))) &&
            ((this.shortCode==null && other.getShortCode()==null) || 
             (this.shortCode!=null &&
              this.shortCode.equals(other.getShortCode()))) &&
            ((this.publisherAttributes==null && other.getPublisherAttributes()==null) || 
             (this.publisherAttributes!=null &&
              java.util.Arrays.equals(this.publisherAttributes, other.getPublisherAttributes()))) &&
            ((this.overridePolicy==null && other.getOverridePolicy()==null) || 
             (this.overridePolicy!=null &&
              this.overridePolicy.equals(other.getOverridePolicy()))) &&
            ((this.shortCodeActivationType==null && other.getShortCodeActivationType()==null) || 
             (this.shortCodeActivationType!=null &&
              this.shortCodeActivationType.equals(other.getShortCodeActivationType()))) &&
            ((this.reinstallFulfillment==null && other.getReinstallFulfillment()==null) || 
             (this.reinstallFulfillment!=null &&
              this.reinstallFulfillment.equals(other.getReinstallFulfillment())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBulkEntitlementId() != null) {
            _hashCode += getBulkEntitlementId().hashCode();
        }
        if (getWebRegkey() != null) {
            _hashCode += getWebRegkey().hashCode();
        }
        if (getShortCode() != null) {
            _hashCode += getShortCode().hashCode();
        }
        if (getPublisherAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPublisherAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPublisherAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOverridePolicy() != null) {
            _hashCode += getOverridePolicy().hashCode();
        }
        if (getShortCodeActivationType() != null) {
            _hashCode += getShortCodeActivationType().hashCode();
        }
        if (getReinstallFulfillment() != null) {
            _hashCode += getReinstallFulfillment().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CreateShortCodeDataType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "createShortCodeDataType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bulkEntitlementId");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "bulkEntitlementId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webRegkey");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "webRegkey"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortCode");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shortCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("publisherAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "publisherAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "simpleAttributeDataType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "attribute"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("overridePolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "overridePolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shortCodeActivationType");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "shortCodeActivationType"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "ShortCodeActivationType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reinstallFulfillment");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "reinstallFulfillment"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:com.macrovision:flexnet/operations", "fulfillmentIdentifierType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
